import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';

import GraphHTTP from 'express-graphql';
import schema from './server/graphql/schema';


import { graphqlSubscribe } from 'graphql-relay-subscription';

import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackConfig from './webpack.config.dev';
import webpackHotMiddleware from 'webpack-hot-middleware';


import models from './server/models';

import isUser from './server/utils/isUser';

import addNotifier from './server/utils/subscription/addNotifier';

import { graphql } from 'graphql';

import mockData from './server/utils/mockData';


let APP_PORT = process.env.PORT || 3000;
let GRAPHQL_PORT = 8080;

let graphQLApp = express();
graphQLApp.use('/graphql', GraphHTTP({
  schema: schema,
  pretty: true,
  graphiql: true,
  context: {db : models , ...isUser('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTAwMSwidXNlcm5hbWUiOiJ2aXZuYXUiLCJyb2xlIjoiVXNlciIsImZpcnN0TmFtZSI6IlZpdmVrIiwibGFzdE5hbWUiOiJOYXV0aXlhbCIsImVtYWlsIjoidml2bmF1QGdtYWlsLmNvbSIsInByb2ZpbGVQaWMiOiJodHRwczovL2dyYXBoLmZhY2Vib29rLmNvbS8xMDIwODQ3NDY4NzYxODgzOS9waWN0dXJlP3R5cGU9bGFyZ2UiLCJpYXQiOjE0ODY3MjQ0Njh9.54CNYvLQ_4HsDzAPeZG91OHel50nUVKgqxvrQmLJXNs')}
}));
graphQLApp.listen(GRAPHQL_PORT, () => console.log(
  `GraphQL Server is now running on http://localhost:${GRAPHQL_PORT}`
));


let app = express();
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
app.use(express.static('public'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb'}));
const compiler = webpack(webpackConfig);
app.use(webpackMiddleware(compiler,{
  hot : true,
  publicPath : webpackConfig.output.publicPath,
  noInfo : true
}));
app.use(webpackHotMiddleware(compiler));
// app.get('*/', (req,res) =>{
//  res.sendFile(path.join(__dirname, '/index.html'));
// });
GLOBAL.notifiers = [];

let graphQLHTTP = GraphHTTP(req =>({
  schema: schema,
  pretty: true,
  context: {db : models,  ...isUser(req.headers['authorization'].split(' ')[1])}
}));


app.use('/graphql', graphQLHTTP);

  /**
   * Listen on provided port, on all network interfaces.
  //  */

  // models.User.bulkCreate(mockData);



models.sequelize.sync().then( () => {
  const graphQLServer = app.listen(APP_PORT, () => console.log('Running on localhost:3000'));
  const io = require('socket.io')(graphQLServer, {
    serveClient: false,
  });
  models.User.findOrCreate({
    where: {id: 0},
    defaults: {
      id: 0,
      firstName: 'Anon',
      lastName: 'User',
      score: 0,
      email: 'anon@anon.com',
      password: 'fcwd&*213OEv13$%nJ',
      username: 'anon',
      role: 'User',
      profilePic: 'https://storage.googleapis.com/humbee_images/cartoon-bee-1.jpg'
    }
  });

  io.on('connection', socket => {
    console.log('Connection made');
    const topics = Object.create(null);
    const unsubscribeMap = Object.create(null);

    const removeNotifier = addNotifier(({ topic, data }) => {
      const topicListeners = topics[topic];
      console.log(topicListeners);
      if (!topicListeners) return;

      topicListeners.forEach(({ id, query, variables }) => {
        graphql(
          schema,
          query,
          data,
          null,
          variables
        ).then((result) => {
          console.log(id);
          socket.emit('subscription update', { id, ...result });
        });
      });
    });

    socket.on('subscribe', ({ id, query, variables }) => {
      function unsubscribe(topic, subscription) {
        const index = topics[topic].indexOf(subscription);
        if (index === -1) return;

        topics[topic].splice(index);

        console.log(
          'Removed subscription for topic %s. Total subscriptions for topic: %d',
          topic,
          topics[topic].length
        );
      }

      function subscribe(topic) {
        topics[topic] = topics[topic] || [];
        const subscription = { id, query, variables };

        topics[topic].push(subscription);

        unsubscribeMap[id] = () => {
          unsubscribe(topic, subscription);
        };

        console.log(
          'New subscription for topic %s. Total subscriptions for topic: %d',
          topic,
          topics[topic].length
        );
      }

      console.log('schema');

      graphqlSubscribe({
        schema,
        query,
        variables,
        context: { subscribe },
      }).then((result) => {
        if (result.errors) {
          console.error('Subscribe failed', result.errors);
        }
      });
    });

    socket.on('unsubscribe', (id) => {
      const unsubscribe = unsubscribeMap[id];
      if (!unsubscribe) return;

      unsubscribe();
      delete unsubscribeMap[id];
      socket.emit('subscription closed', id);
    });

    socket.on('disconnect', () => {
      console.log('Socket disconnect');
      removeNotifier();
    });
  });
});
