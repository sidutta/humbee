
import { AppRegistry } from 'react-native';

import app from './app';

import crashlytics from 'react-native-fabric-crashlytics';
crashlytics.init();


AppRegistry.registerComponent('humbee', () => app);
// class ViewerRoute extends Route {
//
//   static routeParams = {
//     porId: ''
//   };
//
//   static queries = {
//     viewer: (Component, {porId}) => Relay.QL`
//   		query{
//   			Viewer{
//           ${Component.getFragment('viewer', {porId})}
//         }
//   		}
//     `
//   };
//   static routeName = 'ViewerRoute';
//
// }
//
//
// class ViewerInfo extends Component {
//   render () {
//     const viewer = this.props.viewer;
//     return (
//       <View>
//         <Text>{viewer.POR.name}</Text>
//       </View>
//     )
//   }
// }
//
// ViewerInfo = Relay.createContainer(ViewerInfo, {
//   initialVariables : {
//     porId : null
//   },
//   fragments: {
//     viewer: () => Relay.QL`
//       fragment on Viewer {
// 				POR(porId : $porId) {
//           id,
//           name,
//           jurisdiction
// 				}
//       }
//     `
//   }
// });
//
// class test extends Component {
//   render () {
//     return (
//       <RootContainer
//         Component={ViewerInfo}
//         route={new ViewerRoute({porId : 'UE9SOjY='})}
//         renderFetched={(data) => <ViewerInfo {...this.props} {...data} />}
//       />
//     );
//   }
// }
//
//
// class ViewerRoute extends Route {
//
//   static routeParams = {
//     id: ''
//   };
//
//   static queries = {
//     viewer: (Component) => Relay.QL`
//   		query{
//   			node(id : $id){
//           ${Component.getFragment('viewer')}
//         }
//   		}
//     `
//   };
//   static routeName = 'ViewerRoute';
//
// }
//
//
// class ViewerInfo extends Component {
//   render () {
//     const {name} = this.props.viewer;
//     return (
//       <View>
//         <Text>{name}</Text>
//       </View>
//     )
//   }
// }
//
// ViewerInfo = Relay.createContainer(ViewerInfo, {
//   fragments: {
//     viewer: () => Relay.QL`
//       fragment on POR {
//         id,
//         name,
//         jurisdiction
//       }
//     `
//   }
// });

// class test extends Component {
//   render () {
//     return (
//       <RootContainer
//         Component={ViewerInfo}
//         route={new ViewerRoute({id : 'UE9SOjY='})}
//         renderFetched={(data) => <ViewerInfo {...this.props} {...data} />}
//       />
//     );
//   }
// }

//
//
// class ViewerRoute extends Route {
//
//
//   static queries = {
//     viewer: (Component) => Relay.QL`
//   		query{
//         Viewer{
//           ${Component.getFragment('viewer')}
//         }
//
//   		}
//     `
//   };
//   static routeName = 'ViewerRoute';
//
// }
//
//
// class ViewerInfo extends Component {
//   render () {
//     const {imageUrl} = this.props.viewer.debateList[0];
//     return (
//       <View>
//         <Text>{imageUrl}</Text>
//       </View>
//     )
//   }
// }
//
// ViewerInfo = Relay.createContainer(ViewerInfo, {
//   fragments: {
//     viewer: () => Relay.QL`
//       fragment on Viewer {
//         debateList{
//           imageUrl,
//           title
//         }
//       }
//     `
//   }
// });
//
// class test extends Component {
//   render () {
//     return (
//       <RootContainer
//         Component={ViewerInfo}
//         route={new ViewerRoute()}
//         renderFetched={(data) => <ViewerInfo {...this.props} {...data} />}
//       />
//     );
//   }
// }
//
