import Relay from 'react-relay';
import React, { Component } from 'react';
import { View } from 'react-native';
import NavBar from '../NavBar';
import Opinions from '../DebateScreen/Opinions';

import NodeQuery from '../../queries/NodeQuery';
import { createRenderer } from '../../utils/RelayUtils';
import Router from '../../routes/Router';

import DebateScreenContainer from '../Containers/DebateScreenContainer';

class QuizScreen extends Component {

	goToDebate() {
		this.navigator.push(
			Router.getRoute('debateScreen',{
				id : this.lastDebateGraphQLId,
				fromCard : false
			})
		);
	}

	render() {
		const { navigator, viewer, isIntroQuiz } = this.props;

		let enabled = false;
		let goToDebateArg = null;

		if(this.props.viewer.lastDebateId!=-1) {
			enabled = true;
			goToDebateArg = this.goToDebate.bind({lastDebateGraphQLId:this.props.viewer.lastDebateGraphQLId,
																						quizId:this.props.viewer.id,
																						navigator:navigator});
		}

		let title = 'Quiz';

		if(isIntroQuiz) {
			title = 'Intro Quiz';
		}

		return (
			<View style={{flex : 1}}>
				<NavBar navigator={navigator} title={title} opacity={1} isIntroQuiz={isIntroQuiz} />
				<Opinions fromQuizCard={true} goToDebateArg={goToDebateArg} isIntroQuiz={isIntroQuiz} enabled={enabled} id={this.props.viewer.id} navigator={navigator}/>
			</View>
		);
	}
}

export default createRenderer(QuizScreen, {
	queries : NodeQuery,
	queriesParams: ({ id }) => ({
		id: id ? id : null,
	}),
	fragments: {
		viewer: () => Relay.QL`
			fragment on Quiz {
				id,
				lastDebateId,
				lastDebateGraphQLId,
			}
		`
	}
});
// ${Opinions.getFragment('viewer')}
