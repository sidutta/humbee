import React,{ Component } from 'react';
import Relay from 'react-relay';
import {View, Text, ListView, RefreshControl } from 'react-native';

import ViewerQuery from '../../queries/ViewerQuery';
import { createRenderer } from '../../utils/RelayUtils';

import ExtraDimensions from 'react-native-extra-dimensions-android';
import Icon from 'react-native-vector-icons/Ionicons';
import TouchableItem from '../Common/TouchableItem';

import colors from '../../config/colors';

import LeaderBoardUserCard from './LeaderBoardUserCard';

import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

const ds = new ListView.DataSource({
	rowHasChanged: (row1, row2) => row1.id !== row2.id
});

class LeaderBoard extends Component{
	constructor(props){
		super(props);
		this.state = {
			scrollViewHeight : 0,
			pageSize : 10,
			refreshing : false,
			dataSource : ds
		};
		this.onEndReached = this.onEndReached.bind(this);
		this._renderRow = this._renderRow.bind(this);
		this.onRefresh = this.onRefresh.bind(this);
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('Leaderboard');
    }
	}

	componentWillReceiveProps(next) {
		if(next && next.viewer.user.leaderBoard) {
			this.setState({
				dataSource: ds.cloneWithRows([ ...next.viewer.user.leaderBoard.edges ])
			});
		}
	}

	onEndReached(){
		let {pageSize}	= this.state;
		pageSize = pageSize + 10;
		this.props.relay.setVariables({pageSize});
		this.setState({pageSize});
	}

	componentWillMount(){
		let scrollViewHeight = ExtraDimensions.get('REAL_WINDOW_HEIGHT') - ExtraDimensions.get('STATUS_BAR_HEIGHT') - ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT') - 120;
		this.setState({scrollViewHeight});
		this.props.relay.forceFetch();
	}

	onRefresh(){
		const pageSize = 10;
		this.setState({refreshing : true});
		this.props.relay.forceFetch();
		this.props.relay.setVariables({pageSize});
		this.setState({pageSize, refreshing : false});
	}

	_renderRow({node}){
		const { navigator } = this.props;

		return <LeaderBoardUserCard {...node} navigator={navigator}/>;
	}

	render(){
		const { scrollViewHeight, refreshing, dataSource } = this.state;
		const { navigator, viewer } = this.props;
		const { user } = viewer;
		const { firstName, lastName, username, id, index, rankInLeaderboard, score , profilePic, isViewer } = user;
		return(
			<View>
				<View style={{ height : 60, backgroundColor : colors.humbeeGreen, flexDirection : 'row'}} elevation={5}>
					<TouchableItem borderLess={true} onPress={() => navigator.pop()} style={{ flex : 1, alignItems : 'center', justifyContent : 'center'}}>
						<Icon name='md-arrow-back' size={25} style={{ color : 'white'}}/>
					</TouchableItem>
					<View style={{ flex : 8, alignItems : 'center', justifyContent : 'center'}}>
						<Text style={{ textAlign : 'center', color : 'white', fontSize : 16}}>Leader Board</Text>
					</View>
					<View style={{flex : 1}}/>
				</View>
				<ListView
					style={{height : scrollViewHeight}}
					dataSource={dataSource}
					renderRow={row => this._renderRow(row)}
					onEndReached={this.onEndReached}
					refreshControl={
						<RefreshControl
							refreshing={refreshing}
							onRefresh={this.onRefresh}
							colors={[colors.humbeeGreen,'#ff0000', '#00ff00', '#0000ff']}
						/>
					}
				/>
				<LeaderBoardUserCard
					id={id}
					navigator={navigator}
					elevation={5}
					firstName={firstName}
					lastName={lastName}
					profilePic={profilePic}
					username={username}
					rankInLeaderboard={rankInLeaderboard}
					isViewer={isViewer}
					backgroundColor={colors.humbeeGreen}
					score={score}
        />
			</View>
		);
	}
}

LeaderBoard.propTypes= {
	viewer : React.PropTypes.object.isRequired,
	navigator : React.PropTypes.object.isRequired,
	relay : React.PropTypes.object.isRequired
};

export default createRenderer(LeaderBoard,{
	queries : ViewerQuery,
	initialVariables : {
		pageSize : 10
	},
	fragments : {
		viewer : () => Relay.QL`
			fragment on Viewer{
				user{
					firstName,
					lastName,
					profilePic,
					username,
					rankInLeaderboard,
					isViewer,
					score,
					id,
          index,
					leaderBoard(first : $pageSize){
						pageInfo{
							hasNextPage
						},
						edges{
							node{
								firstName,
								lastName,
								username,
								profilePic,
								id,
                index,
								score,
								isViewer,
								rankInLeaderboard
							}
						}
					}
				}
			}
		`
	}
});
