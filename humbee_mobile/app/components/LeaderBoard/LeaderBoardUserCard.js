import React, { Component } from 'react';
import { Text, Image, View } from 'react-native';
import TouchableItem from '../Common/TouchableItem';

import colors from '../../config/colors';
import Router from '../../routes/Router';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';



export default class LeaderBoardUserCard extends Component{
	constructor(props){
		super(props);
		this.openUserProfile = this.openUserProfile.bind(this);
	}

	openUserProfile(){
		const { navigator, id, isViewer, index } = this.props;
    let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
		// if(isViewer){
		// 	navigator.push(
		// 		Router.getRoute('viewerProfile')
		// 	);
		// } else {
      if (__DEV__ == false) {
        tracker.trackScreenView('UserProfile-' + index);
      }
			navigator.push(
				Router.getRoute('userProfile',{id})
			);
		// }
	}

	render(){
		const { firstName, lastName, profilePic, username, score, rankInLeaderboard, elevation, backgroundColor } = this.props;
		return(
			<View style={{height : 61, backgroundColor : backgroundColor ? backgroundColor : null}} elevation={elevation ? elevation : 0}>
				<TouchableItem onPress={this.openUserProfile} style={{height : 60, flex : 1, flexDirection : 'row', padding : 5}}>
					<View style= {{ flex : 2, padding : 10}}>
						<Text style={{color : backgroundColor ? 'white' : 'black',fontFamily : 'helvetica', fontSize : (rankInLeaderboard != -1)? 14 : 12}}>{(rankInLeaderboard != -1)? (rankInLeaderboard + '.') : 'No Rank'}</Text>
					</View>
					<Image style={{ flex : 2, borderRadius : 50}} source={{uri : profilePic}}/>
					<View style={{flex : 6, padding : 5}}>
						<Text style={{flex :1, color : backgroundColor ? 'white' : 'black',fontFamily : 'helvetica', fontSize : 14, fontWeight : 'bold'}}>{firstName + ' '+ lastName}</Text>
						<Text style={{flex : 1,color : backgroundColor ? 'white' : 'black',fontFamily : 'helvetica', fontSize : 14}}>{'@'+ username}</Text>
					</View>
					<Text style={{
						flex : 2,
						alignItems : 'center',
						justifyContent : 'center',
						fontFamily : 'helvetica',
						color : backgroundColor ? 'white' : 'black',
						padding : 10,
						fontSize : 16,
						fontWeight : 'bold'}}>{score}</Text>
				</TouchableItem>
				<View style={{ height : 1, marginRight : 10, marginLeft : 10, backgroundColor : colors.humbeeDarkGrey}}/>
			</View>
		);
	}
}

LeaderBoardUserCard.propTypes={
	firstName : React.PropTypes.string.isRequired,
	lastName : React.PropTypes.string.isRequired,
	profilePic : React.PropTypes.string.isRequired,
	id : React.PropTypes.string.isRequired,
	username : React.PropTypes.string.isRequired,
	navigator : React.PropTypes.object.isRequired,
	score : React.PropTypes.number.isRequired,
	isViewer : React.PropTypes.bool.isRequired,
	rankInLeaderboard : React.PropTypes.number.isRequired,
	elevation : React.PropTypes.number,
	backgroundColor : React.PropTypes.string
};
