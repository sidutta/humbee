import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, Text, Image, Picker } from 'react-native';
import colors from '../../config/colors';

import ViewerQuery from '../../queries/ViewerQuery';
import { createRenderer } from '../../utils/RelayUtils';
import TouchableItem from '../Common/TouchableItem';

import ExtraDimensions from 'react-native-extra-dimensions-android';

import ParallaxScrollView from 'react-native-parallax-scroll-view';

import ReasonCard from '../DebateScreen/Debate/ReasonCard';
import AboutCard from '../Common/AboutCard';
import QuizResultSummaryCard from '../Home/quiz/QuizResultSummaryCard';

import NavBar from '../NavBar';
import UserCard from '../Common/UserCard';
// var Fabric = require('react-native-fabric');
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

class ViewerProfile extends Component {
	constructor(props){
		super(props);
		this.state = {
			scrollY : 0,
			option : 'about',
			screenHeight : 0,
			followers : 10,
			following : 10,
			reasons : 10,
			quizzes : 10
		};
		this.goBack = this.goBack.bind(this);
		this.handleScroll = this.handleScroll.bind(this);
		this.setOption = this.setOption.bind(this);
		this.onEndReached = this.onEndReached.bind(this);
	}

	setOption(option){
		switch(option){
		case 'logout' :
			this.props.onLogout(this);
		}
		this.props.relay.forceFetch();
		this.setState({option});
	}

	handleScroll(e){
		this.setState({scrollY : e.nativeEvent.contentOffset.y});
	}

	goBack(){
		this.props.navigator.pop();
	}

	componentWillMount(){
		let screenHeight = ExtraDimensions.get('REAL_WINDOW_HEIGHT') - ExtraDimensions.get('STATUS_BAR_HEIGHT') - ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT');
		this.setState({screenHeight});
	}

  componentDidMount() {
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('ViewerProfile-' + this.props.viewer.user.index);
    }
    // var { Answers } = Fabric;
    // Answers.logContentView('ViewerProfile', 'ViewerProfile', 'vp-' + this.props.viewer.user.index.toString(), { 'user-id': global.userId });
    // Answers.logCustom('ViewerProfileView', { 'user-id': global.userId });
  }

	onEndReached(){
		const { option, followers, following, reasons, quizzes } = this.state;
		const { relay } = this.props;
		switch(option){
		case 'followers' :
			relay.setVariables({followers : (followers + 10)});
			this.setState({followers : (followers + 10)});
			break;
		case 'following' :
			relay.setVariables({following : (following + 10)});
			this.setState({following : (following + 10)});
			break;
		case 'reasons' :
			relay.setVariables({reasons : (reasons + 10)});
			this.setState({reasons : (reasons + 10)});
			break;
		case 'quizzes' :
			relay.setVariables({quizzes : (quizzes + 10)});
			this.setState({quizzes : (quizzes + 10)});
			break;
		}
	}

	render(){
		const { viewer, navigator} = this.props;
		const NavBarHeight = 60;
		const { user } = viewer;
		const { firstName, lastName, username, profilePic, reasons, quizzes, following, followers} = user;
		const { scrollY, option, screenHeight } = this.state;
		let cards;
		let hasNextPage;
		switch (option) {
  		case  'followers' : {
  			hasNextPage = followers.pageInfo.hasNextPage;
  			cards = followers.edges.map((edge) => (
  				<View style={{ padding : 5 }}>
  					<UserCard navigator={navigator} key={edge.node.id} id={edge.node.id} shareButton={false} followButton={true}/>
  				</View>
  			));
  			break;
  		}
  		case 'following' : {
  			hasNextPage = following.pageInfo.hasNextPage;
  			cards = following.edges.map((edge) => (
  				<View style={{ padding : 5 }}>
  					<UserCard navigator={navigator} key={edge.node.id} id={edge.node.id} shareButton={false} followButton={false}/>
  				</View>
  			));
  			break;
  		}
  		case 'reasons' : {
  			hasNextPage = reasons.pageInfo.hasNextPage;
  			cards = reasons.edges.map((edge) => (
  				<View style={{ padding : 0 }}>
  					<ReasonCard {...edge.node} key={edge.node.id} navigator={navigator} showUser={false} seeDebate={true}/>
  				</View>
  			));
  			break;
  		}
  		case 'quizzes' : {
  			hasNextPage = quizzes.pageInfo.hasNextPage;
  			cards = quizzes.edges.map((edge) => (
  				<View style={{ padding : 5 }}>
  					<QuizResultSummaryCard {...edge.node} index={edge.node.Quiz.index} key={edge.node.id} navigator={navigator}/>
  				</View>
  			));
  			break;
  		}
      case 'about' : {
        cards = [
          <View style={{ padding : 5 }}>
            <AboutCard viewer={viewer.user} key={1} navigator={navigator}/>
          </View>
        ];
        break;
      }
  		default : {
  			cards=<View/>;
  			break;
  		}
		}

		return(
			<View>
				<NavBar navigator={navigator} height={NavBarHeight} title={(scrollY >= 30) ? (firstName + ' ' + lastName) : ''}/>
				<ParallaxScrollView
					style={{ height : (screenHeight-NavBarHeight), backgroundColor : colors.humbeeGrey}}
						headerBackgroundColor={colors.humbeeGreen}
						stickyHeaderHeight={ 70 }
						parallaxHeaderHeight={ 130 }
						backgroundSpeed={5}
						fadeOutForeground={false}
						renderForeground={() => (
							<View elevation={5} style={{ padding : 5, height : 130, flex: 1, flexDirection : 'row', backgroundColor : colors.humbeeGreen}}>
								<Image style={{ marginTop : scrollY, height : 120 - scrollY, borderRadius : 60, flex : (1 - scrollY/120)}} source={{ uri : profilePic }}/>
								<View style={{ flex : scrollY/120}}/>
								<View style={{ flex : 2, marginLeft : 5 }}>
									<View style={{ flex : 1}}>
										<Text style={{ fontFamily : 'helvetica', fontSize : 20, color : 'white'}}>{ firstName + ' ' + lastName }</Text>
										<Text style={{ fontFamily : 'helvetica', fontSize : 16, color : 'white'}}>{'@'+ username }</Text>
									</View>
									<View style={{flex : 1, borderColor : 'white'}}>
										<Picker
										style={{ fontFamily : 'helvetica', color : 'white'}}
										selectedValue={option}
										mode='dropdown'
										onValueChange={(option) => this.setOption(option)}>
											<Picker.Item label="About" value="about" />
											<Picker.Item label="Followers" value="followers" />
											<Picker.Item label="Following" value="following" />
											<Picker.Item label="Reasons" value="reasons" />
											<Picker.Item label="Quizzes" value="quizzes" />
											<Picker.Item label="Log Out" value="logout" />
										</Picker>
									</View>
								</View>
							</View>
						)}
						onScroll={this.handleScroll}>
						{ cards }
						{ hasNextPage && <TouchableItem style={{ height : 50, borderRadius : 2, alignItems : 'center' }} borderLess={true} onPress={this.onEndReached}>
							<Text style={{ textAlign : 'center', color : colors.humbeeGreen, fontFamily : 'helvetica', fontSize : 20, fontWeight : 'bold' }}> Load More </Text>
						</TouchableItem>}
				</ParallaxScrollView>
			</View>
		);
	}
}

export default createRenderer( ViewerProfile, {
	queries : ViewerQuery,
	initialVariables : {
		followers : 10,
		following : 10,
		reasons : 10,
		quizzes : 10
	},
	fragments : {
		viewer : () => Relay.QL`
			fragment on Viewer {
				user {
          index,
					firstName,
					lastName,
					username,
					profilePic,
          topTags {
            text,
            count
          },
          rankInLeaderboard,
					followers(first : $followers) {
            totalCount,
						edges{
							node{
								id
							}
						}
						pageInfo{
							hasNextPage
						}
					},
					following(first : $following) {
            totalCount,
						edges{
							node{
								id
							}
						}
						pageInfo{
							hasNextPage
						}
					},
					reasons(first : $reasons) {
            totalCount,
						edges{
							node{
								id,
                index,
								text,
                beAnonymous,
								agree,
								userId,
								postedTime,
                numberOfComments,
								likes,
								liked,
								debateId,
                ReasonTags
							}
						}
						pageInfo{
							hasNextPage
						}
					},
					quizzes(first : $quizzes) {
            totalCount,
						edges{
							node{
								id,
								quizId
								Quiz {
                  index,
									title,
									numberOfLikes,
									liked,
								},
								MasterCharacterization {
									masterCharacterization,
									thumbnail
								}
							}
						}
						pageInfo{
							hasNextPage
						}
					}
				}
			}
		`
	}
});
