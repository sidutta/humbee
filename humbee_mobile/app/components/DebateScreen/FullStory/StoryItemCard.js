import React, { Component } from 'react';
import { View, Text, Image, PanResponder } from 'react-native';
import TouchableItem from '../../Common/TouchableItem';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../../config/colors';
import SimpleGesture from 'react-native-simple-gesture';

export default class StoryItemCard extends Component{
	constructor(props){
		super(props);
	}

	render(){
		const { height, imageUrl, fullStory, itemNumber, nextItem, title } = this.props;
		return(
			<View  style={{height : height, padding : 0, alignItems : 'center'}}>
				<View style={{ borderRadius : 0, padding : 0}}>
					<Image source={{ uri : imageUrl, height : 200}}/>
					<View style={{ paddingHorizontal : 15, paddingTop : 10}}>
						<Text style={{ textAlign : 'left',fontSize : 16, fontFamily : 'helvetica', marginBottom : 5, color : colors.humbeeGreen}}>{title}</Text>
						<Text style={{ fontSize : 15, fontFamily : 'helvetica', color : 'black' }}>{fullStory}</Text>
					</View>
				</View>

			</View>
		);
	}
}

StoryItemCard.propTypes={
	height : React.PropTypes.number.isRequired,
	title : React.PropTypes.string.isRequired,
	imageUrl : React.PropTypes.string.isRequired,
	fullStory : React.PropTypes.string.isRequired,
	itemNumber : React.PropTypes.number.isRequired,
	nextItem : React.PropTypes.func.isRequired
};
