import Relay from 'react-relay';
import React, {Component} from 'react';
import {ScrollView, View, PanResponder} from 'react-native';
import StoryItemCard from './StoryItemCard';

import NodeQuery from '../../../queries/NodeQuery';
import { createRenderer } from '../../../utils/RelayUtils';
import Router from '../../../routes/Router';

import colors from '../../../config/colors';
import SimpleGesture from 'react-native-simple-gesture';
// var Fabric = require('react-native-fabric');
var Analytics = require('react-native-firebase-analytics');

class FullStory extends Component {

	constructor(props){
		super(props);
		this.state = {
			height : 0
		};
		this.setHeight = this.setHeight.bind(this);
		this.nextItem = this.nextItem.bind(this);
    this.onSwipeUp = this.onSwipeUp.bind(this);
    this.onSwipeDown = this.onSwipeDown.bind(this);
    this.prevScrollPos = 0;
	}

  componentWillMount() {
    this._panResponder = PanResponder.create({
      // Only respond to movements if the gesture is a swipe up
      onMoveShouldSetPanResponder: (e, gs) => {
        let sgs = new SimpleGesture(e,gs);
        if(sgs.isSwipeUp()) {
          this.onSwipeUp();
          return true;
        }
        else if(sgs.isSwipeDown()) {
          this.onSwipeDown();
          return true;
        }
      }
    });
  }


  // componentDidMount() {
  //   var { Answers } = Fabric;
  //   Answers.logContentView('DebateScreen FullStory', 'DebateScreen', 'ds-fs-' + this.props.viewer.index.toString(), { 'user-id': global.userId });

    // Answers.logCustom('DebateScreenView', { 'user-id': global.userId, 'debateId': this.props.viewer.index.toString(), 'page': 'FullStory' });
  // }

	nextItem(itemNumber){
		const {height}= this.state;
		this.refs['SCROLL_REF'].scrollTo({y:(height*itemNumber)});
	}

	setHeight(e){
		const {height} = e.nativeEvent.layout;
		this.setState({height});
	}

  onSwipeDown() {
    const {height} = this.state;
    if(this.prevScrollPos - height < 0) {return;}

    let scrollPos = this.prevScrollPos - height;
    this.refs['SCROLL_REF'].scrollTo({y:scrollPos});
    this.prevScrollPos = scrollPos;
  }

  onSwipeUp() {
    const {height} = this.state;
    let numPages = this .props.viewer.storyItems.length;
    if(this.prevScrollPos>=(numPages-1) * height) {
      return;
    }
    let scrollPos = this.prevScrollPos + height;
    this.refs['SCROLL_REF'].scrollTo({y:scrollPos});
    this.prevScrollPos = scrollPos;
  }

	render(){
		const { storyItems } = this.props.viewer;
		const { height } = this.state;
		let storyItemCards = <View></View>;
		if(storyItems){
			storyItemCards = storyItems.map((storyItem, index) => (
				<StoryItemCard
					key={index}
					nextItem={this.nextItem}
					itemNumber={index+1}
					{...storyItem}
					height={height}
        />
			));
		}
		return(
				<ScrollView
					ref='SCROLL_REF'
					style={{flex : 1,backgroundColor : 'white', padding : 0}}
					onLayout={this.setHeight}
          scrollEnabled={false}
          showsVerticalScrollIndicator={true}
          showsHorizontalScrollIndicator={false}
          pagingEnabled={true}
          {...this._panResponder.panHandlers}
        >
					{storyItemCards}
				</ScrollView>
		);
	}
}

FullStory.propTypes={
	viewer : React.PropTypes.object.isRequired
};


export default Relay.createContainer(FullStory, {
	queries : NodeQuery,
	queriesParams: ({ id }) => ({
		id: id ? id : null,
	}),
	fragments: {
		viewer: () => Relay.QL`
			fragment on Debate {
        index,
				storyItems {
					id,
					title,
					imageUrl,
					fullStory
				}
			}
		`
	}
});
