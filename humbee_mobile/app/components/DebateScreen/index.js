import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, StyleSheet, Text, Modal} from 'react-native';
import { TabViewAnimated, TabBarTop, TabViewPagerAndroid} from 'react-native-tab-view';
import NavBar from '../NavBar';
import FullStory from './FullStory';
import Opinions from './Opinions';
import Debate from './Debate';
import colors from '../../config/colors';
import Icon from 'react-native-vector-icons/Ionicons';
import TouchableItem from '../Common/TouchableItem';

// import Container from '../Containers/Container';

import NodeQuery from '../../queries/NodeQuery';
import { createRenderer } from '../../utils/RelayUtils';
import RelayStore from '../../utils/RelayStore';

import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

// import { Pie } from 'react-native-pathjs-charts';

import CreatePositionMutation from '../../mutations/CreatePositionMutation';

class DebateScreen extends Component {
	constructor(props){
		super(props);
		this.state = {
			index: 0,
			routes: [
				{ key: '1', title: 'Full Story' },
				{ key: '2', title: 'Opinions' },
				{ key: '3', title: 'Debate'}
			],
			enabled : true,
			modalBreakupVisible : true
		};
		this.goToDebate = this.goToDebate.bind(this);
		this.handleChangeTab = this.handleChangeTab.bind(this);
		this.renderHeader = this.renderHeader.bind(this);
		this.renderPager = this.renderPager.bind(this);
		this.renderScene = this.renderScene.bind(this);
		this.createPosition = this.createPosition.bind(this);
    this.tracker = new GoogleAnalyticsTracker('UA-92835078-1');
	}

	handleChangeTab(index){
		this.setState({ index });
	}

	renderHeader(props){
		return <TabBarTop {...props} />;
	}

	renderPager(props){
		return <TabViewPagerAndroid {...props} swipeEnabled={true} animationEnabled={false} />;
	}

  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log(nextProps, nextState)
  //   if(this.state.modalBreakupVisible != nextState.modalBreakupVisible) return false;
  //   return true;
  // }

	renderScene({ route }){

		// // prevent other tabs form being renedered
		// if (Math.abs(this.state.index - this.state.routes.indexOf(route)) > 0) {
		// 	return null;
		// }
    // alert('a)')
		const { viewer, navigator, debateId } = this.props;
    const debateIndex  = viewer.index;
		const { enabled } = this.state;
		switch (route.key) {
		case '1':
      if (__DEV__ == false) {
        this.tracker.trackScreenView('FullStory-' + debateIndex);
      }
			return <FullStory viewer={viewer}/>;
		case '2':
      if (__DEV__ == false) {
        this.tracker.trackScreenView('Opinions-' + debateIndex);
      }
			return <Opinions fromQuizCard={false} goToDebateArg={this.goToDebate} enabled={enabled} id={this.props.viewer.quizId} navigator={navigator}/>;
		case '3':
      if (__DEV__ == false) {
        this.tracker.trackScreenView('Discuss-' + debateIndex);
      }
			return <Debate navigator={navigator} id={this.props.viewer.id} />; //debateId={debateId}
		default:
			return null;
		}
	}

	goToDebate() {
		const index = 2;
		let enabled = false;
		this.props.navigator.pop();
		this.setState({index, enabled});
	}

	createPosition(agree){
		RelayStore.commitUpdate(
			new CreatePositionMutation({
				debateId : this.props.viewer.id,
				agree : agree
			}),{
				onFailure : () => {
					alert('Oops something went wrong. Please try again.');
				}
			}
		);
	}

	componentDidMount() {
		const { loadIndex } = this.props;
		if(loadIndex){
			this.setState({ index : loadIndex})
		}
	}

	render() {
		const { modalBreakupVisible, index,  agree } = this.state;
		const { navigator, viewer } = this.props;
		const {  motionFull, title, userposition, motionShort } = viewer;
		const NavBarHeight = 60;
		// const agreePercent = parseInt(viewer.agree/(viewer.agree + viewer.disagree)*100) + 1;
		const disagreePercent = parseInt(viewer.disagree/(viewer.agree + viewer.disagree)*100);
    const agreePercent = 100 - disagreePercent;
		// let modalBreakupVisible = false;

		// if(this.props.fromCard == false) {
		//   this.setState({modalBreakupVisible : false})
		// }

		const opacity = ( (modalBreakupVisible) && this.props.fromCard ) ? 0.2 : 1;
		return (
			<View style={{flex : 1}}>
				<NavBar navigator={navigator} height={NavBarHeight} title={title} opacity={opacity}/>

          <TabViewAnimated
            style={styles.container}
            navigationState={this.state}
            renderScene={this.renderScene}
            renderHeader={this.renderHeader}
            renderPager={this.renderPager}
            onRequestChangeTab={this.handleChangeTab}
            opacity={opacity}
            lazy={false}
          />



				{
					(this.props.fromCard) &&
					<Modal
						animationType={'fade'}
						transparent={true}
						visible={modalBreakupVisible}
						onRequestClose={() => this.setState({modalBreakupVisible : false})}>
						{
							userposition!=null ? (
							<View elevation={5} style={{ paddingTop : 150, paddingBottom : 100, paddingRight : 20, paddingLeft : 20}}>
								<View style={{ backgroundColor : colors.humbeeGreen, padding : 5, alignItems : 'center'}}>
									<Text style={{ fontFamily:'helvetica', fontSize:16, color : 'white'}}>{motionFull}</Text>
								</View>
								<View style={{ borderWidth : 1, borderColor : colors.humbeeGreen, alignItems : 'center', justifyContent : 'center', backgroundColor : 'white'}}>
									<View style={{ height : 20,marginTop : 40, padding : 5,flexDirection : 'row'}}>
										<View style={{ backgroundColor : 'seagreen', flex : agreePercent}}/>
										<View style={{ backgroundColor : 'tomato', flex : disagreePercent}}/>
									</View>
									<View style={{ height : 40, flexDirection : 'row', justifyContent : 'center', alignItems : 'center'}}>
										<Text style={{ flex : 1, color : colors.humbeeGreen,fontSize : 14, fontFamily : 'helvetica', textAlign : 'center'}}>{agreePercent + '% Agree'}</Text>
										<Text style={{ flex : 1, color : colors.humbeeGreen,fontSize : 14, fontFamily : 'helvetica', textAlign : 'center'}}>{disagreePercent + '% Disagree'}</Text>
									</View>
									<View style={{ height : 50, padding : 10}}>
										<TouchableItem onPress={() => this.setState({modalBreakupVisible : false})} style={{height : 30,padding : 10, justifyContent : 'center', alignItems : 'center', borderRadius : 2, backgroundColor : colors.humbeeGreen}}>
											<Text style={{ color : 'white', fontSize : 16, fontFamily : 'helvetica', textAlign : 'center'}}>Go to debate</Text>
										</TouchableItem>
									</View>
								</View>
							</View>
							) : (
								<View elevation={5} style={{ paddingTop : 200, paddingBottom : 100, paddingRight : 20, paddingLeft : 20}}>
									<View style={{ marginTop : 10, borderWidth : 1, borderColor : colors.humbeeGreen, alignItems : 'center', justifyContent : 'center', backgroundColor : 'white'}}>
										<View style={{padding : 5, alignItems : 'center', backgroundColor : 'white' }}>
											<Text style={{fontFamily:'helvetica', fontSize:20, color : colors.humbeeGreen}}>{title + ':'}</Text>
											<Text style={{fontFamily:'helvetica', fontSize:16, color : colors.humbeeGreen}}>{ motionFull}</Text>
										</View>
										<View style={{flexDirection : 'row', flex : 1, height : 50, padding : 5}}>
											<TouchableItem
												onPress={() => this.createPosition(true)}
												style={{ flex  : 1, borderRadius : 2,justifyContent : 'space-around', backgroundColor : 'seagreen', marginRight : 5}}>
												<Text style={{ textAlign : 'center', fontSize : 14, fontFamily : 'helvetica', color : 'white'}}>Agree</Text>
											</TouchableItem>
											<TouchableItem
												onPress={() => this.createPosition(false)}
												style={{ flex  : 1, borderRadius : 2,justifyContent : 'space-around', backgroundColor : 'tomato', marginLeft : 5}}>
												<Text style={{ textAlign : 'center', fontSize : 14, fontFamily : 'helvetica', color : 'white'}}>Disagree</Text>
											</TouchableItem>
										</View>
									</View>
								</View>
							)
						}
					</Modal>
				}
			</View>
		);
	}
}

export default createRenderer(DebateScreen, {
	queries : NodeQuery,
	queriesParams: ({ id }) => ({
		id: id ? id : null
	}),
	fragments: {
		viewer: () => Relay.QL`
			fragment on Debate {
        id,
        index,
				title,
				motionFull,
				motionShort,
				id,
				agree,
				disagree,
				quizId,
				userposition,
				${FullStory.getFragment('viewer')}
			}
		`
	}
});

const styles = StyleSheet.create({
	container: {
		flex: 1
	}
});
