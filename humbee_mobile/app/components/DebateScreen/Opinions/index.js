import Relay from 'react-relay';
import React, {Component} from 'react';
import { ScrollView, Text, View, Animated, PanResponder } from 'react-native';

import colors from '../../../config/colors';
import QuestionScreen from './QuestionScreen';
import QuizResultScreenContainer from '../../Containers/QuizResultScreenContainer';
import RecommendationByQuizContainer from '../../Containers/RecommendationByQuizContainer';
import FollowerSuggestionByQuizContainer from '../../Containers/FollowerSuggestionByQuizContainer';
import SimilarityToFollowersContainer from '../../Containers/SimilarityToFollowersContainer';
import TransitScreen from './TransitScreen';

import NodeQuery from '../../../queries/NodeQuery';
import { createRenderer } from '../../../utils/RelayUtils';
import Router from '../../../routes/Router';
import RelayStore from '../../../utils/RelayStore';

import SimpleGesture from 'react-native-simple-gesture';

import SetQuizCompleteMutation from '../../../mutations/SetQuizCompleteMutation';

import TouchableItem from '../../Common/TouchableItem';

import Dimensions from 'Dimensions';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

// import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
// var Fabric = require('react-native-fabric');
var Analytics = require('react-native-firebase-analytics');

class Opinions extends Component {

	constructor(props){
		super(props);
		this.state={
			height : 0,
			width : 0,
			questionCards : [],
			loading : false,
			pageSize : 15
		};
		this.nextQuestion = this.nextQuestion.bind(this);
		this.loadRecommendations = this.loadRecommendations.bind(this);
		this.loadFollowerSuggestions = this.loadFollowerSuggestions.bind(this);
		this.loadFollowerSimilarity = this.loadFollowerSimilarity.bind(this);
		this.scrollToNext = this.scrollToNext.bind(this);
		this.scrollToPrev = this.scrollToPrev.bind(this);
		this.scrollToNextWhenQuizComplete = this.scrollToNextWhenQuizComplete.bind(this);
		this.scrollToNextWhenQuizIncomplete = this.scrollToNextWhenQuizIncomplete.bind(this);
		this.updateScrollViewHeight = this.updateScrollViewHeight.bind(this);
    this.updateScrollViewWidth = this.updateScrollViewWidth.bind(this);
    this.scrollToEnd = this.scrollToEnd.bind(this);
    this.scrollToFirstScreen = this.scrollToFirstScreen.bind(this);
		this.prevScrollPos = 0;
    this.prevScrollPosX = 0;
		this.scrollViewHeight = 0;
    this.scrollViewWidth = 0;
		this.maxHeightReached = 0;
    this.maxWidthReached = 0;
		this.quizResultRendered = false;
	}

  componentWillMount() {
    const onSwipeRight = this.scrollToPrev;
    const onSwipeLeft = this.props.viewer.completed? this.scrollToNextWhenQuizComplete: this.scrollToNextWhenQuizIncomplete;

    this._panResponder = PanResponder.create({
      // Only respond to movements if the gesture is a swipe up
      onMoveShouldSetPanResponder: (e, gs) => {
        let sgs = new SimpleGesture(e,gs);
        if(sgs.isSwipeUp()) {
          onSwipeLeft();
          return true;
        }
        else if(sgs.isSwipeDown()) {
          onSwipeRight();
          return true;
        }
      }
    });
  }

	componentDidMount() {
		let windowSize = Dimensions.get('window');
		let windowHeight = windowSize.height;
		let windowWidth = windowSize.width;
		this.props.relay.forceFetch();
		if(this.props.fromQuizCard) {
			windowHeight = windowHeight - windowHeight/10;
		}
		else {
			windowHeight = windowHeight - 130;
		}

		this.setState({height: windowHeight, width: windowWidth});

    // var { Answers } = Fabric;
    // Answers.logContentView('DebateScreen Opinions', 'DebateScreen', 'ds-op-' + this.props.viewer.lastDebateId.toString(), { 'user-id': global.userId });
    // Answers.logCustom('DebateScreenView', { 'user-id': global.userId, 'debateId': this.props.viewer.lastDebateId.toString(), 'page': 'Opinions' });
    this.updateScrollViewHeight( (2+this.props.viewer.numQuestions) * windowHeight );
  }

	updateScrollViewHeight(containerHeight) {
		this.scrollViewHeight = this.scrollViewHeight + containerHeight;
	}

  updateScrollViewWidth(containerWidth) {
    this.scrollViewWidth = this.scrollViewWidth + containerWidth;
  }

	loadRecommendations(masterCharacterizationId) {
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('RecommendationByQuiz-' + this.props.viewer.index);
    }
		this.props.navigator.push(
			Router.getRoute('recommendationByQuiz',{
				masterCharacterizationId:masterCharacterizationId,
				quizId:this.props.quizId,
				height:this.state.height,
				width:this.state.width,
				goToDebate:this.props.goToDebateArg,
				enabled:this.props.enabled
			})
		);
	}

	loadFollowerSuggestions(subquizScores) {
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('FollowerSuggestionByQuiz-' + this.props.viewer.index);
    }
		this.props.navigator.push(
			Router.getRoute('followerSuggestionByQuiz',{
				subquizScoresArg:subquizScores,
				quizId:this.props.quizId,
				height:this.state.height,
				width:this.state.width
			})
		);
	}

	loadFollowerSimilarity(subquizScores) {
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('SimilarityToFollowers-' + this.props.viewer.index);
    }
		this.props.navigator.push(
			Router.getRoute('similarityToFollowers',{
				subquizScoresArg:subquizScores,
				quizId:this.props.quizId,
				height:this.state.height,
				width:this.state.width
			})
		);
	}

	nextQuestion(questionNumber, totalNumberOfQuestions) {

		// if(questionNumber%4==0 && !this.quizResultRendered) {
		// 	let {pageSize}  = this.state;
  //     if(pageSize<30) {
  // 			pageSize = pageSize + 10;
  // 			this.props.relay.setVariables({pageSize});
  // 			this.setState({pageSize});
  //     }
		// }

		if(questionNumber==totalNumberOfQuestions &&
				!this.props.viewer.completed &&
				!this.quizResultRendered) {
			this.setState({loading: true});
			this.lastScreen = (
				<QuizResultScreenContainer
					quizId={this.props.quizId}
					height={this.state.height}
					width={this.state.width}
					loadRecommendations={this.loadRecommendations}
					loadFollowerSuggestions={this.loadFollowerSuggestions}
					loadFollowerSimilarity={this.loadFollowerSimilarity}
          isIntroQuiz={this.props.isIntroQuiz}
				/>
			);
			this.questionList.push(this.lastScreen)
			// this.updateScrollViewHeight(this.state.height);
			this.quizResultRendered = true;
			this.setState({loading: false});
			let quizId = this.props.viewer.id;
			RelayStore.commitUpdate(
				new SetQuizCompleteMutation({
					completed : true,
					quizId : quizId
				})
			);
			this.scrollToNext();
		}
		else {
			this.scrollToNext();
		}
	}

  scrollToFirstScreen() {
    const {height} = this.state;
    this.refs['SCROLL_REF'].scrollTo({y:height});
    this.prevScrollPos = height;
    if(this.prevScrollPos>this.maxHeightReached) {
      this.maxHeightReached = this.prevScrollPos;
    }
  }

	scrollToNext() {
		const {height} = this.state;
		if(this.prevScrollPos + height >= this.scrollViewHeight) {return;}
		let scrollPos = this.prevScrollPos + height;
		this.refs['SCROLL_REF'].scrollTo({y:scrollPos});
		this.prevScrollPos = scrollPos;
    if(scrollPos>this.maxHeightReached) {
		  this.maxHeightReached = scrollPos;
    }
    // const {width} = this.state;
    // if(this.prevScrollPosX + width >= this.scrollViewWidth) {return;}
    // let scrollPosX = this.prevScrollPosX + width;
    // this.refs['SCROLL_REF'].scrollTo({x:scrollPosX});
    // this.prevScrollPosX = scrollPosX;
    // this.maxWidthReached = scrollPosX;
	}

	scrollToNextWhenQuizIncomplete() {
		const {height} = this.state;
		if(this.prevScrollPos + height > this.maxHeightReached) {return;}
		let scrollPos = this.prevScrollPos + height;
		this.refs['SCROLL_REF'].scrollTo({y:scrollPos});
		this.prevScrollPos = scrollPos;
    // const {width} = this.state;
    // if(this.prevScrollPosX + width > this.maxWidthReached) {return;}
    // let scrollPosX = this.prevScrollPosX + width;
    // this.refs['SCROLL_REF'].scrollTo({x:scrollPosX});
    // this.prevScrollPosX = scrollPosX;
	}

	scrollToNextWhenQuizComplete() {
    // let {pageSize}  = this.state;
    // if(pageSize<30) {
    //   pageSize = pageSize + 3;
    //   this.props.relay.setVariables({pageSize});
    //   this.setState({pageSize});
    // }
		this.scrollToNext();
	}

	scrollToPrev() {
		const {height} = this.state;
		if(this.prevScrollPos - height < 0) {return;}

		let scrollPos = this.prevScrollPos - height;
		this.refs['SCROLL_REF'].scrollTo({y:scrollPos});
		this.prevScrollPos = scrollPos;
    // const {width} = this.state;
    // if(this.prevScrollPosX - width < 0) {return;}

    // let scrollPosX = this.prevScrollPosX - width;
    // this.refs['SCROLL_REF'].scrollTo({x:scrollPosX});
    // this.prevScrollPosX = scrollPosX;
	}

  scrollToEnd() {
    this.prevScrollPos = (1+this.props.viewer.numQuestions) * this.state.height
    this.refs['SCROLL_REF'].scrollTo({y:this.prevScrollPos});
  }

	render() {

    const onSwipeRight = this.scrollToPrev;
    const onSwipeLeft = this.props.viewer.completed? this.scrollToNextWhenQuizComplete: this.scrollToNextWhenQuizIncomplete;

		const { Questions, numQuestions } = this.props.viewer;
		this.questionList = Questions.edges.map( (question, index, questions) =>
																					 <QuestionScreen
																						id={question.node.id}
																						key={index}
																						questionNumber={index + 1}
																						nextQuestion={this.nextQuestion}
																						scrollToNext={this.props.scrollToNext}
																						totalNumberOfQuestions={numQuestions}
																						height={this.state.height}
                                            width={this.state.width}
																						quizCompleted={this.props.viewer.completed}
                                            onSwipeRight={onSwipeRight}
                                            onSwipeLeft={onSwipeLeft}
																					 />
																			);

		let buttonText = "Take " + numQuestions + " positions";
    let introScreenBtnFn = this.scrollToFirstScreen;
		if(this.props.viewer.completed || this.quizResultRendered)
		{
			this.questionList.push(
				<QuizResultScreenContainer
					quizId={this.props.viewer.id}
					height={this.state.height}
					width={this.state.width}
					loadRecommendations={this.loadRecommendations}
					loadFollowerSuggestions={this.loadFollowerSuggestions}
					loadFollowerSimilarity={this.loadFollowerSimilarity}
          isIntroQuiz={this.props.isIntroQuiz}
          onSwipeRight={onSwipeRight}
          onSwipeLeft={onSwipeLeft}
				/>
			);
			buttonText = "Check results";
      introScreenBtnFn = this.scrollToEnd;
		}
		else {
			this.questionList.push(
					<TransitScreen
						height={this.state.height}
						title="Quiz Complete"
						description="Loading result..."
            onSwipeRight={onSwipeRight}
            onSwipeLeft={onSwipeLeft}
					/>
				)
		}

    // this.updateScrollViewWidth( (1+numQuestions) * this.state.width );
		// this.updateScrollViewHeight( (1+numQuestions) * this.state.height );

    // <GestureRecognizer
      // onSwipeUp={(state) => {this.props.viewer.completed? this.scrollToNextWhenQuizComplete(): this.scrollToNextWhenQuizIncomplete() }}
      // onSwipeDown={(state) => this.scrollToPrev()}
      // style={{flex : 1, backgroundColor : '#004d40'}}
    // >
    // </GestureRecognizer>

		return(

				<ScrollView
					ref={'SCROLL_REF'}
					scrollEnabled={false}
					showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
					pagingEnabled={true}
          horizontal={false}
          style={{flex : 1, backgroundColor : '#004d40'}}
          {...this._panResponder.panHandlers}
				>
					<TransitScreen
						height={this.state.height}
						title={this.props.viewer.title}
						description={this.props.viewer.description}
						buttonText={buttonText}
						scrollToNext={introScreenBtnFn}
            onSwipeRight={onSwipeRight}
            onSwipeLeft={onSwipeLeft}
					/>
					{this.questionList}
				</ScrollView>

		);
	}
}

export default createRenderer(Opinions, {
	queries : NodeQuery,
	queriesParams: ({ id }) => ({
		id: id ? id : null,
	}),
	initialVariables : {
		pageSize : 15
	},
	fragments: {
		viewer: () => Relay.QL`
			fragment on Quiz {
				id,
        index,
				title,
				description,
				lastDebateId,
				numQuestions,
				completed,
				Questions(first : $pageSize) {
					pageInfo{
						hasNextPage
					},
					edges{
            node{
    					id,
    					answered
					 }
          }
				}
			}
		`
	}
});
