import Relay from 'react-relay';
import React, {Component} from 'react';
import { View, Text, Image, StyleSheet, PanResponder } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import TouchableItem from '../../Common/TouchableItem';
var Line = require('./Line');
import colors from '../../../config/colors';

import ViewerQuery from '../../../queries/ViewerQuery';
import { createRenderer } from '../../../utils/RelayUtils';
import Router from '../../../routes/Router';

import Dimensions from 'Dimensions';
import SimpleGesture from 'react-native-simple-gesture';

class TransitScreen extends Component {

  componentWillMount() {
    let windowSize = Dimensions.get('window');
    this.windowWidth = windowSize.width;
    this._panResponder = PanResponder.create({
      // Only respond to movements if the gesture is a swipe up
      onMoveShouldSetPanResponder: (e, gs) => {
        let sgs = new SimpleGesture(e,gs);
        if(sgs.isSwipeUp()) {
          this.props.onSwipeLeft();
          return true;
        }
        else if(sgs.isSwipeDown()) {
          this.props.onSwipeRight();
          return true;
        }

      }
    });
  }

	render() {
		const { height, buttonText, scrollToNext } = this.props;

		return(
			<View style={[styles.promoView, styles.usecase, {height : height, width:this.windowWidth, padding : 10}]}>

					<Text style={[styles.text, styles.promoHeader]}>{this.props.title}</Text>

					<Line style={{backgroundColor:colors.humbeeDarkGrey}} />

					<Text style={[styles.text, styles.promoDescription]}>
						{this.props.description}
					</Text>

					{
						buttonText
						?
						<TouchableItem
							onPress={() => scrollToNext()}
							style={[ {height : 30, alignItems : 'center', justifyContent : 'center', backgroundColor : colors.humbeeGrey, borderRadius : 2, padding : 20, paddingVertical : 20, margin:30}]}>
								<Text style={{ fontFamily : 'helvetica', fontSize : 14, color : '#004d40', textAlign : 'center'}}>
									{buttonText}
								</Text>
						</TouchableItem>
						:
						<View></View>
					}
			</View>
		);
	}
}

export default Relay.createContainer(TransitScreen, {
	queries : ViewerQuery,
	fragments: {
	}
});

var styles = StyleSheet.create({
	usecase: {
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column',
		flex: 1
	},
	promoView: {
		backgroundColor: 'transparent',
	},
	text: {
		color: colors.humbeeGrey,
		textAlign: 'center'
	},
	promoHeader: {
		fontSize: 24,
		fontWeight: '700',
		alignSelf: 'center'
	},
	promoDescription: {
		fontSize: 18,
		fontWeight: '300',
		width: 220,
		alignSelf: 'center',
	},
	promoText: {
		fontSize: 14,
		fontWeight: '400',
		alignSelf: 'center',
		lineHeight: 22
	},
});
