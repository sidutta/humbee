import Relay from 'react-relay';
import React, {Component} from 'react';
import { ActivityIndicator, RefreshControl, RecyclerViewBackedScrollView, ScrollView, ListView, Text, View, StyleSheet, Image, Dimensions } from 'react-native';

import CreateFollowerMutation from '../../../mutations/CreateFollowerMutation';

import ViewerQuery from '../../../queries/ViewerQuery';
import { createRenderer } from '../../../utils/RelayUtils';
import RelayStore from '../../../utils/RelayStore';

import TouchableItem from '../../Common/TouchableItem';
import colors from '../../../config/colors';
import NavBar from '../../NavBar';

import Icon from 'react-native-vector-icons/Ionicons';

const LoadingIndicator = ({ loading }) => (
  loading ? (
    <View style={ styles.loading }>
      <ActivityIndicator
        animating={ true }
        style={[ styles.loading ]}
        size="large"
      />
    </View>
  ) : null
)

class FollowerSuggestionByQuiz extends Component {

  constructor(props) {
    super(props);

    this.state={
      alike : true,
      pagination: {},
      paginationOpposite: {},
      pageSize : 4,
      pageSizeOpposite : 4,
      dataSource: new ListView.DataSource({
                    rowHasChanged: (row1, row2) => {return true},
                  }),
      dataSourceOpposite: new ListView.DataSource({
                    rowHasChanged: (row1, row2) => {return true},
                  })
    };

    this.first = true;

    this.prevLength = 0;
    this.prevLengthOpposite = 0;

    this.userProfile = this.userProfile.bind(this);
    this.follow = this.follow.bind(this);
    this._renderRow = this._renderRow.bind(this);
    this._onRefresh = this._onRefresh.bind(this);
    this._onRefreshOpposite = this._onRefreshOpposite.bind(this);
    this._renderRowOpposite = this._renderRowOpposite.bind(this);
    this._onEndReached = this._onEndReached.bind(this);
    this._onEndReachedOpposite = this._onEndReachedOpposite.bind(this);
  }

  componentDidMount() {
    this.props.relay.setVariables({
      subquizScores: this.props.subquizScoresArg,
      fetchResults2: true
    });
    if(this.props.viewer && this.props.viewer.followerSuggestion) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows([ ...this.props.viewer.followerSuggestion.strangers.edges ])
      });
      this.setState({
        dataSourceOpposite: this.state.dataSourceOpposite.cloneWithRows([ ...this.props.viewer.followerSuggestion.strangersOpposite.edges ])
      });
    }
    // this.refs['SCROLL_VIEW'].scrollTo({x : 0, y : 0, animated : true});
    // this.refs['SCROLL_VIEW3'].scrollTo({x : 0, y : 0, animated : true});
  }

  componentWillReceiveProps(next) {
    if(next && next.viewer.followerSuggestion) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows([ ...next.viewer.followerSuggestion.strangers.edges ])
      });
    }
    if(next && next.viewer.followerSuggestion) {
      this.setState({
        dataSourceOpposite: this.state.dataSourceOpposite.cloneWithRows([ ...next.viewer.followerSuggestion.strangersOpposite.edges ])
      });
    }
  }

  userProfile(id) {
    const { navigator } = this.props;
    navigator.replace('userProfile',{id : id});
  }

  follow(id, isFollowing){
    RelayStore.commitUpdate(
      new CreateFollowerMutation({
        followingId : id,
        isFollowing : isFollowing
      }),{
        onFailure : () => {
          alert('Oops something went wrong. Please try again.');
        }
      }
    );
  }

  componentWillMount(){
    this.props.relay.forceFetch();
  }

  _onRefresh() {
    this._onEndReached();
  }

  _onRefreshOpposite() {
    this._onEndReachedOpposite();
  }

  _onEndReached() {
    const { pagination } = this.state;
    if (!pagination.loading && this.props.viewer.followerSuggestion.strangers.pageInfo.hasNextPage) {
      this.setState({
        pagination: {loading : true}
      })
      let {pageSize}  = this.state;
      pageSize = pageSize + 4;
      this.props.relay.setVariables({pageSize});
      this.setState({pageSize});
    }
    this.setState({
      pagination: { loading : false }
    })
  }

  _onEndReachedOpposite() {
    const { paginationOpposite } = this.state;
    if (!paginationOpposite.loading && this.props.viewer.followerSuggestion.strangersOpposite.pageInfo.hasNextPage) {
      let setPagination = {loading: true};
      this.setState({
        paginationOpposite: setPagination,
      })
      let {pageSizeOpposite}  = this.state;
      pageSizeOpposite = pageSizeOpposite + 4;
      this.props.relay.setVariables({pageSizeOpposite});
      this.setState({pageSizeOpposite});
    }
    let setPagination = {loading: false};
    this.setState({
      paginationOpposite: setPagination,
    })
  }

  f(stranger) {
    alert(stranger.node.isFollowing);
    this.follow(stranger.node.id, stranger.node.isFollowing)
  }

  _renderRow(stranger) {
    let similarity = stranger.node.similarity;
    let dissimilarity = 100 - stranger.node.similarity;
    if(similarity == 0) similarity = 0.01;
    if(dissimilarity == 0) dissimilarity = 0.01;
    if (stranger.type === 'Loading') {
      return <LoadingIndicator loading={ stranger.loading } />
    }
    else {
      return (
        <View style={{height : 80, borderWidth : 0, flexDirection: 'column',  padding : 8, paddingHorizontal : 3}}>
          <View style={{height : 50, borderWidth : 0, flexDirection: 'row', paddingHorizontal : 12}}>
            <View style={{height : 50, width : 50, borderWidth : 0}}>
              <TouchableItem onPress={this.userProfile.bind(this, stranger.node.id)} style={{flex : 1}}>
                <Image
                  style={{flex: 1, width: null, height: null, resizeMode: 'contain',}}
                  source={{uri: stranger.node.profilePic}}
                />
              </TouchableItem>
            </View>

            <View style={{marginLeft : 20, flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
              <View style={{flexDirection: 'column'}}>
                <View style={{justifyContent: 'space-between'}}>
                  <View>
                    <TouchableItem onPress={this.userProfile.bind(this, stranger.node.id)}>
                      <Text style={{fontSize : 16, fontFamily : 'helvetica', fontWeight : 'bold'}}>{stranger.node.firstName} {stranger.node.lastName}</Text>
                    </TouchableItem>
                  </View>

                </View>
              </View>

              <TouchableItem
                  style={[{height : 28, alignItems : 'center', justifyContent : 'center', backgroundColor : stranger.node.isFollowing ? colors.humbeeGreen : colors.humbeeGrey, borderRadius : 2, width : 60, paddingVertical : 15}]}
                  onPress={ () => this.follow(stranger.node.id, stranger.node.isFollowing)}>
              {
                stranger.node.isFollowing ? (
                  <Icon name='md-checkmark' size={25} style={{color : 'white' }}/>
                ) : (
                  <Text style={{ fontFamily : 'helvetica', fontSize : 14, color : colors.humbeeGreen, textAlign : 'center'}}>Follow</Text>
                )
              }

              </TouchableItem>

            </View>
          </View>
          <View style={{height : 20, flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{width : 90}}>
              <Text style={{ fontSize : 13, fontFamily : 'helvetica', fontWeight : 'bold', textAlign: 'center'}}>{stranger.node.similarity}% Similar</Text>
            </View>
            <View style={{paddingTop: 5, flex: 1}}>
              <View style={{backgroundColor: colors.humbeeGrey, height: 10, borderRadius: 8, paddingHorizontal: 3, justifyContent: 'center'}}>
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: similarity, backgroundColor: colors.humbeeGreen, height: 3, borderTopLeftRadius: 8, borderBottomLeftRadius: 8}} />
                  <View style={{flex: dissimilarity, backgroundColor: colors.humbeeGrey, height: 3, borderTopRightRadius: 8, borderBottomRightRadius: 8}} />
                </View>
              </View>
            </View>

          </View>
        </View>
      );
    }
  }

  _renderRowOpposite(stranger) {
    let dissimilarity = stranger.node.dissimilarity;
    if(dissimilarity == 100) dissimilarity = 99;
    if(dissimilarity == 0) dissimilarity = 0.01;
    let similarity = 100 - dissimilarity;
    if(similarity == 0) similarity = 0.01;


    if (stranger.type === 'Loading') {
      return <LoadingIndicator loading={ stranger.loading } />
    }
    else {
      return (
        <View style={{height : 80, borderWidth : 0, flexDirection: 'column',  padding : 8, paddingHorizontal : 3}}>
          <View style={{height : 50, borderWidth : 0, flexDirection: 'row', paddingHorizontal : 12}}>
            <View style={{height : 50, width : 50, borderWidth : 0}}>
              <TouchableItem onPress={this.userProfile.bind(this, stranger.node.id)} style={{flex : 1}}>
                <Image
                  style={{flex: 1, width: null, height: null, resizeMode: 'contain',}}
                  source={{uri: stranger.node.profilePic}}
                />
              </TouchableItem>
            </View>

            <View style={{marginLeft : 20, flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
              <View style={{flexDirection: 'column'}}>
                <View style={{justifyContent: 'space-between'}}>
                  <View>
                    <TouchableItem onPress={this.userProfile.bind(this, stranger.node.id)}>
                      <Text style={{fontSize : 16, fontFamily : 'helvetica', fontWeight : 'bold'}}>{stranger.node.firstName} {stranger.node.lastName}</Text>
                    </TouchableItem>
                  </View>

                </View>
              </View>

              <TouchableItem
                  style={[{height : 28, alignItems : 'center', justifyContent : 'center', backgroundColor : stranger.node.isFollowing ? colors.humbeeGreen : colors.humbeeGrey, borderRadius : 2, width : 60, paddingVertical : 15}]}
                  onPress={ () => this.follow(stranger.node.id, stranger.node.isFollowing)}>
              {
                stranger.node.isFollowing ? (
                  <Icon name='md-checkmark' size={25} style={{color : 'white' }}/>
                ) : (
                  <Text style={{ fontFamily : 'helvetica', fontSize : 14, color : colors.humbeeGreen, textAlign : 'center'}}>Follow</Text>
                )
              }
              </TouchableItem>

            </View>
          </View>
          <View style={{height : 20, flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{width : 90}}>
              <Text style={{ fontSize : 13, fontFamily : 'helvetica', fontWeight : 'bold', textAlign: 'center'}}>{dissimilarity}% Different</Text>
            </View>
            <View style={{paddingTop: 5, flex: 1}}>
              <View style={{backgroundColor: colors.humbeeGrey, height: 10, borderRadius: 8, paddingHorizontal: 3, justifyContent: 'center'}}>
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: dissimilarity, backgroundColor: 'red', height: 3, borderTopLeftRadius: 8, borderBottomLeftRadius: 8}} />
                  <View style={{flex: similarity, backgroundColor: colors.humbeeGrey, height: 3, borderTopRightRadius: 8, borderBottomRightRadius: 8}} />
                </View>
              </View>
            </View>

          </View>
        </View>
      );
    }
  }


  render() {
    const { width, navigator, viewer, subquizScores } = this.props;
    const { alike, dataSource, dataSourceOpposite } = this.state;
    const windowHeight = (Dimensions.get('window').height);
    const height = windowHeight - windowHeight/10;
    return(
      <View>
        { (subquizScores!='-1') ? (
            <View style={{flex : 1, backgroundColor: colors.humbeeGrey}}>
              <NavBar navigator={navigator} title="Follower Suggestions" opacity={1} elevation={5} />
              <View style={{height : height - 74}}>
                <View style={{flex : 1, backgroundColor: 'white', borderRadius : 2,  padding : 10}}>
                  { alike ?
                  <View style={{flex : 1}}>
                    <View style={{flex : 1, backgroundColor: 'white', borderRadius : 2}}>
                      <Text style={{height:20, fontSize : 16, fontFamily : 'helvetica', fontWeight : 'bold'}}>{alike ? 'Peeps who think alike' : 'Peeps who may change your mind' }</Text>
                      {
                        (viewer.followerSuggestion) ? (
                          (viewer.followerSuggestion.strangers.edges.length!=0) ? (
                            <ListView
                              ref={'SCROLL_VIEW'}
                              style={{flex : 1, marginVertical : 0, backgroundColor: 'white'}}
                              enableEmptySections={ true }
                              renderScrollComponent={props => <RecyclerViewBackedScrollView {...props} />}
                              automaticallyAdjustContentInsets={ false }
                              dataSource={dataSource}
                              renderRow={ row => this._renderRow(row)}
                              renderSeparator={ () =>   <View style={{height:1, backgroundColor: colors.humbeeGrey}} />}
                              refreshControl={
                                <RefreshControl
                                  refreshing={ false }
                                  onRefresh={ () => this._onRefresh(alike)}
                                />
                              }
                              onEndReached={ () => this._onEndReached() }
                              onEndReachedThreshold={ 50 }
                            />
                          )
                          : (
                            <View>
                              <Text>
                                Insufficient data, sorry, please check later!
                              </Text>
                              <ListView
                                ref={'SCROLL_VIEW'}
                                style={{flex : 1, marginVertical : 0, backgroundColor: 'white'}}
                                enableEmptySections={ true }
                                renderScrollComponent={props => <RecyclerViewBackedScrollView {...props} />}
                                automaticallyAdjustContentInsets={ false }
                                dataSource={dataSource}
                                renderRow={ row => this._renderRow(row)}
                                renderSeparator={ () =>   <View style={{height:1, backgroundColor: colors.humbeeGrey}} />}
                                refreshControl={
                                  <RefreshControl
                                    refreshing={ false }
                                    onRefresh={ () => this._onRefresh(alike)}
                                  />
                                }
                                onEndReached={ () => this._onEndReached() }
                                onEndReachedThreshold={ 50 }
                              />
                            </View>
                          )
                        ) : (
                          <View>
                            <Text>
                              Loading suggestions...
                            </Text>
                          </View>
                        )
                      }
                    </View>
                  </View>
                  :
                  <View style={{flex : 1}}>
                    <View style={{flex : 1, backgroundColor: 'white', borderRadius : 2}}>
                      <Text style={{height:20, fontSize : 16, fontFamily : 'helvetica', fontWeight : 'bold'}}>{alike ? 'Peeps who think alike' : 'Peeps who may change your mind' }</Text>
                      {
                        (viewer.followerSuggestion) ? (
                          (viewer.followerSuggestion.strangersOpposite.edges.length!=0) ? (
                            <ListView
                              ref={'SCROLL_VIEW'}
                              renderScrollComponent={props => <RecyclerViewBackedScrollView {...props} />}
                              style={{flex : 1, marginVertical : 0, backgroundColor: 'white'}}
                              enableEmptySections={ true }
                              automaticallyAdjustContentInsets={ false }
                              dataSource={dataSourceOpposite}
                              renderRow={ row => this._renderRowOpposite(row)}
                              renderSeparator={ () =>   <View style={{height:1, backgroundColor: colors.humbeeGrey}} />}
                              refreshControl={
                                <RefreshControl
                                  refreshing={ false }
                                  onRefresh={ () => this._onRefreshOpposite()}
                                />
                              }
                              onEndReached={ () => this._onEndReachedOpposite() }
                              onEndReachedThreshold={ 50 }
                            />
                          )
                          : (
                            <View>
                              <Text>
                                Insufficient data, sorry, please check later!
                              </Text>
                              <ListView
                                ref={'SCROLL_VIEW'}
                                renderScrollComponent={props => <RecyclerViewBackedScrollView {...props} />}
                                style={{flex : 1, marginVertical : 0, backgroundColor: 'white'}}
                                enableEmptySections={ true }
                                automaticallyAdjustContentInsets={ false }
                                dataSource={dataSourceOpposite}
                                renderRow={ row => this._renderRowOpposite(row)}
                                renderSeparator={ () =>   <View style={{height:1, backgroundColor: colors.humbeeGrey}} />}
                                refreshControl={
                                  <RefreshControl
                                    refreshing={ false }
                                    onRefresh={ () => this._onRefreshOpposite()}
                                  />
                                }
                                onEndReached={ () => this._onEndReachedOpposite() }
                                onEndReachedThreshold={ 50 }
                              />
                            </View>
                          )
                        ) : (
                          <View>
                            <Text>
                              Loading suggestions...
                            </Text>
                          </View>
                        )
                      }
                    </View>
                  </View>
                }
                </View>
              </View>
              <View style={{ height : 50,flexDirection : 'row', backgroundColor : colors.humbeeGreen}}>
                <TouchableItem
                  onPress={()  => {
                    if(viewer.followerSuggestion){
                      this.refs['SCROLL_VIEW'].scrollTo({x : 0, y : 0, animated : true});
                    }
                    this.setState({alike : true});
                    // this.forceUpdate();
                  }}
                  style={{ flex : 1, alignItems : 'center', justifyContent : 'space-around'}}>
                  <Text style={{ color : alike ? 'white' : colors.humbeeDarkGrey, fontSize : 14, fontFamily : 'helvetica'}}>THINK ALIKE</Text>
                </TouchableItem>
                <TouchableItem
                  style={{ flex : 1, alignItems : 'center', justifyContent : 'space-around'}}
                  onPress={() => {
                    if(viewer.followerSuggestion){
                      this.refs['SCROLL_VIEW'].scrollTo({x : 0, y : 0, animated : true});
                    }
                    this.setState({alike : false});
                    // this.forceUpdate();
                  }}>
                  <Text style={{ color : alike ? colors.humbeeDarkGrey : 'white', fontSize : 14, fontFamily : 'helvetica'}}>THINK DIFFERENT</Text>
                </TouchableItem>
              </View>
            </View>
          ) : (
            <View>
            </View>
          )
        }
      </View>
    );
  }
}

export default createRenderer(FollowerSuggestionByQuiz, {
  queries : ViewerQuery,
  queriesParams: ({ subquizScores }) => ({
    subquizScores: subquizScores ? subquizScores : null,
  }),
  initialVariables: {
    subquizScores: '-1',
    fetchResults2: false,
    pageSize : 4,
    pageSizeOpposite : 4
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        followerSuggestion(subquizScores: $subquizScores) @include(if: $fetchResults2) {
          strangers(first:$pageSize) {
            pageInfo{
              hasNextPage
            },
            edges {
              node {
                id,
                firstName,
                lastName,
                username,
                profilePic,
                similarity,
                isFollowing
              }
            }
          },
          strangersOpposite(first:$pageSizeOpposite) {
            pageInfo{
              hasNextPage
            },
            edges {
              node {
                id,
                firstName,
                lastName,
                username,
                profilePic,
                dissimilarity,
                isFollowing
              }
            }
          },
        }
      }
    `
  }
});

var styles = StyleSheet.create({
  usecase: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    flex: 1,
    width: null, height: null,
    resizeMode: 'contain',
  },
  promoView: {
    backgroundColor: 'transparent',

  },
  text: {
    color: 'black',
    textAlign: 'center'
  },
  promoHeader: {
    fontSize: 42,
    fontWeight: '700',
    alignSelf: 'center'
  },
  promoDescription: {
    fontSize: 22,
    fontWeight: '300',
    width: 220,
    alignSelf: 'center',
  },
  promoText: {
    fontSize: 14,
    fontWeight: '400',
    alignSelf: 'center',
    lineHeight: 22,
    width: 220,
    color: 'black',
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10
  }
});
