// import Relay from 'react-relay';
// import React, {Component} from 'react';
// import {  ScrollView, Text, View  } from 'react-native';

// import ViewerQuery from '../../../queries/ViewerQuery';
// import { createRenderer } from '../../../utils/RelayUtils';
// import Router from '../../../routes/Router';

// import colors from '../../../config/colors';
// import QuestionScreenContainer from '../../Containers/QuestionScreenContainer';
// import TransitScreen from './TransitScreen';

// import TouchableItem from '../../Common/TouchableItem';

// class SubquizScreen extends Component {

// 	constructor(props) {
// 		super(props);
// 		this.state={
// 			questionCards : []
// 		};
// 		this.nextQuestion = this.nextQuestion.bind(this);

// 		let questionsAnswered = 0;
// 		let numQuestions = 0;

// 		for(let question of this.props.viewer.Questions) {
// 			numQuestions = numQuestions + 1;
// 			if(question.answered) {questionsAnswered = questionsAnswered + 1;}
// 		}

// 		this.questionsAnswered = questionsAnswered;
// 		this.subquizCompleted = (questionsAnswered==numQuestions);
// 		this.numQuestions = numQuestions;
// 	}

// 	nextQuestion(questionNumber, totalNumberOfQuestions, isLastSubquiz) {
// 		this.questionsAnswered = this.questionsAnswered + 1;
// 		if(this.questionsAnswered == this.numQuestions) {
// 			this.subquizCompleted = true;
// 			// this.forceUpdate();
// 		}
// 		this.props.nextQuestion(questionNumber, totalNumberOfQuestions, isLastSubquiz);
// 	}

// 	render() {
// 		const {Questions} = this.props.viewer;
// 		const questionList = Questions.map( (question, index, questions) =>
// 																					 <QuestionScreenContainer
// 																						 questionId={question.id}
// 																						 key={index}
// 																						 questionNumber={index + 1}
// 																						 nextQuestion={this.nextQuestion}
// 																						 scrollToNext={this.props.scrollToNext}
// 																						 totalNumberOfQuestions={this.numQuestions}
// 																						 height={this.props.height}
// 																						 quizCompleted={this.props.quizCompleted}
// 																						 isLastSubquiz={this.props.isLastSubquiz}
// 																					 />
// 																			);

// 		this.props.updateScrollViewHeight( (2+this.numQuestions) * this.props.height );

// 		return(
// 			<View>
// 				<TransitScreen
// 					height={this.props.height}
// 					scrollToNext={this.props.scrollToNext}
// 					viewer={this.props.viewer}
// 					title={this.props.viewer.title}
// 					completed={this.props.completed}
// 					description="Explain the subquiz here: This quiz is Lorum ipsum dimsum."
// 					buttonText={"Take "+this.props.viewer.Questions.length+" positions"}
// 					intro={true}
// 				/>
// 				{questionList}
// 			</View>
// 		);
// 	}
// }

// export default createRenderer(SubquizScreen, {
// 	queries : ViewerQuery,
// 	fragments: {
// 		viewer: () => Relay.QL`
// 			fragment on Subquiz {
// 				id,
// 				title,
// 				Questions {
// 					id,
// 					answered
// 				}
// 			}
// 		`
// 	}
// });
