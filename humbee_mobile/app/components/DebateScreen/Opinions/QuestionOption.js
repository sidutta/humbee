import React, {Component} from 'react';
import { Text} from 'react-native';
import TouchableItem from '../../Common/TouchableItem';

import colors from '../../../config/colors';

export default class QuestionOption extends Component {

	render(){
		const { markAsAnswered, option, style} = this.props
		const { text, id } = option
		return(
			<TouchableItem
				onPress={() => markAsAnswered(id)}
				style={[{ alignItems : 'center', justifyContent : 'center', backgroundColor : colors.humbeeGreen, borderRadius : 2, padding : 5}, style]}>
				<Text style={{ fontFamily : 'helvetica', fontSize : 14, color : colors.humbeeGrey, textAlign : 'center'}}>{text}</Text>
			</TouchableItem>
		);
	}
}
