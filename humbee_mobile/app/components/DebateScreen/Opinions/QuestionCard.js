import React, {Component} from 'react';
import { View, Text, ActivityIndicator } from 'react-native';

import RelayStore from '../../../utils/RelayStore';

import QuestionOption from './QuestionOption';

import CreateResponseMutation from '../../../mutations/CreateResponseMutation';

import colors from '../../../config/colors';

export default class QuestionCard extends Component {
	constructor(props){
		super(props);
		this.markAsAnswered = this.markAsAnswered.bind(this);
	}

	markAsAnswered(optionId) {
		const {nextQuestion, questionNumber, questionId, totalNumberOfQuestions, unsetIsLoading, setIsLoading, editPressed, unsetEditPressed} = this.props;
		setIsLoading();
    let edit = false;
    if(editPressed) {
      edit = true;
      unsetEditPressed();
    }
		RelayStore.commitUpdate(
			new CreateResponseMutation({
				optionId : optionId,
				questionId : questionId,
        edit : edit
			}),{
				onSuccess : () => {
					unsetIsLoading();
					setTimeout(() => {
						nextQuestion(questionNumber, totalNumberOfQuestions);
					}, 1000);
				},
				onFailure : () => {
					unsetIsLoading();
					alert('Network error: Couldn\'t register response');
				}
			}
		);
	}

	render() {
		const { text } = this.props.viewer;
    const { answered } = this.props;
		let options  = this.props.viewer.options;
		let numberOfOptions = options.length;

		let totalVotes = 0;
		let largestVote = 0;

		let indexOfUnsure = -1;

		for(let i=0; i<options.length; i++) {
			if(options[i].text == "unsure"||options[i].text == "Unsure") {
				indexOfUnsure = i;
			}
			else {
				totalVotes = totalVotes + options[i].votes;
				if(options[i].votes>largestVote) {
					largestVote = options[i].votes;
				}
			}
		}

		totalVotes = (totalVotes == 0) ? 1 : totalVotes;
		if(indexOfUnsure!=-1) {
			options.splice(indexOfUnsure, 1);
			numberOfOptions = numberOfOptions - 1;
		}

		let optionsMarkup;
		let sentiment;

		if (numberOfOptions == 2) {
			optionsMarkup = (
				<View style={{ flexDirection : 'row'}}>
					<QuestionOption style={{marginRight : 5, flex: 1}} option={options[0]} key={0} markAsAnswered={this.markAsAnswered}/>
					<QuestionOption style={{marginLeft : 5, flex: 1}} option={options[1]} key={1} markAsAnswered={this.markAsAnswered}/>
				</View>
			);
		}
		else {
			let optionsList = options.map( (option, index) => (
					<QuestionOption style={{marginBottom : 10}} option={option} key={index} markAsAnswered={this.markAsAnswered}/>)
			);
			optionsMarkup = (
					<View>
						{optionsList}
					</View>
			);
		}

		if(numberOfOptions == 2) {
			sentiment = (
				<View style={{ alignItems: 'center', justifyContent: 'center'}}>
					<View style={{ padding : 5, borderRadius : 2, flex: 1, flexDirection: 'row', backgroundColor : 'white'}}>
						<View style={{flex : options[0].votes, backgroundColor : 'seagreen'}}/>
						<View style={{flex : options[1].votes, backgroundColor : 'tomato'}}/>
					</View>
					<View style={{flex : 1, flexDirection : 'row', padding : 5, alignItems : 'center'}}>
						<Text style={{flex : 1, textAlign: 'center', fontSize : 14, fontFamily : 'helvetica', fontWeight : 'bold'}}>{(parseInt((options[0].votes/totalVotes)*100)) + '% ' + options[0].text}</Text>
						<Text style={{flex : 1, textAlign: 'center',fontSize : 14, fontFamily : 'helvetica', fontWeight : 'bold'}}>{(parseInt((options[1].votes/totalVotes)*100)) + '% ' + options[1].text}</Text>
					</View>
				</View>
			);
		}
		else {
			let sentimentBars = options.map( (option, index) =>

				(
					<View>
  					<View style={{backgroundColor : colors.humbeeGrey, borderRadius : 2, padding : 5, marginBottom : 10, flexDirection : 'row'}} key={index}>
  						<View style={{flex : 1, alignItems : 'center', justifyContent : 'center'}}>
  							<Text style={{fontSize : 14, fontFamily : 'helvetica'}}>{parseInt((option.votes/totalVotes)*100) + ' %'}</Text>
  						</View>
  						<View style={{ flex : 4 }}>
  							<View style={{ padding : 2}}>
  								<Text style={{ fontSize : 12, fontFamily : 'helvetica', color : (option.voted)?(colors.humbeeGreen) : 'black'}}>{option.text}</Text>
  							</View>
  						</View>
  					</View>
					</View>
				)
				// {console.log('oo', totalVotes, option, index);return null;}
			);

			sentiment = (
				<View style={{flex : numberOfOptions}}>
					{sentimentBars}
				</View>
			);
		}

		return (
			<View style={{backgroundColor: 'white', borderRadius : 2, padding : 10}}>
				<Text style={{ textAlign : 'center', fontSize : 16, fontFamily : 'helvetica', fontWeight : 'bold', marginBottom : 10}}>{text}</Text>
				{answered ? sentiment : optionsMarkup}
			</View>
		);
	}
}
