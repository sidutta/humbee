// import Relay from 'react-relay';
// import React, {Component} from 'react';
// import { ActivityIndicator, RefreshControl, ListView, ScrollView, Text, View, StyleSheet, Image, Dimensions } from 'react-native';

// import CreateFollowerMutation from '../../../mutations/CreateFollowerMutation';
// import UserProfileContainer from '../../Containers/UserProfileContainer';

// import ViewerQuery from '../../../queries/ViewerQuery';
// import { createRenderer } from '../../../utils/RelayUtils';
// import Router from '../../../routes/Router';
// import RelayStore from '../../../utils/RelayStore';

// import TouchableItem from '../../Common/TouchableItem';
// import colors from '../../../config/colors';
// import NavBar from '../../NavBar';
// import {
//   GoogleAnalyticsTracker
// } from 'react-native-google-analytics-bridge';

// const LoadingIndicator = ({ loading }) => (
//   loading ? (
//     <View style={ styles.loading }>
//       <ActivityIndicator
//         animating={ true }
//         style={[ styles.loading ]}
//         size="large"
//       />
//     </View>
//   ) : null
// )

// class FollowerSuggestionByQuiz extends Component {

//   constructor(props) {
//     super(props);

//     this.state={
//       alike : true,
//       pagination: {},
//       paginationOpposite: {},
//       pageSize : 4,
//       pageSizeOpposite : 4,
//       dataSource: new ListView.DataSource({
//                     rowHasChanged: (row1, row2) => row1 !== row2,
//                   }),
//       dataSourceOpposite: new ListView.DataSource({
//                     rowHasChanged: (row1, row2) => row1 !== row2,
//                   })
//     };

//     this.first = true;

//     this.prevLength = 0;
//     this.prevLengthOpposite = 0;

//     this.userProfile = this.userProfile.bind(this);
//     this.follow = this.follow.bind(this);
//     this._renderRow = this._renderRow.bind(this);
//     this._onRefresh = this._onRefresh.bind(this);
//     this._onEndReached = this._onEndReached.bind(this);
//     this._renderRowOpposite = this._renderRowOpposite.bind(this);
//     this._onRefreshOpposite = this._onRefreshOpposite.bind(this);
//     this._onEndReachedOpposite = this._onEndReachedOpposite.bind(this);
//   }

//   componentDidMount() {
//     this.props.relay.setVariables({
//       subquizScores: this.props.subquizScoresArg,
//       fetchResults2: true
//     });
//   }

//   componentWillReceiveProps(next) {
//     // alert('l')
//     if(next && next.viewer.followerSuggestion) {
//     //   next.viewer.followerSuggestion.strangers.edges.length != this.prevLength) {
//       this.setState({
//         dataSource: this.state.dataSource.cloneWithRows([ ...next.viewer.followerSuggestion.strangers.edges ])
//       });
//       // this.prevLength = next.viewer.followerSuggestion.strangers.edges.length;
//     }

//     if(next && next.viewer.followerSuggestion) {
//       // && next.viewer.followerSuggestion.strangersOpposite.edges.length != this.prevLengthOpposite) {
//       this.setState({
//         dataSourceOpposite: this.state.dataSourceOpposite.cloneWithRows([ ...next.viewer.followerSuggestion.strangersOpposite.edges ])
//       });
//       // this.prevLengthOpposite = next.viewer.followerSuggestion.strangersOpposite.edges.length;
//     }
//   }

//   userProfile(id) {
//     const { navigator } = this.props;
//     navigator.push(
//       Router.getRoute('userProfile',{
//         id : id
//       })
//     );
//   }

//   follow(id, isFollowing){
//     RelayStore.commitUpdate(
//       new CreateFollowerMutation({
//         followingId : id,
//         isFollowing : isFollowing
//       }),{
//         onFailure : () => {
//           alert('Oops something went wrong. Please try again.');
//         }
//       }
//     );
//   }


//   _onRefresh() {
//     this._onEndReached();
//   }

//   _onRefreshOpposite() {
//     this._onEndReachedOpposite();
//   }

//   _onEndReached() {
//     const { pagination } = this.state;
//     if (!pagination.loading && this.props.viewer.followerSuggestion.strangers.pageInfo.hasNextPage) {
//       let setPagination = {loading: true};
//       this.setState({
//         pagination: setPagination,
//       })
//       let {pageSize}  = this.state;
//       pageSize = pageSize + 4;
//       this.props.relay.setVariables({pageSize});
//       this.setState({pageSize});
//     }
//     setPagination = {loading: false};
//     this.setState({
//       pagination: setPagination,
//     })
//   }

//   _onEndReachedOpposite() {
//     const { paginationOpposite } = this.state;
//     if (!paginationOpposite.loading && this.props.viewer.followerSuggestion.strangersOpposite.pageInfo.hasNextPage) {
//       let setPagination = {loading: true};
//       this.setState({
//         paginationOpposite: setPagination,
//       })
//       let {pageSizeOpposite}  = this.state;
//       pageSizeOpposite = pageSizeOpposite + 4;
//       this.props.relay.setVariables({pageSizeOpposite});
//       this.setState({pageSizeOpposite});
//     }
//     setPagination = {loading: false};
//     this.setState({
//       paginationOpposite: setPagination,
//     })
//   }

//   f(stranger) {
//     alert(stranger.node.isFollowing);
//     this.follow(stranger.node.id, stranger.node.isFollowing)
//   }

//   componentWillMount(){
//     this.props.relay.forceFetch();
//   }

//   _renderRow(stranger) {
//     let similarity = stranger.node.similarity;
//     let dissimilarity = 100 - stranger.node.similarity;
//     if(similarity == 0) similarity = 1;
//     if(dissimilarity == 0) dissimilarity = 1;
//     if (stranger.type === 'Loading') {
//       return <LoadingIndicator loading={ stranger.loading } />
//     }
//     else {
//       return (
//         <View style={{height : 80, borderWidth : 0, flexDirection: 'column',  padding : 8, paddingHorizontal : 3}}>
//           <View style={{height : 50, borderWidth : 0, flexDirection: 'row', paddingHorizontal : 12}}>
//             <View style={{height : 50, width : 50, borderWidth : 0}}>
//               <TouchableItem onPress={this.userProfile.bind(this, stranger.node.id)} style={{flex : 1}}>
//                 <Image
//                   style={{flex: 1, width: null, height: null, borderRadius : 200, resizeMode: 'contain',}}
//                   source={{uri: stranger.node.profilePic}}
//                 />
//               </TouchableItem>
//             </View>

//             <View style={{marginLeft : 20, flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
//               <View style={{flexDirection: 'column'}}>
//                 <View style={{justifyContent: 'space-between'}}>
//                   <View>
//                     <TouchableItem onPress={this.userProfile.bind(this, stranger.node.id)}>
//                       <Text style={{fontSize : 16, fontFamily : 'helvetica', fontWeight : 'bold', color : colors.humbeeGreen}}>{stranger.node.firstName} {stranger.node.lastName}</Text>
//                     </TouchableItem>
//                   </View>

//                 </View>
//               </View>

//               <TouchableItem
//                   style={[{height : 28, alignItems : 'center', justifyContent : 'center', backgroundColor : stranger.node.isFollowing ? colors.humbeeGreen : colors.humbeeGrey, borderRadius : 2, width : 60, paddingVertical : 15}]}
//                   onPress={ () => this.follow(stranger.node.id, stranger.node.isFollowing)}
//               >
//                   <Text style={{ fontFamily : 'helvetica', fontSize : 14, color : stranger.node.isFollowing ? 'white' : colors.humbeeGreen, textAlign : 'center'}}>Follow</Text>
//               </TouchableItem>
//             </View>
//           </View>
//           <View style={{height : 20, flexDirection: 'row', justifyContent: 'space-between'}}>
//             <View style={{width : 90}}>
//               <Text style={{ fontSize : 13, fontFamily : 'helvetica', fontWeight : 'bold', textAlign: 'center'}}>{parseInt(stranger.node.similarity)}% Similar</Text>
//             </View>
//             <View style={{paddingTop: 5, flex: 1}}>
//               <View style={{backgroundColor: colors.humbeeGrey, height: 10, borderRadius: 8, paddingHorizontal: 3, justifyContent: 'center'}}>
//                 <View style={{flexDirection: 'row'}}>
//                   <View style={{flex: similarity, backgroundColor: colors.humbeeGreen, height: 3, borderTopLeftRadius: 8, borderBottomLeftRadius: 8}} />
//                   <View style={{flex: dissimilarity, backgroundColor: colors.humbeeGrey, height: 3, borderTopRightRadius: 8, borderBottomRightRadius: 8}} />
//                 </View>
//               </View>
//             </View>

//           </View>
//         </View>
//       );
//     }
//   }

//   _renderRowOpposite(stranger) {
//     let dissimilarity = stranger.node.dissimilarity;
//     if(dissimilarity == 100) dissimilarity = 99;
//     if(dissimilarity == 0) dissimilarity = 1;
//     let similarity = 100 - dissimilarity;
//     if(similarity == 0) similarity = 1;


//     if (stranger.type === 'Loading') {
//       return <LoadingIndicator loading={ stranger.loading } />
//     }
//     else {
//       return (
//         <View style={{height : 80, borderWidth : 0, flexDirection: 'column',  padding : 8, paddingHorizontal : 3}}>
//           <View style={{height : 50, borderWidth : 0, flexDirection: 'row', paddingHorizontal : 12}}>
//             <View style={{height : 50, width : 50, borderWidth : 0}}>
//               <TouchableItem onPress={this.userProfile.bind(this, stranger.node.id)} style={{flex : 1}}>
//                 <Image
//                   style={{flex: 1, width: null, height: null, borderRadius : 200, resizeMode: 'contain',}}
//                   source={{uri: stranger.node.profilePic}}
//                 />
//               </TouchableItem>
//             </View>

//             <View style={{marginLeft : 20, flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
//               <View style={{flexDirection: 'column'}}>
//                 <View style={{justifyContent: 'space-between'}}>
//                   <View>
//                     <TouchableItem onPress={this.userProfile.bind(this, stranger.node.id)}>
//                       <Text style={{fontSize : 16, fontFamily : 'helvetica', fontWeight : 'bold', color : colors.humbeeGreen}}>{stranger.node.firstName} {stranger.node.lastName}</Text>
//                     </TouchableItem>
//                   </View>

//                 </View>
//               </View>

//               <TouchableItem
//                   style={[{height : 28, alignItems : 'center', justifyContent : 'center', backgroundColor : stranger.node.isFollowing ? colors.humbeeGreen : colors.humbeeGrey, borderRadius : 2, width : 60, paddingVertical : 15}]}
//                   onPress={ () => this.follow(stranger.node.id, stranger.node.isFollowing)}
//               >
//                   <Text style={{ fontFamily : 'helvetica', fontSize : 14, color : stranger.node.isFollowing? 'white' : colors.humbeeGreen, textAlign : 'center'}}>Follow</Text>
//               </TouchableItem>
//             </View>
//           </View>
//           <View style={{height : 20, flexDirection: 'row', justifyContent: 'space-between'}}>
//             <View style={{width : 90}}>
//               <Text style={{ fontSize : 13, fontFamily : 'helvetica', fontWeight : 'bold', textAlign: 'center'}}>{dissimilarity}% Different</Text>
//             </View>
//             <View style={{paddingTop: 5, flex: 1}}>
//               <View style={{backgroundColor: colors.humbeeGrey, height: 10, borderRadius: 8, paddingHorizontal: 3, justifyContent: 'center'}}>
//                 <View style={{flexDirection: 'row'}}>
//                   <View style={{flex: dissimilarity, backgroundColor: 'red', height: 3, borderTopLeftRadius: 8, borderBottomLeftRadius: 8}} />
//                   <View style={{flex: similarity, backgroundColor: colors.humbeeGrey, height: 3, borderTopRightRadius: 8, borderBottomRightRadius: 8}} />
//                 </View>
//               </View>
//             </View>

//           </View>
//         </View>
//       );
//     }
//   }


//   render() {
//     const { width, navigator, viewer, subquizScores } = this.props;
//     const { alike, dataSource, dataSourceOpposite } = this.state;
//     const windowHeight = (Dimensions.get('window').height);
//     const height = windowHeight - windowHeight/10;
//     return(
//       <View>
//         { (subquizScores!='-1') ? (
//             <View style={{flex : 1, backgroundColor: colors.humbeeGrey}}>
//               <NavBar navigator={navigator} title="Follower Suggestions" opacity={1} elevation={5} />
//               <View style={{height : height - 74}}>
//                 <View style={{flex : 1, backgroundColor: 'white', borderRadius : 2,  padding : 10}}>
//                   <View style={{flex : 1}}>
//                     <View style={{flex : 1, backgroundColor: 'white', borderRadius : 2}}>
//                       <Text style={{height:20, fontSize : 16, fontFamily : 'helvetica', fontWeight : 'bold'}}>{alike ? 'Peeps who think alike' : 'Peeps who may change your mind' }</Text>
//                       {
//                         (viewer.followerSuggestion) ? (
//                           <ListView
//                             ref={'SCROLL_VIEW'}
//                             style={{flex : 1, marginVertical : 0, backgroundColor: 'white'}}
//                             enableEmptySections={ true }
//                             automaticallyAdjustContentInsets={ false }
//                             dataSource={ alike ?  dataSource  : dataSourceOpposite}
//                             renderRow={ row => {
//                               if(alike){
//                                 this._renderRow(row);
//                               } else {
//                                 this._renderRowOpposite(row);
//                               }
//                             }}
//                             renderSeparator={ () => {
//                               return (
//                                 <View style={{height:1, backgroundColor: colors.humbeeGrey}} />
//                               );
//                             }}
//                             refreshControl={
//                               <RefreshControl
//                                 refreshing={ false }
//                                 onRefresh={ () =>{
//                                   if(alike) {
//                                     this._onRefresh();
//                                   } else {
//                                     this._onRefreshOpposite();
//                                   }
//                                 }}
//                               />
//                             }
//                             onEndReached={ () =>{
//                               if(alike){
//                                 this._onEndReached();
//                               } else {
//                                 this._onEndReachedOpposite();
//                               }
//                             }}
//                             onEndReachedThreshold={ 50 }
//                           />
//                         ) : (
//                           <View></View>
//                         )
//                       }
//                     </View>
//                   </View>
//                 </View>
//               </View>
//               <View style={{ height : 50,flexDirection : 'row', backgroundColor : colors.humbeeGreen}}>
//                 <TouchableItem
//                   onPress={()  => {
//                     if(!alike){
//                       if(viewer.followerSuggestion){
//                         this.refs['SCROLL_VIEW'].scrollTo({x : 0, y : 0, animated : true});
//                       }
//                       this.setState({alike : true});
//                     }
//                   }}
//                   style={{ flex : 1, alignItems : 'center', justifyContent : 'space-around'}}>
//                   <Text style={{ color : alike ? 'white' : colors.humbeeDarkGrey, fontSize : 14, fontFamily : 'helvetica'}}>THINK ALIKE</Text>
//                 </TouchableItem>
//                 <TouchableItem
//                   style={{ flex : 1, alignItems : 'center', justifyContent : 'space-around'}}
//                   onPress={() => {
//                     if(alike){
//                       if(viewer.followerSuggestion){
//                         this.refs['SCROLL_VIEW'].scrollTo({x : 0, y : 0, animated : true});
//                       }
//                       this.setState({alike : false});
//                     }
//                   }}>
//                   <Text style={{ color : alike ? colors.humbeeDarkGrey : 'white', fontSize : 14, fontFamily : 'helvetica'}}>THINK DIFFERENT</Text>
//                 </TouchableItem>
//               </View>
//             </View>
//           ) : (
//             <View>
//             </View>
//           )
//         }
//       </View>
//     );
//   }
// }

// export default createRenderer(FollowerSuggestionByQuiz, {
//   queries : ViewerQuery,
//   queriesParams: ({ subquizScores }) => ({
//     subquizScores: subquizScores ? subquizScores : null,
//   }),
//   initialVariables: {
//     subquizScores: '-1',
//     fetchResults2: false,
//     pageSize : 4,
//     pageSizeOpposite : 4
//   },
//   fragments: {
//     viewer: () => Relay.QL`
//       fragment on Viewer {
//         followerSuggestion(subquizScores: $subquizScores) @include(if: $fetchResults2) {
//           friends(first:5) {
//             edges {
//               node {
//                 firstName,
//                 lastName
//               }
//             }
//           },
//           strangers(first:$pageSize) {
//             pageInfo{
//               hasNextPage
//             },
//             edges {
//               node {
//                 id,
//                 firstName,
//                 lastName,
//                 username,
//                 profilePic,
//                 similarity,
//                 isFollowing
//               }
//             }
//           },
//           strangersOpposite(first:$pageSizeOpposite) {
//             pageInfo{
//               hasNextPage
//             },
//             edges {
//               node {
//                 id,
//                 firstName,
//                 lastName,
//                 username,
//                 profilePic,
//                 dissimilarity,
//                 isFollowing
//               }
//             }
//           },
//         }
//       }
//     `
//   }
// });

// var styles = StyleSheet.create({
//   usecase: {
//     justifyContent: 'center',
//     alignItems: 'center',
//     flexDirection: 'column',
//     flex: 1,
//     width: null, height: null,
//     resizeMode: 'contain',
//   },
//   promoView: {
//     backgroundColor: 'transparent',

//   },
//   text: {
//     color: 'black',
//     textAlign: 'center'
//   },
//   promoHeader: {
//     fontSize: 42,
//     fontWeight: '700',
//     alignSelf: 'center'
//   },
//   promoDescription: {
//     fontSize: 22,
//     fontWeight: '300',
//     width: 220,
//     alignSelf: 'center',
//   },
//   promoText: {
//     fontSize: 14,
//     fontWeight: '400',
//     alignSelf: 'center',
//     lineHeight: 22,
//     width: 220,
//     color: 'black',
//   },
//   loading: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//     paddingVertical: 10
//   }
// });
