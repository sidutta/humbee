import Relay from 'react-relay';
import React, {Component} from 'react';
import {  ScrollView, Text, View, StyleSheet, Image, PanResponder } from 'react-native';

import ViewerQuery from '../../../queries/ViewerQuery';
import { createRenderer } from '../../../utils/RelayUtils';
import Router from '../../../routes/Router';

import TouchableItem from '../../Common/TouchableItem';
import colors from '../../../config/colors';

import SimpleGesture from 'react-native-simple-gesture';

class QuizResultScreen extends Component {

	constructor(props) {
		super(props);
		this.state={
			questionCards : []
		};
		this.loadRecommendations = this.loadRecommendations.bind(this);
		this.loadFollowerSuggestions = this.loadFollowerSuggestions.bind(this);
		this.loadFollowerSimilarity = this.loadFollowerSimilarity.bind(this);
	}

  componentWillMount() {
    this._panResponder = PanResponder.create({
      // Only respond to movements if the gesture is a swipe up
      onMoveShouldSetPanResponder: (e, gs) => {
        let sgs = new SimpleGesture(e,gs);
        if(sgs.isSwipeDown()) {
          this.props.onSwipeRight();
          return true;
        }
      }
    });
  }

	componentDidMount() {
		this.props.relay.setVariables({
			quizIndex: this.props.quizId,
			fetchResults: true
		});
	}

	loadRecommendations() {
		this.props.loadRecommendations(this.props.viewer.quizResult.index);
	}

	loadFollowerSuggestions() {
		this.props.loadFollowerSuggestions(this.props.viewer.quizResult.subquizScores);
	}

	loadFollowerSimilarity() {
		this.props.loadFollowerSimilarity(this.props.viewer.quizResult.subquizScores);
	}

	render() {
		const { height, width, isIntroQuiz } = this.props;
		let masterCharacterization;
		let description;
		let thumbnail;
		if(this.props.viewer.quizResult) {
			masterCharacterization = this.props.viewer.quizResult.masterCharacterization;
			description = this.props.viewer.quizResult.description;
			thumbnail = this.props.viewer.quizResult.thumbnail;
		}
		return(
			<View>
				{ this.props.quizIndex!=-1
					?
					<View style={{height : height-30, width: width}}>
						<View style={{flex : 1, borderRadius : 2,  padding : 10, margin: 20, backgroundColor: 'white'}}>
							<View style={{paddingBottom : 10}}>
								<Text style={[styles.text, styles.promoHeader]}>{masterCharacterization}</Text>
								<Text style={[styles.text, styles.promoDescription]}>{description}</Text>
							</View>
							<View style={{flex : 1, borderRadius : 2,  padding : 10}}>

								<Image
									style={[styles.usecase]}
									source={{uri: thumbnail}}
								>
								</Image>

							</View>
							<TouchableItem
								onPress={this.loadRecommendations}
								style={[{ height : 25, alignItems : 'center', justifyContent : 'center', backgroundColor : colors.humbeeGrey, borderRadius : 2, padding : 10, paddingVertical : 20, marginTop: 10, marginRight : 15, marginLeft : 15}]}>
									<Text style={{ fontFamily : 'helvetica', fontSize : 14, color : '#004d40', textAlign : 'center'}}>Interesting Stuff</Text>
							</TouchableItem>
							{
								(isIntroQuiz)
								?
								<View />
								:
								<TouchableItem
									onPress={this.loadFollowerSuggestions}
									style={[{height : 25, alignItems : 'center', justifyContent : 'center', backgroundColor : colors.humbeeGrey, borderRadius : 2, padding : 10, paddingVertical : 20, marginVertical: 10, marginRight : 15, marginLeft : 15}]}>
										<Text style={{ fontFamily : 'helvetica', fontSize : 14, color : '#004d40', textAlign : 'center'}}>Find new Peeps</Text>
								</TouchableItem>
							}
							{
								(isIntroQuiz)
								?
								<View />
								:
								<TouchableItem
									onPress={this.loadFollowerSimilarity}
									style={[{height : 25, alignItems : 'center', justifyContent : 'center', backgroundColor : colors.humbeeGrey, borderRadius : 2, padding : 10, paddingVertical : 20, marginBottom: 10, marginRight : 15, marginLeft : 15}]}>
										<Text style={{ fontFamily : 'helvetica', fontSize : 14, color : '#004d40', textAlign : 'center'}}>Follower Alignment</Text>
								</TouchableItem>
							}
						</View>
					</View>
					:
					<View style={{height : height-30, width: width}}>
					</View>
				}
			</View>
		);
	}
}

export default Relay.createContainer(QuizResultScreen, {
	queries : ViewerQuery,
	initialVariables: {
		quizIndex: '-1',
		fetchResults: false
	},
	fragments: {
		viewer: () => Relay.QL`
			fragment on Viewer {
				quizResult(quizIndex: $quizIndex) @include(if: $fetchResults) {
					index,
					masterCharacterization,
					thumbnail,
					description,
					subquizScores,
					characterizationPatternWithId
				}
			}
		`
	}
});

var styles = StyleSheet.create({
	usecase: {
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column',
		flex: 1,
		width: null, height: null,
		resizeMode: 'contain',
	},
	promoView: {
		backgroundColor: 'transparent',

	},
	text: {
		color: 'black',
		textAlign: 'center'
	},
	promoHeader: {
		fontSize: 16,
		fontWeight: '700',
		alignSelf: 'center'
	},
	promoDescription: {
		fontSize: 14,
		fontWeight: '300',
		width: 220,
		alignSelf: 'center',
	},
	promoText: {
		fontSize: 12,
		fontWeight: '400',
		alignSelf: 'center',
		lineHeight: 22,
		width: 220,
		color: 'black',

	},
});
