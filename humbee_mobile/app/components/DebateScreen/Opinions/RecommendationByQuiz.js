import Relay from 'react-relay';
import React, {Component} from 'react';
import {  ScrollView, Text, View, StyleSheet, Image, Dimensions } from 'react-native';

import ViewerQuery from '../../../queries/ViewerQuery';
import { createRenderer } from '../../../utils/RelayUtils';
import Router from '../../../routes/Router';

import Icon from 'react-native-vector-icons/Ionicons';

import TouchableItem from '../../Common/TouchableItem';
import colors from '../../../config/colors';
import NavBar from '../../NavBar';

import * as getLayout from './getLayout';

class RecommendationByQuiz extends Component {

	constructor(props) {
		super(props);
		this.state={
			questionCards : [],
			content: []
		};
		this.arrangeContent = this.arrangeContent.bind(this);
		this.arrangeSingleTypeofContent = this.arrangeSingleTypeofContent.bind(this);
		this.arrangeTwoTypesofContent = this.arrangeTwoTypesofContent.bind(this);
		this.updateLayout = this.updateLayout.bind(this);
    this.content = [];
	}

	componentWillMount() {
		this.props.relay.setVariables({
			masterCharacterizationId: this.props.masterCharacterizationId,
			fetchResults: true
		});
	}

  componentWillReceiveProps(next) {
    const { width, navigator } = this.props;

    const windowHeight = (Dimensions.get('window').height);
    const height = windowHeight - windowHeight/10;

    if(next.viewer.recommendationByQuiz && this.content.length==0) {
      this.arrangeContent(height, width, next);
    }
  }

	updateLayout(height, width, layoutOption, obj1, objType1, obj2, objType2, obj3, objType3) {
		switch(layoutOption) {
		case 0:
			this.content.push(getLayout.GetOnePerRow(this, height, width, obj1, objType1));
			break;
		case 1:
			this.content.push(getLayout.GetTwoPerRow(this, height, width, obj1, objType1, obj2, objType2));
			break;
		case 2:
			this.content.push(getLayout.GetThreePerRowFancy(this, height, width, obj1, objType1, obj2, objType2, obj3, objType3));
			break;
		case 3:
			this.content.push(getLayout.GetThreePerRow(this, height, width, obj1, objType1, obj2, objType2, obj3, objType3));
			break;
		}
	}

	arrangeSingleTypeofContent(height, width, index, contentList, listLen, contentTypes) {
		let range = 2;
		if(width>350) range = 4;
		while(index < listLen) {
			let randomNum = (Math.floor(Math.random() * 100))%range;
			if((randomNum == 2 || randomNum == 3) && index + 3 > listLen) {
				randomNum = 1;
			}
			if(randomNum == 1 && index + 2 > listLen) {
				randomNum = 0;
			}
			if(randomNum == 0 && index + 1 > listLen) {
				return;
			}
			switch(randomNum) {
			case 0:
				this.updateLayout(height, width, 0, contentList[index].node, contentTypes[index]);
				index = index + 1;
				break;
			case 1:
				this.updateLayout(height, width, 1, contentList[index].node, contentTypes[index], contentList[index+1].node, contentTypes[index+1]);
				index = index + 2;
				break;
			case 2:
				this.updateLayout(height, width, 2, contentList[index].node, contentTypes[index], contentList[index+1].node, contentTypes[index+1], contentList[index+2].node, contentTypes[index+2]);
				index = index + 3;
				break;
			case 3:
				this.updateLayout(height, width, 3, contentList[index].node, contentTypes[index], contentList[index+1].node, contentTypes[index+1], contentList[index+2].node, contentTypes[index+2]);
				index = index + 3;
				break;
			}
		}
		return;
	}

	arrangeTwoTypesofContent(height, width, articleIndex, videoIndex, articleList, numArticles, videoList, numVideos) {

		let contentList = [];
		let contentTypes = [];

		while(articleIndex < numArticles && videoIndex < numVideos) {
			let randomNum = (Math.floor(Math.random() * 100))%2;
			if(randomNum==0) {
				contentList.push(articleList[articleIndex]);
				articleIndex = articleIndex + 1;
				contentTypes.push('a');
			}
			else if(randomNum==1) {
				contentList.push(videoList[videoIndex]);
				videoIndex = videoIndex + 1;
				contentTypes.push('v');
			}
		}

		if(articleIndex < numArticles) {
			while(articleIndex < numArticles) {
				contentList.push(articleList[articleIndex]);
				articleIndex = articleIndex + 1;
				contentTypes.push('a');
			}
		}
		else if(videoIndex < numVideos) {
			while(videoIndex < numVideos) {
				contentList.push(videoList[videoIndex]);
				videoIndex = videoIndex + 1;
				contentTypes.push('v');
			}
		}

		this.arrangeSingleTypeofContent(height, width, 0, contentList, numArticles + numVideos, contentTypes);

		return;
	}

	arrangeContent(height, width, next) {
		let numArticles = next.viewer.recommendationByQuiz.numArticles;
		let numVideos = next.viewer.recommendationByQuiz.numVideos;

		if(numArticles + numVideos == 0) {
			this.content.push(
				<View style={{width : width, height : height, backgroundColor: colors.humbeeGrey, paddingHorizontal: 5, paddingTop: 2, justifyContent: 'center'}}>
					<Icon name='ios-rainy' size={55} style={{color : 'blue', textAlign:'center'}}/>
					<Text style={{fontFamily: 'Audrey-Normal', color: 'black', fontSize: 20, fontWeight: '700', textAlign: 'center'}}>
						Oops!
					</Text>
					<Text style={{fontFamily: 'Audrey-Normal', color: 'black', fontSize: 20, fontWeight: '700', textAlign: 'center'}}>
						Sorry, we have nothing to recommend here. Please check the other quizzes and debates.
					</Text>
				</View>
			);
			return;
		}

		let articles = next.viewer.recommendationByQuiz.articleList.edges;
		let videos = next.viewer.recommendationByQuiz.videoList.edges;

		let curArticle = 0;
		let curVideo = 0;

		this.content = [];

		this.arrangeTwoTypesofContent(height, width, curArticle, curVideo, articles, numArticles, videos, numVideos)
	}

	render() {
		const { width, navigator } = this.props;

		const windowHeight = (Dimensions.get('window').height);
		const height = windowHeight - windowHeight/10;

		// this.content = [];

		// if(this.props.viewer.recommendationByQuiz) {
		// 	this.arrangeContent(height, width);
		// }

		return(
			<View style={{flex : 1}}>
				<NavBar navigator={navigator} title="HumBee Recommendations" opacity={1} />
				<View style={{height : height, marginTop: 0, padding : 0}}>
					<ScrollView style={{flex : 1, backgroundColor: 'white', paddingBottom: 1}}>
						{this.content}
						{ this.props.enabled && <View style={{ height: 70,flex:1, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
							<View elevation={5} style={{borderRadius : 2, padding : 10 , backgroundColor : colors.humbeeGreen, alignItems:'center', justifyContent:'center'}}>
								<TouchableItem borderLess={true} onPress={this.props.goToDebate}>
									<Text style={{fontFamily : 'helvetica', fontSize : 20, color : 'white'}}>Go to debate</Text>
								</TouchableItem>
							</View>
						</View> }
					</ScrollView>
				</View>
			</View>
		);
	}
}

export default createRenderer(RecommendationByQuiz, {
	queries : ViewerQuery,
	queriesParams: ({ masterCharacterizationId }) => ({
		masterCharacterizationId: masterCharacterizationId ? masterCharacterizationId : null
	}),
	initialVariables: {
		masterCharacterizationId: '-1',
		fetchResults: false
	},
	fragments: {
		viewer: () => Relay.QL`
			fragment on Viewer {
				recommendationByQuiz(masterCharacterizationId: $masterCharacterizationId) @include(if: $fetchResults) {
					numArticles,
					numVideos,
					articleList(first: 100) {
						edges{
							node {
								title,
								thumbnail,
								url,
								description,
								id,
                index,
								liked
							}
						}
					},
					videoList(first: 100) {
						edges{
							node {
                id,
                index,
                liked,
                numberOfLikes,
                numberOfComments,
								title,
								thumbnail,
								videoId,
								description
							}
						}
					},
				}
			}
		`
	}
});

var styles = StyleSheet.create({
	usecase: {
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column',
		flex: 1,
		width: null, height: null,
		resizeMode: 'contain',
	},
	promoView: {
		backgroundColor: 'transparent',

	},
	text: {
		color: 'black',
		textAlign: 'center'
	},
	promoHeader: {
		fontSize: 42,
		fontWeight: '700',
		alignSelf: 'center'
	},
	promoDescription: {
		fontSize: 22,
		fontWeight: '300',
		width: 220,
		alignSelf: 'center',
	},
	promoText: {
		fontSize: 14,
		fontWeight: '400',
		alignSelf: 'center',
		lineHeight: 22,
		width: 220,
		color: 'black',
	},
});
