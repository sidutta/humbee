import Relay from 'react-relay';
import React, {Component} from 'react';
import {  ScrollView, Text, View, StyleSheet, Image } from 'react-native';
import RelayStore from '../../../utils/RelayStore';
import CreateArticleLikeMutation from '../../../mutations/CreateArticleLikeMutation';
import CreateVideoLikeMutation from '../../../mutations/CreateVideoLikeMutation';

import Icon from 'react-native-vector-icons/Ionicons';

import TouchableItem from '../../Common/TouchableItem';
import colors from '../../../config/colors';
import Router from '../../../routes/Router';

import ArticlePage from '../../Home/article/ArticlePage';
import VideoPlayer from '../../Home/video/VideoPlayer';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

function like({id, liked, numberOfLikes}){
  RelayStore.commitUpdate(
    new CreateArticleLikeMutation({
      liked : liked,
      articleId : id,
      numberOfLikes : numberOfLikes
    })
  );
}

function likeModal({context, id, index}) {
  if (__DEV__ == false) {
    let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
    tracker.trackScreenView('ArticleLikeModal-' + index);
  }
  context.props.navigator.push(
    Router.getRoute('articleLikeModal',{
      id : id,
      type : 'ArticleLikeModal'
    })
  );
}

function videoLike({id, liked, numberOfLikes}) {
  RelayStore.commitUpdate(
    new CreateVideoLikeMutation({
      liked : liked,
      videoId : id,
      numberOfLikes : numberOfLikes
    })
  );
}

function engageModal({context, id, index}) {
  if (__DEV__ == false) {
    let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
    tracker.trackScreenView('ArticleEngageModal-' + index);
  }
  context.props.navigator.push(
    Router.getRoute('articleEngageModal',{
      id : id,
      type : 'ArticleEngageModal'
    })
  );
}

function videoEngageModal({context, id, index}) {
  if (__DEV__ == false) {
    let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
    tracker.trackScreenView('VideoEngageModal-' + index);
  }
  context.props.navigator.push(
    Router.getRoute('videoEngageModal',{
      id : id,
      type : 'VideoEngageModal'
    })
  );
}

function openArticle(context, id, liked, index) {
  if (__DEV__ == false) {
    let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
    tracker.trackScreenView('ArticlePage-' + index);
  }
  context.props.navigator.push(
    Router.getRoute('articlePage',{
      id : id,
      like : like,
      engageModal : () => engageModal({context, id, index})
    })
  );
}

function playVideo(context, obj) {
  if (__DEV__ == false) {
    let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
    tracker.trackScreenView('VideoPlayer-' + obj.index);
  }
  let id = obj.id;
  let index = obj.index;
  context.props.navigator.push(
    Router.getRoute('videoPlayer', {
      videoId : obj.videoId,
      id : obj.id,
      title : obj.title,
      description : obj.description,
      liked : obj.liked,
      engageModal : () => videoEngageModal({context, id, index}),
      numberOfLikes : obj.numberOfLikes,
      like : videoLike
    })
  );
}

function openContent(contentType, obj) {
  if(contentType=='a') {
    openArticle(this, obj.id, obj.liked, obj.index)
  }
  else if(contentType=='v') {
    playVideo(this, obj)
  }
}

export function GetOnePerRow(context, height, width, obj, contentType) {
  let thumbnail = obj.thumbnail;
  let title = obj.title;

  let icon;

  if(contentType=='a')
    icon = <Icon name='ios-paper-outline' size={25} style={{color : '#000', textAlign:'right', margin: 10}}/>
  else if(contentType=='v')
    icon = <Icon name='ios-play' size={25} style={{color : '#000', textAlign:'right', margin: 10}}/>

  return (
    <View style={{height : width/2 + width/6, flexDirection: 'row', flexDirection: 'row'}}>
      <View style={{height : width/2 + width/6, flexDirection: 'row', flexDirection: 'column', borderWidth: 1, borderColor: 'white'}}>
        <TouchableItem onPress={openContent.bind(context, contentType, obj)} style={{width : width - 2, height : width/2 + width/6, backgroundColor: 'yellow'}}>
          <Image
            style={{flex: 1, width: null, height: null}}
            source={{uri: thumbnail}}
          >
            <View style={{flex: 1, backgroundColor: 'transparent'}}>
              <Text style={{padding :10, backgroundColor: 'rgba(52,52,52,0.8)', fontFamily: 'Audrey-Normal', color: 'white', fontSize: 20, fontWeight: '700', textAlignVertical: 'bottom'}}>
                {title}
              </Text>
            </View>
            {icon}
          </Image>
        </TouchableItem>
      </View>
    </View>
  );
}

export function GetTwoPerRow(context, height, width, obj1, contentType1, obj2, contentType2) {
  let thumbnail1 = obj1.thumbnail;
  let title1 = obj1.title;
  let thumbnail2 = obj2.thumbnail;
  let title2 = obj2.title;

  let icon1;

  if(contentType1=='a')
    icon1 = <Icon name='ios-paper-outline' size={25} style={{color : '#000'}}/>
  else if(contentType1=='v')
    icon1 = <Icon name='ios-play' size={25} style={{color : '#000', textAlign:'right'}}/>

  let icon2;

  if(contentType2=='a')
    icon2 = <Icon name='ios-paper-outline' size={25} style={{color : '#000'}}/>
  else if(contentType2=='v')
    icon2 = <Icon name='ios-play' size={25} style={{color : '#000', textAlign:'right'}}/>


  return (
    <View style={{height : width/2 + width/6, flexDirection: 'row', flexDirection: 'row'}}>
      <View style={{height : width/2 + width/6, flexDirection: 'row', flexDirection: 'column', borderWidth: 1, borderRightWidth: 0.5, borderColor: 'white'}}>
        <TouchableItem onPress={openContent.bind(context, contentType1, obj1)} style={{width : width/2 - 1.5, height : width/2, backgroundColor: 'red'}}>
          <Image
            style={{flex: 1, width: null, height: null}}
            source={{uri: thumbnail1}}
          >
            <View style={{alignItems:'flex-end', paddingTop: width/2  - 30, paddingRight: 8}}>
              {icon1}
            </View>
          </Image>
        </TouchableItem>
        <View style={{width : width/2 - 1.5, height : width/6, backgroundColor: 'rgba(52,52,52,0.8)', paddingHorizontal: 5, paddingTop: 2}}>
          <Text style={{fontFamily: 'Audrey-Normal', color: 'white', fontSize: 14, fontWeight: '700', textAlignVertical: 'bottom'}}>
            {title1}
          </Text>
        </View>
      </View>
      <View style={{height : width/2 + width/6, flexDirection: 'row', flexDirection: 'column', borderWidth: 1, borderLeftWidth: 0.5, borderColor: 'white'}}>
        <TouchableItem onPress={openContent.bind(context, contentType2, obj2)} style={{width : width/2 - 1.5, height : width/2, backgroundColor: 'blue'}}>
          <Image
            style={{flex: 1, width: null, height: null}}
            source={{uri: thumbnail2}}
          >

            <View style={{alignItems:'flex-end', paddingTop: width/2  - 30, paddingRight: 8}}>
              {icon2}
            </View>

          </Image>
        </TouchableItem>
        <View style={{width : width/2 - 1.5, height : width/6, backgroundColor: 'rgba(52,52,52,0.8)', paddingHorizontal: 5, paddingTop: 2}}>
          <Text style={{fontFamily: 'Audrey-Normal', color: 'white', fontSize: 14, fontWeight: '700', textAlignVertical: 'bottom'}}>
            {title2}
          </Text>
        </View>
      </View>
    </View>
  );
}

export function GetThreePerRowFancy(context, height, width, obj1, contentType1, obj2, contentType2, obj3, contentType3) {
  let thumbnail1 = obj1.thumbnail;
  let title1 = obj1.title;
  let thumbnail2 = obj2.thumbnail;
  let title2 = obj2.title;
  let thumbnail3 = obj3.thumbnail;
  let title3 = obj3.title;

  let icon1;

  if(contentType1=='a')
    icon1 = <Icon name='ios-paper-outline' size={25} style={{color : '#000', textAlign:'right', margin: 5}}/>
  else if(contentType1=='v')
    icon1 = <Icon name='ios-play' size={25} style={{color : '#000', textAlign:'right', margin: 5}}/>

  let icon2;

  if(contentType2=='a')
    icon2 = <Icon name='ios-paper-outline' size={25} style={{color : '#000', textAlign:'right', margin: 5}}/>
  else if(contentType2=='v')
    icon2 = <Icon name='ios-play' size={25} style={{color : '#000', textAlign:'right', margin: 5}}/>

  let icon3;

  if(contentType2=='a')
    icon3 = <Icon name='ios-paper-outline' size={25} style={{color : '#000', textAlign:'right', margin: 5}}/>
  else if(contentType2=='v')
    icon3 = <Icon name='ios-play' size={25} style={{color : '#000', textAlign:'right', margin: 5}}/>

  return (
    <View style={{height : width, flexDirection: 'row', flexDirection: 'row'}}>
      <View style={{height : width, flexDirection: 'column', flexDirection: 'column', borderWidth: 1, borderRightWidth: 0.5, borderColor: 'white'}}>
        <TouchableItem onPress={openContent.bind(context, contentType1, obj1)} style={{width : width/2 - 1.5, height : width/2 - 0.5, borderBottomWidth: 1, backgroundColor: 'red'}}>
          <Image
            style={{flex: 1, width: null, height: null}}
            source={{uri: thumbnail1}}
          >

            <View style={{flex: 1, backgroundColor: 'transparent'}}>
              <Text style={{padding: 3, backgroundColor: 'rgba(52,52,52,0.8)', fontFamily: 'Audrey-Normal', color: 'white', fontSize: 14, fontWeight: '700', textAlignVertical: 'bottom'}}>
                {title1}
              </Text>
            </View>
            {icon1}
          </Image>
        </TouchableItem>
        <TouchableItem onPress={openContent.bind(context, contentType2, obj2)} style={{width : width/2 - 1.5, height : width/2 - 0.5, borderTopWidth: 1, backgroundColor: 'red'}}>
          <Image
            style={{flex: 1, width: null, height: null}}
            source={{uri: thumbnail2}}
          >
            <View style={{flex: 1, backgroundColor: 'transparent'}}>
              <Text style={{padding: 3, backgroundColor: 'rgba(52,52,52,0.8)', fontFamily: 'Audrey-Normal', color: 'white', fontSize: 14, fontWeight: '700', textAlignVertical: 'bottom'}}>
                {title2}
              </Text>
            </View>
            {icon2}
          </Image>
        </TouchableItem>
      </View>
      <View style={{height : width, flexDirection: 'row', flexDirection: 'column', borderWidth: 1, borderLeftWidth: 0.5, borderColor: 'white'}}>
        <TouchableItem onPress={openContent.bind(context, contentType3, obj3)} style={{width : width/2 - 1.5, height : width, backgroundColor: 'blue'}}>
          <Image
            style={{flex: 1, width: null, height: null}}
            source={{uri: thumbnail3}}
          >
            <View style={{flex: 1, backgroundColor: 'transparent'}}>
              <Text style={{padding: 3, backgroundColor: 'rgba(52,52,52,0.8)', fontFamily: 'Audrey-Normal', color: 'white', fontSize: 18, fontWeight: '700', textAlignVertical: 'bottom'}}>
                {title3}
              </Text>
            </View>
            {icon3}
          </Image>
        </TouchableItem>
      </View>
    </View>
  );
}

export function GetThreePerRow(context, height, width, obj1, contentType1, obj2, contentType2, obj3, contentType3) {
  let thumbnail1 = obj1.thumbnail;
  let title1 = obj1.title;
  let thumbnail2 = obj2.thumbnail;
  let title2 = obj2.title;
  let thumbnail3 = obj3.thumbnail;
  let title3 = obj3.title;

  let icon1;

  if(contentType1=='a')
    icon1 = <Icon name='ios-paper-outline' size={25} style={{color : '#000', textAlign:'right', margin: 5}}/>
  else if(contentType1=='v')
    icon1 = <Icon name='ios-play' size={25} style={{color : '#000', textAlign:'right', margin: 5}}/>

  let icon2;

  if(contentType2=='a')
    icon2 = <Icon name='ios-paper-outline' size={25} style={{color : '#000', textAlign:'right', margin: 5}}/>
  else if(contentType2=='v')
    icon2 = <Icon name='ios-play' size={25} style={{color : '#000', textAlign:'right', margin: 5}}/>

  let icon3;

  if(contentType2=='a')
    icon3 = <Icon name='ios-paper-outline' size={25} style={{color : '#000', textAlign:'right', margin: 5}}/>
  else if(contentType2=='v')
    icon3 = <Icon name='ios-play' size={25} style={{color : '#000', textAlign:'right', margin: 5}}/>

  return (
    <View style={{height : width/2 , flexDirection: 'row', flexDirection: 'row'}}>
      <View style={{height : width/2, flexDirection: 'row', flexDirection: 'column', borderWidth: 1, borderRightWidth: 0.5, borderColor: 'white'}}>
        <TouchableItem onPress={openContent.bind(context, contentType1, obj1)} style={{width : width/3 - 1.5, height : width/2, backgroundColor: 'red'}}>
          <Image
            style={{flex: 1, width: null, height: null}}
            source={{uri: thumbnail1}}
          >
            <View style={{flex: 1, backgroundColor: 'transparent'}}>
              <Text style={{padding:3, backgroundColor: 'rgba(52,52,52,0.8)', fontFamily: 'Audrey-Normal', color: 'white', fontSize: 14, fontWeight: '700', textAlignVertical: 'bottom'}}>
                {title1}
              </Text>
            </View>
            {icon1}
          </Image>
        </TouchableItem>
      </View>
      <View style={{height : width/2, flexDirection: 'row', flexDirection: 'column', borderWidth: 1, borderLeftWidth: 0.5, borderRightWidth: 0.5, borderColor: 'white'}}>
        <TouchableItem onPress={openContent.bind(context, contentType2, obj2)} style={{width : width/3 - 1, height : width/2, backgroundColor: 'red'}}>
          <Image
            style={{flex: 1, width: null, height: null}}
            source={{uri: thumbnail2}}
          >
            <View style={{flex: 1, backgroundColor: 'transparent'}}>
              <Text style={{padding:3, backgroundColor: 'rgba(52,52,52,0.8)', fontFamily: 'Audrey-Normal', color: 'white', fontSize: 14, fontWeight: '700', textAlignVertical: 'bottom'}}>
                {title2}
              </Text>
            </View>
            {icon2}
          </Image>
        </TouchableItem>
      </View>
      <View style={{height : width/2, flexDirection: 'row', flexDirection: 'column', borderWidth: 1, borderRightWidth: 0.5, borderColor: 'white'}}>
        <TouchableItem onPress={openContent.bind(context, contentType3, obj3)} style={{width : width/3 - 1.5, height : width/2, backgroundColor: 'red'}}>
          <Image
            style={{flex: 1, width: null, height: null}}
            source={{uri: thumbnail3}}
          >
            <View style={{flex: 1, backgroundColor: 'transparent'}}>
              <Text style={{padding:3,  backgroundColor: 'rgba(52,52,52,0.8)',fontFamily: 'Audrey-Normal', color: 'white', fontSize: 14, fontWeight: '700', textAlignVertical: 'bottom'}}>
                {title3}
              </Text>
            </View>
            {icon3}
          </Image>
        </TouchableItem>
      </View>
    </View>
  );
}
