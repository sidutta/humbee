import Relay from 'react-relay';
import React, {Component} from 'react';
import { View, Text, ActivityIndicator, PanResponder} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import TouchableItem from '../../Common/TouchableItem';
import CreateResponseMutation from '../../../mutations/CreateResponseMutation';
import QuestionCard from './QuestionCard';

import NodeQuery from '../../../queries/NodeQuery';
import { createRenderer } from '../../../utils/RelayUtils';
import Router from '../../../routes/Router';
import RelayStore from '../../../utils/RelayStore';

import colors from '../../../config/colors';
import SimpleGesture from 'react-native-simple-gesture';

class QuestionScreen extends Component {

	constructor(props) {
		super(props);
		this.markAsUnsure = this.markAsUnsure.bind(this);
		this.state = {
			isLoading : false,
      editPressed : false
		}
		this.setIsLoading = this.setIsLoading.bind(this);
		this.unsetIsLoading = this.unsetIsLoading.bind(this);
    this.editResponse = this.editResponse.bind(this);
    this.unsetEditPressed = this.unsetEditPressed.bind(this);
	}

	markAsUnsure(unsureOptionId) {
		const {nextQuestion, questionNumber, id, totalNumberOfQuestions} = this.props;
    let { editPressed } = this.state;
		this.setIsLoading();
    let edit = false;
    if(this.state.editPressed) {
      edit = true;
      this.unsetEditPressed();
    }
		RelayStore.commitUpdate(
			new CreateResponseMutation({
				optionId : unsureOptionId,
				questionId : id,
        edit : edit
			}),{
				onSuccess : () => {
					this.unsetIsLoading();
					setTimeout(() => {
						nextQuestion(questionNumber, totalNumberOfQuestions);
					}, 1000);
				},
				onFailure : () => {
					this.unsetIsLoading();
					alert('Network error: Couldn\'t register response')
				}
			}
		);
	}

	setIsLoading() {
		this.setState({isLoading:true});
	}

	unsetIsLoading() {
		this.setState({isLoading:false});
	}

  editResponse() {
    this.setState({editPressed:true});
  }

  unsetEditPressed() {
    this.setState({editPressed:false});
  }

  componentWillMount() {
    this._panResponder = PanResponder.create({
      // Only respond to movements if the gesture is a swipe up
      onMoveShouldSetPanResponder: (e, gs) => {
        let sgs = new SimpleGesture(e,gs);
        if(sgs.isSwipeUp()) {
          this.props.onSwipeLeft();
          return true;
        }
        else if(sgs.isSwipeDown()) {
          this.props.onSwipeRight();
          return true;
        }
      }
    });
  }

	render() {
		const { nextQuestion, scrollToNext, questionNumber, totalNumberOfQuestions, height, width, quizCompleted } = this.props;
		const questionId = this.props.viewer.id;
		const numberOfOptions = this.props.viewer.options.length - 1; // -1 to discount the unsure option
		let margin = (height - numberOfOptions * 80)/2 - 10;
		let answered = this.props.viewer.answered;
    let { editPressed } = this.state;
    if(editPressed && !quizCompleted) {
      answered = false;
    }
		let callbackOnNext = ( quizCompleted ? () => nextQuestion(questionNumber, totalNumberOfQuestions) : () => nextQuestion(questionNumber, totalNumberOfQuestions) );

		let bottomView;
		const isLoading = this.state.isLoading;
		const setIsLoading = this.setIsLoading;
		const unsetIsLoading = this.unsetIsLoading;
    const unsetEditPressed = this.unsetEditPressed;

		const options = this.props.viewer.options;
		let unsureOptionId;
		for(let i=0; i<options.length; i++) {
			if(options[i].text=="unsure"||options[i].text=="Unsure") {
				unsureOptionId = options[i].id;
				this.markAsUnsureArg = function(unsureOptionId) {this.markAsUnsure(unsureOptionId)}.bind(this, unsureOptionId);
			}
		}

		if(isLoading) {
			bottomView = <ActivityIndicator
											animating={true}
											color={colors.humbeeGreen}
											style={{
												alignItems: 'center',
												justifyContent: 'center',
												padding: 40,
												height: 30}}
											size="large"
										/>
		}
		else {
			bottomView = <TouchableItem
											onPress={this.markAsUnsureArg}
											style={[{height : 30, alignItems : 'center', justifyContent : 'center', backgroundColor : colors.humbeeGrey, borderRadius : 2, padding : 10, paddingVertical : 10, marginBottom: 30}]}
										>
											<Text style={{ fontFamily : 'helvetica', fontSize : 14, color : '#004d40', textAlign : 'center'}}>
												Unsure
											</Text>
										</TouchableItem>
		}

		return(
			<View style={{height : height, width: width, padding : 10, justifyContent: 'space-between'}}>
				<View>
					<View style={{height : 15, padding : 4, borderRadius : 2, flex: 1, flexDirection: 'row', backgroundColor : 'white'}}>
						<View style={{flex : questionNumber, backgroundColor : '#004d40'}}/>
						<View style={{flex : (totalNumberOfQuestions-questionNumber)}}/>
					</View>
					<View style={{ padding : 2}}>
						<Text style={{textAlign:'center', fontSize : 14, fontFamily : 'helvetica', fontWeight : 'bold', color : colors.humbeeGrey}}>{questionNumber + ' out of ' + totalNumberOfQuestions}</Text>
					</View>
				</View>
				<QuestionCard viewer={this.props.viewer} answered={answered} editPressed={editPressed} setIsLoading={setIsLoading} unsetIsLoading={unsetIsLoading} unsetEditPressed={unsetEditPressed} questionNumber={questionNumber} nextQuestion={nextQuestion} questionId={questionId} totalNumberOfQuestions={totalNumberOfQuestions} />


				<View style={{alignItems : 'center', justifyContent : 'center'}}>
          {
            answered && !quizCompleted && !isLoading
            ?
            <TouchableItem
              onPress={ this.editResponse }
              style={[{height : 30, alignItems : 'center', justifyContent : 'center', backgroundColor : colors.humbeeGrey, borderRadius : 2, padding : 10, paddingVertical : 10, marginBottom: 20}]}
            >
              <Text style={{ fontFamily : 'helvetica', fontSize : 14, color : '#004d40', textAlign : 'center'}}>
                Edit Response
              </Text>
            </TouchableItem>
            :
            <View/>
          }
					{
						answered && !isLoading
						?
						<TouchableItem
							onPress={ callbackOnNext }
							style={[{height : 30, alignItems : 'center', justifyContent : 'center', backgroundColor : colors.humbeeGrey, borderRadius : 2, padding : 10, paddingVertical : 10, marginBottom: 30}]}
						>
							<Text style={{ fontFamily : 'helvetica', fontSize : 14, color : '#004d40', textAlign : 'center'}}>
								Next
							</Text>
						</TouchableItem>
						:
						bottomView
					}
				</View>
			</View>
		);
	}
}

const loadingView = <View></View>;

export default createRenderer(QuestionScreen, {
	queries : NodeQuery,
	queriesParams: ({ id }) => ({
		id: id ? id : null,
	}),
	renderLoading : () => loadingView,
	fragments: {
		viewer: () => Relay.QL`
			fragment on Question {
				id,
				text,
				numberOfOptions,
				answered,
				options {
					id,
					text,
					votes,
					voted
				}
			}
		`
	}
});
