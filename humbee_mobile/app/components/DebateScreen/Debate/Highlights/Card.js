import React, {Component} from 'react';
import {View , Text, StyleSheet, Dimensions, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import TouchableItem from '../../../Common/TouchableItem';

import colors from '../../../../config/colors';

export default class Card extends Component {
	render(){
		const {
			name,
			comment,
			source,
			pic
		} = this.props;
		return(
			<View style={styles.container}>
				<View style={{flex : 9, flexDirection : 'row'}}>
					<View style={{flex : 1}}/>
					<Image source={{uri : pic}} style={{flex : 2, borderRadius : 80}}/>
					<View style={{flex : 1, flexDirection : 'row'}}>
						<View style={{flex : 2}}/>
						<View style={{flex : 1}}>
							<TouchableItem style={{flex : 1}} borderLess={true}>
								<Icon name='md-share-alt' size={30} />
							</TouchableItem>
							<View style={{flex : 4}}/>
						</View>
					</View>
				</View>
				<Text style={{fontSize : 24, fontFamily : 'helvetica', flex : 8}}>{comment}</Text>
				<Text style={{fontSize : 20, fontFamily : 'helvetica', flex : 2, textAlign : 'right', color : 'black'}}>{'- ' + name}</Text>
				<Text style={{flex : 1, fontFamily : 'helvetica', fontSize: 10 , fontStyle : 'italic'}}>{'Courtesy : ' + source}</Text>
			</View>
		);
	}
}

let styles = StyleSheet.create({
	container: {
		margin : 5,
		borderRadius : 10,
		padding : 10,
		flex: 1,
		backgroundColor: 'white',
	},
});
