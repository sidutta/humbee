import React, {Component} from 'react';
import {View , Text, StyleSheet,ScrollView , Dimensions} from 'react-native';

import colors from '../../../../config/colors';
import majorOpinions from '../../../../data/majorOpinions';
import Card from './Card';
import Carousel from 'react-native-carousel';

export default class Highlights extends Component {
	constructor(props){
		super(props);
		this.state={
			delay : 100
		};
	}
	componentDidMount(){
		this.setState({delay : 5000});
	}
	render(){

		const cards = majorOpinions.map((opinion, index) =>
			<Card
				{...opinion}
				key={index}/>
		);

		return(

			<View style={{ height : 400}}>
				<Carousel width={Dimensions.get('window').width} delay={this.state.delay} indicatorOffset={-17} >
					{cards}
				</Carousel>
			</View>

		);
	}
}
