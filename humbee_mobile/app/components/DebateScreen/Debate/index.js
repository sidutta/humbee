import React, {Component} from 'react';
import Relay from 'react-relay';
import { Alert, RefreshControl, Modal, Text, View, Picker, TouchableOpacity, ListView, StyleSheet, Keyboard, ActivityIndicator, findNodeHandle, ScrollView, TextInput } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import TouchableItem from '../../Common/TouchableItem';

import NodeQuery from '../../../queries/NodeQuery';

import colors from '../../../config/colors';

import ReasonCard from './ReasonCard';
// import Fabric from 'react-native-fabric';
// import Analytics from 'react-native-firebase-analytics';
import TagScreen from '../TagScreen';
import CreateReasonMutation from '../../../mutations/CreateReasonMutation';
import DeleteReasonMutation from '../../../mutations/DeleteReasonMutation';
import Dimensions from 'Dimensions';
import RelayStore from '../../../utils/RelayStore';
import { createRenderer } from '../../../utils/RelayUtils';

const ds = new ListView.DataSource({
	rowHasChanged: (row1, row2) => row1.id !== row2.id
});

const dsTags = new ListView.DataSource({
	rowHasChanged : (row1, row2) => row1 !== row2
});

class Debate extends Component {

	constructor(props){
		super(props);
		this.state = {
			pageSize : 10,
			isLoading : false,
			refreshing : false,
			category : 'all',
			currentTag : '',
			dataSource : ds,
			dataSourceTags : dsTags,
			modalVisible : false,
			reasonModalVisible: false,
      optionModalVisible : false,
			reason : '',
			fab : true,
			agree: 'x',
			beAnonymous: false,
			tags : [],
      previousReasonId : -1
		};
    this.postMode = "new";
    this.modifyDebateId = "";
		this.onEndReached = this.onEndReached.bind(this);
		this.onRefresh = this.onRefresh.bind(this);
		this.onAllPress = this.onAllPress.bind(this);
		this.onAgreePress = this.onAgreePress.bind(this);
		this.onDisagreePress = this.onDisagreePress.bind(this);
		this.onTagsPress = this.onTagsPress.bind(this);
		this.onTagPress = this.onTagPress.bind(this);
		this._renderRow = this._renderRow.bind(this);
		this.setModalVisible = this.setModalVisible.bind(this);
		this.createReason = this.createReason.bind(this);
		this.onChangeTags = this.onChangeTags.bind(this);
		this.inputFocused = this.inputFocused.bind(this);
    this.onOptionsPress = this.onOptionsPress.bind(this);
    this.deleteReason = this.deleteReason.bind(this);
    this.createNewReason = this.createNewReason.bind(this);
	}

	componentWillReceiveProps(next) {
		// alert('a')
		if(next && next.viewer.reasons) {
			this.setState({
				dataSource: ds.cloneWithRows([ ...next.viewer.reasons.edges ])
			});
		}
		if(next && next.viewer.tags) {
			this.setState({
				dataSourceTags: dsTags.cloneWithRows([ ...next.viewer.tags ])
			});
		}
	}

	componentWillMount() {
		this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
		this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
		// Analytics.setUserId(global.userId);
		// Analytics.logEvent("view_item", {
		// 	'item_id': this.props.viewer.index.toString(),
		// 	'item_location_id': 'DebateScreenDs',
		// 	'content_type': 'debate'
			// 'screen': 'DebateScreenOp-' + this.props.viewer.lastDebateId.toString(),
			// 'user-id': global.userId
		// })
		// Analytics.logEvent("screen_launch", {
		//   'screen': 'DebateScreenDs-' + this.props.viewer.index.toString(),
		//   'user-id': global.userId
		// })
	}

	// componentDidMount() {
		// console.log(this.props.viewer)
		// var { Answers } = Fabric;
		// Answers.logContentView('DebateScreen Discuss', 'DebateScreen', 'ds-ds-' + this.props.viewer.index.toString(), { 'user-id': global.userId });
		// Answers.logCustom('DebateScreenView', { 'user-id': global.userId, 'debateId': this.props.viewer.index.toString(), 'page': 'Discuss' });
	// }

	componentWillUnmount () {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}

	onChangeTags(tags) {
		this.setState({
			tags
		});
	}

	keyboardDidShow() {
		this.setState({fab : false});
	}

	keyboardDidHide() {
		this.setState({fab : true});
	}

	setModalVisible(visible) {
		this.setState({reasonModalVisible: visible});
	}

  createNewReason() {
    this.postMode = "new";
    this.setState({
      reason : '',
      agree: 'x',
      beAnonymous: false,
      tags: [],
      previousReasonId : -1
    });
    this.setModalVisible(true);
  }

	onRefresh(){
		this.setState({refreshing : true});
		let pageSize = 10;
		this.props.relay.setVariables({pageSize});
		this.props.relay.forceFetch();
		this.setState({pageSize, refreshing : false});
	}

	onEndReached() {
		let {pageSize}	= this.state;
		pageSize = pageSize + 10;
		this.props.relay.setVariables({pageSize});
		this.setState({pageSize});
	}

	onAllPress() {
		this.setState({
			category: 'all',
			currentTag : '',
			pageSize : 10
		});
		this.props.relay.setVariables({pageSize : 10, type: 'all'});
	}

	onAgreePress() {
		this.setState({
			category: 'agree',
			currentTag : '',
			pageSize : 10
		});
		this.props.relay.setVariables({pageSize : 10, type: 'agree'});
	}

	onDisagreePress() {
		this.setState({
			category: 'disagree',
			currentTag : '',
			pageSize : 10
		});
		this.props.relay.setVariables({pageSize : 10, type: 'disagree'});
	}

	onTagsPress() {
		this.setState({
			modalVisible : true
		});
	}

  onOptionsPress(reasonId, reason, agree, beAnonymous, tags, previousReasonId) {
    let agreeVal = (agree?"true":"false");
    this.setState({
      optionModalVisible : true,
      reason : reason,
      agree : agreeVal,
      beAnonymous : beAnonymous,
      tags : tags,
      previousReasonId : previousReasonId
    });
    this.modifyDebateId = reasonId;
    this.postMode = "edit";
  }

	onTagPress(tag) {
		this.setState({
			category : 'tags',
			currentTag : tag,
			modalVisible : false,
			pageSize : 10
		});
		this.props.relay.setVariables({pageSize : 10, type: tag});
	}

	_renderRow({node}){
		const { navigator } = this.props;
		// const { category, currentTag } = this.state;
		// if(category == 'tags' && node.ReasonTags.includes(currentTag)){
		// 	return <ReasonCard
		// 		reasonId={node.id}
		// 		showUser={true}
		// 		{...node}
		// 		userId={node.userId}
		// 		navigator={navigator}/>;
		// }
		// if(category=='agree' && !node.agree) {
		// 	return <View></View>;
		// }
		// if(category=='disagree' && node.agree) {
		// 	return <View></View> ;
		// }
		// if(category == 'all' || category == 'agree' || category == 'disagree'){
		return	<ReasonCard
			reasonId={node.id}
			key={node.id}
			showUser={true}
			{...node}
			userId={node.userId}
      onOptionsPress={this.onOptionsPress}
			navigator={navigator}/>;
		// }
		// return <View></View>;
	}

	_renderTags(tag){
		return <TouchableOpacity style={{ borderRadius : 2, marginLeft: 5, marginRight: 5, justifyContent: 'center',marginTop: 6,margin: 0,padding: 0,height: 24 }} borderLess={true} onPress={this.onTagPress.bind(this, tag)}>
			<Text style={{ textAlign : 'center', color : 'black', fontFamily : 'helvetica', fontSize : 16 }}>{tag}</Text>
		</TouchableOpacity>;
	}

	createReason() {
		const { beAnonymous, agree, reason, tags, previousReasonId } = this.state;

    let edit = (this.postMode=="edit"?true:false);
		let agreeVal = true;
		if(agree=='x') {
			alert('Please choose a value for position')
			return;
		}
		else if(agree=='true') {
			agreeVal = true;
		}
		else if(agree=='false') {
			agreeVal = false;
		}
		if(reason=='') {
			alert('Reason cannot be empty')
			return;
		}
		this.setState({isLoading : true});
		RelayStore.commitUpdate(
			new CreateReasonMutation({
				agree : agreeVal,
				reason : reason,
				tags : tags,
				debateId : this.props.viewer.id,
				beAnonymous : beAnonymous,
        edit : edit,
        previousReasonId : previousReasonId
			}),
			{
				onFailure: () => {
					alert('Oops something went wrong, please try again!!');
					this.setState({isLoading : false});
				},
				onSuccess: () => {
					let reasonModalVisible = false;
					this.setState({reasonModalVisible});
					this.setState({isLoading : false});
					this.setState({reason : '', tags: [], beAnonymous: false, agree: 'x'}); {/*tosee*/}
				}
			}
		);
	}

  deleteReason() {

    Alert.alert(
      'Delete Reason',
      'Are you sure you want to delete this reason? (It can\'t be undone)',
      [
        {
          text: 'yes', onPress : () => {
            let reasonId = this.modifyDebateId;
            let debateId = this.props.viewer.id;
            this.setState({isLoading : true, optionModalVisible: false});
            RelayStore.commitUpdate(
              new DeleteReasonMutation({
                reasonId : reasonId,
                debateId : debateId
              }),
              {
                onFailure: () => {
                  alert('Oops something went wrong, please try again!!');
                  this.setState({isLoading : false});
                },
                onSuccess: () => {
                  this.setState({isLoading : false});
                }
              }
            );
          }
        },
        {
          text: 'no', onPress : () =>  {

          }
        }
      ]
    );


  }

	inputFocused() {
		setTimeout(() => {
			// let scrollResponder = this.refs.REASON_TEXT_SCROLL_REF.getScrollResponder();
			// scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
			// 	findNodeHandle(this.refs.reasonText),
			// 	0, //additionalOffset
			// 	true
			// );
      this.refs.REASON_TEXT_SCROLL_REF.scrollTo({y:36});
		}, 50);
	}

	render() {
		const { beAnonymous, dataSource, dataSourceTags, currentTag, modalVisible, reasonModalVisible, reason, fab, agree, isLoading, optionModalVisible } = this.state;
		const {  motionFull, title, userposition, motionShort } = this.props.viewer;
		let windowSize = Dimensions.get('window');
		let windowHeight = windowSize.height;
		const header = (
			<View style={{'flexDirection': 'row', height : 30, marginTop: 5, marginLeft : 5}}>
				<TouchableItem style={{ borderRadius : 2 }} borderLess={true} onPress={this.onAllPress}>
					<Text style={{ textAlign : 'left', color : this.state.category=='all'?colors.humbeeGreen:colors.humbeeDarkGrey, fontFamily : 'helvetica', fontSize : 16, fontWeight : 'bold' }}> ALL </Text>
				</TouchableItem>
				<TouchableItem style={{ borderRadius : 2 }} borderLess={true} onPress={this.onAgreePress}>
					<Text style={{ textAlign : 'left', color : this.state.category=='agree'?colors.humbeeGreen:colors.humbeeDarkGrey, fontFamily : 'helvetica', fontSize : 16, fontWeight : 'bold' }}> AGREE </Text>
				</TouchableItem>
				<TouchableItem style={{ borderRadius : 2 }} borderLess={true} onPress={this.onDisagreePress}>
					<Text style={{ textAlign : 'left', color : this.state.category=='disagree'?colors.humbeeGreen:colors.humbeeDarkGrey, fontFamily : 'helvetica', fontSize : 16, fontWeight : 'bold' }}> DISAGREE </Text>
				</TouchableItem>
				<TouchableItem style={{ borderRadius : 2 }} borderLess={true} onPress={this.onTagsPress}>
					<Text style={{ textAlign : 'left', color : this.state.category=='tags'?colors.humbeeGreen:colors.humbeeDarkGrey, fontFamily : 'helvetica', fontSize : 16, fontWeight : 'bold' }}> {currentTag?'TAGS('+currentTag+')':'TAGS'} </Text>
				</TouchableItem>
			</View>
		);

		const inputProps = {
			keyboardType: 'default',
			placeholder: 'press the return key after typing a tag',
			autoFocus: true,
			returnKeyType: 'done'
		};

		const {refreshing} = this.state;
		let opacity = (modalVisible||reasonModalVisible||optionModalVisible) ? 0.2 : 1;
		return(
				<View opacity={opacity} style={{backgroundColor : colors.humbeeGrey, flex : 1}}>
					<ListView
						refreshControl={
							<RefreshControl
								refreshing={refreshing}
								onRefresh={this.onRefresh}
								colors={[colors.humbeeGreen,'#ff0000', '#00ff00', '#0000ff']}
							/>
						}
						ref='LIST_REF'
						style={{backgroundColor : colors.humbeeGrey}}
						renderHeader={() => header}
						dataSource={dataSource}
						renderRow={ row => this._renderRow(row)}
						onEndReached={this.onEndReached}
						scrollRenderAheadDistance={1000}
						initialListSize={10}
					/>
					<Modal
						animationType={'slide'}
						transparent={true}
						visible={modalVisible}
						onRequestClose={() => this.setState({category : 'all', modalVisible : false})}>
						<View style={{ padding : 20, paddingTop : 120}}>
							<View style={{ borderRadius : 2}}>
								<View style={{height: 34, flexDirection : 'row', justifyContent:'space-between',alignItems:'center', backgroundColor : colors.humbeeGrey, borderColor: colors.humbeeDarkGrey, borderBottomWidth: 1}}>
									<TouchableItem style={{alignItems:'center', justifyContent:'center', width : 30}} borderLess={false} onPress={() => this.setState({modalVisible: false})}>
										<Icon name='md-close' style={{color : colors.humbeeGreen}} size={25}/>
									</TouchableItem>
									<View style={{ alignItems:'center', justifyContent:'center'}}>
										<Text style={{textAlign : 'center', fontSize : 20, fontFamily : 'helvetica', color : colors.humbeeGreen}}>Top Tags</Text>
									</View>
									<View style={{width: 30}}/>
								</View>
								<ListView
									refreshControl={
										<RefreshControl
											refreshing={refreshing}
											onRefresh={this.onRefresh}
											colors={[colors.humbeeGreen,'#ff0000', '#00ff00', '#0000ff']}
										/>
									}
									style={{backgroundColor : 'white', height : 400, borderColor : colors.humbeeDarkGrey, padding : 5}}
									dataSource={dataSourceTags}
									renderRow={ tag => this._renderTags(tag)}
								/>
							</View>
						</View>
					</Modal>
          <Modal
            animationType={'slide'}
            transparent={true}
            visible={optionModalVisible}
            onRequestClose={() => this.setState({optionModalVisible : false})}>
            <View style={{paddingTop : windowHeight-162}}>
              <View style={{ borderRadius : 2}}>
                <View style={{borderRadius : 9, height: 52, flexDirection : 'row', justifyContent:'space-between',alignItems:'center', backgroundColor : colors.humbeeGrey, borderColor: colors.humbeeDarkGrey, borderBottomWidth: 1}}>
                  <View style={{width : 50}}>
                    <TouchableItem style={{alignItems:'center', justifyContent:'center'}} borderLess={true} onPress={() => this.setState({optionModalVisible: false})}>
                      <Icon name='md-close' style={{color : colors.humbeeGreen}} size={25}/>
                    </TouchableItem>
                  </View>
                  <View style={{ alignItems:'center', justifyContent:'center'}}>
                    <Text style={{textAlign : 'center', fontSize : 20, fontFamily : 'helvetica', color : colors.humbeeGreen}}>Reason</Text>
                  </View>
                  <View style={{width: 30}}/>
                </View>
                <View style={{backgroundColor : 'white', borderColor : colors.humbeeDarkGrey}}>
                  <TouchableOpacity style={{ borderRadius : 2, marginLeft: 5, marginRight: 5, justifyContent: 'center',marginTop: 8,padding: 0,height: 30 }} borderLess={true} onPress={() => {this.setModalVisible(true); this.setState({optionModalVisible: false});}}>
                    <Text style={{ textAlign : 'center', color : 'black', fontFamily : 'helvetica', fontSize : 16 }}>Edit</Text>
                  </TouchableOpacity>
                </View>
                <View style={{backgroundColor : 'white', borderColor : colors.humbeeDarkGrey}}>
                  <TouchableOpacity style={{ borderRadius : 2, marginLeft: 5, marginRight: 5, justifyContent: 'center',marginVertical: 8,padding: 0,height: 30 }} borderLess={true} onPress={this.deleteReason}>
                    <Text style={{ textAlign : 'center', color : 'black', fontFamily : 'helvetica', fontSize : 16 }}>Delete</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
					<View elevation={5} style={styles.fabContainer} opacity={opacity}>
						<TouchableItem style={styles.fab} borderLess={true} onPress={this.createNewReason}>
							<Icon name='md-add' size={25} style={{color : 'white'}}/>
						</TouchableItem>
					</View>
					<Modal
						animationType={'fade'}
						transparent={true}
						visible={reasonModalVisible}
						onRequestClose={() => this.setModalVisible(!reasonModalVisible)}>
						{
							(isLoading)
							?
							(
								<ActivityIndicator
									animating={true}
									style={{
										alignItems: 'center',
										justifyContent: 'center',
										padding: 8,
										height: 80}}
									size="large"
								/>
							)
							:
							(
								<View style={{flex : 1, paddingTop : 10, paddingBottom : 60, paddingLeft : 10, paddingRight : 10}}>
									<View style={{flex : 1, backgroundColor : colors.humbeeGrey, padding : 5 , borderWidth : 1, borderColor : colors.humbeeDarkGrey}} elevation={10}>
										<View style={{height: 34, flexDirection : 'row', alignItems:'center', justifyContent: 'space-between', backgroundColor : colors.humbeeGrey, borderColor: colors.humbeeDarkGrey, borderBottomWidth: 1, paddingBottom: 4}}>
											<TouchableItem style={{alignItems:'flex-start', justifyContent:'center', width : 25}} borderLess={false} onPress={() => this.setModalVisible(!reasonModalVisible)}>
												<Icon name='md-close' style={{color : colors.humbeeGreen}} size={25}/>
											</TouchableItem>

											<Text style={{justifyContent:'center', fontSize: 16, fontFamily : 'helvetica',color : colors.humbeeGreen}}>
												{title}
											</Text>

											<TouchableItem onPress={this.createReason} rippleColor={colors.humbeeGreen} style={{ alignItems : 'center', borderRadius : 10, backgroundColor : colors.humbeeGreen, flexDirection : 'row', width : 70, paddingHorizontal: 5}}>
												<Icon style={{flex : 1, color : 'white', justifyContent:'center' }} name='ios-create-outline' size={25}/>
												<Text style={{flex : 2, color : 'white', textAlign : 'center', justifyContent:'center', fontWeight : 'bold', fontFamily : 'helvetica', fontSize : 18}}>Post</Text>
											</TouchableItem>
										</View>

										<View style={{height: 35, flexDirection : 'row', alignItems:'center', justifyContent: 'space-between',padding : 2}}>
											<View style={{flex : 1.2}}>
												<View style={{  height: 35, justifyContent: 'center',borderWidth : 1, borderColor : colors.humbeeDarkGrey, borderRadius : 2}}>
													<Picker
														mode={'dropdown'}
														selectedValue={agree}
														onValueChange={(agree) => this.setState({agree: agree})}>
														<Picker.Item label="Your Position" value="x"/>
														<Picker.Item label="Agree" value="true" />
														<Picker.Item label="Disagree" value="false" />
													</Picker>
												</View>
											</View>
											<View style={{flex : 0.1}} />
											<View style={{flex : 1.2}}>
												<View style={{ height: 35, justifyContent: 'center',borderWidth : 1, borderColor : colors.humbeeDarkGrey, borderRadius : 2}}>
													<Picker
														mode={'dropdown'}
														selectedValue={beAnonymous}
														onValueChange={(beAnonymous) => {this.setState({beAnonymous: beAnonymous})}}>
														<Picker.Item label="As Anonymous" value={true} />
														<Picker.Item label="As Yourself" value={false} />
													</Picker>
												</View>
											</View>
										</View>

										<View style={{ marginTop: 3, height: 62, flexDirection: 'row', alignItems: 'flex-start'}}>
											<Text style={{ marginTop : 11}}>Tags: </Text>
											<TagScreen
												value={this.state.tags}
												onChange={this.onChangeTags}
												tagColor={colors.humbeeGreen}
												tagTextColor="white"
												inputProps={inputProps}
												numberOfLines={3}
												separators={[',']}
											/>
										</View>

										<ScrollView
											keyboardShouldPersistTaps={true}
											ref={'REASON_TEXT_SCROLL_REF'}
											scrollEnabled={true}
											showsVerticalScrollIndicator={false}
											pagingEnabled={true}
											style={{flex : 1}}
											flexDirection='column'
										>
                      <View>
                        <Text
                          style={{justifyContent:'center', fontSize: 16}}
                        >
                          {title+':\n' + motionShort}
                        </Text>
                      </View>
											<View style={{ borderTopWidth : 2, borderColor : colors.humbeeDarkGrey}}>
												<TextInput
													numberOfLines={500}
													textAlignVertical='top'
													underlineColorAndroid='transparent'
													multiline = {true}
													onChangeText={(reason) => this.setState({reason})}
													value={reason}
													maxLength={10000}
													placeholder="Share your reason"
													ref={'reasonText'}
													onFocus={this.inputFocused.bind(this)}
												/>
											</View>
										</ScrollView>
									</View>
								</View>
							)
						}
					</Modal>
				</View>
		);
	}
}

export default createRenderer(Debate, {
	queries : NodeQuery,
	queriesParams: ({ id }) => ({
		id: id ? id : null
	}),
	initialVariables : {
		type : 'all',
		pageSize : 10
	},
	fragments: {
		viewer: () => Relay.QL`
			fragment on Debate {
				id,
				index,
				title,
				motionShort,
				motionFull,
				tags,
				reasons(first : $pageSize, type : $type) {
					pageInfo{
						hasNextPage
					},
					edges {
						node {
							id,
							index,
							debateId,
							text,
							ReasonTags,
							beAnonymous,
							agree,
							userId,
							postedTime,
							likes,
							numberOfComments
							liked,
              isAuthor
						}
					}
				}
			}
		`
	}
});

const styles = StyleSheet.create({
	fabContainer : {
		height: 60,
		width: 60,
		borderRadius: 30,
		position: 'absolute',
		bottom: 15,
		right:15
	},
	fab : {
		backgroundColor: colors.humbeeGreen,
		height: 60,
		width: 60,
		borderRadius: 30,
		alignItems: 'center',
		justifyContent: 'center',
	}
});
