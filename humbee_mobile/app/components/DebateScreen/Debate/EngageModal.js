import Relay from 'react-relay';
import React, {Component} from 'react';
import { View, Text, TextInput, Keyboard, ListView, LayoutAnimation, RefreshControl} from 'react-native';
import colors from '../../../config/colors';
import dismissKeyboard from 'dismissKeyboard';
import ExtraDimensions from 'react-native-extra-dimensions-android';

import NodeQuery from '../../../queries/NodeQuery';
import { createRenderer } from '../../../utils/RelayUtils';
import Router from '../../../routes/Router';
import RelayStore from '../../../utils/RelayStore';

import CommentCard from './CommentCard';

import TouchableItem from '../../Common/TouchableItem';
import Icon from 'react-native-vector-icons/Ionicons';

import CreateCommentMutation from '../../../mutations/CreateCommentMutation';

import InvertibleScrollView from 'react-native-invertible-scroll-view';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

const ds = new ListView.DataSource({
	rowHasChanged: (row1, row2) => row1.id !== row2.id
});

class EngageModal extends Component {
	constructor(props){
		super(props);
		this.state={
			commentText : '',
			listViewHeight : 0,
			keyboardHeight : 0,
			textInputHeight : 0,
			scrollY : 0,
			pageSize : 10,
			dataSource : ds,
			refreshing : false
		};
		this.goBack = this.goBack.bind(this);
		this.handleScroll = this.handleScroll.bind(this);
		this.postComment = this.postComment.bind(this);
		this.likeModal = this.likeModal.bind(this);
		this.onEndReached = this.onEndReached.bind(this);
		this.onRefresh = this.onRefresh.bind(this);
		this._renderRow = this._renderRow.bind(this);
	}

	componentWillReceiveProps(next) {
		if(next && next.viewer.comments) {
			this.setState({
				dataSource: ds.cloneWithRows([ ...next.viewer.comments.edges ])
			});
		}
	}


	postComment(){
		RelayStore.commitUpdate(
			new CreateCommentMutation({
				comment : this.state.commentText,
				reasonId : this.props.viewer.id
			}),
			{
				onFailure: (transaction) => {
					const errorMessage = transaction.getError().source.errors[0].message;
					alert(errorMessage);
				},
				onSuccess: () => {
					this.setState({commentText : ''}); {/*tosee*/}
				}
			}
		);

		dismissKeyboard();
	}

	goBack(){
		dismissKeyboard();
		this.props.navigator.pop();
	}

	likeModal(){
		const { id, index } = this.props.viewer;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('ReasonLikeModal-' + index);
    }
		this.props.navigator.push(
			Router.getRoute('reasonLikeModal', {
				id : id
			})
		);
	}

	keyboardDidShow (e) {
		let {listViewHeight} = this.state;
		let keyboardHeight = e.endCoordinates.height;
		this.setState({keyboardHeight});
		listViewHeight = listViewHeight - keyboardHeight;
		this.setState({listViewHeight});
		LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
	}

	handleScroll(e){
		this.setState({scrollY : e.nativeEvent.contentOffset.y});
	}

	keyboardDidHide () {
		let {listViewHeight, keyboardHeight} = this.state;
		listViewHeight =  listViewHeight + keyboardHeight;
		this.setState({listViewHeight});
		LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
	}

	onEndReached(){
		let {pageSize}	= this.state;
		pageSize = pageSize + 10;
		this.props.relay.setVariables({pageSize});
		this.setState({pageSize});
	}

	onRefresh(){
		this.setState({refreshing : true});
		this.props.relay.forceFetch();
		let pageSize = 10;
		this.props.relay.setVariables({pageSize});
		this.setState({pageSize, refreshing : false});
	}

	_renderRow({node}){
		return <CommentCard viewer={node} navigator={this.props.navigator}/>;
	}

	componentWillMount () {
		this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
		this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
		let listViewHeight = ExtraDimensions.get('REAL_WINDOW_HEIGHT') - ExtraDimensions.get('STATUS_BAR_HEIGHT') - ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT') - 50;
		this.setState({listViewHeight});
	}

	componentWillUnmount () {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}
	render(){
		const {
			textInputHeight,
			listViewHeight,
			commentText,
			scrollY
		} = this.state;
		let listViewHeightFixed = listViewHeight;
		if(textInputHeight > 100){
			listViewHeightFixed = listViewHeight + (textInputHeight-100);
		}
		const { likes } = this.props.viewer;
		const { dataSource, refreshing } = this.state;
		return(
			<View>
				<View elevation={5} style={{height : 50, flexDirection : 'row', backgroundColor : 'white', alignItems : 'center', justifyContent : 'space-around'}}>
					<TouchableItem
						borderLess={true}
						style={{flex : 1, alignItems : 'center', justifyContent : 'space-around'}}
						onPress={this.goBack}>
						<Icon name='ios-arrow-back' size={25} style={{color : colors.humbeeGreen}}/>
					</TouchableItem>
					<Text style={{ flex : 8, color : colors.humbeeGreen, fontSize : 16, fontFamily : 'helvetica', textAlign : 'center'}}>{likes + ' people like it'}</Text>
					<TouchableItem style={{flex : 1, alignItems : 'center', justifyContent : 'space-around'}} onPress={this.likeModal} borderLess={true}>
						<Icon name='ios-arrow-forward' size={25} style={{color : colors.humbeeGreen}}/>
					</TouchableItem>
				</View>
				<View style={{ padding : 5}}>
					<ListView
						style={{backgroundColor : 'white', height : listViewHeightFixed-20 }}
						renderRow={row => this._renderRow(row)}
						onEndReached={this.onEndReached}
						dataSource={dataSource}
						refreshControl={
							<RefreshControl
								refreshing={refreshing}
								onRefresh={this.onRefresh}
								colors={[colors.humbeeGreen,'#ff0000', '#00ff00', '#0000ff']}
							/>
						}
						/>
				</View>
				<View style={{height : Math.min(textInputHeight, 100) + 10, flex : 1, flexDirection : 'row', backgroundColor : 'white', padding : 5 }} elevation={5}>
					<InvertibleScrollView
						ref='INPUT_SCROLL'
						style={{ flex : 9}}
						inverted
						onScroll={this.handleScroll}
						>
						<TextInput
							multiline={true}
							style={{ height : textInputHeight,color : 'black', fontFamily : 'helvetica', fontSize : 16}}
							onChangeText={(commentText) => this.setState({commentText})}
							value={commentText}
							onContentSizeChange={(event) => {
								let newTextInputHeight = event.nativeEvent.contentSize.height;
								let newListViewHeight = listViewHeight - (newTextInputHeight - textInputHeight);
								this.setState({textInputHeight: newTextInputHeight, listViewHeight : newListViewHeight});
								this.refs['INPUT_SCROLL'].scrollTo(scrollY);
							}}
							keyboardType='twitter'
							placeholder={'Post your comment...'}
							placeholderTextColor={colors.humbeeGrey}
							underlineColorAndroid='transparent'
						/>
					</InvertibleScrollView >
					<TouchableItem
						style={{flex : 1, alignItems : 'center', justifyContent : 'center'}}
						borderLess={true}
						onPress={this.postComment}>
						<Icon name='md-send' style={{color : colors.humbeeGreen}} size={30}/>
					</TouchableItem>
				</View>
			</View>
		);
	}
}

EngageModal.propTypes={
	viewer : React.PropTypes.object.isRequired,
	navigator : React.PropTypes.object.isRequired,
	relay : React.PropTypes.object.isRequired
};

export default createRenderer(EngageModal, {
	queries : NodeQuery,
	queriesParams: ({ id }) => ({
		id: id ? id : null,
	}),
	initialVariables : {
		pageSize : 10
	},
	fragments: {
		viewer: () => Relay.QL`
			fragment on Reason {
				id,
        index,
				likes,
				comments(first : $pageSize){
					pageInfo{
						hasNextPage
					},
					edges{
						node{
							id,
							${CommentCard.getFragment('viewer')}
						}
					}
				}
			}
		`
	}
});
