import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, Text, ListView, RefreshControl } from 'react-native';

import NodeQuery from '../../../queries/NodeQuery';
import { createRenderer } from '../../../utils/RelayUtils';


import ExtraDimensions from 'react-native-extra-dimensions-android';

import TouchableItem from '../../Common/TouchableItem';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../../config/colors';
import UserCard from '../../Common/UserCard';

const ds = new ListView.DataSource({
	rowHasChanged: (row1, row2) => row1 !== row2
});


class ReasonLikeModal extends Component{
	constructor(props){
		super(props);
		this.state={
			pageSize : 10,
			scrollViewHeight : 0,
			refreshing : false,
			dataSource : ds
		};
		this.goBack = this.goBack.bind(this);
		this.onEndReached = this.onEndReached.bind(this);
		this._renderRow = this._renderRow.bind(this);
		this.onRefresh = this.onRefresh.bind(this);
	}

	componentWillReceiveProps(next) {
		if(next && next.viewer.reasonLikes) {
			this.setState({
				dataSource: ds.cloneWithRows([ ...next.viewer.reasonLikes.edges ])
			});
		}
	}

	goBack(){
		this.props.navigator.pop();
	}

	onEndReached(){
		let {pageSize}	= this.state;
		pageSize = pageSize + 10;
		this.props.relay.setVariables({pageSize});
		this.setState({pageSize});
	}

	componentWillMount () {
		let scrollViewHeight = ExtraDimensions.get('REAL_WINDOW_HEIGHT') - ExtraDimensions.get('STATUS_BAR_HEIGHT') - ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT') - 50;
		this.setState({scrollViewHeight});
		this.props.relay.forceFetch();
	}
	onRefresh(){
		this.setState({refreshing : true});
		this.props.relay.forceFetch();
		let pageSize =10;
		this.props.relay.setVariables({pageSize});
		this.setState({pageSize, refreshing : false});
	}

	_renderRow({node}){
		const { navigator } = this.props;
		return (
			<View style={{ padding : 5}}>
				<UserCard navigator={navigator} id={node.userId} shareButton={false} followButton={true}/>
			</View>
		);
	}


	render(){
		const { scrollViewHeight, refreshing, dataSource } = this.state;
		return(
			<View style={{backgroundColor : 'white'}}>
				<View elevation={5} style={{height : 50, flexDirection : 'row', backgroundColor : 'white', alignItems : 'center', justifyContent : 'space-around'}}>
					<TouchableItem
						borderLess={true}
						style={{flex : 1, alignItems : 'center', justifyContent : 'space-around'}}
						onPress={this.goBack}>
						<Icon name='ios-arrow-back' size={25} style={{color : colors.humbeeGreen}}/>
					</TouchableItem>
					<Text style={{flex : 8, color : colors.humbeeGreen, fontSize : 16, fontFamily : 'helvetica', textAlign : 'center'}}>Likes</Text>
					<View style={{ flex : 1 }}/>
				</View>
				<ListView
					style={{height : scrollViewHeight}}
					renderRow={ row => this._renderRow(row)}
					dataSource={dataSource}
					onEndReached={this.onEndReached}
					refreshControl={
						<RefreshControl
							refreshing={refreshing}
							onRefresh={this.onRefresh}
							colors={[colors.humbeeGreen,'#ff0000', '#00ff00', '#0000ff']}
						/>
					}
					/>
			</View>
		);
	}
}

export default createRenderer(ReasonLikeModal, {
	queries : NodeQuery,
	queriesParams: ({ id }) => ({
		id: id ? id : null,
	}),
	initialVariables : {
		pageSize : 10
	},
	fragments: {
		viewer: () => Relay.QL`
			fragment on Reason {
				reasonLikes(first : $pageSize){
					pageInfo{
						hasNextPage
					},
					edges{
						node{
							userId
						}
					}
				}
			}
		`
	}
});
