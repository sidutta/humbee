import React, {Component} from 'react';
import {View, Text, Dimensions } from 'react-native';
import ReadMore from '../../Common/ReadMore';
import colors from '../../../config/colors';
import Icon from 'react-native-vector-icons/Ionicons';
import EngageModalContainer from '../../Containers/EngageModalContainer';
import ReasonLikeModalContainer from '../../Containers/ReasonLikeModalContainer';

import RelayStore from '../../../utils/RelayStore';
import Router from '../../../routes/Router';

import TouchableItem from '../../Common/TouchableItem';

import CreateReasonLikeMutation from '../../../mutations/CreateReasonLikeMutation';
import DebateTitleCard from '../../Common/DebateTitleCard';
import UserCard from '../../Common/UserCard';

import DebateScreenContainer from '../../Containers/DebateScreenContainer';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

function intersperse(arr, sep) {

    if (arr.length === 0) {
        return [];
    }

    return arr.slice(1).reduce(function(xs, x, i) {
        return xs.concat([sep, x]);
    }, [arr[0]]);
}

const WINDOW_WIDTH = Dimensions.get('window').width;

export default class ReasonCard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      textHeight : 110
    };
    this.toggleHeight = this.toggleHeight.bind(this);
    this.engageModal = this.engageModal.bind(this);
    this.like = this.like.bind(this);
    this.likeModal = this.likeModal.bind(this);
    this.seeDebate = this.seeDebate.bind(this);
  }

  toggleHeight(textHeight) {
    this.setState({textHeight});
  }

  engageModal() {
    const { navigator, id, index } = this.props;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('EngageModal-' + index);
    }
    navigator.push(
      Router.getRoute('engageModal',{
        id : id
      })
    );
  }

  likeModal() {
    const { id, navigator, index } = this.props;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('ReasonLikeModal-' + index);
    }
    navigator.push(
      Router.getRoute('reasonLikeModal',{
        id : id
      })
    );
  }

  like() {
    const { id, liked, debateId, userId } = this.props;
    RelayStore.commitUpdate(
      new CreateReasonLikeMutation({
        liked : liked,
        reasonId : id,
        debateId : debateId,
        userId : userId
      }),
      {
        onFailure: (transaction) => {
          const errorMessage = transaction.getError().source;
          // console.log(errorMessage);

        },
        onSuccess: (response) => {

        }
      }
    );
  }

  seeDebate() {
    const { debateId, navigator } = this.props;
    navigator.push(
      Router.getRoute('debateScreen',{
        id : debateId,
        fromCard : false,
        loadIndex : 0
      })
    );
  }

  render() {

    const {id,text, index, postedTime, likes, beAnonymous, numberOfComments, liked, position, agree, userId, showUser, seeDebate, debateId, navigator, ReasonTags, isAuthor, reasonId} = this.props;
    let tags = intersperse(ReasonTags, ", ");

    return(
      <View elevation={2} style={{ padding : 5, width: WINDOW_WIDTH-50, backgroundColor : (agree)?'#E8F5E9' : '#FFEBEE' , borderRadius : 2, marginBottom : 10,marginTop : 10, marginRight : (agree)?10:10, marginLeft : (agree)?10:40}}>
        {(showUser||beAnonymous) && <UserCard shareButton={true} id={userId} reasonCard={true} navigator={navigator}/>}
        {seeDebate && <DebateTitleCard id={debateId}/>}

        <View style={{ padding : 5}}>
          <ReadMore
            numberOfLines={4}
            toggleHeight={this.toggleHeight}
            text={text}
            textHeight={110}
            style={{fontSize : 14, fontFamily : 'helvetica', color : 'black'}}/>
        </View>
        <View style={{padding : 5}}>
        {tags.length>0? <Text style={{fontSize : 14, fontFamily : 'helvetica', fontWeight : 'bold', color : 'black'}}>Tags: {tags}</Text>:<View></View>}{/*tags? "Tags:" + tags: ""*/}
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableItem style={{height : 30, padding : 5, paddingRight : 0}} onPress={this.likeModal}>
            <Text style={{fontSize : 14, fontFamily : 'helvetica', color : colors.humbeeGreen}}>
              {likes.toString() + (likes==1?' like \u00B7 ':' likes \u00B7 ')}
            </Text>
          </TouchableItem>
          <TouchableItem style={{height : 30, padding : 5, paddingLeft : 0}} onPress={this.engageModal}>
            <Text style={{fontSize : 14, fontFamily : 'helvetica', color : colors.humbeeGreen}}>
              {numberOfComments.toString() + (numberOfComments==1?' comment':' comments')}
            </Text>
          </TouchableItem>
        </View>
        <View style={{ height : 1, backgroundColor : colors.humbeeGrey, marginTop : 5,marginBottom : 5 }}/>
        <View style={{  flexDirection : 'row', padding : 2}}>
          <TouchableItem
            style={{flex : 1, flexDirection : 'row', justifyContent : 'center'}}
            rippleColor={colors.humbeeDarkGrey}
            borderLess={true}
            onPress={this.like}>
            <Icon name='md-thumbs-up' size={20} style={{color : (liked)? colors.humbeeGreen : colors.humbeeDarkGrey}}/>
          </TouchableItem>
          <TouchableItem onPress={this.engageModal} style={{flex : 1, flexDirection : 'row', justifyContent : 'center'}} rippleColor={colors.humbeeDarkGrey} borderLess={true}>
            <Icon name='ios-text' size={20} style={{color : colors.humbeeDarkGrey}}/>
          </TouchableItem>
          <View style={{flex : 4, height : seeDebate ? 30 : 0}}/>
          { (seeDebate)
            ?
            (
              <TouchableItem onPress={this.seeDebate} style={{flex : 2, alignItems : 'center', flexDirection : 'row', justifyContent : 'center', borderRadius : 2, padding : 2, backgroundColor : colors.humbeeGreen}} rippleColor={colors.humbeeDarkGrey} borderLess={true}>
                <Text style={{ fontSize : 14, fontFamily : 'helvetica', color : 'white'}}>See Debate</Text>
              </TouchableItem>
            )
            :
            (
              (isAuthor)
              ?
              (
                // <TouchableItem onPress={this.seeDebate} style={{flex : 2, alignItems : 'center', flexDirection : 'row', justifyContent : 'center', borderRadius : 2, padding : 2, backgroundColor : colors.humbeeDarkGrey}} rippleColor={colors.humbeeGrey} borderLess={true}>
                <TouchableItem onPress={() => this.props.onOptionsPress(reasonId, text, agree, beAnonymous, ReasonTags, index)} style={{flex : 1, flexDirection : 'row', justifyContent : 'center'}} rippleColor={colors.humbeeDarkGrey} borderLess={true}>
                  <Icon name='ios-more' size={25} style={{color : colors.humbeeDarkGrey}}/>
                </TouchableItem>
                // </TouchableItem>
              )
              :
              <View/>
            )
          }
        </View>
      </View>
    );
  }
}
