import React, { Component } from 'react';
import { Text, Image, View } from 'react-native';
import TouchableItem from '../Common/TouchableItem';

import colors from '../../config/colors';

import Router from '../../routes/Router';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';


export default class SearchUserCard extends Component{
	constructor(props){
		super(props);
		this.openUserProfile = this.openUserProfile.bind(this);
	}

	openUserProfile(){
		const { navigator, id, isViewer, index } = this.props;
    let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
		// if(isViewer){
		// 	navigator.push(
		// 		Router.getRoute('viewerProfile')
		// 	);
		// } else {
      if (__DEV__ == false) {
        tracker.trackScreenView('UserProfile-' + index);
      }
			navigator.push(
				Router.getRoute('userProfile',{id})
			);
		// }
	}

	render(){
		const { firstName, lastName, profilePic, username } = this.props;
		return(
			<View style={{height : 66}}>
				<TouchableItem onPress={this.openUserProfile} style={{height : 65, flex : 1, flexDirection : 'row', padding : 5}}>
					<Image style={{ flex : 1, borderRadius : 50}} source={{uri : profilePic}}/>
					<View style={{flex : 5, padding : 5}}>
						<Text style={{flex :1, color : 'black',fontFamily : 'helvetica', fontSize : 16, fontWeight : 'bold'}}>{firstName + ' '+ lastName}</Text>
						<Text style={{flex : 1,color : 'black',fontFamily : 'helvetica', fontSize : 16}}>{'@'+ username}</Text>
					</View>
				</TouchableItem>
				<View style={{ height : 1, marginLeft : 70, marginRight : 10, backgroundColor : colors.humbeeDarkGrey}}/>
			</View>
		);
	}
}

SearchUserCard.propTypes={
	firstName : React.PropTypes.string.isRequired,
	lastName : React.PropTypes.string.isRequired,
	profilePic : React.PropTypes.string.isRequired,
	id : React.PropTypes.string.isRequired,
	username : React.PropTypes.string.isRequired,
	navigator : React.PropTypes.object.isRequired,
	isViewer : React.PropTypes.bool.isRequired
};
