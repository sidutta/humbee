import Relay from 'react-relay';
import React, {Component} from 'react';
import { View, Text, TextInput, ListView, StyleSheet, ActivityIndicator} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../config/colors';
import dismissKeyboard from 'dismissKeyboard';
import SearchUserCard from './SearchUserCard';
import _ from 'underscore';
import isEmpty from 'lodash/isEmpty';

import ExtraDimensions from 'react-native-extra-dimensions-android';

import ViewerQuery from '../../queries/ViewerQuery';
import { createRenderer } from '../../utils/RelayUtils';

import TouchableItem from '../Common/TouchableItem';

const ds = new ListView.DataSource({
	rowHasChanged: (row1, row2) => row1 !== row2
});

class Search extends Component {

	constructor(props) {
		super(props);
		this.state = {
			searchString: '',
			isLoading : false,
			pageSize : 10,
			dataSource : ds
		};
		this.clear = this.clear.bind(this);
		this.goBack = this.goBack.bind(this);
		this.updateSearchString = this.updateSearchString.bind(this);
		this.onEndReached = this.onEndReached.bind(this);
		this._renderRow = this._renderRow.bind(this);
	}

	componentWillReceiveProps(next) {
		if(next && next.viewer.user.users) {
			this.setState({
				dataSource: ds.cloneWithRows([ ...next.viewer.user.users.edges ])
			});
		}
	}

	clear(){
		this.setState({searchString : ''});
		this.props.relay.setVariables({searchString : ''});
	}

	goBack() {
		dismissKeyboard();
		this.props.navigator.pop();
	}

	updateSearchString(searchString){
		this.setState({searchString});
		this.setState({isLoading : true});
		let debounced = _.debounce(() => {
			this.props.relay.setVariables({searchString});
			this.setState({isLoading : false});
		}, 500);
		debounced();
		this.props.relay.forceFetch();
	}

	onEndReached(){
		let {pageSize}	= this.state;
		pageSize = pageSize + 10;
		this.props.relay.setVariables({pageSize});
		this.setState({pageSize});
	}

	_renderRow({node}){
		const { navigator } = this.props;
		return(
			<SearchUserCard {...node} navigator={navigator}/>
		);
	}

	componentWillMount () {
		let listViewHeight = ExtraDimensions.get('REAL_WINDOW_HEIGHT') - ExtraDimensions.get('STATUS_BAR_HEIGHT') - ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT') - 60;
		this.setState({listViewHeight});
	}

	render() {
		const { isLoading, searchString, dataSource, listViewHeight} = this.state;
		const { viewer } = this.props;
		return(

			<View style={{ flex : 1, backgroundColor : colors.humbeeGrey }}>
				<View style={{ height : 60, flexDirection : 'row', backgroundColor : colors.humbeeGreen }} elevation={5}>
					<TouchableItem style={styles.menuButton} onPress={this.goBack} rippleColor={'white'} borderLess={true}>
						<Icon name='md-arrow-back' style={styles.backIcon} size={25}/>
					</TouchableItem>
					<TextInput
						style={{flex : 8, color : 'white', fontFamily : 'helvetica', fontSize : 16}}
						onChangeText={(searchString) => this.updateSearchString(searchString)}
						value={searchString}
						autoFocus={true}
						placeholder={'Find People...'}
						placeholderTextColor={colors.humbeeGrey}
						underlineColorAndroid='transparent'
					/>
					{
						(this.state.searchString=='') ?
							(<View style={styles.menuButton}/>) :
							(<TouchableItem style={styles.menuButton} onPress={this.clear} rippleColor={'white'} borderLess={true}>
								<Icon name='md-close' style={styles.clearIcon} size={25}/>
							</TouchableItem> )
					}
				</View>
				<View style={{backgroundColor : colors.humbeeGrey}}>
					{ (isLoading) ?
						(<ActivityIndicator
								animating={true}
								color={colors.humbeeGreen}
								style={{
									alignItems: 'center',
									justifyContent: 'center',
									padding: 40,
									height: 200}}
								size="large"/>
						) :(
							(isEmpty(viewer.user.users)) ? (
								<Text style={{ textAlign : 'center', fontSize : 16, color : colors.humbeeGreen, padding : 5}}>{searchString ? 'Sorry no users found with that name' : 'Search for people'}</Text>
							) : (
								<ListView
									style={{height : listViewHeight}}
									renderRow={ row => this._renderRow(row)}
									dataSource={dataSource}
									onEndReached={this.onEndReached}
								/>
							)
						)}
				</View>
			</View>
		);
	}
}

Search.propTypes = {
	NavBarHeight : React.PropTypes.number.isRequired,
	viewer : React.PropTypes.object.isRequired,
	navigator : React.PropTypes.object.isRequired,
	relay : React.PropTypes.object.isRequired
};


let styles= StyleSheet.create({
	menuButton : {
		flex : 1,
		alignItems : 'center',
		justifyContent : 'center'
	},
	backIcon : {
		color : 'white'
	},
	clearIcon : {
		color : colors.humbeeDarkGrey
	}
});


export default createRenderer(Search,{
	queries : ViewerQuery,
	initialVariables : {
		searchString : '',
		pageSize : 10
	},
	fragments : {
		viewer : () => Relay.QL`
			fragment on Viewer{
				user{
					users(first : $pageSize, searchString : $searchString){
						pageInfo{
							hasNextPage
						},
						edges{
							node{
								firstName,
								lastName,
								username,
								profilePic,
								id,
                index,
								isViewer
							}
						}
					}
				}
			}
		`
	}
});
