import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { createRenderer } from '../../utils/RelayUtils';
import NodeQuery from '../../queries/NodeQuery';

class DebateTitleCard extends Component{
	constructor(props){
		super(props);
	}

	render() {

		const { title } = this.props.viewer;
		return(
			<View style={{ height : 30}}>
				<Text style={{ color : 'black', fontSize : 16, fontFamily : 'helvetica', textAlign : 'center'}}>{ title }</Text>
			</View>
		);
	}

}

const renderLoading = <View></View>;

export default createRenderer(DebateTitleCard,{
	queries : NodeQuery,
	renderLoading : () => renderLoading,
	queriesParams: ({ id }) => ({
		id: id ? id : null,
	}),
	fragments : {
		viewer : () => Relay.QL`
			fragment on Debate{
				title
			}
		`
	}
});
