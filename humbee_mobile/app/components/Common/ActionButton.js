import React, { Component } from 'react';
import { View, Text } from 'react-native';

import TouchableItem from './TouchableItem';
import Icon from 'react-native-vector-icons/Ionicons';

export default class ActionButton extends Component {
	render() {
		const { backgroundColor, iconName, text, onPress } = this.props;
		return(
			<TouchableItem onPress={onPress} rippleColor={'white'} style={{ flex : 1,alignItems : 'center', backgroundColor : backgroundColor ,marginTop : 5, marginBottom : 5, flexDirection : 'row', width : 300, height : 25, borderRadius : 20, paddingTop : 5, paddingBottom : 5, paddingRight : 25, paddingLeft : 25}}>
				<Icon style={{flex : 1, color : 'white' }} name={iconName} size={40}/>
				<Text style={{flex : 4, color : 'white',textAlign : 'center', fontWeight : 'bold', fontFamily : 'helvetica', fontSize : 16}}>{text}</Text>
			</TouchableItem>
		);
	}
}
