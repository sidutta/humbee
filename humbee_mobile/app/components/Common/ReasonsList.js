import Relay from 'react-relay';
import React, { Component } from 'react';
import { ListView, View, Text, Dimensions} from 'react-native';
import ReasonCard from '../DebateScreen/Debate/ReasonCard';
import isEmpty from 'lodash/isEmpty';
import { createRenderer } from '../../utils/RelayUtils';
import NodeQuery from '../../queries/NodeQuery';

const ds = new ListView.DataSource({
	rowHasChanged: (row1, row2) => row1 !== row2
});

class ReasonsList extends Component{

	constructor(props){
		super(props);
		this.state={
			dataSource : ds,
			refreshing : false,
			pageSize : 10
		};
		this.onEndReached = this.onEndReached.bind(this);
		this.onRefresh = this.onRefresh.bind(this);
		this._renderRow = this._renderRow.bind(this);
	}

	componentWillReceiveProps(next) {
		if(next && next.viewer.reasons) {
			this.setState({
				dataSource: ds.cloneWithRows([ ...next.viewer.reasons.edges ])
			});
		}
	}

	componentWillMount(){
		this.props.relay.forceFetch();
	}

	onEndReached(){
		let {pageSize}	= this.state;
		pageSize = pageSize + 10;
		this.props.relay.setVariables({pageSize});
		this.setState({pageSize});
	}

	onRefresh(){
		this.setState({refreshing : true});
		this.props.relay.forceFetch();
		let pageSize =10;
		this.props.relay.setVariables({pageSize});
		this.setState({pageSize, refreshing : false});
	}

	_renderRow({node}){
		const { navigator } = this.props;
		return(
			<ReasonCard {...node} key={node.id} navigator={navigator} showUser={false} seeDebate={true}/>
		);
	}

	render(){
		const { dataSource } = this.state;
		return(
			<ListView
				style={[{backgroundColor : 'white', flex : 1}, this.props.style]}
				ref={'SCROLL_REF'}
				scrollEventThrottle={16}
				renderRow={row => this._renderRow(row)}
				dataSource={dataSource}
				onEndReached={this.onEndReached}
				renderHeader={() =>{
					if(isEmpty(this.props.viewer.reasons.edges)){
						return <View style={{ height : 300, padding : 5, alignItems : 'center', justifyContent : 'center'}}>
							<Text style={{ fontSize : 16, fontWeight : 'bold', fontFamily : 'helvetica'}}>No reasons</Text>
						</View>;
					} else {
						return <View style={{ padding : 5, paddingLeft : 10}}><Text style={{ fontSize : 16, fontWeight : 'bold', fontFamily : 'helvetica'}}>Reasons</Text></View>;
					}
				}}
				/>
		);
	}
}

ReasonsList.propTypes={
	viewer : React.PropTypes.object.isRequired,
	navigator : React.PropTypes.object.isRequired,
	relay : React.PropTypes.object.isRequired
};

const { width } = Dimensions.get('window');

const renderLoading = <View style={{ width : width}}></View>;

export default createRenderer(ReasonsList, {
	queries : NodeQuery,
	queriesParams: ({ id }) => ({
		id: id ? id : null,
	}),
	renderLoading : () => renderLoading,
	initialVariables : {
		pageSize : 10
	},
	fragments :{
		viewer : () => Relay.QL`
			fragment on User {
				id,
				reasons(first : $pageSize){
					totalCount,
					edges{
						node{
							id,
							index,
							text,
							beAnonymous,
							agree,
							userId,
							postedTime,
							likes,
							numberOfComments,
							liked,
							debateId,
							ReasonTags
						}
					}
				}
			}
		`
	}
});
