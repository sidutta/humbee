import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import colors from '../../config/colors';
import TouchableItem from './TouchableItem';

import ExtraDimensions from 'react-native-extra-dimensions-android';

export default class UserProfilePicture extends Component{
	constructor(props){
		super(props);
		this.state={
			webViewHeight : 0
		};
	}

	componentWillMount(){
		let webViewHeight = ExtraDimensions.get('REAL_WINDOW_HEIGHT') - ExtraDimensions.get('STATUS_BAR_HEIGHT') - ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT') - 50;
		let webViewWidth = ExtraDimensions.get('REAL_WINDOW_WIDTH');
		this.setState({webViewHeight, webViewWidth});
	}

	render(){
		const {  navigator, profilePic, firstName, lastName } = this.props;
		const { webViewHeight, webViewWidth } = this.state;

		return(
			<View style={{backgroundColor : 'white'}}>
				<View style={{ height : 50, backgroundColor : 'white', flexDirection : 'row'}} elevation={5}>
					<TouchableItem borderLess={true} onPress={() => navigator.pop()} style={{ flex : 1, alignItems : 'center', justifyContent : 'center'}}>
						<Icon name='ios-arrow-back' size={25} style={{ color : colors.humbeeGreen}}/>
					</TouchableItem>
					<View style={{ flex : 8, alignItems : 'center', justifyContent : 'center'}}>
						<Text style={{ textAlign : 'center', color : colors.humbeeGreen, fontSize : 16}}>{firstName +' '+ lastName}</Text>
					</View>
					<View style={{flex : 1}}/>
				</View>
				<Image
					source={{uri:profilePic}}
					style={{height : webViewHeight, width : webViewWidth, backgroundColor: 'black'}}
					resizeMode='contain'
				/>
			</View>
		);
	}
}

UserProfilePicture.propTypes={
	navigator : React.PropTypes.object.isRequired,
	profilePic : React.PropTypes.string.isRequired,
	firstName : React.PropTypes.string.isRequired,
	lastName : React.PropTypes.string.isRequired
};
