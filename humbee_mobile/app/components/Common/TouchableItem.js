import React, { Component } from 'react';
import { TouchableNativeFeedback, TouchableOpacity, View, Platform } from 'react-native';

import colors from '../../config/colors';

const LOLLIPOP = 21;

export default class TouchableItem extends Component{
  render() {
    if (Platform.OS === 'android' && Platform.Version >= LOLLIPOP) {
      return (
        <TouchableNativeFeedback
          {...this.props}
          background={TouchableNativeFeedback.Ripple(this.props.rippleColor || colors.humbeeDarkGrey, this.props.borderLess)}
          delayPressIn={0}
          >
          <View style={this.props.style}>
            {this.props.children}
          </View>
        </TouchableNativeFeedback>
      );
    } else {
      return (
        <TouchableOpacity
          {...this.props}
          style={this.props.style}
          activeOpacity={0.6}>
            {this.props.children}
        </TouchableOpacity>
      );
    }
  }
}
