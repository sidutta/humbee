import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import { createRenderer } from '../../utils/RelayUtils';
import NodeQuery from '../../queries/NodeQuery';
import colors from '../../config/colors';

export default class AboutCard extends Component{
	constructor(props){
		super(props);
		const { width } = Dimensions.get('window');

		this.width = width;
	}


	render() {

		const { topTags, rankInLeaderboard, reasons, quizzes } = this.props.viewer;

		let tagComponents = [];

		if(topTags.length==0) {
			tagComponents.push(
				<View style={{ height : 30, borderColor: colors.humbeeDarkGrey, justifyContent: 'center'}}>
					<Text style={{ color: colors.humbeeGreen, marginHorizontal : 3 }}>
						There is nothing to show here
					</Text>
				</View>
			);
		}

		for(let i=0; i<topTags.length; i=i+2) {
			let topTag = topTags[i];
			let topTag2 = topTags[i+1];
			tagComponents.push(
				<View style={{ height : 30, margin : 2, flexDirection : 'row'}}>
					<View style={{ flex : 1, flexDirection : 'row'}}>
						<View style={{ height : 30, width:5, backgroundColor: colors.humbeeDarkGrey, alignItems: 'center', justifyContent: 'center'}}>

						</View>
						<View style={{ height : 30, width:this.width/3, borderWidth: 3, borderColor: colors.humbeeDarkGrey, justifyContent: 'center'}}>
							<Text style={{ color: colors.humbeeGreen, marginHorizontal : 3, fontSize : 14, fontWeight : 'bold', fontFamily : 'helvetica'}}>
								{topTag.text}
							</Text>
						</View>
						<View style={{ height : 30, justifyContent: 'center', marginLeft : 3}}>
							<Text style={{ color: colors.humbeeGreen, textAlign : 'center', fontWeight : 'bold', fontSize : 14, fontFamily : 'helvetica' }}>
								x {topTag.count}
							</Text>
						</View>
					</View>
					{
						topTag2
						?
						<View style={{ flex : 1, flexDirection : 'row'}}>
							<View style={{ height : 30, width:5, backgroundColor: colors.humbeeDarkGrey, alignItems: 'center', justifyContent: 'center'}}>

							</View>
							<View style={{ height : 30, width:this.width/3, borderWidth: 3, borderColor: colors.humbeeDarkGrey, justifyContent: 'center'}}>
								<Text style={{ color: colors.humbeeGreen, marginHorizontal : 3, fontWeight : 'bold', fontSize : 14, fontFamily : 'helvetica'}}>
									{topTag2.text}
								</Text>
							</View>
							<View style={{ height : 30, justifyContent: 'center', marginLeft : 3}}>
								<Text style={{ color: colors.humbeeGreen, textAlign : 'center', fontWeight : 'bold', fontSize : 14, fontFamily : 'helvetica' }}>
									x {topTag2.count}
								</Text>
							</View>
						</View>
						:
						<View />
					}
				</View>
			);
		}

		return(
			<View style={{ padding : 5}}>
				<Text style={{ paddingLeft : 0, padding : 5, fontSize: 16, fontWeight : 'bold', fontFamily : 'helvetica', marginBottom : 5}}>STATS</Text>
				<View style={{ paddingLeft : 0, padding : 5,flexDirection : 'row', alignItems : 'center'}}>
					<Text style={{flex : 1, fontSize: 14, fontWeight : 'bold', fontFamily : 'helvetica', color: colors.humbeeGreen}}>Leaderboard Rank</Text>
					<Text style={{flex : 1, fontSize: 14, fontWeight : 'bold',fontFamily : 'helvetica', color: colors.humbeeGreen}}>{rankInLeaderboard!=-1? rankInLeaderboard: 'unranked'}</Text>
				</View>
				<View style={{ paddingLeft : 0, padding : 5, flexDirection : 'row', alignItems : 'center'}}>
					<Text style={{flex : 1, fontSize: 14, fontWeight : 'bold', fontFamily : 'helvetica', color: colors.humbeeGreen}}>Quizzes taken</Text>
					<Text style={{flex : 1, fontSize: 14, fontWeight : 'bold',fontFamily : 'helvetica', color: colors.humbeeGreen}}>{quizzes.totalCount}</Text>
				</View>
				<View style={{ paddingLeft : 0, padding : 5, flexDirection : 'row', alignItems : 'center'}}>
					<Text style={{flex : 1, fontSize: 14, fontWeight : 'bold', fontFamily : 'helvetica', color: colors.humbeeGreen}}>Reasons Posted</Text>
					<Text style={{flex : 1, fontSize: 14, fontWeight : 'bold', fontFamily : 'helvetica', color: colors.humbeeGreen}}>{reasons.totalCount}</Text>
				</View>
				<Text style={{fontSize: 16, fontFamily : 'helvetica', fontWeight : 'bold', marginTop: 5}}>TOP TAGS</Text>
				{tagComponents}
			</View>
		);
	}

}

// <Text style={{ color : 'black', fontSize : 20, fontFamily : 'helvetica', textAlign : 'center'}}>{ rankInLeaderboard }</Text>
//         <Text style={{ color : 'black', fontSize : 20, fontFamily : 'helvetica', textAlign : 'center'}}>{ followers.totalCount }</Text>
//         <Text style={{ color : 'black', fontSize : 20, fontFamily : 'helvetica', textAlign : 'center'}}>{ following.totalCount }</Text>
//         <Text style={{ color : 'black', fontSize : 20, fontFamily : 'helvetica', textAlign : 'center'}}>{ reasons.totalCount }</Text>
//         <Text style={{ color : 'black', fontSize : 20, fontFamily : 'helvetica', textAlign : 'center'}}>{ quizzes.totalCount }</Text>
//         <Text style={{ color : 'black', fontSize : 20, fontFamily : 'helvetica', textAlign : 'center'}}>{ rankInLeaderboard }</Text>
