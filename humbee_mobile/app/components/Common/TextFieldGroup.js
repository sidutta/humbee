import React, { Component } from 'react';
import { View, TextInput, Text } from 'react-native';

export default class TextFieldGroup extends Component {
	constructor(props){
		super(props);
	}
	render(){
		const { nextField, onChange, value, error , focusNextField, name } = this.props;
		return(
			<View>
				<TextInput
					{...this.props}
					onSubmitEditing={() => focusNextField(nextField)}
					onChangeText={ (value) => onChange(value, name) }
					value={value}/>
				{ error && <Text style={{color : 'tomato', fontSize : 12, fontFamily : 'helvetica', paddingRight : 5}}>{error}</Text>}
			</View>
		);
	}
}
