import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { createRenderer } from '../../utils/RelayUtils';
import NodeQuery from '../../queries/NodeQuery';
import RelayStore from '../../utils/RelayStore';
import Router from '../../routes/Router';
import TouchableItem from './TouchableItem';
import colors from '../../config/colors';
import Icon from 'react-native-vector-icons/Ionicons';
import CreateFollowerMutation from '../../mutations/CreateFollowerMutation';
import { UserCardDummyView } from '../../utils/DummyViews';

import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';


class userCard extends Component{
	constructor(props){
		super(props);
		this.follow = this.follow.bind(this);
		this.userProfile = this.userProfile.bind(this);
	}

	follow({id, isFollowing}){
		RelayStore.commitUpdate(
			new CreateFollowerMutation({
				followingId : id,
				isFollowing : isFollowing
			}),{
				onFailure : () => {
					alert('Oops something went wrong. Please try again.');
				}
			}
		);
	}

	userProfile(){
		const {viewer, navigator} = this.props;
    let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
		// if(viewer.isViewer){
		// 	navigator.push(
		// 		Router.getRoute('viewerProfile')
		// 	);
		// }
		// else {
      if (__DEV__ == false) {
        tracker.trackScreenView('UserProfile-' + viewer.index);
      }
			navigator.push(
				Router.getRoute('userProfile',{
					id : viewer.id
				})
			);
		// }
	}

	render(){
		const { firstName, lastName, profilePic, id, isFollowing, isViewer} = this.props.viewer;
		const { shareButton, followButton, reasonCard} = this.props;
		return(
			<View style={{height : 40, flex : 2, flexDirection : 'row'}}>
				<Image source={{uri : profilePic || 'https://storage.googleapis.com/humbee_images/cartoon-bee-1.jpg'}} style={{flex : reasonCard ? 3 : 2.5, borderRadius : 20}}/>
				<View style={{flex : 16, alignItems : 'center', justifyContent : 'center', flexDirection : 'row', paddingLeft : 10}}>
					<TouchableItem onPress={this.userProfile} style={{flex : 3}}>
						<Text style={{ fontSize : 16, fontFamily : 'helvetica', color : colors.humbeeGreen}}>{firstName + ' ' + lastName}</Text>
					</TouchableItem>
					<View style={{flex : 1}}/>
				</View>
				{
					shareButton ?  (
						<View style={{flex : 3}}/>
					) : (
						(followButton && !isViewer ) ? (
							<TouchableItem
								elevation={5}
								style={{flex : 3, borderRadius : 2, marginTop : 5, marginBottom : 5, marginLeft : 5, alignItems : 'center', justifyContent : 'space-around', backgroundColor : isFollowing ? colors.humbeeGreen : colors.humbeeGrey}}
								borderLess={true}
								onPress={ () => this.follow({id, isFollowing})}>
								{
									(isFollowing) ? (
										<Icon name='md-checkmark' size={20} style={{color : colors.humbeeGrey }}/>
									) : (
										<Icon name='md-person-add' size={20} style={{color : colors.humbeeGreen }}/>
									)
								}
							</TouchableItem>
						) : (
							<View style={{flex : 3}}/>
						)
					)
				}
			</View>
		);
	}
}

// var userCardcon = Relay.createContainer(userCard, {
//   fragments: {
//     viewer: () => Relay.QL`
//       fragment on User {
//         firstName,
//         lastName,
//         profilePic,
//         id,
//         isFollowing,
//         isViewer
//       }
//     `
//   }
// });

// export default class UserCard extends Component {

//   render() {
//     let {id} = this.props;

//     return(
//       <Relay.Renderer
//         Container={userCardcon}
//         queryConfig={new nodeRoute({id})}
//         environment={RelayStore}
//         render={({done, error, props, retry, stale}) => {
//           if(error){
//             alert(error);
//           }
//           else if(props){
//             return <userCardcon {...props} {...this.props}/>;
//           }
//         }}
//       />
//     );
//   }
// }

export default createRenderer(userCard, {
	queries : NodeQuery,
	queriesParams: ({ id }) => ({
		id: id ? id : null,
	}),
	renderLoading : () => UserCardDummyView,
	fragments: {
		viewer: () => Relay.QL`
			fragment on User {
				firstName,
				lastName,
				profilePic,
				id,
        index,
				isFollowing,
				isViewer
			}
		`
	}
});
