import React, { Component } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';

import colors from '../../config/colors';

export default class ReadMore extends Component {
	constructor(props){
		super(props);
		this.state = {
			showAllText : false,
			height : 0
		};
		this.toggle = this.toggle.bind(this);
		this._onLayoutDidChange = this._onLayoutDidChange.bind(this);
	}

	_onLayoutDidChange(e){
		const layout = e.nativeEvent.layout;
		this.setState({height: layout.height});
	}

	toggle(){
		const { showAllText } = this.state;
		this.setState({ showAllText : !showAllText });
	}

	render(){
		const { numberOfLines, text, style } = this.props;

		const {  showAllText, height } = this.state;

		let shouldShowReadMore = false;
		if(height > numberOfLines * 16.5){
			shouldShowReadMore = true;
		}

		let footer;

		if(shouldShowReadMore){
			footer = (showAllText) ? (
				<Text onPress={this.toggle}>Read Less</Text>
			) : (
				<Text onPress={this.toggle}>Read More</Text>
			);
		}

		return(
			<View>
				<View>
					<Text
						numberOfLines={!showAllText ? numberOfLines : 0}
						onLayout={this._onLayoutDidChange}
						style={style}
						selectable={true}>
						{text}
					</Text>
					{footer}
				</View>
			</View>
		);
	}
}
