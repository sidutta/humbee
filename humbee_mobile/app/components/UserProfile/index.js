import Relay from 'react-relay';
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ActivityIndicator,
  Dimensions,
  Alert,
  AsyncStorage
} from 'react-native';

import { createRenderer } from '../../utils/RelayUtils';
import NodeQuery from '../../queries/NodeQuery';
import RelayStore from '../../utils/RelayStore';

import TouchableItem from '../Common/TouchableItem';
import FollowerList from '../Common/FollowerList';

import FollowingList from '../Common/FollowingList';
import ReasonsList from '../Common/ReasonsList';
import QuizResultsList from '../Common/QuizResultsList';

import Icon from 'react-native-vector-icons/Ionicons';

import colors from '../../config/colors';
import CreateFollowerMutation from '../../mutations/CreateFollowerMutation';

import Router from '../../routes/Router';

import AboutCard from '../Common/AboutCard';

import ResetRelayStore from '../../utils/ResetRelayStore';
import { LoginManager } from 'react-native-fbsdk';

const HEADER_MAX_HEIGHT = 190;
const HEADER_MIN_HEIGHT = 60;
const HEADER_SCROLL_DISTANCE = (HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT);

class UserProfile extends Component{

  constructor(props){
    super(props);
    const { width, height } = Dimensions.get('window');
    this.state = {
      scrollY: new Animated.Value(0),
      activeTab  : 0,
      width: width,
    };
    this.search = this.search.bind(this);
    this.goToPage = this.goToPage.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.onLogout = this.onLogout.bind(this);
    this.logout = this.logout.bind(this);
    this.userProfilePicture = this.userProfilePicture.bind(this);
  }

  onLogout() {
    Alert.alert(
      'Logout',
      'Are you sure you want to logout',
      [
        {
          text: 'yes', onPress : this.logout
        },
        {
          text: 'no', onPress : () =>  this.setState({activeTab : 0})
        }
      ]
    );
    this.setState({activeTab : 0});
  }

  logout() {
    const { navigator } = this.props;
    try{
      LoginManager.logOut();
      AsyncStorage.removeItem('token',() => {
        ResetRelayStore();
        navigator.immediatelyResetStack([Router.getRoute('login')],0);
      });
    } catch(e) {
      alert(e)
      // alert('Oops something went wrong. Try again');
    }
  }

  search(){
    this.props.navigator.push(
      Router.getRoute('search')
    );
  }

  userProfilePicture({ profilePic, firstName, lastName}){
    this.props.navigator.push(
      Router.getRoute('userProfilePicture',{
        firstName,
        lastName,
        profilePic
      })
    );
  }

  follow({id, isFollowing}){
    RelayStore.commitUpdate(
      new CreateFollowerMutation({
        followingId : id,
        isFollowing : isFollowing
      }),{
        onFailure : () => {
          alert('Oops something went wrong. Please try again.');
        }
      }
    );
  }

  goToPage(index){
    this.setState({activeTab : index});
    const x = index*this.state.width;
    this.refs['SCROLL_REF_HORIZONTAL'].scrollTo({x:x ,y : 0, animated : true});
    this.refs['SCROLL_REF_VERTICAL']
  }

  onScroll(e){
    const x = (e.nativeEvent.contentOffset.x);
    let { activeTab } = this.state;
    if( x == 0){
      activeTab =0;
    }
    if( x == this.state.width){
      activeTab = 1;
    }
    if( x == 2*this.state.width){
      activeTab = 2;
    }
    if( x == 3*this.state.width){
      activeTab = 3;
    }
    if( x == 4*this.state.width){
      activeTab = 4;
    }

    if(activeTab !== this.state.activeTab){
      this.setState({activeTab});
      this.refs['SCROLL_REF_VERTICAL'].scrollTo({x : 0, y : 0, animated : true});
    }
  }

  render() {

    const { firstName, lastName, isFollowing, followers, following, id, username, profilePic, isViewer} = this.props.viewer;

    const { activeTab, width } = this.state;

    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate : 'clamp'
    });

    const headerPosition = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -HEADER_SCROLL_DISTANCE],
      extrapolate : 'clamp'
    });


    const imageHeight = this.state.scrollY.interpolate({
      inputRange : [0,HEADER_SCROLL_DISTANCE/2, HEADER_SCROLL_DISTANCE],
      outputRange : [80,40,20],
      extrapolate : 'clamp'
    });

    const imagePosition = this.state.scrollY.interpolate({
      inputRange : [0,HEADER_SCROLL_DISTANCE/2, HEADER_SCROLL_DISTANCE],
      outputRange : [5,5+HEADER_SCROLL_DISTANCE/2,-HEADER_SCROLL_DISTANCE/4],
      extrapolate : 'clamp'
    });

    const imageOpacity = this.state.scrollY.interpolate({
      inputRange : [0,HEADER_SCROLL_DISTANCE/4,HEADER_SCROLL_DISTANCE],
      outputRange : [1,1,0],
      extrapolate : 'clamp'
    });
    const imageLeft = this.state.scrollY.interpolate({
      inputRange : [0,HEADER_SCROLL_DISTANCE/2, HEADER_SCROLL_DISTANCE],
      outputRange : [20,30,40],
      extrapolate : 'clamp'
    });

    const textMargin = this.state.scrollY.interpolate({
      inputRange : [0,HEADER_SCROLL_DISTANCE*48/100,HEADER_SCROLL_DISTANCE*49/100, HEADER_SCROLL_DISTANCE],
      outputRange : [200,200,0,0],
      extrapolate : 'clamp'
    });
    const topImageHeight = this.state.scrollY.interpolate({
      inputRange : [0,HEADER_SCROLL_DISTANCE*48/100,HEADER_SCROLL_DISTANCE*49/100, HEADER_SCROLL_DISTANCE],
      outputRange : [0,0,40,40],
      extrapolate : 'clamp'
    });

    return(
        <View style={{ flex : 1}}>
          <View style={{ zIndex : 2,height : 60,flexDirection : 'row', backgroundColor : colors.humbeeGreen, alignItems : 'center'}}>
            <TouchableItem style={{flex : 1, alignItems : 'center', justifyContent : 'space-around'}} onPress={() => this.props.navigator.pop()} borderLess={true}>
              <Icon name='md-arrow-back' style={{color : 'white'}} size={25}/>
            </TouchableItem>
            <Animated.View
              style={{
                flex : 7,
                marginLeft : 10,
                marginTop : textMargin,
                flexDirection : 'row',
                alignItems : 'center'
              }}>
              <Animated.Image source={{uri : profilePic}} style={{ height : topImageHeight,flex : 1, borderRadius : 25}}/>
              <Text
                style={{
                  color: 'white',
                  fontSize: 16,
                  fontFamily : 'helvetica',
                  marginLeft : 10,
                  flex : 5
                }}>{firstName + ' ' + lastName}</Text>
            </Animated.View>
            <TouchableItem style={{ flex : 2, alignItems : 'center', justifyContent : 'space-around'}} onPress={this.search} borderLess={true}>
              <Icon name='md-search' size={25} style={{color : 'white'}}/>
            </TouchableItem>
          </View>
          <ScrollView
            ref='SCROLL_REF_VERTICAL'
            overScrollMode="never"
            tabLabel="ios-person-add"
            onScroll={Animated.event(
              [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}]
            )}>
            <Animated.View style={{zIndex : 3,width : width, position : 'absolute', top : headerPosition,backgroundColor : 'white'}}>
              <View style={{ backgroundColor : colors.humbeeGreen, height : 50}}/>
              <View style={{ height : 50, backgroundColor : 'white', flexDirection : 'row'}}>
                <View style={{ flex : 3}}/>
                {
                  (isViewer)
                  ?
                  (
                    <TouchableItem
                      elevation={5}
                      style={{flex : 1, borderRadius : 2, marginTop : 10, marginBottom : 10, marginRight : 25, alignItems : 'center', justifyContent : 'space-around', backgroundColor : colors.humbeeGreen}}
                      borderLess={false}
                      onPress={this.onLogout}>
                        <Text style={{color : 'white', fontFamily : 'helvetica', fontSize : 16}}>Log Out</Text>
                    </TouchableItem>
                  )
                  :
                  (
                    <TouchableItem
                      elevation={5}
                      style={{flex : 1, borderRadius : 2, marginTop : 10, marginBottom : 10, marginRight : 25, alignItems : 'center', justifyContent : 'space-around', backgroundColor : colors.humbeeGreen}}
                      borderLess={false}
                      onPress={ () => this.follow({id, isFollowing})}>
                      {
                          (isFollowing) ? (
                            <Icon name='md-checkmark' size={25} style={{color : 'white' }}/>
                          ) : (
                            <Text style={{color : 'white', fontFamily : 'helvetica', fontSize : 16}}>Follow</Text>
                          )
                      }
                    </TouchableItem>
                  )
                }
              </View>
              <Animated.View style={[styles.imageContainer, {top : imagePosition, left : imageLeft, opacity : imageOpacity}]}>
                <TouchableOpacity onPress={() => this.userProfilePicture({ profilePic, firstName, lastName})}>
                  <Animated.Image
                    style={[styles.image,{ height : imageHeight, width : imageHeight}]}
                    source={{uri : profilePic}}/>
                </TouchableOpacity>
              </Animated.View>
              <View style={{marginLeft : 20}}>
                <Text style={{fontSize : 18, fontWeight : 'bold', fontFamily : 'helvetica', color : 'black'}}>{firstName + ' ' + lastName}</Text>
              </View>
              <View style={{marginLeft : 20}}>
                <Text style={{fontSize : 14, fontFamily : 'helvetica', color : 'black'}}>{'@' + username}</Text>
              </View>
              <View style={{flexDirection : 'row', padding : 10,marginLeft : 10, alignItems : 'center', justifyContent : 'center'}}>
                <Text style={{ flex : 1,fontSize : 14, fontFamily : 'helvetica', color : 'black'}}>{followers.totalCount} FOLLOWERS</Text>
                <Text style={{ flex : 1,fontSize : 14, fontFamily : 'helvetica', color : 'black'}}>{following.totalCount} FOLLOWING</Text>
                <View style={{ flex : 1}}/>
              </View>
              <View style={{ height : 10, backgroundColor : colors.humbeeGrey}}/>
            </Animated.View>
            <Animated.View style={{ height : headerHeight}}/>
            <ScrollView
              horizontal={true}
              pagingEnabled={true}
              ref={'SCROLL_REF_HORIZONTAL'}
              onScroll={(e) => this.onScroll(e)}
              showsHorizontalScrollIndicator={false}
              style={{flex : 1}}
            >
            {
              (activeTab == 0) ?(
                <TouchableWithoutFeedback style={{ flex : 1}}>
                  <View style={{ padding : 5 , width : width, flex : 1}}>
                    <AboutCard viewer={this.props.viewer} navigator={navigator}/>
                  </View>
                </TouchableWithoutFeedback>
              ) : (
                <View style={{ width : width, padding : 10, justifyContent : 'center'}}>
                  <ActivityIndicator animating={true} color={colors.humbeeGreen} size={'large'}/>
                </View>
              )
            }
            {
              (activeTab == 1) ?(
                <TouchableWithoutFeedback style={{ flex : 1}}>
                  <FollowerList
                    style={{ width : width,flex : 1 }}
                    navigator={this.props.navigator}
                    viewer={this.props.viewer}
                    id={this.props.viewer.id}
                  />
                </TouchableWithoutFeedback>
              ) : (
                <View style={{ width : width, padding : 10,justifyContent : 'center'}}>
                  <ActivityIndicator animating={true} color={colors.humbeeGreen} size={'large'}/>
                </View>
              )
            }
            {
              (activeTab == 2) ?(
                <TouchableWithoutFeedback style={{ flex: 1}}>
                  <FollowingList
                    id={this.props.viewer.id}
                    style={{ width : width, flex : 1}}
                    viewer={this.props.viewer}
                    navigator={this.props.navigator}
                    onScroll={Animated.event(
                      [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}]
                    )}/>
                </TouchableWithoutFeedback>
              ) : (
                <View style={{ width : width, padding : 10, justifyContent : 'center'}}>
                  <ActivityIndicator animating={true} color={colors.humbeeGreen} size={'large'}/>
                </View>
              )
            }
            {
              (activeTab == 3) ?(
                <TouchableWithoutFeedback style={{ flex : 1}}>
                  <ReasonsList
                    id={this.props.viewer.id}
                    style={{ width : width, flex: 1 }}
                    navigator={this.props.navigator}
                    viewer={this.props.viewer}
                    onScroll={Animated.event(
                      [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}]
                    )}/>
                </TouchableWithoutFeedback>
              ) : (
                <View style={{ width : width, padding : 10,justifyContent : 'center'}}>
                  <ActivityIndicator animating={true} color={colors.humbeeGreen} size={'large'}/>
                </View>
              )
            }
            {
              (activeTab == 4) ?(
                <TouchableWithoutFeedback style={{ flex : 1}}>
                  <QuizResultsList
                    id={this.props.viewer.id}
                    style={{ width : width, flex: 1 }}
                    navigator={this.props.navigator}
                    viewer={this.props.viewer}
                    onScroll={Animated.event(
                      [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}]
                    )}/>
                </TouchableWithoutFeedback>
              ) : (
                <View style={{ width : width, padding : 10, justifyContent : 'center'}}>
                  <ActivityIndicator animating={true} color={colors.humbeeGreen} size={'large'}/>
                </View>
              )
            }
            </ScrollView>
          </ScrollView>
          <View elevation={5} style={{ height : 40, backgroundColor : 'white', flexDirection : 'row', alignItems : 'center', justifyContent : 'center'}}>
            <TouchableOpacity onPress={() => this.goToPage(0)} style={{flex  : 1, alignItems : 'center',justifyContent : 'center'}}>
              <Icon
                name={'ios-paper'}
                style={{ color : (activeTab == 0)? colors.humbeeGreen : colors.humbeeGrey}}
                size={25}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.goToPage(1)} style={{flex  : 1, alignItems : 'center',justifyContent : 'center'}}>
              <Icon
                name={'ios-person'}
                style={{ color : (activeTab == 1)? colors.humbeeGreen : colors.humbeeGrey}}
                size={25}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.goToPage(2)} style={{flex : 1,  alignItems : 'center',justifyContent : 'center'}}>
              <Icon
                name={'ios-person-add'}
                style={{ color : (activeTab == 2)? colors.humbeeGreen : colors.humbeeGrey}}
                size={25}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.goToPage(3)} style={{flex : 1,  alignItems : 'center',justifyContent : 'center'}}>
              <Icon
                name={'ios-megaphone'}
                style={{ color : (activeTab == 3)? colors.humbeeGreen : colors.humbeeGrey}}
                size={25}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.goToPage(4)} style={{flex : 1,  alignItems : 'center',justifyContent : 'center'}}>
              <Icon
                name={'ios-ribbon'}
                style={{ color : (activeTab == 4)? colors.humbeeGreen : colors.humbeeGrey}}
                size={25}
              />
            </TouchableOpacity>
          </View>
        </View>
    );
  }
}

UserProfile.propTypes={
  viewer : React.PropTypes.object.isRequired,
  navigator : React.PropTypes.object.isRequired,
  relay : React.PropTypes.object.isRequired
};

const styles = StyleSheet.create({
  card: {
    borderWidth: 1,
    backgroundColor: '#fff',
    borderColor: 'rgba(0,0,0,0.1)',
    margin: 5,
    height: 150,
    padding: 15,
    shadowColor: '#ccc',
    shadowOffset: { width: 2, height: 2, },
    shadowOpacity: 0.5,
    shadowRadius: 3,
  },
  header: {
    overflow: 'hidden',
    backgroundColor : 'white'
  },
  scrollViewContent: {
    flex : 1,
    marginTop : HEADER_MAX_HEIGHT
  },
  imageContainer : {
    position : 'absolute',
    padding : 3,
    backgroundColor : 'white',
    borderRadius : 12
  },
  image: {
    borderRadius : 10
  }
});

export default createRenderer( UserProfile, {
  queries : NodeQuery,
  queriesParams: ({ id }) => ({
    id: id ? id : null,
  }),
  initialVariables : {
    pageSize : 10
  },
  fragments :{
    viewer : ({pageSize}) => Relay.QL`
      fragment on User {
        id,
        index,
        isViewer,
        firstName,
        lastName,
        username,
        profilePic,
        isFollowing,
        topTags {
          text,
          count
        },
        quizzes(first : 1) {
          totalCount
        },
        reasons(first : 1){
          totalCount
        },
        followers(first : 1){
          totalCount
        },
        following(first : 1){
          totalCount
        },
        rankInLeaderboard
      }
    `
  }
});
