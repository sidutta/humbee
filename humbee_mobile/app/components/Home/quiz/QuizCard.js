import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, Image, Text, Platform } from 'react-native';
import colors from '../../../config/colors';

import NodeQuery from '../../../queries/NodeQuery';
import { createRenderer } from '../../../utils/RelayUtils';
import Router from '../../../routes/Router';
import RelayStore from '../../../utils/RelayStore';

import Icon from 'react-native-vector-icons/Ionicons';

import TouchableItem from '../../Common/TouchableItem';

import Share from 'react-native-share';

import CreateQuizLikeMutation from '../../../mutations/CreateQuizLikeMutation';

import { NewsFeedCardsDummyView } from '../../../utils/DummyViews';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

class QuizCard extends Component {

  constructor(props){
    super(props);
    this.state={
      response : false
    };
    this.openQuiz = this.openQuiz.bind(this);
    this.like = this.like.bind(this);
    this.engageModal = this.engageModal.bind(this);
    this.likeModal = this.likeModal.bind(this);
    this.share = this.share.bind(this);
  }

  openQuiz() {
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('QuizScreen-' + this.props.viewer.index);
    }
    this.props.navigator.push(
      Router.getRoute('quizScreen',{
        id : this.props.viewer.id
      })
    );
  }

  like() {
    const { id, liked, numberOfLikes } = this.props.viewer;
    RelayStore.commitUpdate(
      new CreateQuizLikeMutation({
        liked : liked,
        quizId : id,
        numberOfLikes : numberOfLikes
      })
    );
  }


  likeModal() {
    const {navigator, viewer} = this.props;
    const { id, index } = viewer;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('QuizLikeModal-' + index);
    }

    navigator.push(
      Router.getRoute('quizLikeModal',{
        id : id,
        type : 'QuizLikeModal'
      })
    );
  }

  share() {
    const { viewer } = this.props;
    const { title } = viewer;
    let shareItem = {
      title: title,
      message: title + ". For more such engaging quizzes join India's first Political Social Network : HumBee. Download at https://betas.to/YLFhyyeC",
      subject: title  //  for email
    };
    Share.open(shareItem);
  }

  engageModal() {
    const { navigator, viewer } = this.props;
    const { id, index } = viewer;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('QuizEngageModal-' + index);
    }
    navigator.push(
      Router.getRoute('quizEngageModal',{
        id : id,
        type : 'QuizEngageModal'
      })
    );
  }

  render(){
    const { title, imageUrl, numberOfLikes, liked, numberOfComments} = this.props.viewer;
    return(
      <View style={{ padding : 5}}>
        <View style={{ padding : 5, borderRadius : 2, backgroundColor : 'white'}}>
          <Text onPress={this.openQuiz} style={{ fontSize : 16, fontFamily : 'helvetica', textAlign : 'center', marginBottom : 5, color : 'black'}}>{title}</Text>
          {
            (Platform.OS === 'android' && Platform.Version > 21)?
              (<View>
                <Image source={{uri: imageUrl}} style={{flex : 1, height : 160, alignItems : 'center', justifyContent : 'space-around'}}>
                  <View style={{flex : 2}}/>
                  <View style={{ flex : 1, flexDirection : 'row'}}>
                    <View style={{ flex : 2}}/>
                    <TouchableItem
                      style={{ flex : 1, alignItems : 'center', justifyContent : 'space-around'}}
                      borderLess={true}
                      elevation = {5}
                      onPress={this.openQuiz}>
                      <View style={{backgroundColor : colors.humbeeGreen, borderRadius : 2, padding : 5 }}>
                        <Text style={{ color : 'white', fontWeight : 'bold', fontSize : 18, fontFamily : 'helvetica'}}>Take Quiz</Text>
                      </View>
                    </TouchableItem>
                  </View>
                </Image>
              </View>)
              :
              (<Image source={{uri: imageUrl}} style={{height : 160}}>
                <TouchableItem onPress={this.openQuiz} borderLess={false}/>
              </Image>)
          }
          <View style={{flexDirection: 'row'}}>
            <TouchableItem style={{height : 30, padding : 5, paddingRight : 0}} onPress={this.likeModal}>
              <Text style={{fontSize : 14, fontFamily : 'helvetica', color : colors.humbeeGreen}}>
                {numberOfLikes.toString() + (numberOfLikes==1?' like \u00B7 ':' likes \u00B7 ')}
              </Text>
            </TouchableItem>
            <TouchableItem style={{height : 30, padding : 5, paddingLeft : 0}} onPress={this.engageModal}>
              <Text style={{fontSize : 14, fontFamily : 'helvetica', color : colors.humbeeGreen}}>
                {numberOfComments.toString() + (numberOfComments==1?' comment':' comments')}
              </Text>
            </TouchableItem>
          </View>
          <View style={{height : 1,backgroundColor : colors.humbeeGrey,marginTop : 5,marginBottom : 5}}/>
          <View style={{ flex : 1, flexDirection : 'row'}}>
            <TouchableItem
              style={{flex : 1,alignItems : 'center', justifyContent : 'center' }}
              rippleColor={colors.humbeeDarkGrey}
              borderLess={true}
              onPress={this.like}>
              <Icon name='md-thumbs-up' size={20} style={{color : (liked)? (colors.humbeeGreen):(colors.humbeeDarkGrey)}}/>
            </TouchableItem>
            <TouchableItem
              style={{flex : 1,alignItems : 'center', justifyContent : 'center' }}
              rippleColor={colors.humbeeDarkGrey}
              borderLess={true}
              onPress={this.engageModal}>
              <Icon name='ios-text' size={20} style={{color : colors.humbeeDarkGrey}}/>
            </TouchableItem>
            <TouchableItem
              style={{flex : 1, alignItems : 'center', justifyContent : 'center' }}
              rippleColor={colors.humbeeDarkGrey}
              borderLess={true}
              onPress={this.share}>
              <Icon name='md-share-alt' size={20} style={{color : colors.humbeeDarkGrey}}/>
            </TouchableItem>
          </View>
        </View>
      </View>
    );
  }
}

QuizCard.propTypes = {
  navigator : React.PropTypes.object.isRequired,
  viewer : React.PropTypes.object.isRequired
};

export default createRenderer(QuizCard, {
  queries : NodeQuery,
  queriesParams: ({ id }) => ({
    id: id ? id : null,
  }),
  renderLoading : () => NewsFeedCardsDummyView,
  fragments : {
    viewer : () => Relay.QL`
      fragment on Quiz {
        title,
        imageUrl,
        numberOfLikes,
        numberOfComments,
        liked,
        id,
        index
      }
    `
  }
});
