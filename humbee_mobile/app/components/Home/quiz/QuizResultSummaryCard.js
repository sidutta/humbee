import React, {Component} from 'react';
import { View, Text, Image } from 'react-native';
import ReadMore from '../../Common/ReadMore';
import colors from '../../../config/colors';
import Icon from 'react-native-vector-icons/Ionicons';
import EngageModalContainer from '../../Containers/EngageModalContainer';
import ReasonLikeModalContainer from '../../Containers/ReasonLikeModalContainer';

import RelayStore from '../../../utils/RelayStore';

import Router from '../../../routes/Router';

import TouchableItem from '../../Common/TouchableItem';

import CreateQuizLikeMutation from '../../../mutations/CreateQuizLikeMutation';
import Container from '../../Containers/Container';

import QuizScreenContainer from '../../Containers/QuizScreenContainer';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

export default class QuizResultSummaryCard extends Component {

	constructor(props) {
		super(props);
		this.engageModal = this.engageModal.bind(this);
		this.like = this.like.bind(this);
		this.likeModal = this.likeModal.bind(this);
		this.seeQuiz = this.seeQuiz.bind(this);
	}

	engageModal() {
		const { quizId, navigator, index } = this.props;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('QuizEngageModal-' + index);
    }
		navigator.push(
			Router.getRoute('quizEngageModal',{
				id : quizId,
				type : 'QuizEngageModal'
			})
		);
	}

	likeModal() {
		const { quizId, navigator, index } = this.props;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('QuizLikeModal-' + index);
    }
		navigator.push(
			Router.getRoute('quizLikeModal',{
				type : 'QuizLikeModal',
				id : quizId
			})
		);
	}

	like() {
		const { quizId, Quiz } = this.props;
		const { liked, numberOfLikes } = Quiz;
		RelayStore.commitUpdate(
			new CreateQuizLikeMutation({
				liked : liked,
				quizId : quizId,
        numberOfLikes : numberOfLikes
			})
		);
	}

	seeQuiz() {
		const { quizId, navigator, index } = this.props;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('QuizScreen-' + index);
    }
		navigator.push(
			Router.getRoute('quizScreen', {
				id : this.props.quizId
			})
		);
	}

	render() {

		const {id, Quiz, MasterCharacterization, seeQuiz, quizId, navigator} = this.props;
		const {title, numberOfLikes, liked} = Quiz;
		const {masterCharacterization, thumbnail} = MasterCharacterization;

		const containerHeight =  110 + 150;

		return(
				<View elevation={2} style={{ padding : 5, backgroundColor : '#E8F5E9' , borderRadius : 2, marginBottom : 5,marginTop : 5}}>

					<Text style={{textAlign : 'center', fontSize: 16, padding : 5, color : 'black'}}>
						{title}
					</Text>

					<View style={{ height : 150}}>
						<Image source={{uri: thumbnail}} style={{flex: 1, width: null, height: null, resizeMode: 'contain', alignItems: 'center', justifyContent: 'space-around'}}>
						</Image>
					</View>

					<Text style={{textAlign : 'center', fontSize: 14}}>
						{masterCharacterization}
					</Text>

					<TouchableItem style={{height : 20}} onPress={this.likeModal}>
						<Text style={{fontSize : 14, fontFamily : 'helvetica', color : colors.humbeeGreen}}>{numberOfLikes + ' people like this'}</Text>
					</TouchableItem>
					<View style={{ height : 1, backgroundColor : colors.humbeeGrey, marginTop : 5,marginBottom : 5 }}/>
					<View style={{ height : 30, flexDirection : 'row', padding : 2}}>
						<TouchableItem
							style={{flex : 1, flexDirection : 'row', justifyContent : 'center'}}
							rippleColor={colors.humbeeDarkGrey}
							borderLess={true}
							onPress={this.like}>
							<Icon name='md-thumbs-up' size={20} style={{color : (liked)? colors.humbeeGreen : colors.humbeeDarkGrey}}/>
						</TouchableItem>
						<TouchableItem onPress={this.engageModal} style={{flex : 1, flexDirection : 'row', justifyContent : 'center'}} rippleColor={colors.humbeeDarkGrey} borderLess={true}>
							<Icon name='ios-text' size={20} style={{color : colors.humbeeDarkGrey}}/>
						</TouchableItem>
						<View style={{flex : 4}}/>
						<TouchableItem onPress={this.seeQuiz} style={{flex : 2, alignItems : 'center', flexDirection : 'row', justifyContent : 'center', borderRadius : 2, backgroundColor : colors.humbeeGreen}} rippleColor={colors.humbeeDarkGrey} borderLess={true}>
							<Text style={{ fontSize : 14, fontFamily : 'helvetica', color : 'white'}}>See Quiz</Text>
						</TouchableItem>
					</View>
				</View>
		);
	}
}
