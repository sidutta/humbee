import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, Image, Text, Platform } from 'react-native';

import NodeQuery from '../../../queries/NodeQuery';
import { createRenderer } from '../../../utils/RelayUtils';
import Router from '../../../routes/Router';
import RelayStore from '../../../utils/RelayStore';

import colors from '../../../config/colors';

import Icon from 'react-native-vector-icons/Ionicons';

import Share  from 'react-native-share';

import TouchableItem from '../../Common/TouchableItem';

import CreateVideoLikeMutation from '../../../mutations/CreateVideoLikeMutation';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

import { NewsFeedCardsDummyView } from '../../../utils/DummyViews';
class VideoCard extends Component {
  constructor(props){
    super(props);
    this.playVideo = this.playVideo.bind(this);
    this.like = this.like.bind(this);
    this.engageModal = this.engageModal.bind(this);
    this.likeModal = this.likeModal.bind(this);
    this.share = this.share.bind(this);
  }

  playVideo() {
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('VideoPlayer-' + this.props.viewer.index);
    }
    this.props.navigator.push(
      Router.getRoute('videoPlayer',{
        videoId : this.props.viewer.videoId,
        id : this.props.viewer.id,
        title : this.props.viewer.title,
        description : this.props.viewer.description,
        liked : this.props.viewer.liked,
        engageModal : this.engageModal,
        numberOfLikes : this.props.viewer.numberOfLikes,
        like : this.like
      })
    );
  }

  share() {
    const { viewer } = this.props;
    const { title } = viewer;
    let shareItem = {
      title: title,
      message: title + ". For more interesting content join India's first Political Social Network : HumBee. Download at https://betas.to/YLFhyyeC",
      subject: title  //  for email
    };
    Share.open(shareItem);
  }

  like() {
    const { id, liked, numberOfLikes } = this.props.viewer;
    RelayStore.commitUpdate(
      new CreateVideoLikeMutation({
        liked : liked,
        videoId : id,
        numberOfLikes : numberOfLikes
      })
    );
  }

  likeModal(){
    const {navigator, viewer} = this.props;
    const { id, index } = viewer;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('VideoLikeModal-' + index);
    }
    navigator.push(
      Router.getRoute('videoLikeModal',{
        type : 'VideoLikeModal',
        id : id
      })
    );
  }

  engageModal(){
    const { navigator, viewer } = this.props;
    const { id, index } = viewer;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('VideoEngageModal-' + index);
    }
    navigator.push(
      Router.getRoute('videoEngageModal',{
        id : id,
        type : 'VideoEngageModal'
      })
    );
  }

  render(){
    const { title, description, thumbnail, numberOfLikes, liked, numberOfComments} = this.props.viewer;

    return(
      <View style={{ padding : 5}}>
        <View style={{ padding : 5, borderRadius : 2, backgroundColor : 'white'}}>
          <Text style={{ fontSize : 16, fontFamily : 'helvetica', textAlign : 'center', marginBottom : 5, color : 'black'}}>{title}</Text>
          {
            (Platform.OS === 'android' && Platform.Version < 21)?
            (<TouchableItem onPress={this.playVideo}>
              <Image source={{uri: thumbnail}} style={{height : 160, alignItems : 'center', justifyContent : 'space-around'}}>
                <View style={{  height : 160,alignItems : 'center', justifyContent : 'center'}}>
                  <Icon size={50} name='md-play' style={{ color : colors.humbeeGreen}}/>
                </View>
              </Image>
            </TouchableItem>) :
            (<Image source={{uri: thumbnail}} style={{height : 160}}>
              <TouchableItem onPress={this.playVideo} borderLess={false}>
                <View style={{  height : 160,alignItems : 'center', justifyContent : 'center'}}>
                  <View style={{backgroundColor : colors.humbeeGreen, padding : 10, paddingRight : 15, paddingLeft : 15, borderRadius : 5}}>
                  <Icon size={25} name='md-play' style={{ color : 'white'}}/>
                  </View>
                </View>
              </TouchableItem>
            </Image>)
          }
          <Text style={{ fontSize : 14, fontFamily : 'helvetica', textAlign : 'center'}}>{description}</Text>
          <View style={{flexDirection: 'row'}}>
            <TouchableItem style={{height : 30, padding : 5, paddingRight : 0}} onPress={this.likeModal}>
              <Text style={{fontSize : 14, fontFamily : 'helvetica', color : colors.humbeeGreen}}>
                {numberOfLikes.toString() + (numberOfLikes==1?' like \u00B7 ':' likes \u00B7 ')}
              </Text>
            </TouchableItem>
            <TouchableItem style={{height : 30, padding : 5, paddingLeft : 0}} onPress={this.engageModal}>
              <Text style={{fontSize : 14, fontFamily : 'helvetica', color : colors.humbeeGreen}}>
                {numberOfComments.toString() + (numberOfComments==1?' comment':' comments')}
              </Text>
            </TouchableItem>
          </View>
          <View style={{height : 1,backgroundColor : colors.humbeeGrey,marginTop : 5,marginBottom : 5}}/>
          <View style={{ flex : 1, flexDirection : 'row'}}>
            <TouchableItem onPress={this.like} style={{flex : 1,alignItems : 'center', justifyContent : 'center' }} rippleColor={colors.humbeeDarkGrey} borderLess={true}>
              <Icon name='md-thumbs-up' size={20} style={{color : (liked)? (colors.humbeeGreen):(colors.humbeeDarkGrey)}}/>
            </TouchableItem>
            <TouchableItem onPress={this.engageModal} style={{flex : 1,alignItems : 'center', justifyContent : 'center' }} rippleColor={colors.humbeeDarkGrey} borderLess={true}>
              <Icon name='ios-text' size={20} style={{color : colors.humbeeDarkGrey}}/>
            </TouchableItem>
            <TouchableItem
              style={{flex : 1, alignItems : 'center', justifyContent : 'center' }}
              rippleColor={colors.humbeeDarkGrey}
              borderLess={true}
              onPress={this.share}>
              <Icon name='md-share-alt' size={20} style={{color : colors.humbeeDarkGrey}}/>
            </TouchableItem>
          </View>
        </View>
      </View>
    );
  }
}

VideoCard.propTypes = {
  navigator : React.PropTypes.object.isRequired,
  viewer : React.PropTypes.object.isRequired
};

export default createRenderer(VideoCard,{
  queries : NodeQuery,
  queriesParams: ({ id }) => ({
    id: id ? id : null,
  }),
  renderLoading : () => NewsFeedCardsDummyView,
  fragments : {
    viewer : () => Relay.QL`
      fragment on Video{
        id,
        index,
        title,
        videoId,
        thumbnail,
        description,
        numberOfLikes,
        numberOfComments,
        liked
      }
    `
  }
});
