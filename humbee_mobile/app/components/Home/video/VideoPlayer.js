import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, Dimensions, BackAndroid, Text } from 'react-native';
import YouTube from 'react-native-youtube';

import {createRenderer} from '../../../utils/RelayUtils';
import NodeQuery from '../../../queries/NodeQuery';

import TouchableItem from '../../Common/TouchableItem';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../../config/colors';
import RelayStore from '../../../utils/RelayStore';

import ExtraDimensions from 'react-native-extra-dimensions-android';
import Share from 'react-native-share';
import {
			// AndroidBackButtonBehavior.
			getBackButtonManager
		} from '@exponent/ex-navigation';

import Orientation from 'react-native-orientation';

import CreateVideoLikeMutation from '../../../mutations/CreateVideoLikeMutation';

// var Fabric = require('react-native-fabric');
var Analytics = require('react-native-firebase-analytics');

class VideoPlayer extends Component{
	constructor(props){
		super(props);
		this.state={
			hidden : false,
      webViewHeight : 0
		};
    this.share = this.share.bind(this);
    this.like = this.like.bind(this);
	}

  componentWillMount() {
    // Orientation.unlockAllOrientations()
    this.backButtonManager = getBackButtonManager();
    this.backButtonManager.disable();
    let webViewHeight = ExtraDimensions.get('REAL_WINDOW_HEIGHT') - ExtraDimensions.get('STATUS_BAR_HEIGHT') - ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT') - 50;
    this.setState({webViewHeight});
    // Analytics.logEvent("view_item", {
    //   'item_id': this.props.videoId,
    //   'item_location_id': 'VideoPlayer',
    //   'content_type': 'video'
    //   // 'screen': 'DebateScreenOp-' + this.props.viewer.lastDebateId.toString(),
    //   // 'user-id': global.userId
    // })
  }

  componentDidMount() {
    BackAndroid.addEventListener('hardwareBackPress', () => {
      Orientation.lockToPortrait();
      let { width, height } = Dimensions.get('window');
      if(height<width) {
        let temp = width;
        width = height;
        height = temp;
      }
      Dimensions.set({'window':{width:width, height:height}});
      // console.log('ggg', width, height);
      this.backButtonManager.enable();
      this.props.navigator.pop();
      return true;
    });
    // Analytics.logEvent("view_item", {
    //   'item_id': this.props.videoId,
    //   'item_location_id': 'VideoPlayer',
    //   'content_type': 'video'
    //   // 'screen': 'DebateScreenOp-' + this.props.viewer.lastDebateId.toString(),
    //   // 'user-id': global.userId
    // })
  }

  componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress', () => {
      return true;
    });
  }

  share() {
    const { title } = this.props;
    let shareItem = {
      title: title,
      message: title + ". For more join India's first Political Social Network : HumBee. Download at https://betas.to/YLFhyyeC",
      subject: title  //  for email
    };
    Share.open(shareItem);
  }

  like() {
    const { id, liked, numberOfLikes } = this.props.viewer;

    RelayStore.commitUpdate(
      new CreateVideoLikeMutation({
        liked : liked,
        videoId : id,
        numberOfLikes : numberOfLikes
      })
    );
  }

	render(){
		const { navigator, videoId, id, title, description, engageModal, viewer } = this.props;
    const { liked, numberOfLikes } = viewer;
		const { hidden, webViewHeight } = this.state;

    let upperHeight = webViewHeight;

		return(
			<View style={{flex : 1, backgroundColor : colors.humbeeGrey}}>
        <View style={{height: upperHeight}}>
          <Text style={{ fontSize : 25, fontFamily : 'helvetica', textAlign : 'center', marginBottom : 5, color : 'black', marginTop: 20}}>{title}</Text>
  				<YouTube
  					videoId={videoId}
  					play={true}
            controls={1}
            fs={true}
            playsInline={true}
  					hidden={hidden}
  					apiKey='AIzaSyBaYvgoKlhXoKEj3063OgiH3W5Mz7DzrR4'
  					style={{ height: 200, backgroundColor: 'black', marginTop: 80}}
  				/>
  				<TouchableItem style={{alignSelf : 'center'}}
  					onPress={() => {
  						this.setState({hidden : true});
              let { width, height } = Dimensions.get('window');
              if(height<width) {
                let temp = width;
                width = height;
                height = temp;
              }
              Dimensions.set({'window':{width:width, height:height}});
              this.backButtonManager.enable();
              navigator.pop();
  					}}
  					borderLess={true}>
  					<Icon name='md-close' size={50}/>
  				</TouchableItem>
        </View>
        <View elevation={5} style={{ height : 50, flexDirection : 'row', backgroundColor : colors.humbeeGrey}}>
          <TouchableItem
            style={{flex : 1,alignItems : 'center', justifyContent : 'center', flexDirection : 'row' }}
            rippleColor={colors.humbeeDarkGrey}
            borderLess={true}
            onPress={this.like.bind(this)}>
            <Icon name='md-thumbs-up' size={20} style={{flex : 1,color : (liked)? (colors.humbeeGreen):(colors.humbeeDarkGrey), textAlign : 'right'}}/>
            <Text style={{ padding : 10,textAlign : 'left', flex : 1, fontSize : 14, fontFamily : 'helvetica', color : (liked)? (colors.humbeeGreen):(colors.humbeeDarkGrey)}}>{numberOfLikes}</Text>
          </TouchableItem>
          <TouchableItem
            style={{flex : 1,alignItems : 'center', justifyContent : 'center' }}
            rippleColor={colors.humbeeDarkGrey}
            borderLess={true}
            onPress={engageModal}>
            <Icon name='ios-text' size={20} style={{color : colors.humbeeDarkGrey}}/>
          </TouchableItem>
          <TouchableItem
            style={{flex : 1, alignItems : 'center', justifyContent : 'center' }}
            rippleColor={colors.humbeeDarkGrey}
            borderLess={true}
            onPress={this.share}>
            <Icon name='md-share-alt' size={20} style={{color : colors.humbeeDarkGrey}}/>
          </TouchableItem>
        </View>
			</View>
		);
	}
}


export default createRenderer(VideoPlayer,{
  queries : NodeQuery,
  queriesParams: ({ id }) => ({
    id: id ? id : null,
  }),
  fragments : {
    viewer : () => Relay.QL`
      fragment on Video {
        id,
        numberOfLikes,
        liked
      }
    `
  }
});
