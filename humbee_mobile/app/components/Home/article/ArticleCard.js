import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, Image, Text, Platform } from 'react-native';

import NodeQuery from '../../../queries/NodeQuery';
import { createRenderer } from '../../../utils/RelayUtils';
import Router from '../../../routes/Router';
import RelayStore from '../../../utils/RelayStore';

import Icon from 'react-native-vector-icons/Ionicons';

import colors from '../../../config/colors';
import Share from 'react-native-share';

import CreateArticleLikeMutation from '../../../mutations/CreateArticleLikeMutation';

import TouchableItem from '../../Common/TouchableItem';

import { NewsFeedCardsDummyView } from '../../../utils/DummyViews';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';


class ArticleCard extends Component {
  constructor(props){
    super(props);
    this.openArticle = this.openArticle.bind(this);
    this.like = this.like.bind(this);
    this.engageModal = this.engageModal.bind(this);
    this.likeModal = this.likeModal.bind(this);
    this.share = this.share.bind(this);
  }

  openArticle(){
    const { viewer, navigator } = this.props;
    const { id, index } = viewer;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('ArticlePage-' + index);
    }
    navigator.push(
      Router.getRoute('articlePage',{
        id : id,
        engageModal : this.engageModal,
        like : this.like,
      })
    );
  }

  share() {
    const { viewer } = this.props;
    const { title } = viewer;
    let shareItem = {
      title: title,
      message: title + ". For more join India's first Political Social Network : HumBee. Download at https://betas.to/YLFhyyeC",
      subject: title  //  for email
    };
    Share.open(shareItem);
  }


  like({id, liked}){
    const { numberOfLikes } = this.props.viewer;
    RelayStore.commitUpdate(
      new CreateArticleLikeMutation({
        liked : liked,
        articleId : id,
        numberOfLikes : numberOfLikes
      }),
      {
        onSuccess: () => {
          // alert('succcess')
        },
        onFailure: (transaction) => {
          alert(transaction.getError().source.errors[0].message)
        },
      }
    );
  }

  likeModal(){
    const {navigator, viewer} = this.props;
    const { id, index } = viewer;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('ArticleLikeModal-' + index);
    }
    navigator.push(
      Router.getRoute('articleLikeModal',{
        id : id,
        type : 'ArticleLikeModal'
      })
    );
  }

  engageModal(){
    const { navigator, viewer } = this.props;
    const { id, index } = viewer;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('ArticleEngageModal-' + index);
    }
    navigator.push(
      Router.getRoute('articleEngageModal',{
        id : id,
        type : 'ArticleEngageModal'
      })
    );
  }

  render(){
    const { title, description, thumbnail, numberOfLikes, liked, id, numberOfComments} = this.props.viewer;
    return(
      <View style={{padding : 5}}>
        <View style={{ padding : 5, borderRadius : 2, backgroundColor : 'white'}}>
          <Text style={{ fontSize : 16, fontFamily : 'helvetica', textAlign : 'center', marginBottom : 5, color : 'black'}}>{title}</Text>
          {
            (Platform.OS === 'android' && Platform.Version < 21)?
            (<TouchableItem onPress={this.openArticle}>
              <Image resizeMode='contain' source={{uri: thumbnail}} style={{height : 160, alignItems : 'center', justifyContent : 'space-around'}}/>
            </TouchableItem>) :
            (<Image resizeMode='contain' source={{uri: thumbnail}} style={{height : 160}}>
              <TouchableItem onPress={this.openArticle} style={{height : 160}}borderLess={false}/>
            </Image>)
          }
          <Text style={{ fontSize : 14, fontFamily : 'helvetica', textAlign : 'center'}}>{description}</Text>
          <View style={{flexDirection: 'row'}}>
            <TouchableItem style={{height : 30, padding : 5, paddingRight : 0}} onPress={this.likeModal}>
              <Text style={{fontSize : 14, fontFamily : 'helvetica', color : colors.humbeeGreen}}>
                {numberOfLikes.toString() + (numberOfLikes==1?' like \u00B7 ':' likes \u00B7 ')}
              </Text>
            </TouchableItem>
            <TouchableItem style={{height : 30, padding : 5, paddingLeft : 0}} onPress={this.engageModal}>
              <Text style={{fontSize : 14, fontFamily : 'helvetica', color : colors.humbeeGreen}}>
                {numberOfComments.toString() + (numberOfComments==1?' comment':' comments')}
              </Text>
            </TouchableItem>
          </View>
          <View style={{height : 1,backgroundColor : colors.humbeeGrey,marginTop : 5,marginBottom : 5}}/>
          <View style={{ flex : 1, flexDirection : 'row'}}>
            <TouchableItem
              style={{flex : 1,alignItems : 'center', justifyContent : 'center' }}
              rippleColor={colors.humbeeDarkGrey}
              borderLess={true}
              onPress={() => this.like({id, liked})}>
              <Icon name='md-thumbs-up' size={20} style={{color : (liked)? (colors.humbeeGreen):(colors.humbeeDarkGrey)}}/>
            </TouchableItem>
            <TouchableItem
              style={{flex : 1,alignItems : 'center', justifyContent : 'center' }}
              rippleColor={colors.humbeeDarkGrey}
              borderLess={true}
              onPress={this.engageModal}>
              <Icon name='ios-text' size={20} style={{color : colors.humbeeDarkGrey}}/>
            </TouchableItem>
            <TouchableItem
              style={{flex : 1, alignItems : 'center', justifyContent : 'center' }}
              rippleColor={colors.humbeeDarkGrey}
              borderLess={true}
              onPress={this.share}>
              <Icon name='md-share-alt' size={20} style={{color : colors.humbeeDarkGrey}}/>
            </TouchableItem>
          </View>
        </View>
      </View>
    );
  }
}

ArticleCard.propTypes = {
  navigator : React.PropTypes.object.isRequired,
  viewer : React.PropTypes.object.isRequired
};

export default createRenderer(ArticleCard,{
  queries : NodeQuery,
  queriesParams: ({ id }) => ({
    id: id ? id : null,
  }),
  renderLoading : () => NewsFeedCardsDummyView,
  fragments : {
    viewer : () => Relay.QL`
      fragment on Article{
        title,
        url,
        thumbnail,
        description,
        numberOfLikes,
        numberOfComments,
        liked,
        id,
        index
      }
    `
  }
});
