'use strict';
import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, Text, ScrollView, ListView, ActivityIndicator, DrawerLayoutAndroid, Image, AsyncStorage, Alert, RefreshControl } from 'react-native';
import colors from '../../config/colors';
import TouchableItem from '../Common/TouchableItem';

import FCM from 'react-native-fcm';
import RelayStore from '../../utils/RelayStore';
import AddDeviceTokenMutation from '../../mutations/AddDeviceTokenMutation';

import NavBar from '../NavBar';

import ViewerQuery from '../../queries/ViewerQuery';
import { createRenderer } from '../../utils/RelayUtils';
import Router from '../../routes/Router';
import DebateCard from './debate/DebateCard';
import VideoCard from './video/VideoCard';
import ArticleCard from './article/ArticleCard';
import PictoralPostCard from './pictoralPost/PictoralPostCard';
import QuizCard from './quiz/QuizCard';
import Icon from 'react-native-vector-icons/Ionicons';

import {
	GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';


class Home extends Component {

	constructor(props) {
		super(props);
		this.state={
			visible : false,
			type : 'all',
			pageSize : 10,
			activeOption : 'all',
			refreshing : false,
			loading : false,
			dataSource : new ListView.DataSource({
				rowHasChanged: (row1, row2) => { return row1!==row2; }
			})
		};
		this.openDrawer = this.openDrawer.bind(this);
		this.userProfile = this.userProfile.bind(this);
		this.leaderBoard = this.leaderBoard.bind(this);
		this.feedback = this.feedback.bind(this);
		this.filter = this.filter.bind(this);
		this.onEndReached = this.onEndReached.bind(this);
		this.onRefresh = this.onRefresh.bind(this);
		this._renderRow = this._renderRow.bind(this);
	}

	componentDidMount() {

		FCM.getFCMToken().then(token => {
			RelayStore.commitUpdate(
				new AddDeviceTokenMutation({token, deviceType : 'Mobile'})
			);
		});

		FCM.getInitialNotification().then(notif => {
			if(notif.notificationType == 'DebateInvitation' && this.props.automaticLogin){
				this.props.navigator.push(
					Router.getRoute('debateScreen',{
						id : notif.debateId,
						fromCard : true
					})
				);
			}
		});

		// var { Crashlytics } = Fabric;
		// const {user} = this.props.viewer;
		// Crashlytics.setUserName(user.firstName + ' ' + user.lastName);
		// Crashlytics.setUserEmail(user.email);
		// Crashlytics.setUserIdentifier(user.index.toString());

		// var { Answers } = Fabric;
		// Answers.logContentView('timeline', 'timeline', 'timeline', { 'user-id': user.index.toString() });
		// global.userId = user.index.toString();
	}

	componentWillReceiveProps(next) {
		if(next && next.viewer.newsFeedItems) {
			this.setState({
				dataSource: this.state.dataSource.cloneWithRows([ ...next.viewer.newsFeedItems.edges ])
			});
		}
	}

	userProfile() {
		const { navigator, NavBarHeight, viewer} = this.props;
		// navigator.push(
		// 	Router.getRoute('viewerProfile',{
		// 		NavBarHeight : NavBarHeight,
		// 	})
		// );
		if (__DEV__ == false) {
			let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
			tracker.trackScreenView('UserProfile-' + viewer.user.index);
		}
		navigator.push(Router.getRoute('userProfile',{id:viewer.user.id, NavBarHeight : NavBarHeight}));
		this.refs['DRAWER_REF'].closeDrawer();
	}

	onEndReached(){
		let {pageSize}	= this.state;
		pageSize = pageSize + 10;
		this.props.relay.setVariables({pageSize});
		this.setState({pageSize});
	}

	leaderBoard() {
		this.props.navigator.push(
			Router.getRoute('leaderBoard')
		);
		this.refs['DRAWER_REF'].closeDrawer();
	}

	feedback() {
		this.props.navigator.push(
			Router.getRoute('feedback')
		);
		this.refs['DRAWER_REF'].closeDrawer();
	}

	openDrawer() {
		this.refs['DRAWER_REF'].openDrawer();
	}

	componentWillMount(){
		this.props.relay.forceFetch();
	}

	filter(type){
		this.setState({loading : true});
		this.refs['DRAWER_REF'].closeDrawer();
		this.refs['SCROLL_REF'].scrollTo({x : 0, y : 0, animated : true});
		this.props.relay.setVariables({type, pageSize : 10});
		this.setState({pageSize : 10, activeOption : type, loading : false});
	}

	onRefresh() {
		this.setState({refreshing : true});
		this.props.relay.forceFetch();
		let pageSize = 10;
		this.props.relay.setVariables({pageSize});
		this.setState({pageSize, refreshing : false});
		// console.log(this.props.viewer.newsFeedItems.edges)
		// this.setState({
		//   dataSource: this.state.dataSource.cloneWithRows([ ...this.props.viewer.newsFeedItems.edges ])
		// });
		// console.log(this.state.dataSource)
	}

	_renderRow({node}){
		const { navigator } = this.props;
		switch(node.type){
		case 'DebateType':
			return <DebateCard id={node.debateId} key={node.debateId+'D'} type='DebateCard' navigator={navigator}/>;
		case 'VideoType':
			return <VideoCard id={node.videoId} key={node.videoId+'V'} type='VideoCard' navigator={navigator}/>;
		case 'ArticleType' :
			return <ArticleCard id={node.articleId} key={node.articleId+'A'} type='ArticleCard'  navigator={navigator}/>;
		case 'PictoralPostType' :
			return <PictoralPostCard id={node.pictoralPostId} key={node.pictoralPostId+'P'} type='PictoralPostCard'  navigator={navigator}/>;
		case 'QuizType' :
			return <QuizCard id={node.quizId} key={node.quizId+'Q'} type='QuizCard' navigator={navigator}/>;
		}
	}

	render() {
		const { viewer } = this.props;
		const { user } = viewer;
		const { firstName, lastName, profilePic, username } = user;
		const { activeOption, refreshing, dataSource, loading } = this.state;
		const navigationView = (
			<View>
				<TouchableItem
					onPress={ () => this.userProfile() }
					style={{height : 150, width : 300, padding : 10, paddingLeft : 20, backgroundColor : colors.humbeeGreen}}>
					<Image style={{height : 80, width : 80, borderRadius : 50 , marginBottom : 10,}} source={{uri : profilePic }}></Image>
					<Text style={{fontSize : 16, fontFamily : 'helvetica', color : 'white' }}>{firstName +' '+ lastName}</Text>
					<Text style={{fontSize : 16, fontFamily : 'helvetica', color : 'white' }}>@{username}</Text>
				</TouchableItem>
				<ScrollView>
					<TouchableItem
						style={{height : 50, paddingLeft : 20, width : 300, padding : 10, alignItems : 'center', flexDirection : 'row', backgroundColor : (activeOption == 'all')? colors.humbeeGrey : 'white'}}
						onPress={() => this.filter('all')}>
						<Icon name='ios-home' size={25} style={{ flex : 1, color : colors.humbeeGreen}}/>
						<Text style={{ flex : 5, fontSize : 16, fontWeight : 'bold', fontFamily : 'helvetica', color : colors.humbeeGreen }}>Home</Text>
					</TouchableItem>
					<TouchableItem
						style={{height : 50, paddingLeft : 20,  width : 300, padding : 10,alignItems : 'center', flexDirection : 'row', backgroundColor : (activeOption == 'video')? colors.humbeeGrey : 'white'}}
						onPress={() => this.filter('video')}>
						<Icon name='ios-videocam' size={25} style={{ flex : 1, color : colors.humbeeGreen}}/>
						<Text style={{ flex : 5, fontSize : 16, fontWeight : 'bold', fontFamily : 'helvetica', color : colors.humbeeGreen }}>Videos</Text>
					</TouchableItem>
					<TouchableItem
						style={{height : 50, paddingLeft : 20,  width : 300, padding : 10, alignItems : 'center', flexDirection : 'row',backgroundColor : (activeOption == 'quiz')? colors.humbeeGrey : 'white'}}
						onPress={() => this.filter('quiz')}>
						<Icon name='ios-ribbon' size={25} style={{ flex : 1, color : colors.humbeeGreen}}/>
						<Text style={{ flex : 5, fontSize : 16, fontWeight : 'bold', fontFamily : 'helvetica', color : colors.humbeeGreen }}>Quizzes</Text>
					</TouchableItem>
					<TouchableItem
						style={{height : 50, paddingLeft : 20,  width : 300, padding : 10, alignItems : 'center', flexDirection : 'row',backgroundColor : (activeOption == 'debate')? colors.humbeeGrey : 'white'}}
						onPress={() => this.filter('debate')}>
						<Icon name='ios-megaphone' size={25} style={{ flex : 1, color : colors.humbeeGreen}}/>
						<Text style={{ flex : 5, fontSize : 16, fontWeight : 'bold', fontFamily : 'helvetica', color : colors.humbeeGreen }}>Debates</Text>
					</TouchableItem>
					<TouchableItem
						style={{height : 50, paddingLeft : 20,  width : 300, padding : 10, alignItems : 'center', flexDirection : 'row', backgroundColor : (activeOption == 'article')? colors.humbeeGrey : 'white'}}
						onPress={() => this.filter('article')}>
						<Icon name='ios-paper' size={25} style={{ flex : 1, color : colors.humbeeGreen}}/>
						<Text style={{ flex : 5, fontSize : 16, fontWeight : 'bold', fontFamily : 'helvetica', color : colors.humbeeGreen }}>Articles</Text>
					</TouchableItem>
					<TouchableItem
						style={{height : 50, paddingLeft : 20,  width : 300, padding : 10, alignItems : 'center', flexDirection : 'row', backgroundColor : (activeOption == 'pictoralPost')? colors.humbeeGrey : 'white'}}
						onPress={() => this.filter('pictoralPost')}>
						<Icon name='md-happy' size={25} style={{ flex : 1, color : colors.humbeeGreen}}/>
						<Text style={{ flex : 5, fontSize : 16, fontWeight : 'bold', fontFamily : 'helvetica', color : colors.humbeeGreen }}>Memes</Text>
					</TouchableItem>
					<TouchableItem style={{height : 50, paddingLeft : 20,  width : 300, alignItems : 'center', flexDirection : 'row', padding : 10}} onPress={this.leaderBoard}>
						<Icon name='md-clipboard' size={25} style={{ flex : 1, color : colors.humbeeGreen}}/>
						<Text style={{ flex : 5, fontSize : 16, fontWeight : 'bold', fontFamily : 'helvetica', color : colors.humbeeGreen }}>Leaderboard</Text>
					</TouchableItem>
					<TouchableItem style={{height : 50, paddingLeft : 20,  width : 300, alignItems : 'center', flexDirection : 'row', padding : 10}} onPress={this.feedback}>
						<Icon name='ios-list-box-outline' size={25} style={{ flex : 1, color : colors.humbeeGreen}}/>
						<Text style={{ flex : 5, fontSize : 16, fontWeight : 'bold', fontFamily : 'helvetica', color : colors.humbeeGreen }}>Feedback</Text>
					</TouchableItem>
				</ScrollView>
			</View>
		);

		let navbarTitle = 'Home';

		if(activeOption=='video') {
			navbarTitle = 'Videos';
		}
		else if(activeOption=='quiz') {
			navbarTitle = 'Quizzes';
		}
		else if(activeOption=='debate') {
			navbarTitle = 'Debates';
		}
		else if(activeOption=='article') {
			navbarTitle = 'Articles';
		}
		else if(activeOption=='pictoralPost') {
			navbarTitle = 'Memes';
		}

		return (

			<DrawerLayoutAndroid
				drawerWidth={300}
				drawerPosition={DrawerLayoutAndroid.positions.Left}
				ref={'DRAWER_REF'}
				renderNavigationView={() => navigationView}>
				<View style={{flex: 1}}>
					<NavBar openDrawer={this.openDrawer} title={navbarTitle} navigator={this.props.navigator} height={this.props.NavBarHeight}/>
					{
						loading ? (
							<View style={{flex : 1, alignItems : 'center', justifyContent : 'center'}}>
								<ActivityIndicator size='large' color={colors.humbeeGreen}/>
							</View>
						) : (
							<ListView
								style={{flex : 1, backgroundColor : colors.humbeeGrey}}
								ref={'SCROLL_REF'}
								refreshControl={
									<RefreshControl
										refreshing={refreshing}
										onRefresh={this.onRefresh}
										colors={[colors.humbeeGreen,'#ff0000', '#00ff00', '#0000ff']}
									/>
								}
								renderRow={row => this._renderRow(row)}
								dataSource={dataSource}
								onEndReached={this.onEndReached}
								scrollRenderAheadDistance={1000}
								pageSize={3}
							/>
						)
					}
				</View>
			</DrawerLayoutAndroid>

		);
	}
}

Home.propTypes={
	viewer : React.PropTypes.object.isRequired,
	navigator : React.PropTypes.object.isRequired,
	ScrollViewHeight : React.PropTypes.number.isRequired,
	title : React.PropTypes.string.isRequired,
	NavBarHeight : React.PropTypes.number.isRequired,
	relay : React.PropTypes.object.isRequired
};

export default createRenderer(Home, {
	queries : ViewerQuery,
	initialVariables : {
		type : 'all',
		pageSize : 10
	},
	fragments: {
		viewer: () => Relay.QL`
			fragment on Viewer {
				user {
					id,
					index,
					firstName,
					lastName,
					email,
					profilePic,
					username
				},
				newsFeedItems(first : $pageSize, type : $type) {
					pageInfo{
						hasNextPage
					},
					edges{
						node{
							type,
							videoId,
							debateId,
							articleId,
							pictoralPostId,
							quizId
						}
					}
				}
			}
		`
	}
});
