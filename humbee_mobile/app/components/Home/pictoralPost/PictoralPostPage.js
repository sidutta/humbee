import Relay from 'react-relay';
import React, { Component } from 'react';
import { WebView, View, Text, Image } from 'react-native';

import {createRenderer} from '../../../utils/RelayUtils';
import NodeQuery from '../../../queries/NodeQuery';

import Icon from 'react-native-vector-icons/Ionicons';

import colors from '../../../config/colors';
import TouchableItem from '../../Common/TouchableItem';

import Share from 'react-native-share';

import ExtraDimensions from 'react-native-extra-dimensions-android';

class PictoralPostPage extends Component{
  constructor(props){
    super(props);
    this.state={
      webViewHeight : 0
    };
    this.share = this.share.bind(this);
  }

  share() {
    const { viewer } = this.props;
    const { title } = viewer;
    let shareItem = {
      title: title,
      message: title + ". For more join India's first Political Social Network : HumBee. Download at https://betas.to/YLFhyyeC",
      subject: title  //  for email
    };
    Share.open(shareItem);
  }

  componentWillMount(){
    let webViewHeight = ExtraDimensions.get('REAL_WINDOW_HEIGHT') - ExtraDimensions.get('STATUS_BAR_HEIGHT') - ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT') - 100;
    let webViewWidth = ExtraDimensions.get('REAL_WINDOW_WIDTH');
    this.setState({webViewHeight, webViewWidth});
  }

  render(){
    const {  navigator, like, engageModal, viewer } = this.props;
    const { title, url, liked, numberOfLikes, id } = viewer;
    const { webViewHeight, webViewWidth } = this.state;

    let page;

    if(url.endsWith(".jpg")||url.endsWith(".jpeg")||url.endsWith(".png")||url.endsWith(".gif")) {
      page = <Image
                source={{uri:url}}
                style={{height : webViewHeight, width : webViewWidth, backgroundColor: 'black'}}
                resizeMode='contain'
              />
    }
    else {
      page = <WebView
              source={{uri:url}}
              style={{height : webViewHeight, width : webViewWidth, backgroundColor: 'white'}}
              startInLoadingState={false}
              scalesPageToFit={true}
            />;
    }

    return(
      <View style={{backgroundColor : 'white'}}>
        <View style={{ height : 50, backgroundColor : colors.humbeeGreen, flexDirection : 'row'}} elevation={5}>
          <TouchableItem borderLess={true} onPress={() => navigator.pop()} style={{ flex : 1, alignItems : 'center', justifyContent : 'center'}}>
            <Icon name='md-arrow-back' size={25} style={{ color : 'white'}}/>
          </TouchableItem>
          <View style={{ flex : 8, alignItems : 'center', justifyContent : 'center'}}>
            <Text style={{ textAlign : 'center', color : 'white', fontSize : 16}}>{title}</Text>
          </View>
          <View style={{flex : 1}}/>
        </View>
        {page}
        <View elevation={5} style={{ height : 50, flexDirection : 'row', backgroundColor : colors.humbeeGrey}}>
          <TouchableItem
            style={{flex : 1,alignItems : 'center', justifyContent : 'center', flexDirection : 'row' }}
            rippleColor={colors.humbeeDarkGrey}
            borderLess={true}
            onPress={() => like({id, liked})}>
            <Icon name='md-thumbs-up' size={20} style={{flex : 1,color : (liked)? (colors.humbeeGreen):(colors.humbeeDarkGrey), textAlign : 'right'}}/>
            <Text style={{ padding : 10,textAlign : 'left', flex : 1, fontSize : 14, fontFamily : 'helvetica', color : (liked)? (colors.humbeeGreen):(colors.humbeeDarkGrey)}}>{numberOfLikes}</Text>
          </TouchableItem>
          <TouchableItem
            style={{flex : 1,alignItems : 'center', justifyContent : 'center' }}
            rippleColor={colors.humbeeDarkGrey}
            borderLess={true}
            onPress={engageModal}>
            <Icon name='ios-text' size={20} style={{color : colors.humbeeDarkGrey}}/>
          </TouchableItem>
          <TouchableItem
            style={{flex : 1, alignItems : 'center', justifyContent : 'center' }}
            rippleColor={colors.humbeeDarkGrey}
            borderLess={true}
            onPress={this.share}>
            <Icon name='md-share-alt' size={20} style={{color : colors.humbeeDarkGrey}}/>
          </TouchableItem>
        </View>
      </View>
    );
  }
}

PictoralPostPage.propTypes={
  navigator : React.PropTypes.object.isRequired,
  viewer : React.PropTypes.object.isRequired,
  like : React.PropTypes.func.isRequired,
  engageModal : React.PropTypes.func.isRequired
};

export default createRenderer(PictoralPostPage, {
  queries : NodeQuery,
  queriesParams: ({ id }) => ({
    id: id ? id : null,
  }),
  fragments : {
    viewer : () => Relay.QL`
      fragment on PictoralPost {
        id,
        title,
        url,
        numberOfLikes,
        liked
      }
    `
  }
});
