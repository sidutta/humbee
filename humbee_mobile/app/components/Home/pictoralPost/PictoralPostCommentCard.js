import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import TouchableItem from '../../Common/TouchableItem';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../../config/colors';
import ReadMore from '../../Common/ReadMore';

import NodeQuery from '../../../queries/NodeQuery';
import { createRenderer } from '../../../utils/RelayUtils';
import Router from '../../../routes/Router';
import RelayStore from '../../../utils/RelayStore';

import CreatePictoralPostCommentLikeMutation from '../../../mutations/CreatePictoralPostCommentLikeMutation';
import UserCard from '../../Common/UserCard';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';


class PictoralPostCommentCard extends Component {
	constructor(props){
		super(props);
		this.state={
			containerHeight : 60
		};
		this.like = this.like.bind(this);
		this.toggleHeight = this.toggleHeight.bind(this);
		this.likeModal = this.likeModal.bind(this);
	}

	like(){
		const { id, liked } = this.props.viewer;
		RelayStore.commitUpdate(
			new CreatePictoralPostCommentLikeMutation({
				liked : liked,
				pictoralPostCommentId : id
			})
		);
	}

	likeModal(){
		const { id, index } = this.props.viewer;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('PictoralPostCommentLikeModal-' + index);
    }
		this.props.navigator.push(
			Router.getRoute('pictoralPostCommentLikeModal',{
				id : id,
				type : 'PictoralPostCommentLikeModal'
			})
		);
	}

	toggleHeight(containerHeight){
		this.setState({containerHeight});
	}

	render(){
		const { numberOfLikes, text, postedTime, userId, liked } = this.props.viewer;

		return(
			<View style={{backgroundColor : 'white', borderBottomWidth : 1, borderColor : colors.humbeeDarkGrey, marginBottom : 5}}>
				<UserCard navigator={this.props.navigator} id={userId} shareButton={false}/>
				<View style={{ marginLeft:5, marginBottom : 5}}>
					<ReadMore
						style={{fontSize : 14, fontFamily : 'helvetica', color : 'black'}}
						numberOfLines={4}
						textHeight={60}
						text={text}
						toggleHeight={this.toggleHeight}/>
				</View>
				<View style={{flexDirection : 'row', alignItems : 'center', marginBottom : 2}}>
					<TouchableItem style={{ flex : 1}} borderLess={true} onPress={this.like}>
						<Icon name='md-thumbs-up' style={{textAlign : 'center', color : (liked)? colors.humbeeGreen : colors.humbeeDarkGrey}} size={20}/>
					</TouchableItem>
					<TouchableItem style={{ flex : 1}} borderLess={true} onPress={this.likeModal}>
						<Text style={{fontSize : 14, fontFamily : 'helvetica', color : colors.humbeeGreen, textAlign : 'center'}}>{numberOfLikes + ' likes'}</Text>
					</TouchableItem>
					<Text style={{flex : 2, textAlign : 'center'}}>{postedTime + ' ago'}</Text>
				</View>
			</View>
		);
	}
}

export default Relay.createContainer(PictoralPostCommentCard, {
	queries : NodeQuery,
	queriesParams: ({ id }) => ({
		id: id ? id : null,
	}),
	fragments: {
		viewer: () => Relay.QL`
			fragment on PictoralPostComment {
				id,
				text,
				userId,
				postedTime,
				numberOfLikes,
				liked,
        index
			}
		`
	}
});
