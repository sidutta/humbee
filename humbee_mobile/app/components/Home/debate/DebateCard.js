'use strict';

import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import NodeQuery from '../../../queries/NodeQuery';
import Router from '../../../routes/Router';
import RelayStore from '../../../utils/RelayStore';
import { createRenderer } from '../../../utils/RelayUtils';

import Share from 'react-native-share';
import TouchableItem from '../../Common/TouchableItem';

import colors from '../../../config/colors';

import CreatePositionMutation from '../../../mutations/CreatePositionMutation';
import CreateDebateLikeMutation from '../../../mutations/CreateDebateLikeMutation';

import { NewsFeedCardsDummyView } from '../../../utils/DummyViews';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

class DebateCard extends Component {

  constructor(props) {
    super(props);
    this.state={
      response : false
    };
    this.openDebate = this.openDebate.bind(this);
    this.createPosition = this.createPosition.bind(this);
    this.share = this.share.bind(this);
    this.invite = this.invite.bind(this);
    this.likeModal = this.likeModal.bind(this);
  }

  invite(){
    const { title, id, index } = this.props.viewer;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('DebateInviteModal-' + index);
    }
    this.props.navigator.push(
      Router.getRoute('inviteModal',{
        debateId : id,
        debateTitle : title
      })
    );
  }

  like({id, liked}){
    RelayStore.commitUpdate(
      new CreateDebateLikeMutation({
        liked : liked,
        debateId : id
      }),
      {
        onSuccess: () => {
          // alert('succcess')
        },
        onFailure: (transaction) => {
          alert(transaction.getError().source.errors[0].message);
        },
      }
    );
  }

  likeModal(){
    const {navigator, viewer} = this.props;
    const { id, index } = viewer;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('DebateLikeModal-' + index);
    }
    navigator.push(
      Router.getRoute('debateLikeModal',{
        id : id,
        type : 'DebateLikeModal'
      })
    );
  }

  openDebate() {
    this.props.navigator.push(
      Router.getRoute('debateScreen',{
        id : this.props.viewer.id,
        fromCard : true
      })
    );
  }

  share() {
    const { viewer } = this.props;
    const { title, motionShort } = viewer;
    let shareItem = {
      title: title,
      message: title + ' : ' +  motionShort +  ". Answer on India's first Political Social Network : HumBee. Download at https://betas.to/YLFhyyeC",
      subject: title + ' : ' + motionShort //  for email
    };
    Share.open(shareItem);
  }

  createPosition(agree){
    RelayStore.commitUpdate(
      new CreatePositionMutation({
        debateId : this.props.viewer.id,
        agree : agree
      }),{
        onFailure : () => {
          alert('Oops something went wrong. Please try again.');
        },
        onSuccess : () => {
          this.openDebate();
        }
      }
    );
  }

  render() {

    const { viewer } = this.props;
    const { title, motionShort, motionFull, imageUrl, userposition, liked, numberOfLikes, id} = viewer;
    const agreePercent = parseInt(viewer.agree/(viewer.agree + viewer.disagree)*100);
    const disagreePercent = parseInt(viewer.disagree/(viewer.agree + viewer.disagree)*100);

    let rightButtonText;
    if(userposition!=null) {
      if((userposition && disagreePercent==0) || (!userposition && agreePercent==0)) {
        rightButtonText = 'See Debate';
      }
      else {
        rightButtonText = ((userposition)? disagreePercent.toString() + '% Disagreed' : agreePercent.toString() + '% Agreed') + '\nSee Why?';
      }
    }

    return (
      <View style={styles.container} elevation={5} ref='debateCard'>
        <View style={styles.card}>
          <Image source={{uri: imageUrl}} style={styles.imageContainer}>
            <View>
              <Text style={styles.text}>{title + ':'}</Text>
              <Text style={styles.text}>{ motionShort}</Text>
            </View>
          </Image>
          <View style={styles.footer}>
            <View style={styles.motionFullContainer}>
              <Text style={styles.motionFull}>{motionFull}</Text>
            </View>
            <View style={styles.divider}/>
            { (userposition!=null) ?
              (
                <View style={styles.sentimentContainerTouch}>
                  <View style={{ flex  : 1,justifyContent : 'space-around', marginRight : 5}}>
                    <Text style={{ textAlign : 'center', fontSize : 14, fontFamily : 'helvetica', color : (userposition)?'seagreen':'tomato'}}>{(userposition)? agreePercent.toString() + '% Agreed' : disagreePercent.toString() + '% Disagreed'}</Text>
                  </View>
                  <TouchableItem
                    style={{ flex  : 1, borderRadius : 2,justifyContent : 'space-around', backgroundColor : colors.humbeeGreen, marginLeft : 5}}
                    onPress={this.openDebate}
                  >
                    <Text style={{ textAlign : 'center', fontSize : 14, fontFamily : 'helvetica', color : 'white'}}>{rightButtonText}</Text>
                  </TouchableItem>
                </View>
              )
              :
              (
                <View style={styles.sentimentContainerTouch}>
                  <TouchableItem
                    onPress={() => this.createPosition(true)}
                    style={{ flex  : 1, borderRadius : 2,justifyContent : 'space-around', backgroundColor : 'seagreen', marginRight : 5}}>
                    <Text style={{ textAlign : 'center', fontSize : 14, fontFamily : 'helvetica', color : 'white'}}>Agree</Text>
                  </TouchableItem>
                  <TouchableItem
                    onPress={() => this.createPosition(false)}
                    style={{ flex  : 1, borderRadius : 2,justifyContent : 'space-around', backgroundColor : 'tomato', marginLeft : 5}}>
                    <Text style={{ textAlign : 'center', fontSize : 14, fontFamily : 'helvetica', color : 'white'}}>Disagree</Text>
                  </TouchableItem>
                </View>
              )
            }
            <View style={styles.divider}/>
            <View>
              <TouchableItem style={{height : 30, padding : 5, paddingRight : 0}} onPress={this.likeModal}>
                <Text style={{fontSize : 14, fontFamily : 'helvetica', color : colors.humbeeGreen}}>
                  {numberOfLikes.toString() + (numberOfLikes==1?' like':' likes')}
                </Text>
              </TouchableItem>
            </View>
            <View style={styles.divider}/>
            <View style={styles.actionBar}>
              <TouchableItem
                style={styles.actionIconsTouchable}
                rippleColor={colors.humbeeDarkGrey}
                borderLess={true}
                onPress={() => this.like({id, liked})}>
                <Icon name='md-thumbs-up' size={20} style={[styles.actionIcons, {color : liked ? colors.humbeeGreen : colors.humbeeDarkGrey}]}/>
              </TouchableItem>
              <TouchableItem onPress={this.invite} style={styles.actionIconsTouchable} rippleColor={colors.humbeeDarkGrey} borderLess={true}>
                <Icon name='md-person-add' size={20} style={styles.actionIcons}/>
              </TouchableItem>
              <TouchableItem onPress={this.share} style={styles.actionIconsTouchable} rippleColor={colors.humbeeDarkGrey} borderLess={true}>
                <Icon name='md-share-alt' size={20} style={styles.actionIcons}/>
              </TouchableItem>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

DebateCard.propTypes = {
  viewer : React.PropTypes.object.isRequired,
  navigator : React.PropTypes.object.isRequired
};


export default createRenderer(DebateCard,{
  queries : NodeQuery,
  queriesParams: ({ id }) => ({
    id: id ? id : null,
  }),
  renderLoading : () => NewsFeedCardsDummyView,
  fragments : {
    viewer : () => Relay.QL`
      fragment on Debate {
        id,
        index,
        userposition,
        quizId,
        title,
        motionFull,
        motionShort,
        imageUrl,
        agree,
        disagree,
        liked,
        numberOfLikes,
      }
    `
  }
});

let styles = StyleSheet.create({
  card : {
    backgroundColor : 'white',
    borderRadius : 2,
    padding : 5,
    position : 'relative'
  },
  actionBar : {
    flexDirection : 'row',
    alignItems : 'center'
  },
  actionIconsTouchable : {
    justifyContent : 'center',
    flexDirection : 'row',
    flex : 1
  },
  actionIcons : {
    color : colors.humbeeDarkGrey
  },
  container : {
    padding : 5
  },
  motionFull : {
    textAlign : 'center',
    fontSize : 14,
    fontFamily : 'helvetica'
  },
  motionFullContainer : {
    alignItems : 'center',
    padding : 10,
    paddingTop : 2,
    paddingBottom : 2
  },
  footer : {
    flexDirection : 'column'
  },
  imageContainer : {
    height : 180,
    alignItems : 'center',
    justifyContent : 'space-around'
  },
  spacing : {
    height : 60
  },
  text : {
    fontSize: 14,
    fontFamily : 'helvetica',
    textAlign: 'center',
    color: 'white'
  },
  sentimentContainer : {
    padding : 10,
    paddingTop : 2,
    paddingBottom : 2,
    flexDirection : 'column'
  },
  sentimentContainerTouch : {
    padding : 5,
    paddingTop : 2,
    paddingBottom : 2,
    flexDirection : 'row',
    height : 45
  },
  sentimentBarContainer : {
    flexDirection : 'row'
  },
  sentimentTextContainer : {
    flexDirection : 'row',
    alignItems : 'center'
  },
  sentimentText : {
    fontSize : 14,
    fontFamily : 'helvetica',
    color : '#000',
    textAlign : 'center'
  },
  divider : {
    height : 1,
    backgroundColor : colors.humbeeGrey,
    marginTop : 5,
    marginBottom : 5
  }
});
