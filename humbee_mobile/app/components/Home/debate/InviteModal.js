import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, Text, ScrollView, Image } from 'react-native';

import ViewerQuery from '../../../queries/ViewerQuery';
import { createRenderer } from '../../../utils/RelayUtils';
import RelayStore from '../../../utils/RelayStore';

import ExtraDimensions from 'react-native-extra-dimensions-android';

import TouchableItem from '../../Common/TouchableItem';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../../config/colors';
import CheckBox from 'react-native-check-box';
import InviteForDebateMutation from '../../../mutations/InviteForDebateMutation';
import isEmpty from 'lodash/isEmpty';

class InviteModal extends Component{
	constructor(props){
		super(props);
		let invitees =[];
		if(props.viewer.user.followers.edges){
			invitees = props.viewer.user.followers.edges.map(({node}) => {
				return { userId : node.id, invite : true };
			});
		}
		this.state={
			pageSize : 1000,
			scrollViewHeight : 0,
			invitees : invitees,
			isLoading : false
		};
		this.onEndReached = this.onEndReached.bind(this);
		this.handleClick = this.handleClick.bind(this);
		this.invite = this.invite.bind(this);
	}


	onEndReached(){
		let {pageSize}	= this.state;
		pageSize = pageSize + 10;
		this.props.relay.setVariables({pageSize});
		this.setState({pageSize});
	}

	handleClick(index){
		let {invitees} = this.state;
		let {invite, userId} = invitees[index];
		invitees[index] = { userId : userId, invite : !invite};
		this.setState({invitees});
	}

	invite(){
		let {invitees} = this.state;
		let inviteeList = invitees.reduce((last, now) => {
			const { userId, invite} = now;
			if( invite ){
				return [...last, userId];
			} else {
				return last;
			}
		}, []);
		RelayStore.commitUpdate(
			new InviteForDebateMutation(
				{
					inviteeList,
					debateId : this.props.debateId,
					debateTitle : this.props.debateTitle
				}
			),
			{
				onSuccess : () => {
					alert('Invites sent successfully');
					this.props.navigator.pop();
				}
			}
		);
	}

	componentDidMount () {
		let scrollViewHeight = ExtraDimensions.get('REAL_WINDOW_HEIGHT') - ExtraDimensions.get('STATUS_BAR_HEIGHT') - ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT') - 100;
		this.setState({scrollViewHeight});
		const { followers } = this.props.viewer.user;
		const { edges } = followers;
		if(isEmpty(edges)){
			alert('You have no followers');
			this.props.navigator.pop();
		}
	}

	render(){
		const { scrollViewHeight } = this.state;
		const { followers } = this.props.viewer.user;
		const {  invitees } = this.state;
		const { edges, pageInfo } = followers;
		const { hasNextPage } = pageInfo;
		const userCards = edges.map(({node}, index) => {
			return (
					<View key={node.id} style={{ height : 70, padding : 5, flexDirection : 'row', alignItems : 'center', justifyContent : 'space-around'}}>
						<Image style={{ flex : 3, height : 60, borderRadius : 30 }} source={{ uri : node.profilePic }}/>
						<CheckBox
							style={{flex: 14, padding: 5}}
							onClick={()=> this.handleClick(index)}
							leftTextStyle={{ fontSize : 16, fontFamily : 'helvetica', color : 'black'}}
							isChecked={invitees[index].invite}
							leftText={node.firstName + ' ' + node.lastName + '(@' + node.username + ')'}/>
					</View>
				);
		});
		return(
				<View>
					<View elevation={5} style={{height : 50, flexDirection : 'row', backgroundColor : 'white', alignItems : 'center'}}>
						<TouchableItem borderLess={true} onPress={() => this.props.navigator.pop()} style={{ flex : 1, alignItems : 'center', justifyContent : 'center'}}>
							<Icon name='md-arrow-back' size={25} style={{ color : colors.humbeeGreen}}/>
						</TouchableItem>
						<Text style={{flex : 6, color : colors.humbeeGreen, fontSize : 16, fontFamily : 'helvetica', textAlign : 'center'}}>Invite followers to the debate</Text>
						<View style={{flex : 1}}/>
					</View>
					<ScrollView style={{height : scrollViewHeight}}>
						{userCards}
						{ hasNextPage && <TouchableItem style={{ height : 40, borderRadius : 2, alignItems : 'center' }} borderLess={true} onPress={this.onEndReached}>
							<Text style={{ textAlign : 'center', color : colors.humbeeGreen, fontFamily : 'helvetica', fontSize : 16, fontWeight : 'bold' }}> Load More </Text>
						</TouchableItem>}
					</ScrollView>
					<TouchableItem
						elevation={5}
						onPress={this.invite}
						style={{height : 50,backgroundColor : colors.humbeeGreen, alignItems : 'center', justifyContent : 'space-around'}}>
						<Text style={{ color : 'white', fontSize : 16, fontFamily : 'helvetica'}}>Send Invite</Text>
					</TouchableItem>
				</View>
		);
	}
}

export default createRenderer(InviteModal, {
	queries: ViewerQuery,
	initialVariables : {
		pageSize : 20
	},
	fragments: {
		viewer: () => Relay.QL`
			fragment on Viewer{
				user {
					followers(first : $pageSize){
						pageInfo{
							hasNextPage
						},
						edges{
							node{
								id,
								firstName,
								lastName,
								username,
								profilePic
							}
						}
					}
				}
			}
		`
	}
});
