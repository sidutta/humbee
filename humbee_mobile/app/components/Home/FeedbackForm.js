import Relay from 'react-relay';
import React, {Component} from 'react';
import {  WebView, ScrollView, Text, View, StyleSheet, Image, PanResponder } from 'react-native';
import colors from '../../config/colors';
import TouchableItem from '../Common/TouchableItem';

import Icon from 'react-native-vector-icons/Ionicons';

import ExtraDimensions from 'react-native-extra-dimensions-android';

import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

export default class FeedbackForm extends Component {

  constructor(props){
    super(props);
    this.state = {
      scrollViewHeight : 0,
    };
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('Feedback');
    }
  }

  componentWillMount(){
    let scrollViewHeight = ExtraDimensions.get('REAL_WINDOW_HEIGHT') - ExtraDimensions.get('STATUS_BAR_HEIGHT') - ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT') - 60;
    this.setState({scrollViewHeight});
  }

  render() {
    const { navigator } = this.props;
    const { scrollViewHeight } = this.state;
    return (
      <View>
        <View style={{ height : 60, backgroundColor : colors.humbeeGreen, flexDirection : 'row', justifyContent: 'space-between' }} elevation={5}>
          <TouchableItem borderLess={true} onPress={() => navigator.pop()} style={{ marginLeft:5, alignItems : 'center', justifyContent : 'center', width: 25}}>
            <Icon name='md-arrow-back' size={25} style={{ color : 'white'}}/>
          </TouchableItem>
          <View style={{ alignItems : 'center', justifyContent : 'center'}}>
            <Text style={{ textAlign : 'center', color : 'white', fontSize : 16}}>Feedback</Text>
          </View>
          <View style={{width : 35}}/>
        </View>
        <WebView
          source={{uri: 'https://docs.google.com/forms/d/e/1FAIpQLSfhBVfNGsc0dijCUVzVo_-ls29lyilnk1J2jhDDSDmP9CnxQA/viewform'}}
          style={{height : scrollViewHeight}}
        />
      </View>
    );
  }

}
