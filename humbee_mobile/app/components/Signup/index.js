import React, { Component } from 'react';

import RelayStore from '../../utils/RelayStore';
import Router from '../../routes/Router';

import Relay from 'react-relay';
import SignupMutation from '../../mutations/SignupMutation';
import LoginMutation from '../../mutations/SignupMutation';

import {View, ActivityIndicator, KeyboardAvoidingView, Text, TextInput, Dimensions, Keyboard, AsyncStorage, StyleSheet, Image } from 'react-native';

import dismissKeyboard from 'dismissKeyboard';

import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../config/colors';

import ActionButton from '../Common/ActionButton';
import TouchableItem from '../Common/TouchableItem';
import TextFieldGroup from '../Common/TextFieldGroup';

import GetStarted from './GetStarted';
// var Fabric = require('react-native-fabric');
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

export default class Signup extends Component {
	constructor(props){
		super(props);
    const { width, height } = Dimensions.get('window');
		this.state = {
			bottomBar : true,
			email : '',
			username : '',
			firstName : '',
			lastName : '',
			password : '',
			isLoading : false,
      size: { width, height },
			errors : {}
		};
		this.focusNextField = this.focusNextField.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	keyboardDidShow() {
		let bottomBar = false;
		this.setState({bottomBar});
	}

	keyboardDidHide() {
		let bottomBar = true;
		this.setState({bottomBar});
	}

	componentWillMount(){
		this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
		this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
	}

	componentWillUnmount () {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}

	focusNextField(nextField) {
		this.refs[nextField].focus();
	}

  validateEmail(email)  {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let errorState = this.state.errors;
    errorState.email = null;
    this.setState({ errors : errorState , isLoading : false });
    return re.test(email);
  };

  validatePassword(password) {
    let length = password.length;
    if(length < 8) {
      return {valid:false, err:"Password must have atleast 8 characters"};
    }
    let hasLetter = false;
    let hasNum = false;
    let hasSpclChar = false;
    for(let i=0; i<length; i++) {
      let x = password.charCodeAt(i);
      if((x>64&&x<91)||(x>96&&x<123)||(x>127&&x<155)||(x>159&&x<166)) {
        hasLetter = true;
      }
      else if(x>47&&x<58) {
        hasNum = true;
      }
      else {
        hasSpclChar = true;
      }
    }
    if(hasNum&&hasLetter&&hasSpclChar) {
      let errorState = this.state.errors;
      errorState.password = null;
      this.setState({ errors : errorState, isLoading : false });
    }
    return {valid:hasNum&&hasLetter&&hasSpclChar, err:"Password must have atleast one alphabet, number and special character"};
  };

	onSubmit() {
		const {username,email,firstName,lastName,password} = this.state;
    if(!this.validateEmail(email)) {
      let errorState = this.state.errors;
      errorState.email = "Please enter a valid email id!";
      this.setState({ errors : errorState , isLoading : false });
      return;
    }
    let pwdValid = this.validatePassword(password);
    if(!(pwdValid.valid)) {
      let errorState = this.state.errors;
      errorState.password = pwdValid.err;
      this.setState({ errors : errorState, isLoading : false });
      return;
    }
		this.setState({isLoading : true});
		RelayStore.commitUpdate(
			new SignupMutation({
				email:		email,
				password: password,
				firstName: firstName,
				lastName: lastName,
				username : username
			}),
			{
				onFailure: (transaction) => {
          // var { Answers } = Fabric;
					const errorMessage = transaction.getError().source.errors[0].message;
					this.setState({ errors : errorMessage , isLoading : false });
          // Answers.logSignUp('Email', false, {'reason': 'trycatch'});
				},
				onSuccess: (response) => {
					if(response.signup.id){
						dismissKeyboard();
            // var { Answers } = Fabric;
            // Answers.logSignUp('Email', true);
            if (__DEV__ == false) {
              let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
              tracker.trackScreenView('GetStarted');
            }
						this.props.navigator.push(
							Router.getRoute('getStarted',{
								id : response.signup.id,
	              introQuizId : this.props.introQuizId
							})
						);
					}
				}
			}
		);
	}

  _onLayoutDidChange = (e) => {
    const layout = e.nativeEvent.layout;
    this.setState({ size: { width: layout.width, height: layout.height } });
  }

	render(){

		const { email, username, firstName, lastName, password, errors, isLoading } = this.state;
		return(
			<View style={{flex : 1}}>
			{
				(isLoading) ? (
					<View style={styles.content}>
						<View style={styles.loading}>
							<ActivityIndicator size='large' color={colors.humbeeGreen}/>
						</View>
					</View>
				) : (
					<View  style={{flex : 1}} onLayout={this._onLayoutDidChange}>
						<View  style={{flex: 9}}>
		          <View style={[styles.logocontainer, {width: this.state.size.width}]}>
		            <Text
		              style={[styles.logotext]}
		            >
		              HUM
		            </Text>
		              <Image
		                style={{width: 35, height: 35}}
		                source={require('./bee-icon.png')}
		              />
		            <Text
		              style={[styles.logotext]}
		            >
		              BEE
		            </Text>
		          </View>
							<KeyboardAvoidingView
								behavior={'paddingLeft'}
								keyboardShouldPersistTaps={'never'}
								style={{padding : 10}}
								behavior={'padding'}>
								<View style={{flex : 1, flexDirection : 'row'}}>
									<View style={{flex : 1}}>
										{ }
										<TextInput
											placeholder='First Name'
											ref='0'
		                  autoFocus={true}
											returnKeyType={'next'}
											onSubmitEditing={() => this.focusNextField('1')}
											value={firstName}
											blurOnSubmit={false}
											onChangeText={(firstName) => this.setState({firstName})}
                      underlineColorAndroid={colors.humbeeGreen}
		                />
										{ errors.firstName && <Text style={{color : 'tomato', fontSize : 12, fontFamily : 'helvetica', paddingRight : 5}}>{errors.firstName}</Text>}
									</View>
									<View style={{flex : 1}}>
										<TextInput
											placeholder='Last Name'
											ref='1'
											returnKeyType={'next'}
											onSubmitEditing={() => this.focusNextField('2')}
											value={lastName}
											blurOnSubmit={false}
											onChangeText={(lastName) => this.setState({lastName})}
                      underlineColorAndroid={colors.humbeeGreen}
		                />
										{ errors.lastName && <Text style={{color : 'tomato', fontSize : 12, fontFamily : 'helvetica', paddingRight : 5}}>{errors.lastName}</Text>}
									</View>
								</View>
								<View style={{flex : 1, flexDirection : 'row'}}>
									<View style={{flex : 1}}>
										<TextInput
				              placeholder='Email'
				              ref='2'
				              returnKeyType={'next'}
				              onSubmitEditing={() => this.focusNextField('3')}
				              value={email}
				              blurOnSubmit={false}
				              onChangeText={(email) => this.setState({email})}
                      underlineColorAndroid={colors.humbeeGreen}
				            />
		            		{ errors.email && <Text style={{color : 'tomato', fontSize : 12, fontFamily : 'helvetica', paddingRight : 5}}>{errors.email}</Text>}
									</View>
									<View style={{flex : 1}}>
										<TextInput
				              placeholder='Username'
				              ref='3'
				              returnKeyType={'next'}
				              onSubmitEditing={() => this.focusNextField('4')}
				              value={username}
				              blurOnSubmit={false}
				              onChangeText={(username) => this.setState({username})}
                      underlineColorAndroid={colors.humbeeGreen}
				            />
				            { errors.username && <Text style={{color : 'tomato', fontSize : 12, fontFamily : 'helvetica', paddingRight : 5}}>{errors.username}</Text>}
									</View>
								</View>
								<TextInput
									placeholder='Password'
									ref='4'
									returnKeyType={'done'}
									value={password}
									onChangeText={(password) => this.setState({password})}
									secureTextEntry={true}
									onSubmitEditing={this.onSubmit}
                  underlineColorAndroid={colors.humbeeGreen}
		            />
								{ errors.password && <Text style={{color : 'tomato', fontSize : 12, fontFamily : 'helvetica', paddingRight : 5}}>{errors.password}</Text>}
							</KeyboardAvoidingView>
							<View style={{marginTop : 30,alignItems : 'center', justifyContent : 'center', height : 140}}>
								<ActionButton
									backgroundColor={colors.humbeeGreen}
									text='Signup with Humbee'
									iconName='ios-key-outline'
									onPress={this.onSubmit}/>
								<Text style={{textAlign:'center'}}>Or</Text>
								<ActionButton
									backgroundColor='#3B5998'
									text='Signup with Facebook'
									iconName='logo-facebook'
									onPress={() => this.props.fbLogin(this, this.props.introQuizId)}/>
							</View>
						</View>
						{ (this.state.bottomBar) ? (<TouchableItem style={{flex : 1, backgroundColor : colors.humbeeGrey, alignItems:'center', justifyContent:'center'}}
							onPress={this.props.emailLogin}>
							<Text style={{ fontSize : 16, fontFamily : 'helvetica'}}>Already have an account? Login</Text>
						</TouchableItem>) : (<View style={{flex : 1}}/>)
						}
					</View>
				)
			}
			</View>
		);
	}
}

var styles = StyleSheet.create({
  logocontainer: {
    backgroundColor: 'transparent',
    marginTop: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logotext: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 35,
    textAlign: 'center',
    color:'black',
    alignSelf: 'center',
    fontFamily: 'Audrey-Bold',
  },
  content: {
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: '#cfcfcf',
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'lightgray',
  }
});
