import Relay from 'react-relay';
import React, {Component} from 'react';
import { ActivityIndicator, RefreshControl, ListView, ScrollView, Text, View, StyleSheet, Image, Dimensions } from 'react-native';

import RelayStore from '../../utils/RelayStore';

import ViewerQuery from '../../queries/ViewerQuery';
import { createRenderer } from '../../utils/RelayUtils';
import Router from '../../routes/Router';

import CreateFollowerMutation from '../../../mutations/CreateFollowerMutation';
import UserProfileContainer from '../../Containers/UserProfileContainer';

import TouchableItem from '../../Common/TouchableItem';
import colors from '../../../config/colors';
import NavBar from '../../NavBar';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';


const LoadingIndicator = ({ loading }) => (
	loading ? (
		<View style={ styles.loading }>
			<ActivityIndicator
				animating={ true }
				style={[ styles.loading ]}
				size="large"
			/>
		</View>
	) : null
)

class FollowerSuggestionByQuiz extends Component {

	constructor(props) {
		super(props);

		this.state={
			pagination: {},
			pageSize : 4,
			pageSizeOpposite : 4,
			dataSource: new ListView.DataSource({
										rowHasChanged: (row1, row2) => row1 !== row2,
									}),
		};

		// this.prevLength = 0;

		this.userProfile = this.userProfile.bind(this);
		this.follow = this.follow.bind(this);
		this._renderRow = this._renderRow.bind(this);
		this._onRefresh = this._onRefresh.bind(this);
		this._onEndReached = this._onEndReached.bind(this);
	}

	componentDidMount() {
		this.props.relay.setVariables({
			subquizScores: this.props.subquizScoresArg,
			fetchResults3: true
		});
	}

	componentWillReceiveProps(next) {
		if(next && next.viewer.followerSuggestion) {
		//   next.viewer.followerSuggestion.strangers.edges.length != this.prevLength) {
			this.setState({
				dataSource: this.state.dataSource.cloneWithRows([ ...next.viewer.followerSuggestion.friends.edges ])
			});
			// this.prevLength = next.viewer.followerSuggestion.strangers.edges.length;
		}
	}

	userProfile(id, index) {
		const { navigator } = this.props;
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('UserProfile-' + index);
    }
		navigator.push(
			Router.getRoute('userProfile',{
				id : id
			})
		);
	}

	follow(id, isFollowing){
		RelayStore.commitUpdate(
			new CreateFollowerMutation({
				followingId : id,
				isFollowing : isFollowing
			}),{
				onFailure : () => {
					alert('Oops something went wrong. Please try again.');
				}
			}
		);
	}

	_onRefresh() {
		this._onEndReached();
	}

	_onEndReached() {
		const { pagination } = this.state;
		if (!pagination.loading && this.props.viewer.followerSuggestion.friends.pageInfo.hasNextPage) {
			let setPagination = {loading: true};
			this.setState({
				pagination: setPagination,
			})
			let {pageSize}  = this.state;
			pageSize = pageSize + 4;
			this.props.relay.setVariables({pageSize});
			this.setState({pageSize});
		}
		setPagination = {loading: false};
		this.setState({
			pagination: setPagination,
		})
	}

	f(friend) {
		this.follow(friend.node.id, friend.node.isFollowing)
	}

	_renderRow(friend) {
		if (friend.type === 'Loading') {
			return <LoadingIndicator loading={ friend.loading } />
		}
		else {
			let similarity = friend.node.similarity;
			let dissimilarity = 100 - friend.node.similarity;
			if(similarity == 0) similarity = 0.01;
			if(dissimilarity == 0) dissimilarity = 0.01;
			return (
				<View style={{height : 100, borderWidth : 0, flexDirection: 'column',  padding : 8, paddingHorizontal : 18}}>
					<View style={{height : 50, borderWidth : 0, flexDirection: 'row'}}>
						<View style={{height : 50, width : 50, borderWidth : 0}}>
							<TouchableItem onPress={this.userProfile.bind(this, friend.node.id, friend.node.index)} style={{flex : 1}}>
								<Image
									style={{flex: 1, width: null, height: null, resizeMode: 'contain',}}
									source={{uri: friend.node.profilePic}}
								/>
							</TouchableItem>
						</View>
						<View style={{marginLeft : 15, flexDirection: 'column'}}>
							<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
								<View>
								<TouchableItem onPress={this.userProfile.bind(this, friend.node.id, friend.node.index)}>
									<Text style={{fontSize : 16, fontFamily : 'helvetica', fontWeight : 'bold'}}>{friend.node.firstName} {friend.node.lastName} (@{friend.node.username})</Text>
								</TouchableItem>
								</View>
								<View>
								<TouchableItem
										style={[{height : 28, alignItems : 'center', justifyContent : 'center', backgroundColor : friend.node.isFollowing ? colors.humbeeGreen : colors.humbeeGrey, borderRadius : 2, width : 60, paddingVertical : 15}]}
										onPress={ this.f.bind(this, friend) }
									>
										<Text style={{ fontFamily : 'helvetica', fontSize : 14, color : '#004d40', textAlign : 'center'}}>Follow</Text>
								</TouchableItem>
								</View>
							</View>
						</View>
					</View>
					<View style={{height : 30, flexDirection: 'column'}}>
						<View style={{backgroundColor: colors.humbeeGrey, height: 10, borderRadius: 8, paddingHorizontal: 3, justifyContent: 'center'}}>
							<View style={{flexDirection: 'row'}}>
								<View style={{flex: similarity, backgroundColor: colors.humbeeGreen, height: 3, borderTopLeftRadius: 8, borderBottomLeftRadius: 8}} />
								<View style={{flex: dissimilarity, backgroundColor: 'red', height: 3, borderTopRightRadius: 8, borderBottomRightRadius: 8}} />
							</View>
						</View>
						<Text style={{marginLeft:5, marginTop:3, fontSize : 13, fontFamily : 'helvetica', fontWeight : 'bold', textAlign: 'center'}}>{friend.node.similarity}% Similar</Text>
					</View>
				</View>
			);
		}
	}


	render() {
		const { width, navigator } = this.props;
		const windowHeight = (Dimensions.get('window').height);
		const height = windowHeight - windowHeight/10;
		return(
			<View>
				{ this.props.subquizScores!='-1'
					?
					<View style={{flex : 1, backgroundColor: colors.humbeeGrey}}>
						<NavBar navigator={navigator} title="Follower Alignment" opacity={1} />
						<View style={{height : height - 20, padding : 20}}>
							<View style={{flex : 1, backgroundColor: 'white', borderRadius : 2,  paddingVertical : 10}}>
								<View style={{flex : 1}}>
									<View style={{flex : 1, backgroundColor: 'white', borderRadius : 2}}>

										{
											this.props.viewer.followerSuggestion
											?
											<ListView
												style={{flex : 1, marginVertical : 0, backgroundColor: 'white'}}
												enableEmptySections={ true }
												automaticallyAdjustContentInsets={ false }
												dataSource={ this.state.dataSource }
												renderRow={ row => this._renderRow(row) }
												refreshControl={
													<RefreshControl
														refreshing={ false }
														onRefresh={ () => this._onRefresh() }
													/>
												}
												renderHeader={ () => {
														return (
															<View>
																<View style={{marginLeft: 10, marginBottom: 10}}>
																 <Text style={{height:20, fontSize : 16, fontFamily : 'helvetica', fontWeight : 'bold'}}>Your alignment with those you follow</Text>
																</View>
																<View style={{height:1, backgroundColor: colors.humbeeGrey}} />
															</View>
														)
													}
												}
												renderSeparator={ () => {
														return (
															<View style={{height:1, backgroundColor: colors.humbeeGrey}} />
														)
													}
												}
												onEndReached={ () => this._onEndReached() }
												onEndReachedThreshold={ 50 }
											/>
											:
											<View></View>
										}
									</View>
								</View>
							</View>
						</View>
					</View>
					:
					<View>
					</View>
				}
			</View>
		);
	}
}

export default createRenderer(FollowerSuggestionByQuiz, {
	queries: ViewerQuery,
	initialVariables: {
		subquizScores: '-1',
		fetchResults3: false,
		pageSize : 4
	},
	fragments: {
		viewer: () => Relay.QL`
			fragment on Viewer {
				followerSuggestion(subquizScores: $subquizScores) @include(if: $fetchResults3) {
					friends(first:$pageSize) {
						pageInfo{
							hasNextPage
						},
						edges {
							node {
								id,
                index,
								firstName,
								lastName,
								username,
								profilePic,
								similarity,
								isFollowing
							}
						}
					}
				}
			}
		`
	}
});

var styles = StyleSheet.create({
	usecase: {
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column',
		flex: 1,
		width: null, height: null,
		resizeMode: 'contain',
	},
	promoView: {
		backgroundColor: 'transparent',

	},
	text: {
		color: 'black',
		textAlign: 'center'
	},
	promoHeader: {
		fontSize: 42,
		fontWeight: '700',
		alignSelf: 'center'
	},
	promoDescription: {
		fontSize: 22,
		fontWeight: '300',
		width: 220,
		alignSelf: 'center',
	},
	promoText: {
		fontSize: 14,
		fontWeight: '400',
		alignSelf: 'center',
		lineHeight: 22,
		width: 220,
		color: 'black',
	},
	loading: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		paddingVertical: 10
	}
});
