import React , {Component} from 'react';
import { View, Text, AsyncStorage, Image, ActivityIndicator } from 'react-native';
import dismissKeyboard from 'dismissKeyboard';

import RelayStore from '../../utils/RelayStore';
import Router from '../../routes/Router';

import QuizScreenContainer from '../Containers/QuizScreenContainer';

import SetProfilePictureMutation from '../../mutations/SetProfilePictureMutation';

// import setNetworkLayer from '../../utils/setNetworkLayer';

import TouchableItem from '../Common/TouchableItem';
import Icon from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-crop-picker';
import colors from '../../config/colors';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

export default class GetStarted extends Component {
	constructor(props){
		super(props);
		this.state = {
			error : '',
			isLoading : false,
			image : ''
		};
		this.pickImage = this.pickImage.bind(this);
		this.uploadImage = this.uploadImage.bind(this);
	}


	pickImage(){
		ImagePicker.openPicker({
			width: 300,
			height: 300,
			cropping: true,
			cropperCircleOverlay: true,
			includeBase64: true
		}).then(image => {
			this.setState({
				image: `data:${image.mime};base64,`+ image.data
			});
		}).catch(() => alert('Oops something went wrong!! Please try again.'));
	}

	uploadImage({skip}){
		const { image } = this.state;
		const { id } = this.props;
		this.setState({isLoading : true});
		RelayStore.commitUpdate(
			new SetProfilePictureMutation({
				id : id,
				image : (skip)? 'skip' : image
			}),
			{
				onFailure : () => {
					this.setState({isLoading : false});
					alert('Oops something went wrong! Please try again');
				},
				onSuccess : (response) => {
					if(response.setProfilePicture.token){
						dismissKeyboard();
						try {
							AsyncStorage.setItem('token', response.setProfilePicture.token); // eslint-disable-line
							// try{
							// 	setNetworkLayer(response.setProfilePicture.token);
							// } catch(error){
							// 	alert('Oops something went wrong!');
							// }
							// this.props.navigator.push({
							//  component : HomeContainer,
							//  passProps : {
							//    title : 'Home',
							//    token : response.setProfilePicture.token
							//  }
							// });
              if (__DEV__ == false) {
                let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
                tracker.trackScreenView('IntroQuizScreen');
              }
							this.props.navigator.push(
								Router.getRoute('quizScreen',{
									id : this.props.introQuizId,
									isIntroQuiz : true
								})
							);
						} catch (error) {
							alert('Oops something went wrong!');
						}
					}
				}
			}
		);
	}

	render(){
		const { image, isLoading } = this.state;
		return(
			<View style={{ backgroundColor : colors.humbeeGrey, flex: 1}}>
				{
					isLoading ? (
						<View>
							<ActivityIndicator
								animating={true}
								color={colors.humbeeGreen}
								style={{
									alignItems: 'center',
									justifyContent: 'center',
									paddingTop: 40,
									paddingBottom : 5,
									height: 200}}
								size="large"/>
							<Text style={{ textAlign : 'center', fontSize : 24, fontFamily : 'helvetica', color : colors.humbeeGreen}}>Setting up your profile</Text>
						</View>
					) : (
						<View>
							<TouchableItem onPress={() => this.uploadImage({skip : true})} style={{ height : 50,padding : 10, flexDirection : 'row'}}>
								<View style={{ flex : 14}}/>
								<Text style={{ textAlign : 'right', fontSize : 20, fontFamily : 'helvetica', fontWeight : 'bold', color : colors.humbeeGreen, flex : 4}}>
									Skip
								</Text>
								<Icon name='ios-arrow-forward' size={25} style={{textAlign : 'right', color : colors.humbeeGreen, flex : 1}}/>
							</TouchableItem>
							<View style={{justifyContent : 'center', alignItems : 'center'}}>
								<Text style={{ fontSize : 20,padding : 10, fontFamily : 'helvetica', color : colors.humbeeGreen, fontWeight : 'bold', alignItems : 'center'}}>Set a profile picture</Text>
								{
									image ?
									(
										<View style={{ padding : 10}}>
											<Image style={{ height : 300, width : 300,borderRadius : 150}} source={{uri : image}}/>
											<TouchableItem onPress={this.pickImage} style={{ alignItems : 'center', justifyContent : 'center', padding : 10}}>
												<Icon name='md-camera' size={40} style={{textAlign : 'center', color : colors.humbeeGreen}}/>
											</TouchableItem>
											<TouchableItem onPress={() => this.uploadImage({skip : false})} style={{ backgroundColor : colors.humbeeGreen,flexDirection : 'row',alignItems : 'center', justifyContent : 'center', padding : 10, borderRadius : 2, height : 50}}>
												<Icon name='md-checkmark' size={30} style={{ color : 'white', marginRight : 5}}/>
												<Text style={{ fontSize : 24, fontFamily : 'helvetica', color : 'white'}}>
													Upload Image
												</Text>
											</TouchableItem>
										</View>
									):(
										<View style={{ height : 300, width : 300, borderRadius : 150, alignItems : 'center',justifyContent : 'center', backgroundColor : 'white', padding : 10}}>
											<Text style={{ textAlign : 'center', fontSize : 30, fontFamily : 'helvetica' }}>
												Choose{'\n'}
												Profile Picture{'\n'}
												From Gallery
											</Text>
											<TouchableItem onPress={this.pickImage} style={{ alignItems : 'center', justifyContent : 'center', padding : 10}}>
												<Icon name='md-camera' size={40} style={{color : colors.humbeeGreen}}/>
											</TouchableItem>
										</View>
									)
								}
								</View>
							</View>
					)
				}
				</View>
		);
	}
}

GetStarted.propTypes={
	navigator : React.PropTypes.object.isRequired,
	id : React.PropTypes.number.isRequired
};
