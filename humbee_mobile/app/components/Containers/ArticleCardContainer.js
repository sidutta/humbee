'use strict';


import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import { ActivityIndicator } from 'react-native';
import colors from '../../config/colors';
import ArticleCard from '../Home/article/ArticleCard';

import articleRoute from '../../routes/articleRoute';

export default class ArticleCardContainer extends Component {
	render() {
		const { articleId } = this.props;
		return (
			<RootContainer
				Component={ArticleCard}
				route={new articleRoute({articleId})}
				renderFetched={(data) => <ArticleCard {...this.props} {...data} />}
				renderLoading={() => <ActivityIndicator
																animating={true}
																color={colors.humbeeGreen}
																style={{
																	alignItems: 'center',
																	justifyContent: 'center',
																	padding: 40,
																	height: 200}}
																size="large"/>}/>

		);
	}
}
