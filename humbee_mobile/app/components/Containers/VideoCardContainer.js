'use strict';


import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import { ActivityIndicator } from 'react-native';
import colors from '../../config/colors';
import VideoCard from '../Home/video/VideoCard';

import videoRoute from '../../routes/videoRoute';

export default class VideoCardContainer extends Component {
	render() {
		const { videoId } = this.props;
		return (
			<RootContainer
				Component={VideoCard}
				route={new videoRoute({videoId})}
				renderFetched={(data) => <VideoCard {...this.props} {...data} />}
				renderLoading={() => <ActivityIndicator
																animating={true}
																color={colors.humbeeGreen}
																style={{
																	alignItems: 'center',
																	justifyContent: 'center',
																	padding: 40,
																	height: 200}}
																size="large"/>}/>

		);
	}
}
