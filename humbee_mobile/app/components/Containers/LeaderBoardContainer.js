'use strict';


import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import { ActivityIndicator } from 'react-native';

import colors from '../../config/colors';

import LeaderBoard from '../LeaderBoard';

import homeRoute from '../../routes/homeRoute';

export default class LeaderBoardContainer extends Component {
	render() {
		return (
			<RootContainer
				Component={LeaderBoard}
				route={new homeRoute()}
				renderFetched={(data) => <LeaderBoard {...this.props} {...data} />}
				forceFetch={true}
				renderLoading={ () => <ActivityIndicator
																animating={true}
																color={colors.humbeeGreen}
																style={{
																	alignItems: 'center',
																	justifyContent: 'center',
																	padding: 40,
																	height: 200}}
																size="large"
															/>
											}
			/>
		);
	}
}
