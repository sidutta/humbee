'use strict';

import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import ActionButton from '../Common/ActionButton';

import colors from '../../config/colors';

import { ActivityIndicator, View, Text } from 'react-native';

import DebateScreen from '../DebateScreen';

import debateRoute from '../../routes/debateRoute';

export default class DebateScreenContainer extends Component {
	render() {
		const { debateId, quizId } = this.props;
		return (
			<RootContainer
				Component={DebateScreen}
				route={new debateRoute({debateId, quizId})}
				renderFetched={(data) => <DebateScreen {...this.props} {...data} />}
				renderLoading={ () => <ActivityIndicator
																animating={true}
																color={colors.humbeeGreen}
																style={{
																	alignItems: 'center',
																	justifyContent: 'center',
																	padding: 40,
																	height: 200}}
																size="large"
															/>
											}
				renderFailure={
					(error, retry) => {
						<View>
							<Text>Ooops something went wrong!!! Please click below to retry</Text>
							<ActionButton
								backgroundColor={colors.humbeeGreen}
								text='Post your reason'
								iconName='ios-create-outline'
								onPress={retry}
							/>
						</View>;
					}
				}
			/>
		);
	}
}
