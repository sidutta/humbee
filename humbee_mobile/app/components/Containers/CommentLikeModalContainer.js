'use strict';


import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import CommentLikeModal from '../DebateScreen/Debate/CommentLikeModal';

import commentLikeModalRoute from '../../routes/commentLikeModalRoute';

export default class ReasonLikeModalContainer extends Component {
	render() {
		const { commentId } = this.props;
		return (
			<RootContainer
				Component={CommentLikeModal}
				route={new commentLikeModalRoute({commentId})}
				renderFetched={(data) => <CommentLikeModal {...this.props} {...data} />}
			/>
		);
	}
}
