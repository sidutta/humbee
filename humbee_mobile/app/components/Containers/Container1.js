'use strict';

import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native';
import colors from '../../config/colors';
import {
	RootContainer
} from 'react-relay';
import ArticleCommentCard from '../Home/article/ArticleCommentCard';
import ArticleEngageModal from '../Home/article/ArticleEngageModal';
import ArticleLikeModal from '../Home/article/ArticleLikeModal';
import ArticleCommentLikeModal from '../Home/article/ArticleCommentLikeModal';
import VideoCommentCard from '../Home/video/VideoCommentCard';
import VideoEngageModal from '../Home/video/VideoEngageModal';
import VideoLikeModal from '../Home/video/VideoLikeModal';
import VideoCommentLikeModal from '../Home/video/VideoCommentLikeModal';
import QuizCommentCard from '../Home/quiz/QuizCommentCard';
import QuizEngageModal from '../Home/quiz/QuizEngageModal';
import QuizLikeModal from '../Home/quiz/QuizLikeModal';
import QuizCommentLikeModal from '../Home/quiz/QuizCommentLikeModal';


import nodeRoute from '../../routes/nodeRoute';

export default class Container extends Component {
	render() {
		const { id, type } = this.props;
		let component;
		switch(type){
		case 'ArticleCommentCard':
			component = ArticleCommentCard;
			break;
		case 'ArticleEngageModal' :
			component = ArticleEngageModal;
			break;
		case 'ArticleLikeModal' :
			component = ArticleLikeModal;
			break;
		case 'ArticleCommentLikeModal' :
			component = ArticleCommentLikeModal;
			break;
		case 'QuizCommentCard':
			component = QuizCommentCard;
			break;
		case 'QuizEngageModal' :
			component = QuizEngageModal;
			break;
		case 'QuizLikeModal' :
			component = QuizLikeModal;
			break;
		case 'QuizCommentLikeModal' :
			component = QuizCommentLikeModal;
			break;
		case 'VideoCommentCard':
			component = VideoCommentCard;
			break;
		case 'VideoEngageModal' :
			component = VideoEngageModal;
			break;
		case 'VideoLikeModal' :
			component = VideoLikeModal;
			break;
		case 'VideoCommentLikeModal' :
			component = VideoCommentLikeModal;
			break;
		}
		return (
			<RootContainer
				Component={component}
				route={new nodeRoute({id})}
				renderFetched={(data) => {
					switch(type){
					case 'ArticleCommentCard':
						return <ArticleCommentCard {...this.props} {...data} />;
					case 'ArticleEngageModal' :
						return <ArticleEngageModal {...this.props} {...data} />;
					case 'ArticleLikeModal' :
						return <ArticleLikeModal {...this.props} {...data} />;
					case 'ArticleCommentLikeModal' :
						return <ArticleCommentLikeModal {...this.props} {...data}/>;
					case 'QuizCommentCard':
						return <QuizCommentCard {...this.props} {...data} />;
					case 'QuizEngageModal' :
						return <QuizEngageModal {...this.props} {...data} />;
					case 'QuizLikeModal' :
						return <QuizLikeModal {...this.props} {...data} />;
					case 'QuizCommentLikeModal' :
						return <QuizCommentLikeModal {...this.props} {...data}/>;
					case 'VideoCommentCard':
						return <VideoCommentCard {...this.props} {...data} />;
					case 'VideoEngageModal' :
						return <VideoEngageModal {...this.props} {...data} />;
					case 'VideoLikeModal' :
						return <VideoLikeModal {...this.props} {...data} />;
					case 'VideoCommentLikeModal' :
						return <VideoCommentLikeModal {...this.props} {...data}/>;
					}
				}}
				renderLoading={ () => <ActivityIndicator
																animating={true}
																color={colors.humbeeGreen}
																style={{
																	alignItems: 'center',
																	justifyContent: 'center',
																	padding: 40,
																	height: 200}}
																size="large"
															/>
											}
			/>
		);
	}
}

Container.propTypes ={
	id : React.PropTypes.string.isRequired,
	type : React.PropTypes.string.isRequired
};
