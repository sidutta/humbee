'use strict';


import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import { ActivityIndicator } from 'react-native';
import colors from '../../config/colors';
import DebateCard from '../Home/debate/DebateCard';

import debateRoute from '../../routes/debateRoute';

export default class DebateCardContainer extends Component {
	render() {
		const { debateId } = this.props;
    let quizId = 1;
		return (
			<RootContainer
				Component={DebateCard}
				route={new debateRoute({debateId,quizId})}
				renderFetched={(data) => <DebateCard {...this.props} {...data} />}
				renderLoading={() => <ActivityIndicator
																animating={true}
																color={colors.humbeeGreen}
																style={{
																	alignItems: 'center',
																	justifyContent: 'center',
																	padding: 40,
																	height: 200}}
																size="large"/>}/>

		);
	}
}
