import Relay  from 'react-relay';
import React, { Component } from 'react';
import RelayStore from '../../utils/RelayStore';
import nodeRoute from '../../routes/nodeRoute';
import DebateCard from '../Home/debate/DebateCard';
import ArticleCard from '../Home/article/ArticleCard';
import VideoCard from '../Home/video/VideoCard';
import QuizCard from '../Home/quiz/QuizCard';
import UserCard from '../Common/UserCard';
import DebateTitleCard from '../Common/DebateTitleCard';
import ArticleCommentCard from '../Home/article/ArticleCommentCard';
import QuizCommentCard from '../Home/quiz/QuizCommentCard';
import VideoCommentCard from '../Home/video/VideoCommentCard';
import CommentCard from '../DebateScreen/Debate/CommentCard';
import QuestionScreen from '../DebateScreen/Opinions/QuestionScreen';
import Opinions from '../DebateScreen/Opinions';


export default class Container extends Component {
	render(){
		const { id, type } = this.props;

		let component;

		switch(type){
		// case 'VideoCard' :
		// 	component = VideoCard;
		// 	break;
		// case 'ArticleCard' :
		// 	component = ArticleCard;
		// 	break;
		// case 'DebateCard' :
		// 	component = DebateCard;
		// 	break;
		// case 'QuizCard' :
		// 	component = QuizCard;
		// 	break;
		// case 'DebateTitleCard' :
		// 	component = DebateTitleCard;
		// 	break;
		// case 'UserCard' :
		// 	component = UserCard;
		// 	break;
		// case 'ArticleCommentCard' :
		// 	component = ArticleCommentCard;
		// 	break;
		// case 'VideoCommentCard' :
		// 	component = VideoCommentCard;
		// 	break;
		case 'QuizCommentCard' :
			component = QuizCommentCard;
			break;
		case 'CommentCard' :
			component = CommentCard;
			break;
		// case 'QuestionScreen' :
		// 	component = QuestionScreen;
		// 	break;
		// case 'Opinions' :
		// 	component = Opinions;
		// 	break;
		}
		return(
			<Relay.Renderer
				Container={component}
				queryConfig={new nodeRoute({id})}
				environment={RelayStore}
				render={({done, error, props, retry, stale}) => {
					if(error){
						console.log(error);
					}
          else if(props){
						switch(type){
						case 'VideoCard' :
							return <VideoCard {...props} {...this.props}/>;
						case 'ArticleCard' :
							return <ArticleCard {...props} {...this.props}/>;
						case 'DebateCard' :
							return <DebateCard {...props} {...this.props}/>;
						case 'QuizCard' :
							return <QuizCard {...props} {...this.props}/>;
						case 'DebateTitleCard' :
							return <DebateTitleCard {...props} {...this.props}/>;
						case 'UserCard' :
							return <UserCard {...props} {...this.props}/>;
						case 'ArticleCommentCard' :
							return <ArticleCommentCard {...props} {...this.props}/>;
						case 'VideoCommentCard' :
							return <VideoCommentCard {...props} {...this.props}/>;
						case 'QuizCommentCard' :
							return <QuizCommentCard {...props} {...this.props}/>;
						case 'CommentCard' :
							return <CommentCard {...props} {...this.props}/>;
						case 'QuestionScreen' :
              return <QuestionScreen {...props} {...this.props}/>;
						case 'Opinions' :
							return <Opinions {...props} {...this.props}/>;
						}
					}
				}}
			/>
		);
	}
}
