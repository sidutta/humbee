'use strict';

import React, { Component } from 'react';
import Relay from 'react-relay';

import RelayStore from '../../utils/RelayStore';

import RecommendationByQuiz from '../DebateScreen/Opinions/RecommendationByQuiz';

import recommendationByQuizRoute from '../../routes/recommendationByQuizRoute';

export default class RecommendationByQuizCardContainer extends Component {
	render() {
		const { masterCharacterizationId } = this.props;
		return (
			<Relay.Renderer
				Component={RecommendationByQuiz}
				queryConfig={new recommendationByQuizRoute({masterCharacterizationId})}
				environment={RelayStore}
				render={({done, error, props, retry, stale}) => {
					if(error){
						console.log(error);
					} else if(props){
						return <RecommendationByQuiz {...this.props} {...props}/>;
					}
				}}
			/>
		);
	}
}
