'use strict';


import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import { ActivityIndicator } from 'react-native';
import colors from '../../config/colors';

import Search from '../Search';

import homeRoute from '../../routes/homeRoute';

export default class SearchContainer extends Component {
	render() {
		return (
			<RootContainer
				Component={Search}
				route={new homeRoute()}
				renderFetched={(data) => <Search {...this.props} {...data} />}
				renderLoading={() => <ActivityIndicator
																animating={true}
																color={colors.humbeeGreen}
																style={{
																	alignItems: 'center',
																	justifyContent: 'center',
																	padding: 40,
																	height: 200}}
																size="large"/>}/>
		);
	}
}
