'use strict';

import React, { Component } from 'react';
import Relay from 'react-relay';

import RelayStore from '../../utils/RelayStore';

import QuizResultScreen from '../DebateScreen/Opinions/QuizResultScreen';

import quizResultRoute from '../../routes/quizResultRoute';

export default class QuizResultScreenContainer extends Component {
	render() {
		const { quizId } = this.props;
		return (
			<Relay.Renderer
				Container={QuizResultScreen}
				queryConfig={new quizResultRoute({quizId})}
				environment={RelayStore}
				render={({done, error, props, retry, stale}) =>{
					if(error){
						console.log(error);
					} else if(props){
						return <QuizResultScreen {...props} {...this.props}/>;
					}
				}}
			/>
		);
	}
}
