'use strict';

import React, { Component } from 'react';
import {
  RootContainer
} from 'react-relay';

import FollowerSuggestionByQuiz from '../DebateScreen/Opinions/FollowerSuggestionByQuiz';

import followerSuggestionByQuizRoute from '../../routes/followerSuggestionByQuizRoute';

export default class FollowerSuggestionByQuizCardContainer extends Component {
  render() {
    const { subquizScoresArg } = this.props;
    return (
      <RootContainer
        Component={FollowerSuggestionByQuiz}
        route={new followerSuggestionByQuizRoute({subquizScoresArg})}
        renderFetched={(data) => <FollowerSuggestionByQuiz {...this.props} {...data} />}
      />
    );
  }
}
