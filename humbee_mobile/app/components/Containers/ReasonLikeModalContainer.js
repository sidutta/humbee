'use strict';


import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import ReasonLikeModal from '../DebateScreen/Debate/ReasonLikeModal';

import reasonLikeModalRoute from '../../routes/reasonLikeModalRoute';

export default class ReasonLikeModalContainer extends Component {
	render() {
		const { reasonId } = this.props;
		return (
			<RootContainer
				Component={ReasonLikeModal}
				route={new reasonLikeModalRoute({reasonId})}
				renderFetched={(data) => <ReasonLikeModal {...this.props} {...data} />}
			/>
		);
	}
}
