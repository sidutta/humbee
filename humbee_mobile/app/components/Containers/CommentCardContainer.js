'use strict';

import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import CommentCard from '../DebateScreen/Debate/CommentCard';

import commentCardRoute from '../../routes/commentCardRoute';

export default class CommentCardContainer extends Component {
	render() {
		const { commentId } = this.props;
		return (
			<RootContainer
				Component={CommentCard}
				route={new commentCardRoute({commentId})}
				renderFetched={(data) => <CommentCard {...this.props} {...data} />}
			/>
		);
	}
}
