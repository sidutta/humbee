'use strict';


import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import ViewerProfile from '../ViewerProfile';

import { ActivityIndicator } from 'react-native';

import colors from '../../config/colors';



import homeRoute from '../../routes/homeRoute';

export default class ViewerProfileContainer extends Component {
	constructor(props){
		super(props);
	}
	render() {
		return (
			<RootContainer
				Component={ViewerProfile}
				route={new homeRoute()}
				renderFetched={(data) => <ViewerProfile {...this.props} {...data} />}
				renderLoading={() => <ActivityIndicator
																animating={true}
																color={colors.humbeeGreen}
																style={{
																	alignItems: 'center',
																	justifyContent: 'center',
																	padding: 40,
																	height: 200}}
																size="large"/>}
			/>
		);
	}
}
