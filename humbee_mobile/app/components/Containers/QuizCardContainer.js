'use strict';


import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import { ActivityIndicator } from 'react-native';
import colors from '../../config/colors';

import QuizCard from '../Home/quiz/QuizCard';
import quizRoute from '../../routes/quizRoute';

export default class QuizCardContainer extends Component {
	render() {
		const { quizId } = this.props;
		return (
			<RootContainer
				Component={QuizCard}
				route={new quizRoute({quizId})}
				renderFetched={(data) => <QuizCard {...this.props} {...data} />}
				renderLoading={() => <ActivityIndicator
																animating={true}
																color={colors.humbeeGreen}
																style={{
																	alignItems: 'center',
																	justifyContent: 'center',
																	padding: 40,
																	height: 200}}
																size="large"/>}/>

		);
	}
}
