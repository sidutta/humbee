'use strict';

import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import ActionButton from '../Common/ActionButton';

import colors from '../../config/colors';

import { ActivityIndicator, View, Text } from 'react-native';

import QuizScreen from '../QuizScreen';

import quizRoute from '../../routes/quizRoute';

export default class QuizScreenContainer extends Component {
	render() {
		const { quizId } = this.props;
		return (
			<RootContainer
				Component={QuizScreen}
				route={new quizRoute({quizId})}
				renderFetched={ (data) => <QuizScreen {...this.props} {...data} /> }
				renderLoading={ () => <ActivityIndicator
																animating={true}
																color={colors.humbeeGreen}
																style={{
																	alignItems: 'center',
																	justifyContent: 'center',
																	padding: 40,
																	height: 200}}
																size="large"
                              />
                      }
				renderFailure={ (error, retry) => {
					<View>
						<Text>Ooops something went wrong!!! Please click below to retry</Text>
						<ActionButton
							backgroundColor={colors.humbeeGreen}
							text='Post your reason'
							iconName='ios-create-outline'
							onPress={retry}/>
					</View>;
        } }
      />
		);
	}
}
