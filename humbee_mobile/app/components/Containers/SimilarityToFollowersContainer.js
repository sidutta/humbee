'use strict';

import React, { Component } from 'react';
import Relay from 'react-relay';

import RelayStore from '../../utils/RelayStore';

import SimilarityToFollowers from '../DebateScreen/Opinions/SimilarityToFollowers';

import similarityToFollowerRoute from '../../routes/similarityToFollowerRoute';

export default class FollowerSuggestionByQuizCardContainer extends Component {
	render() {
		const { subquizScoresArg } = this.props;
		return (
			<Relay.Renderer
				Component={SimilarityToFollowers}
				queryConfig={new similarityToFollowerRoute({subquizScoresArg})}
				environment={RelayStore}
				render={({done, error, props, retry, stale}) => {
					if(error){
						console.log(error);
					} else if(props){
						return <SimilarityToFollowers {...props} {...this.props}/>;
					}
				}}
			/>
		);
	}
}
