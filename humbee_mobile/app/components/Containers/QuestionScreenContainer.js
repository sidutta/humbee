'use strict';

import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import QuestionScreen from '../DebateScreen/Opinions/QuestionScreen';

import questionRoute from '../../routes/questionRoute';

export default class QuestionCardContainer extends Component {
	render() {
		const { questionId } = this.props;
		return (
			<RootContainer
				Component={QuestionScreen}
				route={new questionRoute({questionId})}
				renderFetched={(data) => <QuestionScreen {...this.props} {...data} />}
			/>
		);
	}
}
