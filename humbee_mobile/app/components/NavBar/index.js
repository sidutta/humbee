import React, { Component } from 'react';
import { View, Text, Dimensions, AsyncStorage, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import TouchableItem from '../Common/TouchableItem';
import colors from '../../config/colors';
import Router from '../../routes/Router';
import {
  GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

export default class NavBar extends Component {
	constructor(props){
		super(props);

		this.state={
			height : 0
		};

		this.goBack = this.goBack.bind(this);
		this.search = this.search.bind(this);
		this.skipIntroQuiz = this.skipIntroQuiz.bind(this);
	}

	goBack(){
		this.props.navigator.pop();
	}

	search(){
    if (__DEV__ == false) {
      let tracker = new GoogleAnalyticsTracker('UA-92835078-1');
      tracker.trackScreenView('Search');
    }
		this.props.navigator.push(
			Router.getRoute('search')
		);
	}

	componentWillMount(){
		const height = (Dimensions.get('window').height)/10;
		this.setState({height});
	}

	skipIntroQuiz() {
		AsyncStorage.getItem('token').then((token) => {
			this.props.navigator.replace('home');
		})
	}

	render() {
		const {
			opacity,
			title,
			openDrawer,
			search,
			elevation,
			isIntroQuiz
		} = this.props;

		const {
			height
		} = this.state;


		let topLeftButton;

		if(isIntroQuiz) {
			topLeftButton = (<View style={{paddingHorizontal: 5}}/>)
		}
    else if(title=='Home'||title=='Quizzes'||title=='Articles'||title=='Videos'||title=='Debates'||title=='Memes') {
      topLeftButton = (<TouchableItem style={styles.menuButton} onPress={openDrawer} borderLess={true}>
                <Icon name='md-menu' style={styles.menuIcon} size={25}/>
              </TouchableItem>)
    }
		else {
			topLeftButton = (<TouchableItem style={styles.menuButton} onPress={this.goBack} borderLess={true}>
								<Icon name='md-arrow-back' style={styles.menuIcon} size={25}/>
							</TouchableItem>)
		}

		return (

				<View opacity={opacity} style={[styles.navbar,{height: height}]} elevation={((title=='Home') ? 5 : 0) || elevation}>
					{topLeftButton}
					<View style={styles.spacingCenter}>
						<Text style={styles.title}>{title}</Text>
					</View>
					{
						isIntroQuiz
						?
						<TouchableItem style={styles.searchButton} onPress={this.skipIntroQuiz} borderLess={true}>
							<Text style={styles.title}>Home</Text>
						</TouchableItem>
						:
						<TouchableItem style={styles.searchButton} onPress={this.search} borderLess={true}>
							<Icon name='md-search' size={25} style={styles.searchIcon}/>
						</TouchableItem>
					}
				</View>
		);
	}
}


let styles = StyleSheet.create({
	navbar : {
		backgroundColor : colors.humbeeGreen,
		flexDirection : 'row'
	},
	title : {
		fontFamily : 'helvetica',
		fontSize : 16,
		color : 'white'
	},
	menuButton : {
		flex : 2,
		justifyContent : 'center',
		alignItems : 'center'
	},
	menuIcon : {
		color : 'white'
	},
	searchButton : {
		flex : 3,
		justifyContent : 'center',
		alignItems : 'center'
	},
	searchIcon : {
		color : 'white'
	},
	spacingCenter :{
		flex : 10,
		justifyContent : 'center'
	}
});
