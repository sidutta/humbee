// Taken from https://github.com/appintheair/react-native-buyscreen
// MIT License

'use strict';

var React = require('react');
var Line = require('./Line');

import { Text, Image, View, StyleSheet } from 'react-native';
import colors from '../../config/colors';

/**
 * Intended to be item in the Carousel
 * Displays promo text on top of the image
*/
var IntroImage = React.createClass({
  propType: {
    style: View.propTypes.style,
  },
  render: function() {
    return (
      <Image
        style={[this.props.style, styles.usecase]}
        source={this.props.image}
      >
        <View style={styles.promoView}>
          <Text style={[styles.text, styles.promoHeader]}>{this.props.header}</Text>
          <Text style={[styles.text, styles.promoDescription]}>{this.props.description}</Text>
          <Line style={{backgroundColor:'#0ea378'}} />
          <Text style={[styles.text, styles.promoText]}>{this.props.promoText}</Text>
        </View>
      </Image>
    );
  }
});

var styles = StyleSheet.create({
  usecase: {
    flexDirection: 'column',
    flex: 1
  },
  promoView: {
    backgroundColor: 'transparent',
  },
  text: {
    color: 'white',
    textAlign: 'center'
  },
  promoHeader: {
    fontSize: 42,
    fontWeight: '700',
    alignSelf: 'center'
  },
  promoDescription: {
    fontSize: 22,
    fontWeight: '300',
    width: 220,
    alignSelf: 'center',
  },
  promoText: {
    fontSize: 14,
    fontWeight: '400',
    alignSelf: 'center',
    lineHeight: 22,
    width: 220,
  },
});

module.exports = IntroImage;
