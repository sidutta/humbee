
import React, { Component } from 'react';
import { View , Text , KeyboardAvoidingView, TextInput , AsyncStorage } from 'react-native';
import dismissKeyboard from 'dismissKeyboard';

import RelayStore from '../../utils/RelayStore';
import Router from '../../routes/Router';

import SetUsernameMutation from '../../mutations/SetUsernameMutation';

import FollowSuggestionsContainer from '../Containers/FollowSuggestionsContainer';
import setNetworkLayer from '../../utils/setNetworkLayer';

import ActionButton from '../Common/ActionButton';
import colors from '../../config/colors';


export default class UsernameEdit extends Component {
	constructor(props){
		super(props);
		const { id } = this.props;
		this.state = {
			username : '',
			errors : {},
			isLoading : false
		};
		this.setUsername = this.setUsername.bind(this);
	}

	setUsername(){
		RelayStore.commitUpdate(
			new SetUsernameMutation({
				id : this.props.id,
				username : this.state.username
			}),
			{
				onFailure: (transaction) => {
					const errorMessage = transaction.getError().source.errors[0].message;
					this.setState({ errors : errorMessage , isLoading : false });
				},
				onSuccess: (response) => {
					if(response.setUsername.token){
						try {
							AsyncStorage.setItem('token', response.setUsername.token);
							dismissKeyboard();
							// setNetworkLayer(response.setUsername.token);
							this.props.navigator.replace('followSuggestions',{
								token : response.setUsername.token,
								introQuizId : this.props.introQuizId
							});
						} catch (error) {
							alert(error);
						}
					}
				}
			}
		);
	}

	render(){

		const {	username, errors } = this.state;
		return(
			<View style={{flex :1}}>
				<Text style={{ flex : 1,padding : 10, fontSize : 20, fontFamily : 'helvetica'}}>
					Please choose a username
				</Text>
				<View style={{flex : 4}}>
					<KeyboardAvoidingView style={{ padding :10}} behavior={'padding'}>
						<TextInput
							placeholder='Username'
							autoFocus={true}
							returnKeyType={'done'}
							value={username}
							blurOnSubmit={false}
							onChangeText={(username) => this.setState({username})}/>
						{ errors.username && <Text style={{color : 'tomato', fontSize : 12, fontFamily : 'helvetica', paddingRight : 10}}>{errors.username}</Text>}
					</KeyboardAvoidingView>
					<View style={{ alignItems :'center', justifyContent : 'center', height : 60}}>
						<ActionButton
							backgroundColor={colors.humbeeGreen}
							text='Set Username'
							iconName='ios-arrow-dropright-circle'
							onPress={this.setUsername}/>
					</View>
				</View>
			</View>
		);
	}
}
