'use strict';

import Relay from 'react-relay';
import React, { Component } from 'react';
import { View , Text, Image, Dimensions, StyleSheet, AsyncStorage } from 'react-native';
import { LoginManager, AccessToken } from 'react-native-fbsdk';

import ViewerQuery from '../../queries/ViewerQuery';
import { createRenderer } from '../../utils/RelayUtils';
import Router from '../../routes/Router';
import RelayStore from '../../utils/RelayStore';

import ActionButton from '../Common/ActionButton';
import TouchableItem from '../Common/TouchableItem';
import Carousel from 'react-native-carousel';
import EmailLogin from './EmailLogin';
import Signup from '../Signup';
import UsernameEdit from './UsernameEdit';
import dismissKeyboard from 'dismissKeyboard';
import setNetworkLayer from '../../utils/setNetworkLayer';

import FbLoginMutation from '../../mutations/FbLoginMutation';

import colors from '../../config/colors';

class Login extends Component {
	constructor(props){
		super(props);
		this.state ={
			accessToken : '',
			userID : '',
			splash : true,
			isLoading : false
		};
		this.fbLogin = this.fbLogin.bind(this);
		this.emailLogin = this.emailLogin.bind(this);
		this.signup = this.signup.bind(this);
	}
	fbLogin(context){
		LoginManager.logInWithReadPermissions(['public_profile','email']).then(
			function(result) {
				if (result.isCancelled) {
					alert('Login cancelled');
				} else {
					AccessToken.getCurrentAccessToken().then(
						(data) => {
							RelayStore.commitUpdate(
								new FbLoginMutation({
									accessToken : data.accessToken.toString(),
									userID : data.userID.toString()
								}),
								{
									onFailure: (transaction) => {
										const errorMessage = transaction.getError().source.errors[0].message;
										alert('Oops something went wrong, please try again!!');
										context.setState({ error : errorMessage , isLoading : false });
									},
									onSuccess: (response) => {
										if(response.fbLogin.id){
											context.props.navigator.push(
												Router.getRoute('usernameEdit',{
													id : response.fbLogin.id
												})
											);
										} else if(response.fbLogin.token){
											try {
												AsyncStorage.setItem('token', response.fbLogin.token);
												dismissKeyboard();
												// setNetworkLayer(response.fbLogin.token);
												context.props.navigator.push(
													Router.getRoute('home',{
														title : 'Home',
														token : response.fbLogin.token
													})
												);
											} catch (error) {
												alert('Oops something went wrong!');
											}
										}
									}
								}
							);
						}
					);
				}
			},
			function(error) {
				alert('Login fail with error: ' + error);
			}
		);
	}

	signup(){
		this.props.navigator.push(
			Router.getRoute('signup',{
				emailLogin : this.emailLogin,
				fbLogin : this.fbLogin
			})
		);
	}

	emailLogin(){
		this.props.navigator.push(
			Router.getRoute('emailLogin',{
				fbLogin : this.fbLogin,
				signup : this.signup
			})
		);
	}

	componentWillMount(){
		let splash = false;
		AsyncStorage.getItem('token').then((token) => {
			// setNetworkLayer(token);
			if(token){
				this.props.navigator.push(
					Router.getRoute('home',{
						title : 'Home',
						token : token
					})
				);
				this.setState({splash});
			} else {
				this.setState({splash});
			}
		});
	}

	render() {

		const { splash } = this.state;

		return (

			<View style={{flex : 1}}>
			{ (!splash) && <View style={{flex : 1}}>
					<View style={{flex : 7}}>
					<Carousel  width={Dimensions.get('window').width} delay={5000} indicatorOffset={-15} >
						<View>
							<Image style={{height : 350 }} source={{uri : 'https://storage.googleapis.com/humbee_images/sample.jpg'}}/>
						</View>
						<View>
							<Image style={{height : 350 }} source={{uri : 'https://storage.googleapis.com/humbee_images/sample.jpg'}}/>
						</View>
					</Carousel>
				</View>
				<View style={{flex : 2, alignItems : 'center', justifyContent : 'center'}}>
					<ActionButton
						backgroundColor='#3B5998'
						text='Log in with Facebook'
						iconName='logo-facebook'
						onPress={() => this.fbLogin(this)}/>
					<ActionButton
						backgroundColor={colors.humbeeGreen}
						text='Log in with Email'
						iconName='md-mail'
						onPress={this.emailLogin}/>
				</View>
				<TouchableItem style={{flex : 1, marginTop : 5, alignItems : 'center', justifyContent : 'center', backgroundColor : colors.humbeeGrey}}
					elevation={5}
					onPress={this.signup}>
					<Text style={{ fontSize : 16, fontFamily : 'helvetica'}}>{'Don' + "'" +'t have an account? Sign up'}</Text>
				</TouchableItem>
				</View>}
			</View>
		);
	}
}

export default createRenderer(Login, {
	queries: ViewerQuery,
	fragments: {
		viewer: () => Relay.QL`
			fragment on Viewer {
				id
			}
		`
	}
});
