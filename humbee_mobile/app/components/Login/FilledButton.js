'use strict';

var React = require('react');
import { View, StyleSheet, TouchableHighlight, Text } from 'react-native';

var FilledButton = React.createClass({
	propTypes:{
			style: View.propTypes.style,
			titleStyle: Text.propTypes.style,
	},
	render: function() {
		return (
			<TouchableHighlight
				onPress={this.props.onPress}
				underlayColor={this.props.highlightedColor}
				style={[styles.buttonBack, this.props.style]}
			>
				<View>
					<Text
						style={[styles.buttonText, this.props.titleStyle]}
					>
						{this.props.title}
					</Text>
					<Text style={styles.subtitleText}>{this.props.subtitle}</Text>
				</View>
			</TouchableHighlight>
		);
	}
});

var styles = StyleSheet.create({
	buttonText: {
		backgroundColor: 'transparent',
		fontSize: 16,
		textAlign: 'center'
	},
	buttonBack: {
		padding: 12,
		borderRadius: 4
	},
	subtitleText : {
		backgroundColor : 'transparent',
		fontSize : 14,
		textAlign : 'center',
		color : 'white'
	}
});

module.exports = FilledButton;
