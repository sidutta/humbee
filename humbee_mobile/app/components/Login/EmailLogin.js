'use strict';

import Relay from 'react-relay';
import React, { Component } from 'react';

import { View, KeyboardAvoidingView, Text, TextInput, Keyboard, AsyncStorage, StyleSheet, ActivityIndicator} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../config/colors';

import RelayStore from '../../utils/RelayStore';
import Router from '../../routes/Router';

import dismissKeyboard from 'dismissKeyboard';

import ActionButton from '../Common/ActionButton';
import TouchableItem from '../Common/TouchableItem';
import setNetworkLayer from '../../utils/setNetworkLayer';

import LoginMutation from '../../mutations/LoginMutation';

import SendVerificationCode from './SendVerificationCode';

// var Fabric = require('react-native-fabric');

export default class EmailLogin extends Component {
	constructor(props){
		super(props);
		this.state = {
			bottomBar : true,
			identifier : '',
			inputPassword : ''
		};
		this.login = this.login.bind(this);
		this.gotoSendVerificationCodePage = this.gotoSendVerificationCodePage.bind(this);
		this.focusNextField = this.focusNextField.bind(this);
	}

	keyboardDidShow() {
		let bottomBar = false;
		this.setState({bottomBar});
	}

	keyboardDidHide() {
		let bottomBar = true;
		this.setState({bottomBar});
	}

	componentWillMount(){
		this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
		this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
	}

	componentWillUnmount () {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}

	gotoSendVerificationCodePage(){
		this.props.navigator.push(
			Router.getRoute('sendVerificationCode',{
				signup : this.props.signup,
				fbLogin : this.props.fbLogin,
				introQuizId : this.props.introQuizId
			})
		);
	}


	login(){
		const {identifier ,inputPassword} = this.state;
		this.setState({isLoading : true});
		RelayStore.commitUpdate(
			new LoginMutation({
				identifier : identifier,
				inputPassword : inputPassword
			}),
			{
				onFailure: (transaction) => {
          // var { Answers } = Fabric;
					const errorMessage = transaction.getError().source.errors[0].message;
          // Answers.logLogin('Email', false, {'reason': errorMessage});
					this.setState({ error : errorMessage , isLoading : false });
				},
				onSuccess: (response) => {
          this.setState({ isLoading : false });
          // var { Answers } = Fabric;
					if(response.login.token){
						dismissKeyboard();
						try {
							AsyncStorage.setItem('token', response.login.token); // eslint-disable-line
							dismissKeyboard();
							// setNetworkLayer(response.login.token);
              // Answers.logLogin('Email', true);
							this.props.navigator.push(
								Router.getRoute('home',{
									title : 'Home',
									token : response.login.token
								})
							);
						}
            catch (error) {
              // Answers.logLogin('Email', false, {'reason': 'trycatch'});
							alert('Oops something went wrong!');
						}
					}
				}
			}
		);
		// this.props.navigator.push({
		// 	component : HomeContainer,
		// 	passProps : {
		// 		title : 'Home'
		// 	}
		// });
	}

	focusNextField(nextField){
		this.refs[nextField].focus();
	}

	render(){
		return(
			<View style={{flex : 1}}>
				<View style={{flex: 9}}>
					<KeyboardAvoidingView style={{padding :10}} behavior={'position'}>
					{ this.state.error && <Text style={{color : 'tomato', fontSize : 20, fontFamily : 'helvetica', paddingRight : 5}}>{this.state.error}</Text>}
							<TextInput
								placeholder='Email/username'
								returnKeyType={'next'}
								onSubmitEditing={() => this.focusNextField('1')}
								autoFocus={true}
								underlineColorAndroid={'seagreen'}
								blurOnSubmit={false}
								onChangeText={(identifier) => this.setState({identifier})}/>
							<TextInput
								placeholder='Password'
								returnKeyType={'done'}
								ref='1'
								secureTextEntry={true}
								onChangeText={(inputPassword) => this.setState({inputPassword})}/>
							<TouchableItem rippleColor={colors.humbeeGreen}>
								<Text style={{color : colors.humbeeGreen, fontSize : 16, fontFamily : 'helvetica'}} onPress={this.gotoSendVerificationCodePage}>
									Forgot your Password?
								</Text>
							</TouchableItem>
					</KeyboardAvoidingView>
					<View style={{alignItems : 'center', justifyContent : 'center', height : 140}}>
						<ActionButton
							backgroundColor={colors.humbeeGreen}
							text='Log in to Humbee'
							iconName='ios-key-outline'
							onPress={this.login}/>
						<Text style={{textAlign:'center'}}>Or</Text>
						<ActionButton
							backgroundColor='#3B5998'
							text='Log in with Facebook'
							iconName='logo-facebook'
							onPress={() =>this.props.fbLogin(this, this.props.introQuizId)}/>
					</View>
				</View>
        { (this.state.isLoading) &&
              <View style={[styles.loadingcontainer]}>
                <ActivityIndicator size='large' color={colors.humbeeGreen} style={[styles.loadingsymbol]}/>
              </View>
            }
				{ (this.state.bottomBar) ? (<TouchableItem style={{flex : 1, backgroundColor : colors.humbeeGrey, alignItems:'center', justifyContent:'center'}}
					onPress={this.props.signup}>
					<Text style={{ fontSize : 16, fontFamily : 'helvetica'}}>{'Don' + "'" +'t have an account? Sign up'}</Text>
				</TouchableItem>) : (<View style={{flex : 1}}/>)
				}
			</View>
		);
	}
}

var styles = StyleSheet.create({
  loadingcontainer: {
    backgroundColor: 'transparent',
    // position: 'absolute',
    bottom: 180,
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingsymbol: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
});

