'use strict';

import React, { Component } from 'react';

import { View, KeyboardAvoidingView, Text, TextInput, Keyboard} from 'react-native';

import RelayStore from '../../utils/RelayStore';

import Icon from 'react-native-vector-icons/Ionicons';

import colors from '../../config/colors';

import dismissKeyboard from 'dismissKeyboard';

import ActionButton from '../Common/ActionButton';
import TouchableItem from '../Common/TouchableItem';

import Router from '../../routes/Router';

import VerificationCodeCheckMutation from '../../mutations/VerificationCodeCheckMutation';
import VerificationCodeMutation from '../../mutations/VerificationCodeMutation';

import PasswordReset from './PasswordReset';

export default class VerificationCodeCheck extends Component {
	constructor(props){
		super(props);
		this.state = {
			bottomBar : true,
			claimedCode : ''
		};
		this.check = this.check.bind(this);
		this.sendAgain = this.sendAgain.bind(this);
		this.focusNextField = this.focusNextField.bind(this);
	}

	keyboardDidShow() {
		let bottomBar = false;
		this.setState({bottomBar});
	}

	keyboardDidHide() {
		let bottomBar = true;
		this.setState({bottomBar});
	}

	componentWillMount(){
		this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
		this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
	}

	componentWillUnmount () {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}

	check(){
		const {claimedCode} = this.state;
		this.setState({isLoading : true});
		RelayStore.commitUpdate(
			new VerificationCodeCheckMutation({
				identifier : this.props.identifier,
				claimedCode : claimedCode
			}),
			{
				onFailure: (transaction) => {
					const errorMessage = transaction.getError().source.errors[0].message;
					this.setState({ error : errorMessage , isLoading : false });
				},
				onSuccess: (response) => {
					if(response.verificationCodeCheck.verificationResult){
						dismissKeyboard();
						try {
							this.props.navigator.push(
								Router.getRoute('passwordReset',{
									identifier : this.props.identifier,
									signup : this.props.signup,
									fbLogin : this.props.fbLogin,
									introQuizId : this.props.introQuizId
								})
							);
						} catch (error) {
							this.setState({ error : error, isLoading : false });
						}
					} else {
						this.setState({ error : 'Oops something went wrong!', isLoading : false });
					}
				}
			}
		);
	}

	sendAgain() {
		this.setState({isLoading : true});
		RelayStore.commitUpdate(
			new VerificationCodeMutation({
				identifier : this.props.identifier
			}),
			{
				onFailure: (transaction) => {
					const errorMessage = transaction.getError().source.errors[0].message;
					this.setState({ error : errorMessage , isLoading : false });
				},
				onSuccess: (response) => {
					if(response.verificationCode.maskedEmail){
						dismissKeyboard();
						this.setState({ error : 'Sent again!', isLoading : false });
					} else {
						this.setState({ error : 'Oops something went wrong!', isLoading : false });
					}
				}
			}
		);
	}

	focusNextField(nextField){
		this.refs[nextField].focus();
	}

	render(){
		return(
			<View style={{flex : 1}}>
				<View style={{flex: 9}}>
					<KeyboardAvoidingView style={{padding :10}} behavior={'position'}>
						<Text style={{textAlign:'center'}}>An email containing the verification code has sent to the registered email adderess ({this.props.maskedEmail}).</Text>
						{ this.state.error && <Text style={{color : 'tomato', fontSize : 20, fontFamily : 'helvetica', paddingRight : 5}}>{this.state.error}</Text>}
							<TextInput
								placeholder='Verification code'
								returnKeyType={'done'}
								onSubmitEditing={() => this.focusNextField('1')}
								autoFocus={true}
								underlineColorAndroid={'seagreen'}
								blurOnSubmit={false}
								onChangeText={(claimedCode) => this.setState({claimedCode})}/>
								<TouchableItem rippleColor={colors.humbeeGreen}>
									<Text style={{color : colors.humbeeGreen, fontSize : 16, fontFamily : 'helvetica'}} onPress={this.sendAgain}>
										Send again?
									</Text>
								</TouchableItem>
					</KeyboardAvoidingView>
					<View style={{alignItems : 'center', justifyContent : 'center', height : 70}}>
						<ActionButton
							backgroundColor={colors.humbeeGreen}
							text='Verify'
							iconName='ios-checkmark'
							onPress={this.check}/>
					</View>
				</View>
				{ (this.state.bottomBar) ? (<TouchableItem style={{flex : 1, backgroundColor : colors.humbeeGrey, alignItems:'center', justifyContent:'center'}}
					onPress={this.props.signup}>
					<Text style={{ fontSize : 16, fontFamily : 'helvetica'}}>{'Don' + "'" +'t have an account? Sign up'}</Text>
				</TouchableItem>) : (<View style={{flex : 1}}/>)
				}
			</View>
		);
	}
}
