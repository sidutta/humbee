'use strict';

import React, { Component } from 'react';
import { View, KeyboardAvoidingView, Text, TextInput, Keyboard} from 'react-native';

import RelayStore from '../../utils/RelayStore';
import Router from '../../routes/Router';

import Icon from 'react-native-vector-icons/Ionicons';

import colors from '../../config/colors';

import dismissKeyboard from 'dismissKeyboard';

import ActionButton from '../Common/ActionButton';
import TouchableItem from '../Common/TouchableItem';

import VerificationCodeMutation from '../../mutations/VerificationCodeMutation';

import verificationCodeCheck from './VerificationCodeCheck';

export default class SendVerificationCode extends Component {
	constructor(props){
		super(props);
		this.state = {
			bottomBar : true,
			identifier : ''
		};
		this.send = this.send.bind(this);
		this.focusNextField = this.focusNextField.bind(this);
	}

	keyboardDidShow() {
		let bottomBar = false;
		this.setState({bottomBar});
	}

	keyboardDidHide() {
		let bottomBar = true;
		this.setState({bottomBar});
	}

	componentWillMount(){
		this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
		this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
	}

	componentWillUnmount () {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}

	send() {
		const {identifier} = this.state;
		this.setState({isLoading : true});
		RelayStore.commitUpdate(
			new VerificationCodeMutation({
				identifier : identifier
			}),
			{
				onFailure: (transaction) => {
					const errorMessage = transaction.getError().source.errors[0].message;
					this.setState({ error : errorMessage , isLoading : false });
				},
				onSuccess: (response) => {
					if(response.verificationCode.maskedEmail){
						dismissKeyboard();
						try {
							this.props.navigator.push(
								Router.getRoute('verificationCodeCheck',{
									maskedEmail : response.verificationCode.maskedEmail,
									identifier : identifier,
									signup : this.props.signup,
									fbLogin : this.props.fbLogin,
									introQuizId : this.props.introQuizId
								})
							);
						} catch (error) {
							this.setState({ error : error, isLoading : false });
						}
					} else {
						this.setState({ error : 'Oops something went wrong!', isLoading : false });
					}
				}
			}
		);
	}

	focusNextField(nextField){
		this.refs[nextField].focus();
	}

	render(){
		return(
			<View style={{flex : 1}}>
				<View style={{flex: 9}}>
					<KeyboardAvoidingView style={{padding :10}} behavior={'position'}>
						<Text style={{textAlign:'center'}}>Please enter your registered email address or username. An email containing the verification code will be sent to the registered email adderess.</Text>
						{ this.state.error && <Text style={{color : 'tomato', fontSize : 20, fontFamily : 'helvetica', paddingRight : 5}}>{this.state.error}</Text>}
						<TextInput
							placeholder='Email/username'
							returnKeyType={'done'}
							onSubmitEditing={() => this.focusNextField('1')}
							autoFocus={true}
							underlineColorAndroid={'seagreen'}
							blurOnSubmit={false}
							onChangeText={(identifier) => this.setState({identifier})}
						/>
					</KeyboardAvoidingView>
					<View style={{alignItems : 'center', justifyContent : 'center', height : 70}}>
						<ActionButton
							backgroundColor={colors.humbeeGreen}
							text='Send'
							iconName='ios-send'
							onPress={this.send}
						/>
					</View>
				</View>
				{ (this.state.bottomBar)
					?
					(
						<TouchableItem style={{flex : 1, backgroundColor : colors.humbeeGrey, alignItems:'center', justifyContent:'center'}}
					onPress={this.props.signup}>
							<Text style={{ fontSize : 16, fontFamily : 'helvetica'}}>{'Don' + "'" +'t have an account? Sign up'}</Text>
						</TouchableItem>
					)
					:
					(<View style={{flex : 1}}/>)
				}
			</View>
		);
	}
}
