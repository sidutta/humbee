'use strict';

import React, { Component } from 'react';

import { View, KeyboardAvoidingView, Text, TextInput, Keyboard} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import RelayStore from '../../utils/RelayStore';
import Router from '../../routes/Router';

import colors from '../../config/colors';

import dismissKeyboard from 'dismissKeyboard';

import ActionButton from '../Common/ActionButton';
import TouchableItem from '../Common/TouchableItem';

import passwordValidator from '../../utils/validations/password';

import PasswordResetMutation from '../../mutations/PasswordResetMutation';

import EmailLogin from './EmailLogin';

export default class PasswordReset extends Component {
	constructor(props){
		super(props);
		this.state = {
			identifier : this.props.identifier,
			inputPassword : '',
			inputPasswordConfirm : '',
		};
		this.changePassword = this.changePassword.bind(this);
		this.focusNextField = this.focusNextField.bind(this);
	}

	keyboardDidShow() {
		let bottomBar = false;
		this.setState({bottomBar});
	}

	keyboardDidHide() {
		let bottomBar = true;
		this.setState({bottomBar});
	}

	componentWillMount(){
		this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
		this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
	}

	componentWillUnmount () {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}

	gotoEmailLoginPage(){
		this.props.navigator.push(
			Router.getRoute('emailLogin',{
				signup : this.props.signup,
				fbLogin : this.props.fbLogin,
				introQuizId : this.props.introQuizId
			})
		);
	}

	changePassword() {
		const {identifier, inputPassword, inputPasswordConfirm} = this.state;
		this.setState({isLoading : true});

		const data = {};
		data.password = inputPassword;
		const passwordValidationResult = passwordValidator(data);
		if(!(passwordValidationResult.isValid)) {
			this.setState({ error : passwordValidationResult.errors.password, isLoading : false });
			return;
		}

		if(inputPassword != inputPasswordConfirm) {
			this.setState({ error : 'Passwords don\'t match', isLoading : false });
			return;
		}

		RelayStore.commitUpdate(
			new PasswordResetMutation({
				identifier : identifier,
				password : inputPassword
			}),
			{
				onFailure: (transaction) => {
					const errorMessage = transaction.getError().source.errors[0].message;
					this.setState({ error : errorMessage , isLoading : false });
				},
				onSuccess: (response) => {
					if(response.passwordReset.passwordResetResult){
						dismissKeyboard();
						try {
							this.props.navigator.push(
								Router.getRoute('emailLogin',{
									signup : this.props.signup,
									fbLogin : this.props.fbLogin,
									introQuizId : this.props.introQuizId
								})
							);
						} catch (error) {
							this.setState({ error : error, isLoading : false });
						}
					} else {
						this.setState({ error : 'Oops something went wrong!', isLoading : false });
					}
				}
			}
		);
	}

	focusNextField(nextField){
		this.refs[nextField].focus();
	}

	render(){
		return(
			<View style={{flex : 1}}>
				<View style={{flex: 9}}>
					<KeyboardAvoidingView style={{padding :10}} behavior={'position'}>
						<Text style={{textAlign:'center'}}>Please choose a new password of atleast 8 characters, containing atleast one letter, number and speacial character.</Text>
						{ this.state.error && <Text style={{color : 'tomato', fontSize : 20, fontFamily : 'helvetica', paddingRight : 5}}>{this.state.error}</Text>}
							<TextInput
								placeholder='New password'
								returnKeyType={'next'}
								ref='1'
								secureTextEntry={true}
								onChangeText={(inputPassword) => this.setState({inputPassword})}/>
								<TextInput
									placeholder='Confirm your new password'
									returnKeyType={'done'}
									ref='1'
									secureTextEntry={true}
									onChangeText={(inputPasswordConfirm) => this.setState({inputPasswordConfirm})}/>
					</KeyboardAvoidingView>
					<View style={{alignItems : 'center', justifyContent : 'center', height : 70}}>
						<ActionButton
							backgroundColor={colors.humbeeGreen}
							text='Change'
							iconName='md-key'
							onPress={this.changePassword}/>
					</View>
				</View>
				{ (this.state.bottomBar) ? (<TouchableItem style={{flex : 1, backgroundColor : colors.humbeeGrey, alignItems:'center', justifyContent:'center'}}
					onPress={this.gotoEmailLoginPage}>
					<Text style={{ fontSize : 16, fontFamily : 'helvetica'}}>{'Don' + "'" +'t have an account? Sign up'}</Text>
				</TouchableItem>) : (<View style={{flex : 1}}/>)
				}
			</View>
		);
	}
}
