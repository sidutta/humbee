import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, Text, ScrollView, Image, ActivityIndicator} from 'react-native';

import ViewerQuery from '../../queries/ViewerQuery';
import { createRenderer } from '../../utils/RelayUtils';
import Router from '../../routes/Router';
import RelayStore from '../../utils/RelayStore';

import ExtraDimensions from 'react-native-extra-dimensions-android';

import TouchableItem from '../Common/TouchableItem';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../config/colors';
import CheckBox from 'react-native-check-box';
import BulkFollowMutation from '../../mutations/BulkFollowMutation';

import isEmpty from 'lodash/isEmpty';

class FollowSuggestions extends Component{
	constructor(props) {
		super(props);
		let following =[];
		if(props.viewer.user.followSuggestions.edges){
			following = props.viewer.user.followSuggestions.edges.map(({node}) => {
				return { userId : node.id, follow : true };
			});
		}
		this.state={
			pageSize : 100,
			scrollViewHeight : 0,
			following : following,
			isLoading : false
		};
		this.skip = this.skip.bind(this);
		this.onEndReached = this.onEndReached.bind(this);
		this.handleClick = this.handleClick.bind(this);
		this.follow = this.follow.bind(this);
	}

	skip() {
		this.props.navigator.replace('quizScreen',{
			id : this.props.introQuizId,
			isIntroQuiz : true
		});
	}

	onEndReached() {
		let {pageSize}	= this.state;
		pageSize = pageSize + 10;
		this.props.relay.setVariables({pageSize});
		this.setState({pageSize});
	}

	handleClick(index) {
		let {following} = this.state;
		let {follow, userId} = following[index];
		following[index] = { userId : userId, follow : !follow};
		this.setState(following);
	}

	follow() {
		let {following} = this.state;
		let followingList = following.reduce((last, now) => {
			const { userId, follow} = now;
			if( follow ){
				return [...last, userId];
			} else {
				return last;
			}
		}, []);
		RelayStore.commitUpdate(
			new BulkFollowMutation({ followingList }),
			{
				onFailure : () => alert('Oops! Something went wrong'),
				onSuccess : () => this.props.navigator.replace('quizScreen',{
					id : this.props.introQuizId,
					isIntroQuiz : true
				})
			}
		);
	}

	componentDidMount() {
		let scrollViewHeight = ExtraDimensions.get('REAL_WINDOW_HEIGHT') - ExtraDimensions.get('STATUS_BAR_HEIGHT') - ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT') - 100;
		this.setState({scrollViewHeight});
		const { followSuggestions } = this.props.viewer.user;
		const { edges } = followSuggestions;
		if(isEmpty(edges)){
			this.props.navigator.replace('quizScreen',{
        id : this.props.introQuizId,
        isIntroQuiz : true
      })
		}
	}

	render() {
		const { scrollViewHeight } = this.state;
		const { followSuggestions } = this.props.viewer.user;
		const {  following, isLoading } = this.state;
		const { edges, pageInfo } = followSuggestions;
		const { hasNextPage } = pageInfo;
		const userCards = edges.map(({node}, index) => {
			return (
					<View key={node.id} style={{ height : 70, padding : 5, flexDirection : 'row', alignItems : 'center', justifyContent : 'space-around'}}>
						<Image style={{ flex : 3, height : 60, borderRadius : 30 }} source={{ uri : node.profilePic }}/>
						<CheckBox
							style={{flex: 14, padding: 5}}
							onClick={()=> this.handleClick(index)}
							leftTextStyle={{ fontSize : 16, fontFamily : 'helvetica', color : 'black'}}
							isChecked={following[index].follow}
							leftText={node.firstName + ' ' + node.lastName + '(@' + node.username + ')'}/>
					</View>
				);
		});
		return(
			<View>
				{
					isLoading ? (
						<View>
							<ActivityIndicator
								animating={true}
								color={colors.humbeeGreen}
								style={{
									alignItems: 'center',
									justifyContent: 'center',
									padding: 40,
									height: 200}}
								size="large"/>
								<Text style={{ textAlign : 'center', color : colors.humbeeGreen, fontSize : 24, fontFamily : 'helvetica'}}>Setting up your profile</Text>
							</View>
					) : (
						<View>
							<View elevation={5} style={{height : 50, flexDirection : 'row', backgroundColor : 'white', alignItems : 'center'}}>
								<View style={{ flex : 1}}/>
								<Text style={{flex : 6, color : colors.humbeeGreen, fontSize : 16, fontFamily : 'helvetica', textAlign : 'center'}}>Your Friends on Humbee</Text>
								<TouchableItem
									borderLess={true}
									style={{flex : 1, alignItems : 'center', justifyContent : 'space-around', flexDirection : 'row'}}
									onPress={this.skip}>
									<Text style={{ color : colors.humbeeGreen, fontSize : 16, fontFamily : 'helvetica', textAlign : 'center'}}>Skip</Text>
									<Icon name='ios-arrow-forward' size={25} style={{color : colors.humbeeGreen}}/>
								</TouchableItem>
							</View>
							<ScrollView style={{height : scrollViewHeight}}>
								{userCards}
								{ hasNextPage && <TouchableItem style={{ height : 50, borderRadius : 2, alignItems : 'center' }} borderLess={true} onPress={this.onEndReached}>
									<Text style={{ textAlign : 'center', color : colors.humbeeGreen, fontFamily : 'helvetica', fontSize : 16, fontWeight : 'bold' }}> Load More </Text>
								</TouchableItem>}
							</ScrollView>
							<TouchableItem
								elevation={5}
								onPress={this.follow}
								style={{height : 50,backgroundColor : colors.humbeeGreen, alignItems : 'center', justifyContent : 'space-around'}}>
								<Text style={{ color : 'white', fontSize : 16, fontFamily : 'helvetica'}}>Follow</Text>
							</TouchableItem>
						</View>
					)
				}
			</View>
		);
	}
}

export default createRenderer(FollowSuggestions, {
	queries: ViewerQuery,
	initialVariables : {
		pageSize : 100
	},
	fragments: {
		viewer: () => Relay.QL`
			fragment on Viewer{
				user {
					followSuggestions(first : $pageSize){
						pageInfo{
							hasNextPage
						},
						edges{
							node{
								id,
								firstName,
								lastName,
								username,
								profilePic
							}
						}
					}
				}
			}
		`
	}
});
