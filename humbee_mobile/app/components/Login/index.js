// uses https://github.com/appintheair/react-native-looped-carousel
// MIT license

'use strict';

import Relay from 'react-relay';
import React, { Component } from 'react';
import { View, Text, Image, ActivityIndicator, Dimensions, StyleSheet, AsyncStorage } from 'react-native';
import { LoginManager, AccessToken } from 'react-native-fbsdk';

import Carousel from 'react-native-looped-carousel';

import ViewerQuery from '../../queries/ViewerQuery';
import { createRenderer } from '../../utils/RelayUtils';
import Router from '../../routes/Router';
import RelayStore from '../../utils/RelayStore';

import ActionButton from '../Common/ActionButton';
import TouchableItem from '../Common/TouchableItem';
import EmailLogin from './EmailLogin';
import Signup from '../Signup';
import UsernameEdit from './UsernameEdit';
import dismissKeyboard from 'dismissKeyboard';
import setNetworkLayer from '../../utils/setNetworkLayer';
import IntroImage from './IntroImage';
import FilledButton from './FilledButton';

import FbLoginMutation from '../../mutations/FbLoginMutation';

import colors from '../../config/colors';

// var Fabric = require('react-native-fabric');

class Login extends Component {
  constructor(props){
    super(props);
    const { width, height } = Dimensions.get('window');
    this.state ={
      accessToken : '',
      userID : '',
      splash : true,
      isLoading : false,
      size: { width, height},
    };
    this.fbLogin = this.fbLogin.bind(this);
    this.emailLogin = this.emailLogin.bind(this);
    this.signup = this.signup.bind(this);
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
  }

  _onLayoutDidChange = (e) => {
    const layout = e.nativeEvent.layout;
    this.setState({ size: { width: layout.width, height: layout.height } });
  }

  handleAppStateChange(appState){
    if(appState == 'background'){
      PushNotification.localNotificationSchedule({
        message: 'My Notification Message', // (required)
        date: new Date(Date.now() + (10 * 1000)) // in 60 secs
      });
    }
  }

  fbLogin(context, introQuizId){
    context.setState({  isLoading : true });
    LoginManager.logInWithReadPermissions(['public_profile','email', 'user_friends']).then(
      function(result) {
        if (result.isCancelled) {
          alert('Login cancelled');
          context.setState({ isLoading : false });
        } else {
          AccessToken.getCurrentAccessToken()
          .then(
            (data) => {
              RelayStore.commitUpdate(
                new FbLoginMutation({
                  accessToken : data.accessToken.toString(),
                  userID : data.userID.toString()
                }),
                {
                  onFailure: (transaction) => {
                    // var { Answers } = Fabric;
                    const errorMessage = transaction.getError().source.errors[0].message;
                    alert('Please provide the required permissions to login through Facebook');
                    context.setState({ error : errorMessage , isLoading : false });
                    // Answers.logLogin('Facebook', false, {'reason': errorMessage});
                  },
                  onSuccess: (response) => {
                    context.setState({  isLoading : false });
                    if(response.fbLogin.id){
                      // var { Answers } = Fabric;
                      // Answers.logSignUp('Facebook', true);
                      context.props.navigator.replace('usernameEdit', {
                        id : response.fbLogin.id,
                        introQuizId : introQuizId
                      });
                    }
                    else if(response.fbLogin.token){
                      try {
                        // var { Answers } = Fabric;
                        // Answers.logLogin('Facebook', true);
                        AsyncStorage.setItem('token', response.fbLogin.token);
                        dismissKeyboard();
                        // setNetworkLayer(response.fbLogin.token);
                        context.props.navigator.replace('home');
                      }
                      catch (error) {
                        // var { Answers } = Fabric;
                        // Answers.logLogin('Facebook', false, {'reason': 'trycatch'});
                        alert('Oops something went wrong!');
                      }
                    }
                  }
                }
              );
            }
          )
          .catch( () => {
            context.setState({ isLoading : false });
          });
        }
      },
      function(error) {
        alert('Login fail with error: ' + error);
      }
    );
  }

  signup() {
    this.props.navigator.push(
      Router.getRoute('signup', {
        emailLogin : this.emailLogin,
        fbLogin : this.fbLogin,
        introQuizId: this.props.viewer.introQuizId
      })
    );
  }

  emailLogin() {
    this.props.navigator.push(
      Router.getRoute('emailLogin', {
        emailLogin : this.emailLogin,
        fbLogin : this.fbLogin,
        signup : this.signup,
        introQuizId: this.props.viewer.introQuizId
      })
    );
  }

  componentWillMount(){
    let splash = false;
    AsyncStorage.getItem('token').then((token) => {
      if(token){
        // setNetworkLayer(token);
        this.props.navigator.replace('home',{ automaticLogin : true});
      } else {
        this.setState({splash});
      }
    });
  }

  render() {

    const { splash, isLoading } = this.state;

    var pages = [<IntroImage
                key="promo1"
                image={require('./Images/home_screen_1.png')}
                header="Humbee"
                description="India's first political social network"
                promoText="Your one stop destination for all things political"
                style={this.state.size}
              />,
              <IntroImage
                key="promo2"
                image={require('./Images/home_screen_2.png')}
                header="Debate"
                description="Make your voice heard"
                promoText="Participate in key political debates and make an impact"
                style={this.state.size}
              />,
              <IntroImage
                key="promo3"
                image={require('./Images/home_screen_3.png')}
                header="Alignment"
                description="Quizzes to find political alignment"
                promoText="Answer simple questions to find out where you stand on key political issues"
                style={this.state.size}
              />,
              <IntroImage
                key="promo4"
                image={require('./Images/home_screen_4.png')}
                header="Content"
                description="Discover politics like never before"
                promoText="Curated political content representing all viewpoints"
                style={this.state.size}
              />];

      var options = [
                    <View style={[styles.option]} key="button1">
                      <FilledButton
                        style={{backgroundColor:colors.humbeeGreen}}
                        highlightedColor='#007655'
                        title="SIGN UP"
                        subtitle="New to Humbee?"
                        titleStyle={{color:'white'}}
                        onPress={this.signup}
                        fb={false}
                      />
                    </View>,
                    <View style={[styles.option]} key="button2">
                      <FilledButton
                        style={{backgroundColor:"#3B5998"}}
                        highlightedColor='#007655'
                        title="FACEBOOK"
                        subtitle="Login with facebook"
                        titleStyle={{color:'white'}}
                        onPress={() => this.fbLogin(this, this.props.viewer.introQuizId)}
                        fb={true}
                      />
                    </View>
                    ];

    // if(!splash) {
      return (
          <View style={{ flex: 1 }} onLayout={this._onLayoutDidChange}>

            <Carousel
              delay={2000}
              style={this.state.size}
              autoplay
            >
              {pages}
            </Carousel>
            { (isLoading) &&
              <View style={[styles.loadingcontainer]}>
                <ActivityIndicator size='large' color={colors.humbeeGreen} style={[styles.loadingsymbol]}/>
              </View>
            }
            {!(splash) && (!isLoading) && <View style={[styles.options, {width: this.state.size.width}]}>
              {options}
            </View>}
            {!(splash) && (!isLoading) && <View style={[styles.logintextcontainer, {width: this.state.size.width}]}>
              <Text
                style={[styles.logintext]}
                onPress={this.emailLogin}
              >
                ALREADY A MEMBER? LOGIN
              </Text>
            </View>}
          </View>
      );
    // }
  }
}

var styles = StyleSheet.create({
  option: {
    flex:1,
    paddingHorizontal: 10,
  },
  options: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 74,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection : 'row'
  },
  loadingcontainer: {
    backgroundColor: 'transparent',
    // position: 'absolute',
    bottom: 180,
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingsymbol: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  logintextcontainer: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 30,
    marginTop: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logintext: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 16,
    textAlign: 'center',
    color:'white',
    alignSelf: 'center'
  }
});

export default createRenderer(Login, {
  queries: ViewerQuery,
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        introQuizId
      }
    `
  }
});
