import {
	createRouter,
} from '@exponent/ex-navigation';


import InviteModal from '../components/Home/debate/InviteModal';
import Login from '../components/Login';
import EmailLogin from '../components/Login/EmailLogin';
import Signup from '../components/Signup';
import ArticleCard from '../components/Home/article/ArticleCard';
import PictoralPostCard from '../components/Home/pictoralPost/PictoralPostCard';
import VideoCard from '../components/Home/video/VideoCard';
import DebateCard from '../components/Home/debate/DebateCard';
import QuizCard from '../components/Home/quiz/QuizCard';
import Home from '../components/Home';
import GetStarted from '../components/Signup/GetStarted';
import QuizScreen from '../components/QuizScreen';
import UserProfile from '../components/UserProfile';
// import ViewerProfile from '../components/ViewerProfile';
import DebateScreen from '../components/DebateScreen';
import Search from '../components/Search';
import PasswordReset from '../components/Login/PasswordReset';
import FollowSuggestions from '../components/Login/FollowSuggestions';
import UsernameEdit from '../components/Login/UsernameEdit';
import CommentCard from '../components/DebateScreen/Debate/CommentCard';
import CommentLikeModal from '../components/DebateScreen/Debate/CommentLikeModal';
import ArticleCommentCard from '../components/Home/article/ArticleCommentCard';
import ArticleEngageModal from '../components/Home/article/ArticleEngageModal';
import ArticleLikeModal from '../components/Home/article/ArticleLikeModal';
import ArticleCommentLikeModal from '../components/Home/article/ArticleCommentLikeModal';
import PictoralPostCommentCard from '../components/Home/pictoralPost/PictoralPostCommentCard';
import PictoralPostEngageModal from '../components/Home/pictoralPost/PictoralPostEngageModal';
import PictoralPostLikeModal from '../components/Home/pictoralPost/PictoralPostLikeModal';
import PictoralPostCommentLikeModal from '../components/Home/pictoralPost/PictoralPostCommentLikeModal';
import VideoCommentCard from '../components/Home/video/VideoCommentCard';
import VideoEngageModal from '../components/Home/video/VideoEngageModal';
import VideoLikeModal from '../components/Home/video/VideoLikeModal';
import VideoCommentLikeModal from '../components/Home/video/VideoCommentLikeModal';
import QuizCommentCard from '../components/Home/quiz/QuizCommentCard';
import QuizEngageModal from '../components/Home/quiz/QuizEngageModal';
import QuizLikeModal from '../components/Home/quiz/QuizLikeModal';
import QuizCommentLikeModal from '../components/Home/quiz/QuizCommentLikeModal';
import EngageModal from '../components/DebateScreen/Debate/EngageModal';
import FollowerSuggestionByQuiz from '../components/DebateScreen/Opinions/FollowerSuggestionByQuiz';
import LeaderBoard from '../components/LeaderBoard';
import QuestionScreen from '../components/DebateScreen/Opinions/QuestionScreen';
import QuizResultScreen from '../components/DebateScreen/Opinions/QuizResultScreen';
import ReasonCard from '../components/DebateScreen/Debate/ReasonCard';
import ReasonLikeModal from '../components/DebateScreen/Debate/ReasonLikeModal';
import RecommendationByQuiz from '../components/DebateScreen/Opinions/RecommendationByQuiz';
import SimilarityToFollowers from '../components/DebateScreen/Opinions/SimilarityToFollowers';
import SubquizScreen from '../components/DebateScreen/Opinions/SubquizScreen';
import UserCard from '../components/Common/UserCard';
import VideoPlayer from '../components/Home/video/VideoPlayer';
import ArticlePage from '../components/Home/article/ArticlePage';
import PictoralPostPage from '../components/Home/pictoralPost/PictoralPostPage';
import SendVerificationCode from '../components/Login/SendVerificationCode';
import VerificationCodeCheck from '../components/Login/VerificationCodeCheck';
import DebateLikeModal from '../components/Home/debate/DebateLikeModal';
import UserProfilePicture from '../components/Common/UserProfilePicture';
import FeedbackForm from '../components/Home/FeedbackForm';

const Router = createRouter(() => ({
	feedback : () => FeedbackForm,
  inviteModal : () => InviteModal,
	sendVerificationCode : () => SendVerificationCode,
	verificationCodeCheck : () => VerificationCodeCheck,
	articleCommentCard : () => ArticleCommentCard,
	articleLikeModal : () => ArticleLikeModal,
	articleEngageModal : () => ArticleEngageModal,
	articleCommentLikeModal : () => ArticleCommentLikeModal,
  pictoralPostCommentCard : () => PictoralPostCommentCard,
  pictoralPostLikeModal : () => PictoralPostLikeModal,
  pictoralPostEngageModal : () => PictoralPostEngageModal,
  pictoralPostCommentLikeModal : () => PictoralPostCommentLikeModal,
	videoCommentCard : () => VideoCommentCard,
	videoLikeModal : () => VideoLikeModal,
	videoEngageModal : () => VideoEngageModal,
	videoCommentLikeModal : () => VideoCommentLikeModal,
	quizCommentCard : () => QuizCommentCard,
	quizLikeModal : () => QuizLikeModal,
	quizEngageModal : () => QuizEngageModal,
	quizCommentLikeModal : () => QuizCommentLikeModal,
	login: () => Login,
	emailLogin : () => EmailLogin,
	signup : () => Signup,
	getStarted : () => GetStarted,
	quizScreen : () => QuizScreen,
	userProfile : () => UserProfile,
	// viewerProfile : () => ViewerProfile,
	debateScreen : () => DebateScreen,
	search : () => Search,
	passwordReset : () => PasswordReset,
	followSuggestions : () => FollowSuggestions,
	usernameEdit : () => UsernameEdit,
	home : () => Home,
	articleCard : () => ArticleCard,
  pictoralPostCard : () => PictoralPostCard,
	commentCard : () => CommentCard,
	quizCard : () => QuizCard,
	VideoCard : () => VideoCard,
	DebateCard : () => DebateCard,
	commentLikeModal : () => CommentLikeModal,
	engageModal : () => EngageModal,
	followerSuggestionByQuiz : () => FollowerSuggestionByQuiz,
	leaderBoard : () => LeaderBoard,
	questionScreen : () => QuestionScreen,
	quizResultScreen : () => QuizResultScreen,
	reasonCard : () => ReasonCard,
	reasonLikeModal : () => ReasonLikeModal,
	recommendationByQuiz : () => RecommendationByQuiz,
	similarityToFollowers : () => SimilarityToFollowers,
	subquizScreen : () => SubquizScreen,
	userCard : () => UserCard,
	videoPlayer : () => VideoPlayer,
	articlePage : () => ArticlePage,
  pictoralPostPage : () => PictoralPostPage,
	debateLikeModal : () => DebateLikeModal,
  userProfilePicture : () => UserProfilePicture
}));

export default Router;
