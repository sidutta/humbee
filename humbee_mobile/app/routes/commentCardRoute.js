'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class commentCardRoute extends Route {
  static routeParams = {
    commentId : ''
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      node(id : $commentId) {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'commentCardRoute';
}
