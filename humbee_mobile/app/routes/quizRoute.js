'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class quizRoute extends Route {
  static routeParams = {
    quizId : ''
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      node(id : $quizId) {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'quizRoute';
}
