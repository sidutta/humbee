'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class viewerRoute extends Route {
  static paramDefinitions = {
    //
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      Viewer {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'viewerRoute';
}
