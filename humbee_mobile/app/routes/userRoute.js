'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class userRoute extends Route {
  static routeParams = {
    userId : ''
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      node(id : $userId) {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'userRoute';
}
