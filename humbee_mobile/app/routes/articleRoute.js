'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class articleRoute extends Route {
  static routeParams = {
    articleId : ''
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      node(id : $articleId) {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'articleRoute';
}
