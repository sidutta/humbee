'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class debateRoute extends Route {
  static routeParams = {
    debateId : '',
    quizId : '1'
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      node(id : $debateId) {
        ${Component.getFragment('viewer')}
      }
    }`,
    viewer2: (Component) => Relay.QL`query {
      node(id : $quizId) {
        ${Component.getFragment('viewer2')}
      }
    }`
  };
  static routeName = 'debateRoute';
}
