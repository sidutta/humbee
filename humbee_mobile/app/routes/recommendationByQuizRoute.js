'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class recommendationByQuizRoute extends Route {
  static routeParams = {
    masterCharacterizationId : ''
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      Viewer {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'recommendationByQuizRoute';
}
