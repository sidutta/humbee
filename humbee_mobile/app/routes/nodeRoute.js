'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class nodeRoute extends Route {
  static routeParams = {
    id : ''
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      node(id : $id) {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'nodeRoute';
}
