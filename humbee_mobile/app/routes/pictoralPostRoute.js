'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class pictoralPostRoute extends Route {
  static routeParams = {
    pictoralPostId : ''
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      node(id : $pictoralPostId) {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'pictoralPostRoute';
}
