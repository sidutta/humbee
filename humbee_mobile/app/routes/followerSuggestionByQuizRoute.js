'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class followerSuggestionByQuizRoute extends Route {
  static routeParams = {
    subquizScoresArg : ''
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      Viewer {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'followerSuggestionByQuizRoute';
}
