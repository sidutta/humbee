'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class videoRoute extends Route {
  static routeParams = {
    videoId : ''
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      node(id : $videoId) {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'videoRoute';
}
