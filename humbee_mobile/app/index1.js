'use strict';

import Relay from 'react-relay';
import React, { Component } from 'react';
import Login from './components/Login';
import { Navigator, Dimensions, AsyncStorage, BackAndroid } from 'react-native';
import FCM from 'react-native-fcm';

var _navigator; // we fill this up upon on first navigation.

BackAndroid.addEventListener('hardwareBackPress', () => {
	if (_navigator.getCurrentRoutes().length === 1  ) {
		 return false;
	}
	console.log(_navigator.getCurrentRoutes());
	if(_navigator.getCurrentRoutes()[_navigator.getCurrentRoutes().length-1].passProps.isIntroQuiz) {
		// AsyncStorage.getItem('token').then((token) => {
		//   _navigator.push({
		//    component : HomeContainer,
		//    passProps : {
		//      title : 'Home',
		//      token : token
		//    }
		//   });
		// })
		return true;
	}
	else {
	 _navigator.pop();
	}
	return true;
});

class app extends Component {
	constructor(props){
		super(props);
		this.state={
			NavBarHeight : 0
		};
		this.renderScene = this.renderScene.bind(this);
		this.configureScene = this.configureScene.bind(this);
	}

	renderScene(route, navigator){
		_navigator = navigator;
		const { NavBarHeight } = this.state;
		return React.createElement(route.component, {...this.props, ...route.passProps, route, navigator, NavBarHeight });
	}

	configureScene(route){
		if(route.sceneConfig == 'FloatFromBottom'){
			return Navigator.SceneConfigs.FloatFromBottom;
		}
		if( route.sceneConfig == 'PushFromRight'){
			return Navigator.SceneConfigs.PushFromRight;
		}
		return Navigator.SceneConfigs.FadeAndroid;
	}

	componentWillMount(){
		const ScreenHeight = Dimensions.get('window').height;
		const NavBarHeight = ScreenHeight / 10;
		this.setState({ NavBarHeight});
	}

	componentDidMount() {
		FCM.requestPermissions(); // for iOS
		FCM.getFCMToken().then(token => {
			console.log(token)
			alert(token);
				// store fcm token in your server
		});
		this.notificationListener = FCM.on('notification', (notif) => {
				// there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
			if(notif.local_notification){
					//this is a local notification
			}
			if(notif.opened_from_tray){
				//app is open/resumed because user clicked banner
			}
		});
		this.refreshTokenListener = FCM.on('refreshToken', (token) => {
			alert(token);
			console.log(token)
				// fcm token may not be available on first load, catch it here
		});
	}

	componentWillUnmount() {
			// stop listening for events
		this.notificationListener.remove();
		this.refreshTokenListener.remove();
	}

	render() {
		return (
			<Navigator
				configureScene={ this.configureScene }
				initialRoute={{ component : Login, passProps: { title : 'Login' , viewer : this.props.viewer} }}
				renderScene={this.renderScene}
			/>
		);
	}
}

export default Relay.createContainer(app, {
	fragments: {
		viewer: () => Relay.QL`
			fragment on Viewer {
				${Login.getFragment('viewer')}
			}
		`
	}
});
