import RelayStore from './RelayStore';
import NetworkLayer from './NetworkLayer';
import { urlMiddleware } from 'react-relay-network-layer';
import { AsyncStorage } from 'react-native';

export default function ResetRelayStore(){
  RelayStore.reset(
    new NetworkLayer([
      urlMiddleware({
        url : () => 'http://192.168.43.65:8080/graphql',
        batchUrl: () => 'https://staging-dot-humb-app.appspot-preview.com/graphql/batch'
      }),
      next => req => AsyncStorage.getItem('token').then((token) => {
        req.headers['Authorization'] = 'Bearer ' + token;
        return next(req);
      })
      .catch((e) => {
        req.header['Authorization'] = 'Bearer freeToken';
        // console.log(e)
        return next(req);
      })
    ], { disableBatchQuery : true})
  );
}
