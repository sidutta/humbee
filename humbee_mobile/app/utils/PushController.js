import  { Component } from 'react';
import RelayStore from './RelayStore';
import AddDeviceTokenMutation from '../mutations/AddDeviceTokenMutation';

import FCM, {FCMEvent} from 'react-native-fcm';

export default class PushController extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		FCM.requestPermissions();
		this.notificationListener = FCM.on(FCMEvent.Notification, (notif) => {
			// console.log(notif);
			alert('something');
				// there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
			if(notif.local_notification){
				alert('something');
			}
			if(notif.opened_from_tray){
				alert('something');
			}
		});
		this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, (token) => {
			alert(token);
			// console.log(token)
				// fcm token may not be available on first load, catch it here
		});
	}

	showLocalNotification(notif) {
		FCM.presentLocalNotification({
			title: notif.title,
			body: notif.body,
			priority: 'high',
			click_action: notif.click_action,
			show_in_foreground: true,
			local: true
		});
	}

	render() {
		return null;
	}
}
