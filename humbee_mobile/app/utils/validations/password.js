import isEmpty from 'lodash/isEmpty';

export default function validateInput(data) {
  let errors = {};

  const password = data.password;
  const length = password.length;
  if(length < 8) {
    errors.password = 'Password must have atleast 8 characters.';
  }

  let hasLetter = false;
  let hasNum = false;
  let hasSpecialChar = false;

  for(let i=0; i<length; i++) {
    let x = password.charCodeAt(i);

    if((x>64&&x<91)||(x>96&&x<123)||(x>127&&x<155)||(x>159&&x<166)) {
      hasLetter = true;
    }
    else if(x>47&&x<58) {
      hasNum = true;
    }
    else {
      hasSpecialChar = true;
    }
  }

  if(!(hasLetter && hasNum && hasSpecialChar)) {
    errors.password = 'Password must have atleast one alphabet, number and special character.';
  }

  return {
    errors,
    isValid : isEmpty(errors)
  };
}
