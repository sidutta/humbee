import React from 'react';
import colors from '../config/colors';
import { View } from 'react-native';
const NewsFeedCardsDummyView = (
	<View style={{ height : 270, padding : 5}}>
		<View elevation={5} style={{ borderRadius : 5, backgroundColor : 'white', padding : 5}}>
			<View style={{ height : 30, backgroundColor : 'white'}}/>
			<View style={{ height : 150, backgroundColor : colors.humbeeGrey}}/>
			<View style={{ height : 35 , backgroundColor : 'white', padding : 5, paddingLeft : 0, flexDirection : 'row'}}>
				<View style={{ flex : 1, backgroundColor : colors.humbeeGrey}}/>
				<View style={{flex : 3}}/>
			</View>
			<View style={{ height : 35, padding : 5, paddingLeft : 0, flexDirection : 'row'}}>
				<View style={{ flex : 1, marginRight : 5, backgroundColor : colors.humbeeGrey}}/>
				<View style={{ flex : 1, backgroundColor : colors.humbeeGrey}}/>
				<View style={{ flex : 1, marginLeft : 5, backgroundColor : colors.humbeeGrey}}/>
			</View>
		</View>
	</View>
);
const UserCardDummyView = (
	<View style={{ padding : 5}}>
		<View style={{ height : 60, flexDirection : 'row'}}>
			<View style={{ flex : 4.5, borderRadius : 60, backgroundColor : colors.humbeeGrey}}/>
			<View style={{ flex : 16, paddingTop : 20, paddingBottom : 20, paddingRight : 50, paddingLeft : 20}}>
				<View style={{height : 15, backgroundColor : colors.humbeeGrey}}/>
			</View>
		</View>
	</View>
);

export default {
	UserCardDummyView : UserCardDummyView,
	NewsFeedCardsDummyView : NewsFeedCardsDummyView
};
