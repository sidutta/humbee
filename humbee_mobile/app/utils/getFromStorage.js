import { AsyncStorage } from 'react-native';

export default async function getFromStorage(key){
  let value = await AsyncStorage.getItem(key);
  return value;
}
