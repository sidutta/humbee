import { AsyncStorage } from 'react-native';

export default async function setToStorage(key, value){
  await AsyncStorage.setItem(key, value);
}
