import Relay from 'react-relay';

export default class DeleteReasonMutation extends Relay.Mutation {

  getMutation () {
    return Relay.QL`
      mutation {
        deleteReason
      }
    `;
  }

  getVariables () {
    return {
      reasonId : this.props.reasonId,
      debateId : this.props.debateId
    };
  }

  getFatQuery () {
    return Relay.QL`
      fragment on DeleteReasonPayload {
        reasonId,
        debate {
          reasons,
          tags
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'NODE_DELETE',
      parentName: 'debate',
      parentID: this.props.debateId,
      connectionName: 'reasons',
      deletedIDFieldName: 'reasonId',
    }];
  }
}
