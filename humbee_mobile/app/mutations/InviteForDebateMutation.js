import Relay from 'react-relay';

export default class InviteForDebateMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { inviteForDebate }
		`;
	}

	getVariables () {
		return {
			inviteeList : this.props.inviteeList,
			debateTitle : this.props.debateTitle,
			debateId : this.props.debateId
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on InviteForDebatePayload {
				success
			}
		`;
	}

	getConfigs() {
		return [{
			type: 'REQUIRED_CHILDREN',
			children: [
				Relay.QL`
					fragment on InviteForDebatePayload {
						success
					}
				`,
			]
		}];
	}
}
