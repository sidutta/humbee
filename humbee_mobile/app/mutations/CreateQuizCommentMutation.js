import Relay from 'react-relay';

export default class CreateQuizCommentMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		  mutation { createQuizComment }
		`;
	}

	getVariables () {
		return {
			comment: this.props.comment,
			quizId : this.props.quizId
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateQuizCommentPayload {
				quiz {
          id,
          numberOfComments,
          quizComments
        }
			}
		`;
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					quiz : this.props.quizId
				}
			}];
	}
}
