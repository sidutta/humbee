import RelayMutationType from 'react-relay';
import Relay from 'react-relay';

export default class SetProfilePictureMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { setProfilePicture }
		`;
	}

	getVariables () {
		return {
			id: this.props.id,
			image : this.props.image
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on SetProfilePicturePayload {
				token
			}
		`;
	}

	getConfigs() {
	return [{
		type: 'REQUIRED_CHILDREN',
		children: [
			Relay.QL`
				fragment on SetProfilePicturePayload {
					token
				}
			`,
		]
	}];
}

	static fragments = {
  };
}
