import Relay from 'react-relay';

export default class CreateVideoCommentLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		  mutation { createVideoCommentLike }
		`;
	}

	getVariables () {
		return {
			videoCommentId : this.props.videoCommentId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateVideoCommentLikePayload {
				videoComment {
          id,
          numberOfLikes,
          liked
        }
			}
		`;
	}

	getOptimisticResponse(){
		let { liked, videoCommentId, numberOfLikes } = this.props;
    if(liked) numberOfLikes = numberOfLikes - 1;
    else numberOfLikes = numberOfLikes + 1
		return {
			videoComment : {
        id : videoCommentId,
				liked : !liked,
        numberOfLikes: numberOfLikes
			}
		};
	}

  getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					videoComment : this.props.videoCommentId
				}
			}];
	}
}
