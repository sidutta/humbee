import Relay from 'react-relay';

export default class CreatePictoralPostLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
			mutation { createPictoralPostLike }
		`;
	}

	getVariables () {
		return {
			pictoralPostId : this.props.pictoralPostId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreatePictoralPostLikePayload {
				pictoralPost {
					id,
					numberOfLikes,
					liked
				}
			}
		`;
	}

	getOptimisticResponse() {
		let { liked, pictoralPostId, numberOfLikes } = this.props;
    if(liked) numberOfLikes = numberOfLikes - 1;
    else numberOfLikes = numberOfLikes + 1
		return {
			pictoralPost : {
				id : pictoralPostId,
				liked : !liked,
        numberOfLikes : numberOfLikes
			}
		};
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					pictoralPost : this.props.pictoralPostId
				}
			}
		];
	}
}
