import Relay from 'react-relay';

export default class AddDeviceTokenMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { addDeviceToken }
		`;
	}

	getVariables () {
		return {
			token: this.props.token,
			deviceType : this.props.deviceType
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on AddDeviceTokenPayload {
				success
			}
		`;
	}

	getConfigs() {
		return [{
			type: 'REQUIRED_CHILDREN',
			children: [
				Relay.QL`
					fragment on AddDeviceTokenPayload {
						success
					}
				`,
			]
		}];
	}
}
