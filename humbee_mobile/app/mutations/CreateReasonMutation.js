import Relay from 'react-relay';

export default class CreateReasonMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		  mutation { createReason }
		`;
	}

	getVariables () {
		return {
			reason: this.props.reason,
			agree : this.props.agree,
      tags : this.props.tags,
			debateId : this.props.debateId,
      beAnonymous : this.props.beAnonymous,
      edit : this.props.edit,
      previousReasonId : this.props.previousReasonId
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateReasonPayload {
        reasonId,
				reason,
				debate {
					reasons,
          tags
				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					debate : this.props.debateId
				}
			},
      {
        type: 'NODE_DELETE',
        parentName: 'debate',
        parentID: this.props.debateId,
        connectionName: 'reasons',
        deletedIDFieldName: 'reasonId',
      }
    ];
	}
}
