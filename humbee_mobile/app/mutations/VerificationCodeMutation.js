import RelayMutationType from 'react-relay';
import Relay from 'react-relay';

export default class VerificationCodeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { verificationCode }
		`;
	}

	getVariables () {
		return {
			identifier: this.props.identifier
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on VerificationCodePayload {
				maskedEmail
			}
		`;
	}

	getConfigs() {
		return [{
			type: 'REQUIRED_CHILDREN',
			children: [
				Relay.QL`
					fragment on VerificationCodePayload {
						maskedEmail
					}
				`,
			]
		}];
	}

	static fragments = {
  };
}
