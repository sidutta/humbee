import Relay from 'react-relay';

export default class CreateArticleCommentLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		  mutation { createArticleCommentLike }
		`;
	}

	getVariables () {
		return {
			articleCommentId : this.props.articleCommentId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateArticleCommentLikePayload {
				articleComment {
          id,
          numberOfLikes,
          liked
        }
			}
		`;
	}

	getOptimisticResponse(){
		let { liked, articleCommentId } = this.props;
		return {
			articleComment : {
        id : articleCommentId,
				liked : !liked
			}
		};
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					articleComment : this.props.articleCommentId
				}
			}];
	}
}
