import Relay from 'react-relay';

export default class BulkFollowMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { bulkFollow }
		`;
	}

	getVariables () {
		return {
			followingList : this.props.followingList
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on BulkFollowPayload {
				success
			}
		`;
	}

	getConfigs() {
		return [{
			type: 'REQUIRED_CHILDREN',
			children: [
				Relay.QL`
					fragment on BulkFollowPayload {
						success
					}
				`,
			]
		}];
	}
}
