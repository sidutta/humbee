import Relay from 'react-relay';

export default class CreateReasonLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		  mutation { createReasonLike }
		`;
	}

	getVariables () {
		return {
			reasonId : this.props.reasonId,
			liked : this.props.liked,
      debateId: this.props.debateId,
      userId: this.props.userId
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateReasonLikePayload {
				reason {
          liked,
          likes
        }
			}
		`;
	}

	getOptimisticResponse(){
		let { liked, reasonId } = this.props;
		return {
			reason : {
        id: reasonId,
				liked : !liked
			}
		};
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					reason : this.props.reasonId
				}
			}];
	}
}
