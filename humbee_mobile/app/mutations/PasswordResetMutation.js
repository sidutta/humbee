import RelayMutationType from 'react-relay';
import Relay from 'react-relay';

export default class PasswordResetMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { passwordReset }
		`;
	}

	getVariables () {
		return {
			identifier : this.props.identifier,
			password : this.props.password
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on PasswordResetPayload {
				passwordResetResult
			}
		`;
	}

	getConfigs() {
		return [{
			type: 'REQUIRED_CHILDREN',
			children: [
				Relay.QL`
					fragment on PasswordResetPayload {
						passwordResetResult
					}
				`,
			]
		}];
	}

	static fragments = {
  };
}
