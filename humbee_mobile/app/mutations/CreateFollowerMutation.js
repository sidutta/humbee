import Relay from 'react-relay';

export default class CreateFollowerMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createFollower }
		`;
	}

	getVariables () {
		return {
			followingId : this.props.followingId,
			isFollowing : this.props.isFollowing
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateFollowerPayload {
				user {
					isFollowing
				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					user : this.props.followingId
				}
			}
    ];
	}
}
