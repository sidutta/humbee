import Relay from 'react-relay';

export default class CreateCommentLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createCommentLike }
		`;
	}

	getVariables () {
		return {
			commentId : this.props.commentId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateCommentLikePayload {
				comment {
          id,
          numberOfLikes,
          liked
        }
			}
		`;
	}

	getOptimisticResponse(){
		let { liked, commentId, numberOfLikes } = this.props;
    if(liked) numberOfLikes = numberOfLikes - 1;
    else numberOfLikes = numberOfLikes + 1
		return {
			comment : {
        id : commentId,
				liked : !liked,
        numberOfLikes : numberOfLikes
			}
		};
	}
	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					comment : this.props.commentId
				}
			}];
	}
}
