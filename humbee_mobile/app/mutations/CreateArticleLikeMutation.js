import Relay from 'react-relay';

export default class CreateArticleLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
			mutation { createArticleLike }
		`;
	}

	getVariables () {
		return {
			articleId : this.props.articleId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateArticleLikePayload {
				article {
					id,
					numberOfLikes,
					liked
				}
			}
		`;
	}

	getOptimisticResponse() {
		let { liked, articleId, numberOfLikes } = this.props;
    if(liked) numberOfLikes = numberOfLikes - 1;
    else numberOfLikes = numberOfLikes + 1
		return {
			article : {
				id : articleId,
				liked : !liked,
        numberOfLikes : numberOfLikes
			}
		};
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					article : this.props.articleId
				}
			}
		];
	}
}
