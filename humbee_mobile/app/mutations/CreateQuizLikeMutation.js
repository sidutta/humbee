import Relay from 'react-relay';

export default class CreateQuizLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createQuizLike }
		`;
	}

	getVariables () {
		return {
			quizId : this.props.quizId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateQuizLikePayload {
				quiz {
          id,
          numberOfLikes,
          liked
        }
			}
		`;
	}

	getOptimisticResponse(){
		let { liked, quizId, numberOfLikes } = this.props;
    if(liked) numberOfLikes = numberOfLikes - 1;
    else numberOfLikes = numberOfLikes + 1
		return {
			quiz : {
        id : quizId,
				liked : !liked,
        numberOfLikes : numberOfLikes
			}
		};
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					quiz : this.props.quizId
				}
			}];
	}
}
