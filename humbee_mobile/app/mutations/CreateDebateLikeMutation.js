import Relay from 'react-relay';

export default class CreateDebateLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
			mutation { createDebateLike }
		`;
	}

	getVariables () {
		return {
			debateId : this.props.debateId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateDebateLikePayload {
				debate {
					id,
					numberOfLikes,
					liked
				}
			}
		`;
	}

	getOptimisticResponse() {
		let { liked, debateId } = this.props;
		return {
			debate : {
				id : debateId,
				liked : !liked
			}
		};
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					debate : this.props.debateId
				}
			}
		];
	}
}
