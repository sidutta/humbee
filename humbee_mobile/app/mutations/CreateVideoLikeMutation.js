import Relay from 'react-relay';

export default class CreateVideoLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		  mutation { createVideoLike }
		`;
	}

	getVariables () {
		return {
			videoId : this.props.videoId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateVideoLikePayload {
				video {
          id,
          numberOfLikes,
          liked
        }
			}
		`;
	}

	getOptimisticResponse(){
		let { liked, videoId, numberOfLikes } = this.props;
    if(liked) numberOfLikes = numberOfLikes - 1;
    else numberOfLikes = numberOfLikes + 1
		return {
			video : {
        id : videoId,
				liked : !liked,
        numberOfLikes : numberOfLikes
			}
		};
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					video : this.props.videoId
				}
			}
    ];
	}
}
