import Relay from 'react-relay';

export default class CreatePictoralPostCommentLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		  mutation { createPictoralPostCommentLike }
		`;
	}

	getVariables () {
		return {
			pictoralPostCommentId : this.props.pictoralPostCommentId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreatePictoralPostCommentLikePayload {
				pictoralPostComment {
          id,
          numberOfLikes,
          liked
        }
			}
		`;
	}

	getOptimisticResponse(){
		let { liked, pictoralPostCommentId } = this.props;
		return {
			pictoralPostComment : {
        id : pictoralPostCommentId,
				liked : !liked
			}
		};
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					pictoralPostComment : this.props.pictoralPostCommentId
				}
			}];
	}
}
