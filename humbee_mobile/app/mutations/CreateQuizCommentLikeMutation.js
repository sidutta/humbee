import Relay from 'react-relay';

export default class CreateQuizCommentLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createQuizCommentLike }
		`;
	}

	getVariables () {
		return {
			quizCommentId : this.props.quizCommentId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateQuizCommentLikePayload {
				quizComment {
          id,
          numberOfLikes,
          liked
        }
			}
		`;
	}

	getOptimisticResponse(){
		let { quizCommentId, liked, numberOfLikes } = this.props;
    if(liked) numberOfLikes = numberOfLikes - 1;
    else numberOfLikes = numberOfLikes + 1
		return {
			quizComment : {
        id : quizCommentId,
				liked : !liked,
        numberOfLikes : numberOfLikes
			}
		};
	}
	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					quizComment : this.props.quizCommentId
				}
			}];
	}
}
