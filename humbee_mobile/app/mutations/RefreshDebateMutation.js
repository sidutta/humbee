import Relay from 'react-relay';

export default class RefreshDebateMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { refreshDebate }
		`;
	}

	getVariables () {
		return {
			debateId : this.props.debateId
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on RefreshDebatePayload {
        debate{
					reasons
				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					debate : this.props.debateId
				}
			}];
	}
}
