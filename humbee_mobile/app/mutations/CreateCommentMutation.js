import Relay from 'react-relay';

export default class CreateCommentMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		  mutation { createComment }
		`;
	}

	getVariables () {
		return {
			comment: this.props.comment,
			reasonId : this.props.reasonId
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateCommentPayload {
				reason {
          id,
          numberOfComments,
          comments
        }
			}
		`;
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					reason : this.props.reasonId
				}
			}
    ];
	}
}
