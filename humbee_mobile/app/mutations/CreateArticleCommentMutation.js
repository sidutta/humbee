import Relay from 'react-relay';

export default class CreateArticleCommentMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		  mutation { createArticleComment }
		`;
	}

	getVariables () {
		return {
			comment: this.props.comment,
			articleId : this.props.articleId
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateArticleCommentPayload {
				article {
          id,
          articleComments
        }
			}
		`;
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					article : this.props.articleId
				}
			}
    ];
	}
}
