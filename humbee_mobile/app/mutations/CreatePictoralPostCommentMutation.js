import Relay from 'react-relay';

export default class CreatePictoralPostCommentMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		  mutation { createPictoralPostComment }
		`;
	}

	getVariables () {
		return {
			comment: this.props.comment,
			pictoralPostId : this.props.pictoralPostId
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreatePictoralPostCommentPayload {
				pictoralPost {
          id,
          pictoralPostComments
        }
			}
		`;
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					pictoralPost : this.props.pictoralPostId
				}
			}
    ];
	}
}
