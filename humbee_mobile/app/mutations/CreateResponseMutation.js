import Relay from 'react-relay';

export default class CreateResponseMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createResponse }
		`;
	}

	getVariables () {
    alert(this.props.previousReasonId)
		return {
			optionId : this.props.optionId,
			questionId : this.props.questionId,
      edit : this.props.edit
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateResponsePayload {
				question
			}
		`;
	}

	getOptimisticResponse(){
    let { questionId } = this.props;
		return {
			question : {
				answered : true,
        id : questionId
			}
		};
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					question : this.props.questionId
				}
			}
    ];
	}
}
