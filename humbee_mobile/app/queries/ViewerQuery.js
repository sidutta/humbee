import Relay from 'react-relay';

const ViewerQuery = {
	viewer: () => Relay.QL`
		query {
			Viewer
		}
	`,
};

export default ViewerQuery;
