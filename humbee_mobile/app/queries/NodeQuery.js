import Relay from 'react-relay';

const NodeQuery = {
	viewer: (Component) => Relay.QL`
		query {
			node(id : $id){
				${Component.getFragment('viewer')}
			}
		}
	`,
};

export default NodeQuery;
