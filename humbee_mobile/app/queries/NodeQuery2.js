import Relay from 'react-relay';

const NodeQuery2 = {
	viewer: (Component) => Relay.QL`query {
		node(id : $debateId) {
			${Component.getFragment('viewer')}
		}
	}`,
	viewer2: (Component) => Relay.QL`query {
		node(id : $quizId) {
			${Component.getFragment('viewer2')}
		}
	}`
};

export default NodeQuery2;
