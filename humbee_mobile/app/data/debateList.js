const debateList = [
	{
		title : 'India strikes back',
		motionShort : 'Befitting response or over-reaction?',
		motionFull : 'Do you stand by the Indian Government action against Pakistani terrorist outfits?',
		agree : 40,
		disagree : 60,
		imageUrl : 'https://storage.googleapis.com/humbee_images/surgical_strikes.jpg',
		id : 1
	},
	{
		title : 'GST deadlock',
		motionShort : 'What is the way forward?',
		motionFull : 'The union government has delivered, now the states need to deliver.',
		agree : 25,
		disagree : 75,
		imageUrl : 'https://storage.googleapis.com/humbee_images/gst.jpg',
		id : 2
	},
	{
		title : 'Hillary vs Trump',
		motionShort : 'Race to the bottom',
		motionFull : 'The Americans can do better than the two, time to rethink the two-party system?',
		agree : 35,
		disagree : 65,
		imageUrl : 'https://storage.googleapis.com/humbee_images/trump_hillary.jpg',
		id : 3
	},
	{
		title : 'Syrian humanitarian crisis',
		motionShort : 'Everyone is responsible',
		motionFull : 'The crisis has highlighted the deep flaws in international order.',
		agree : 56,
		disagree : 44,
		imageUrl : 'https://storage.googleapis.com/humbee_images/syrian_boy.jpg',
		id : 4
	}
]

module.exports = debateList;
