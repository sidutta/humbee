const questions = [
	{
		questionId : 1,
		text : 'Do you believe that recent surgical strikes could lead to a nuclear war',
		numberOfOptions : 2,
		options : [
			{
				text : 'Yes',
				votes : 60
			},
			{
				text : 'No',
				votes : 40
			}
		]
	},
	{
		questionId : 2,
		text : 'Do you think that India should provide video evidence for the strikes',
		numberOfOptions : 2,
		options : [
			{
				text : 'Yes',
				votes : 85
			},
			{
				text : 'No',
				votes : 15
			}
		]
	},
	{
		questionId : 3,
		text : 'Do you agree that the ruling party is trying to derive political gains from the strikes',
		numberOfOptions : 4,
		options : [
			{
				text : 'Yes and there is nothing wrong with that',
				votes : 30
			},
			{
				text : 'Yes and it is unethical',
				votes : 20
			},
			{
				text : 'No they have been professional about it',
				votes : 25
			},
			{
				text : 'No they should publicize it even more',
				votes : 25
			}
		]
	},
	{
		questionId : 4,
		text : 'Who do you think deserves the credit for the surgical strikes',
		numberOfOptions : 3,
		options : [
			{
				text : 'Indian Army alone',
				votes : 30
			},
			{
				text : 'Indian Army and the Government',
				votes : 60
			},
			{
				text : 'The Government alone',
				votes : 10
			}
		]
	},
	{
		questionId : 5,
		text : 'Do you believe that recent surgical strikes could lead to a nuclear war',
		numberOfOptions : 2,
		options : [
			{
				text : 'Yes',
				votes : 60
			},
			{
				text : 'No',
				votes : 40
			}
		]
	},
	{
		questionId : 6,
		text : 'Do you think that India should provide video evidence for the strikes',
		numberOfOptions : 2,
		options : [
			{
				text : 'Yes',
				votes : 85
			},
			{
				text : 'No',
				votes : 15
			}
		]
	},
	{
		questionId : 7,
		text : 'Do you agree that the ruling party is trying to derive political gains from the strikes',
		numberOfOptions : 4,
		options : [
			{
				text : 'Yes and there is nothing wrong with that',
				votes : 30
			},
			{
				text : 'Yes and it is unethical',
				votes : 20
			},
			{
				text : 'No they have been professional about it',
				votes : 25
			},
			{
				text : 'No they should publicize it even more',
				votes : 25
			}
		]
	},
	{
		questionId : 8,
		text : 'Who do you think deserves the credit for the surgical strikes',
		numberOfOptions : 3,
		options : [
			{
				text : 'Indian Army alone',
				votes : 30
			},
			{
				text : 'Indian Army and the Government',
				votes : 60
			},
			{
				text : 'The Government alone',
				votes : 10
			}
		]
	}
];

module.exports = questions;
