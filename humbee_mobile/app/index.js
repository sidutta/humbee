import React, { Component } from 'react';
import {
	NavigationProvider,
	StackNavigation,
} from '@exponent/ex-navigation';

import { AsyncStorage, View, StatusBar } from 'react-native';

import RelayStore from './utils/RelayStore';
import Router from './routes/Router';
import NetworkLayer from './utils/NetworkLayer';
import { urlMiddleware } from 'react-relay-network-layer';
import codePush from 'react-native-code-push';

RelayStore.reset(
	new NetworkLayer([
		urlMiddleware({
			url : () => 'http://192.168.43.65:8080/graphql',
			batchUrl : () => 'https://staging-dot-humb-app.appspot-preview.com/graphql/batch'
		}),
		next => req => AsyncStorage.getItem('token').then((token) => {
			req.headers['Authorization'] = 'Bearer ' + token;
			return next(req);
		})
		.catch((e) => {
			req.header['Authorization'] = 'Bearer freeToken';
			// console.log(e)
			return next(req);
		})
	], { disableBatchQuery : true})
);

class RelayApp extends Component {

	codePushStatusDidChange(status) {
		switch(status) {
		case codePush.SyncStatus.CHECKING_FOR_UPDATE:
			// console.log('Checking for updates.');
			break;
		case codePush.SyncStatus.DOWNLOADING_PACKAGE:
			// console.log('Downloading package.');
			break;
		case codePush.SyncStatus.INSTALLING_UPDATE:
			// console.log('Installing update.');
			break;
		case codePush.SyncStatus.UP_TO_DATE:
			// console.log('Up-to-date.');
			break;
		case codePush.SyncStatus.UPDATE_INSTALLED:
			// console.log('Update installed.');
			break;
		}
	}

	codePushDownloadDidProgress(progress) {
			console.log(progress.receivedBytes + ' of ' + progress.totalBytes + ' received.');
	}
	render() {
		return (
			<View style={{flex : 1}}>
				<StatusBar
					backgroundColor={'#005054'}
					barStyle='light-content'
				/>
				<NavigationProvider router={Router}>
					<StackNavigation
						id='master'
						initialRoute={Router.getRoute('login')}
					/>
				</NavigationProvider>
			</View>
		);
	}
}

export default RelayApp = codePush(RelayApp);
