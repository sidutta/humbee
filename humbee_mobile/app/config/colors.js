'use strict';
const colors = {
	humbeeGrey : '#E0E0E0',
	humbeeGreen : '#006064',
	humbeeDarkGrey : '#9E9E9E'
};

module.exports = colors;
