import { GraphQLSchema } from 'graphql';

import QueryType from './type/QueryType';
import MutationType from './type/MutationType';
import SubscriptionType from './type/SubscriptionType';

const Schema = new GraphQLSchema({
	query : QueryType,
	mutation : MutationType,
  // subscription : SubscriptionType
});

export default Schema;
