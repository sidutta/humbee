export default (sequelize, DataTypes)	=> {
	var monthlyScore = sequelize.define('MonthlyScore', {
		month : {
			type : DataTypes.ENUM,
			values: ['January', 'February', 'March', 'April','May','June','July','August','September','October','November','December'],
			allowNull : false
		},
		year : {
			type : DataTypes.ENUM,
			values: ['2017', '2018', '2019', '2020','2021', '2022', '2023'],
			allowNull : false
		},
		value : {
			type : DataTypes.INTEGER,
			allowNull : true,
			defaultValue : () => 0
		},
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'MonthlyScoreType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				monthlyScore.belongsTo(models.User, {
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		}
	});
	return monthlyScore;
};
