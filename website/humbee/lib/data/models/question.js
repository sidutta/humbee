export default (sequelize, DataTypes)	=> {
	var question = sequelize.define('Question', {
		text : {
			type : DataTypes.STRING,
			allowNull : false
		},
		numberOfOptions : {
			type: DataTypes.INTEGER,
			allowNull: false
		},
    rank : {
      type : DataTypes.INTEGER,
      allowNull : true
    },
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'QuestionType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
        question.hasMany(models.Response);
				question.belongsTo(models.Subquiz,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				question.hasMany(models.Option);
			}
		}
	});
	return question;
};
