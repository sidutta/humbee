export default (sequelize, DataTypes)	=> {
	var reason = sequelize.define('Reason', {
		text : {
			type : DataTypes.TEXT,
			allowNull : false
		},
    agree : {
      type : DataTypes.BOOLEAN,
      allowNull : false
    },
    beAnonymous  : {
      type : DataTypes.BOOLEAN,
      allowNull : false
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: true
    },
    previousReasonId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'ReasonType';
			}
		}
	},
  {
		classMethods : {
			associate : function(models){
				reason.hasMany(models.ReasonLike);
				reason.hasMany(models.Comment);
        reason.belongsToMany(models.ReasonTag, { through: models.ReasonAndTagsAssociation});
        reason.belongsTo(models.Debate, {
          onDelete : 'cascade',
          foreignKey : {
            allowNull : false
          }
        });
        reason.belongsTo(models.User,{
          onDelete : 'cascade',
          foreignKey : {
            allowNull : false
          }
        });
			}
		}
	});
	return reason;
};
