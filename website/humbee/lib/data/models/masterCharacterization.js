export default (sequelize, DataTypes) => {
  var masterCharacterization = sequelize.define('MasterCharacterization', {
    characterizationPattern : {
      type : DataTypes.STRING,
      allowNull : false
    },
    masterCharacterization : {
      type : DataTypes.STRING,
      allowNull : false
    },
    description : {
      type : DataTypes.STRING,
      allowNull : true
    },
    thumbnail : {
      type: DataTypes.STRING,
      allowNull: true
    },
    category : {
      type : DataTypes.STRING,
      allowNull : true
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'MasterCharacterizationType';
      }
    }
  },{
    classMethods : {
      associate : function(models){
        masterCharacterization.belongsTo(models.Quiz, {
          onDelete : 'cascade',
          foreignKey : {
            allowNull : false
          }
        });
      }
    }
  });
  return masterCharacterization;
};
