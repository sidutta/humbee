export default (sequelize, DataTypes)	=> {
	var commentLike = sequelize.define('CommentLike', {
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'CommentLikeType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				commentLike.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				commentLike.belongsTo(models.Comment, {
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		},
		indexes: [
			{
	      fields: ['CommentId', 'UserId'],
  		}
		]
	});
	return commentLike;
};
