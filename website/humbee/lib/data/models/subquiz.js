export default (sequelize, DataTypes) => {
	var subquiz = sequelize.define('Subquiz', {
		title: {
			type: DataTypes.STRING,
			allowNull: true
		},
		description: {
			type: DataTypes.STRING,
			allowNull: true
		},
		imageUrl : {
			type : DataTypes.STRING,
			allowNull : true
		},
		category : {
			type : DataTypes.ENUM,
			values: ['social', 'environment', 'economics'],
			allowNull : true
		},
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'SubquizType';
			}
		},
	},{
		classMethods : {
			associate : function(models){
				subquiz.belongsToMany(models.Quiz, {through: models.QuizDivision});
				subquiz.hasMany(models.Question);
				subquiz.hasMany(models.OpinionCharacterization);
			}
		}
	});
	return subquiz;
};
