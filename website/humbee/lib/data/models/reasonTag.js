export default (sequelize, DataTypes) => {
  var reasonTag = sequelize.define('ReasonTag', {
    text : {
      type : DataTypes.STRING,
      allowNull : false,
      unique: true
    },
    type : {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'ReasonTagType';
      }
    }
  },{
    classMethods : {
      associate : function(models) {
        reasonTag.belongsToMany(models.Reason, { through: models.ReasonAndTagsAssociation});
      }
    }
  });
  return reasonTag;
};
