export default (sequelize, DataTypes) => {
  var quizDivision = sequelize.define('QuizDivision', {
    QuizId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    SubquizId: {
      type: DataTypes.INTEGER,
      allowNull : true
    },
    rank : {
      type : DataTypes.INTEGER,
      allowNull : true
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'QuizDivision';
      }
    },
  });
  return quizDivision;
};
