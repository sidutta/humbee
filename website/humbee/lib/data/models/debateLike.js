export default (sequelize, DataTypes)	=> {
	var debateLike = sequelize.define('DebateLike', {
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'DebateLikeType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				debateLike.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				debateLike.belongsTo(models.Debate, {
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		},
		indexes: [
			{
	      fields: ['DebateId', 'UserId'],
  		}
		]
	});
	return debateLike;
};
