export default (sequelize, DataTypes)	=> {
	var pictoralPostComment = sequelize.define('PictoralPostComment', {
		text : {
			type : DataTypes.TEXT,
			allowNull : false
		},
		type : {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'PictoralPostCommentType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				pictoralPostComment.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				pictoralPostComment.belongsTo(models.PictoralPost,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				pictoralPostComment.hasMany(models.PictoralPostCommentLike);
			}
		},
		indexes: [
			{
	      fields: ['PictoralPostId'],
  		}
		]
	});
	return pictoralPostComment;
};
