export default (sequelize, DataTypes) => {
  var quizAndCommunitiesAssociation = sequelize.define('QuizAndCommunitiesAssociation', {
    CommunityId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    QuizId: {
      type: DataTypes.INTEGER,
      allowNull : false
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'QuizAndCommunitiesAssociation';
      }
    },
  }, {
    indexes: [
      {
        fields: ['QuizId'],
      }
    ]
  });
  return quizAndCommunitiesAssociation;
};
