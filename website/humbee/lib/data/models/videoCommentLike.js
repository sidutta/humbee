export default (sequelize, DataTypes)	=> {
	var videoCommentLike = sequelize.define('VideoCommentLike', {
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'VideoCommentLikeType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				videoCommentLike.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				videoCommentLike.belongsTo(models.VideoComment, {
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		},
		indexes: [
			{
	      fields: ['VideoCommentId', 'UserId'],
  		}
		]
	});
	return videoCommentLike;
};
