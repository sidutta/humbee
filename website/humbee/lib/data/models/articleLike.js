export default (sequelize, DataTypes)	=> {
	var articleLike = sequelize.define('ArticleLike', {
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'ArticleLikeType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				articleLike.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				articleLike.belongsTo(models.Article, {
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		},
		indexes: [
			{
	      fields: ['ArticleId', 'UserId'],
  		}
		]
	});
	return articleLike;
};
