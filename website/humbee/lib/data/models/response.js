export default (sequelize, DataTypes)	=> {
	var response = sequelize.define('Response', {
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'ResponseType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				response.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				response.belongsTo(models.Option,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				response.belongsTo(models.Question,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});

			}
		}
	});
	return response;
};
