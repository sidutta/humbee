export default (sequelize, DataTypes)	=> {
	var articleCommentLike = sequelize.define('ArticleCommentLike', {
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'ArticleCommentLikeType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				articleCommentLike.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				articleCommentLike.belongsTo(models.ArticleComment, {
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		},
		indexes: [
			{
	      fields: ['ArticleCommentId', 'UserId'],
  		}
		]
	});
	return articleCommentLike;
};
