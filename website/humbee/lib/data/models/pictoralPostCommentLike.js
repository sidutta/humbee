export default (sequelize, DataTypes)	=> {
	var pictoralPostCommentLike = sequelize.define('PictoralPostCommentLike', {
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'PictoralPostCommentLikeType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				pictoralPostCommentLike.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				pictoralPostCommentLike.belongsTo(models.PictoralPostComment, {
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		},
		indexes: [
			{
	      fields: ['PictoralPostCommentId', 'UserId'],
  		}
		]
	});
	return pictoralPostCommentLike;
};
