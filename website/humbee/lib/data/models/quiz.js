export default (sequelize, DataTypes)	=> {
	var quiz = sequelize.define('Quiz', {
		title: {
			type: DataTypes.STRING,
			allowNull: false
		},
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
		imageUrl : {
			type : DataTypes.STRING,
			allowNull : false
		},
		active: {
			type: DataTypes.BOOLEAN,
			defaultValue: false,
			allowNull: true
		},
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'QuizType';
			}
		},
	},{
		classMethods : {
			associate : function(models){
				quiz.belongsToMany(models.Subquiz, {through: models.QuizDivision});
				quiz.hasMany(models.QuizLike);
				quiz.hasMany(models.QuizComment);
        quiz.belongsTo(models.Debate, { as: 'LastDebate', constraints: false });
        quiz.belongsToMany(models.Community, { through: models.QuizAndCommunitiesAssociation});
			}
		}
	});
	return quiz;
};
