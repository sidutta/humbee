export default (sequelize, DataTypes)	=> {
	var deviceToken = sequelize.define('DeviceToken', {
		token: {
			type: DataTypes.STRING,
			allowNull: false
		},
		deviceType: {
			type: DataTypes.STRING,
			allowNull : false
		},
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'DeviceTokenType';
			}
		},
	},{
		classMethods : {
			associate : function(models){
				deviceToken.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		}
	});
	return deviceToken;
};
