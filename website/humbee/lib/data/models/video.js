export default (sequelize, DataTypes)	=> {
	var video = sequelize.define('Video', {
		title : {
			type : DataTypes.STRING,
			allowNull : false
		},
		thumbnail : {
			type: DataTypes.STRING,
			allowNull: false
		},
		videoId : {
			type: DataTypes.STRING,
			allowNull : false
		},
		description : {
			type : DataTypes.STRING,
			allowNull : false
		},
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'VideoType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				video.hasMany(models.VideoComment);
				video.hasMany(models.VideoLike);
				video.belongsToMany(models.Community, { through: models.VideoAndCommunitiesAssociation});
			}
		}
	});
	return video;
};
