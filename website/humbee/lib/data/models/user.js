export default (sequelize, DataTypes)	=> {
	var user = sequelize.define('User', {
		firstName: {
			type: DataTypes.STRING,
			allowNull: false
		},
		lastName: {
			type: DataTypes.STRING,
			allowNull : true
		},
		score : {
			type : DataTypes.INTEGER,
			allowNull : true,
			defaultValue : () => 0
		},
		email: {
			type: DataTypes.STRING,
			validate: {
				isEmail: true,
			},
			allowNull : true,
			unique: {
				args: true,
				message: 'Email must be unique.'
			}
		},
		password: {
			type: DataTypes.STRING,
			allowNull : false
		},
		username: {
			type: DataTypes.STRING,
			allowNull : true,
			unique: {
				args: true,
				message: 'Username must be unique.'
			}
		},
		role : {
			type: DataTypes.STRING,
			allowNull : false
		},
		profilePic : {
			type : DataTypes.STRING,
			allowNull : true
		},
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'UserType';
			}
		},
	},{
		classMethods : {
			associate : function(models){
				user.hasMany(models.SocialProfile);
				user.hasMany(models.Position);
				user.hasMany(models.Reason);
				user.hasMany(models.Comment);
				user.hasMany(models.CommentLike);
				user.hasMany(models.ReasonLike);
				user.hasMany(models.Response);
				user.hasMany(models.MonthlyScore);
				user.hasMany(models.DeviceToken);
				user.belongsToMany(models.User, { as: 'Followers', foreignKey: 'FollowedId', through: models.Following});
				user.belongsToMany(models.User, { as: 'Followeds', foreignKey: 'FollowerId', through: models.Following});
			}
		}
	});
	return user;
};
