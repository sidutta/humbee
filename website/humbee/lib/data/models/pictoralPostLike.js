export default (sequelize, DataTypes)	=> {
	var pictoralPostLike = sequelize.define('PictoralPostLike', {
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'PictoralPostLikeType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				pictoralPostLike.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				pictoralPostLike.belongsTo(models.PictoralPost, {
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		},
		indexes: [
			{
	      fields: ['PictoralPostId', 'UserId'],
  		}
		]
	});
	return pictoralPostLike;
};
