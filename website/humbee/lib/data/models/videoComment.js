export default (sequelize, DataTypes)	=> {
	var videoComment = sequelize.define('VideoComment', {
		text : {
			type : DataTypes.TEXT,
			allowNull : false
		},
		type : {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'VideoCommentType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				videoComment.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				videoComment.belongsTo(models.Video,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				videoComment.hasMany(models.VideoCommentLike);
			}
		},
		indexes: [
			{
	      fields: ['VideoId'],
  		}
		]
	});
	return videoComment;
};
