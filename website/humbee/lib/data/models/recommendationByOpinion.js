export default (sequelize, DataTypes) => {
  var recommendationByOpinion = sequelize.define('RecommendationByOpinion', {
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'RecommendationByOpinion';
      }
    },
    entityId: {
      type: DataTypes.INTEGER,
    },
    entitytype: {
      type: DataTypes.STRING,
    },
    rank: {
      type: DataTypes.INTEGER,
    },

  },{
    classMethods : {
      associate : function(models){
        recommendationByOpinion.belongsTo(models.MasterCharacterization,{
          onDelete : 'cascade',
          foreignKey : {
            allowNull : false
          }
        });
      }
    }
  });
  return recommendationByOpinion;
};
