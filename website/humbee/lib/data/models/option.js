export default (sequelize, DataTypes)	=> {
	var option = sequelize.define('Option', {
		text : {
			type : DataTypes.STRING,
			allowNull : false
		},
		votes : {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue : () => 0
		},
    weight : {
      type: DataTypes.FLOAT,
      allowNull: true
    },
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'OptionType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
        option.hasMany(models.Response);
				option.belongsTo(models.Question,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		}
	});
	return option;
};
