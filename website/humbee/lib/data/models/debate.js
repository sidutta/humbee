export default (sequelize, DataTypes)	=> {
	var debate = sequelize.define('Debate', {
		title: {
			type: DataTypes.STRING,
			allowNull: false
		},
		motionShort: {
			type: DataTypes.STRING,
			allowNull : false
		},
		motionFull: {
			type: DataTypes.STRING,
			allowNull : false
		},
		agree: {
			type: DataTypes.INTEGER,
			allowNull : false
		},
		disagree: {
			type: DataTypes.INTEGER,
			allowNull : false
		},
		active: {
			type: DataTypes.BOOLEAN,
			defaultValue: false,
			allowNull: true
		},
		duration : {
			type: DataTypes.INTEGER,
			allowNull : false
		},
		imageUrl : {
			type : DataTypes.STRING,
			allowNull : false
		},
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'DebateType';
			}
		},
	},{
		classMethods : {
			associate : function(models){
				debate.hasMany(models.Position);
        debate.hasMany(models.Reason);
				debate.belongsTo(models.Quiz);
				debate.hasMany(models.Comment);
				debate.hasMany(models.StoryItem);
				debate.belongsToMany(models.Community, { through: models.DebateAndCommunitiesAssociation});
			}
		}
	});
	return debate;
};
