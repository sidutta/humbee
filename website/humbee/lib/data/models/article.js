export default (sequelize, DataTypes)	=> {
	var article = sequelize.define('Article', {
		title : {
			type : DataTypes.STRING,
			allowNull : false
		},
		thumbnail : {
			type: DataTypes.STRING,
			allowNull: false
		},
		url : {
			type: DataTypes.STRING,
			allowNull : false
		},
		description : {
			type : DataTypes.STRING,
			allowNull : false
		},
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'ArticleType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				article.hasMany(models.ArticleComment);
				article.hasMany(models.ArticleLike);
				article.belongsToMany(models.Community, { through: models.ArticleAndCommunitiesAssociation});
			}
		}
	});
	return article;
};
