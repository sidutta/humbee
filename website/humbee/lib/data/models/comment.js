export default (sequelize, DataTypes)	=> {
	var comment = sequelize.define('Comment', {
		text : {
			type : DataTypes.TEXT,
			allowNull : false
		},
    beAnonymous  : {
      type : DataTypes.BOOLEAN,
      allowNull : false
    },
		type : {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'CommentType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				comment.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				comment.belongsTo(models.Reason,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				comment.hasMany(models.CommentLike);
			}
		},
		indexes: [
			{
	      fields: ['ReasonId'],
  		}
		]
	});
	return comment;
};
