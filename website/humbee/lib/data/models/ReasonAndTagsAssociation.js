export default (sequelize, DataTypes) => {
  var reasonAndTagsAssociation = sequelize.define('ReasonAndTagsAssociation', {
    ReasonTagId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    ReasonId: {
      type: DataTypes.INTEGER,
      allowNull : true
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'ReasonAndTagsAssociation';
      }
    },
  }, {
    indexes: [
      {
        fields: ['ReasonId', 'ReasonTagId'],
      }
    ]
  });
  return reasonAndTagsAssociation;
};
