export default (sequelize, DataTypes) => {
  var community = sequelize.define('Community', {
    title : {
      type : DataTypes.STRING,
      allowNull : false
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'CommunityType';
      }
    }
  },{
    classMethods : {
      associate : function(models) {
        community.belongsToMany(models.Article, { through: models.ArticleAndCommunitiesAssociation});
        community.belongsToMany(models.Debate, { through: models.DebateAndCommunitiesAssociation});
        community.belongsToMany(models.PictoralPost, { through: models.PictoralPostAndCommunitiesAssociation});
        community.belongsToMany(models.Quiz, { through: models.QuizAndCommunitiesAssociation});
        community.belongsToMany(models.Video, { through: models.VideoAndCommunitiesAssociation});
      }
    }
  });
  return community;
};
