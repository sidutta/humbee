export default (sequelize, DataTypes)	=> {
	var videoLike = sequelize.define('VideoLike', {
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'VideoLikeType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				videoLike.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				videoLike.belongsTo(models.Video, {
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		},
		indexes: [
			{
	      fields: ['VideoId', 'UserId'],
  		}
		]
	});
	return videoLike;
};
