export default (sequelize, DataTypes) => {
  var pictoralPostAndCommunitiesAssociation = sequelize.define('PictoralPostAndCommunitiesAssociation', {
    CommunityId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    PictoralPostId: {
      type: DataTypes.INTEGER,
      allowNull : false
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'PictoralPostAndCommunitiesAssociation';
      }
    },
  }, {
    indexes: [
      {
        fields: ['PictoralPostId'],
      }
    ]
  });
  return pictoralPostAndCommunitiesAssociation;
};
