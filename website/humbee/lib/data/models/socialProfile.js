export default (sequelize, DataTypes)	=> {
	var socialProfile = sequelize.define('SocialProfile', {
		provider: {
			type: DataTypes.STRING,
			allowNull : false
		},
		accessToken : {
			type : DataTypes.TEXT,
			allowNull : false
		},
		userID : {
			type : DataTypes.STRING,
			allowNull : false
		},
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'socialProfileType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				socialProfile.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		}
	});
	return socialProfile;
};
