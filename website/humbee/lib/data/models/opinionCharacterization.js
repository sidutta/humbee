export default (sequelize, DataTypes) => {
  var opinionCharacterization = sequelize.define('OpinionCharacterization', {
    characterization : {
      type : DataTypes.STRING,
      allowNull : false
    },
    code : {
      type : DataTypes.STRING,
      allowNull : true
    },
    description : {
      type : DataTypes.STRING,
      allowNull : true
    },
    thumbnail : {
      type: DataTypes.STRING,
      allowNull: false
    },
    minScore : {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue : () => 0
    },
    maxScore : {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue : () => 0
    },
    noOfUsers : {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'OpinionCharacterizationType';
      }
    }
  },{
    classMethods : {
      associate : function(models){
        opinionCharacterization.belongsTo(models.Subquiz, {
          onDelete : 'cascade',
          foreignKey : {
            allowNull : false
          }
        });
      }
    }
  });
  return opinionCharacterization;
};
