export default (sequelize, DataTypes)	=> {
	var reasonLike = sequelize.define('ReasonLike', {
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'ReasonLikeType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				reasonLike.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				reasonLike.belongsTo(models.Reason, {
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		},
		indexes: [
			{
	      fields: ['ReasonId', 'UserId'],
  		}
		]
	});
	return reasonLike;
};
