export default (sequelize, DataTypes) => {
  var debateAndCommunitiesAssociation = sequelize.define('DebateAndCommunitiesAssociation', {
    CommunityId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    DebateId: {
      type: DataTypes.INTEGER,
      allowNull : false
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'DebateAndCommunitiesAssociation';
      }
    },
  }, {
    indexes: [
      {
        fields: ['DebateId'],
      }
    ]
  });
  return debateAndCommunitiesAssociation;
};
