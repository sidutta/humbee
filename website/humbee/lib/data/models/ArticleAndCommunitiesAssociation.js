export default (sequelize, DataTypes) => {
  var articleAndCommunitiesAssociation = sequelize.define('ArticleAndCommunitiesAssociation', {
    CommunityId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    ArticleId: {
      type: DataTypes.INTEGER,
      allowNull : false
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'ArticleAndCommunitiesAssociation';
      }
    },
  }, {
    indexes: [
      {
        fields: ['ArticleId'],
      }
    ]
  });
  return articleAndCommunitiesAssociation;
};
