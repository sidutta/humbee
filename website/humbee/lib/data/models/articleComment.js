export default (sequelize, DataTypes)	=> {
	var articleComment = sequelize.define('ArticleComment', {
		text : {
			type : DataTypes.TEXT,
			allowNull : false
		},
		type : {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'ArticleCommentType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				articleComment.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				articleComment.belongsTo(models.Article,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				articleComment.hasMany(models.ArticleCommentLike);
			}
		},
		indexes: [
			{
	      fields: ['ArticleId'],
  		}
		]
	});
	return articleComment;
};
