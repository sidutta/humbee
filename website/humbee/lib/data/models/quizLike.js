export default (sequelize, DataTypes)	=> {
	var quizLike = sequelize.define('QuizLike', {
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'QuizLikeType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				quizLike.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				quizLike.belongsTo(models.Quiz, {
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		},
		indexes: [
			{
	      fields: ['QuizId', 'UserId'],
  		}
		]
	});
	return quizLike;
};
