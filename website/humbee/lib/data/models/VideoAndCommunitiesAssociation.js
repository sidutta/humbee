export default (sequelize, DataTypes) => {
  var videoAndCommunitiesAssociation = sequelize.define('VideoAndCommunitiesAssociation', {
    CommunityId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    VideoId: {
      type: DataTypes.INTEGER,
      allowNull : false
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'VideoAndCommunitiesAssociation';
      }
    },
  }, {
    indexes: [
      {
        fields: ['VideoId'],
      }
    ]
  });
  return videoAndCommunitiesAssociation;
};
