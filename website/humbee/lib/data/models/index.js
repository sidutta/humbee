
import fs	from 'fs';
import path	from'path';
import Sequelize from 'sequelize';
import dataloaderSequelize from 'dataloader-sequelize';


let db = {};

let sequelize = new Sequelize(
  'humbee_development',
  process.env.PSQL_UNAME,
  process.env.PSQL_PWD,
  {
    dialect: 'postgres',
    host: process.env.PSQL_HOST
  }
);

fs
	.readdirSync(__dirname)
	.filter( (file) => {
		return (file.indexOf('.') !== 0) && (file !== 'index.js');
	})
	.forEach( (file) => {
		let model = sequelize.import(path.join(__dirname, file));
		db[model.name] = model;
	});

Object.keys(db).forEach( (modelName) => {
	if ('associate' in db[modelName]) {
		db[modelName].associate(db);
	}
});

dataloaderSequelize(sequelize);

db.sequelize = sequelize;

db.Sequelize = Sequelize;

export default db;
