export default (sequelize, DataTypes)	=> {
	var pictoralPost = sequelize.define('PictoralPost', {
		title : {
			type : DataTypes.STRING,
			allowNull : false
		},
		thumbnail : {
			type: DataTypes.STRING,
			allowNull: false
		},
		url : {
			type: DataTypes.STRING,
			allowNull : false
		},
		description : {
			type : DataTypes.STRING,
			allowNull : false
		},
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'PictoralPostType';
			}
		}
	},
	{
		classMethods : {
			associate : function(models){
				pictoralPost.hasMany(models.PictoralPostComment);
				pictoralPost.hasMany(models.PictoralPostLike);
				pictoralPost.belongsToMany(models.Community, { through: models.PictoralPostAndCommunitiesAssociation});
			}
		}
	}
	);
	return pictoralPost;
};
