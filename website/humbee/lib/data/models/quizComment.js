export default (sequelize, DataTypes)	=> {
	var quizComment = sequelize.define('QuizComment', {
		text : {
			type : DataTypes.TEXT,
			allowNull : false
		},
		type : {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'QuizCommentType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				quizComment.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				quizComment.belongsTo(models.Quiz,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				quizComment.hasMany(models.QuizCommentLike);
			}
		},
		indexes: [
			{
	      fields: ['QuizId'],
  		}
		]
	});
	return quizComment;
};
