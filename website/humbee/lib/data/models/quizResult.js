export default (sequelize, DataTypes) => {
  var response = sequelize.define('QuizResult', {
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'QuizResultType';
      }
    }
  },{
    classMethods : {
      associate : function(models) {
        response.belongsTo(models.User, {
          onDelete : 'cascade',
          foreignKey : {
            allowNull : false
          }
        });
        response.belongsTo(models.Quiz, {
          onDelete : 'cascade',
          foreignKey : {
            allowNull : false
          }
        });
        response.belongsTo(models.MasterCharacterization, {
          onDelete : 'cascade',
          foreignKey : {
            allowNull : false
          }
        });
      }
    }
  });
  return response;
};
