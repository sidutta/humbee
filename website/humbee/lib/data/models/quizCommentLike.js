export default (sequelize, DataTypes)	=> {
	var quizCommentLike = sequelize.define('QuizCommentLike', {
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'QuizCommentLikeType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				quizCommentLike.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				quizCommentLike.belongsTo(models.QuizComment, {
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		},
		indexes: [
			{
	      fields: ['QuizCommentId', 'UserId'],
  		}
		]
	});
	return quizCommentLike;
};
