export default (sequelize, DataTypes)	=> {
	var verificationCode = sequelize.define(
		'VerificationCode',
		{
			code : {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			type: {
				type: new DataTypes.VIRTUAL(DataTypes.STRING),
				get() {
					return 'VerificationCodeType';
				}
			}
		},
		{
			classMethods : {
				associate : function(models){
					// currently id == UserId.
					// To allow multiple verification-codes to be valid (not only the latest one).
					verificationCode.belongsTo(models.User);
				}
			}
		});
	return verificationCode;
};
