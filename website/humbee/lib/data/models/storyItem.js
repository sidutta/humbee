export default (sequelize, DataTypes)	=> {
	var storyItem = sequelize.define('StoryItem', {
		title: {
			type: DataTypes.STRING,
			allowNull: false
		},
		fullStory: {
			type: DataTypes.TEXT,
			allowNull : false
		},
		imageUrl: {
			type: DataTypes.STRING,
			allowNull : true
		},
    rank: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'StoryItemType';
			}
		},
	},{
		classMethods : {
			associate : function(models){
				storyItem.belongsTo(models.Debate,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		}
	});
	return storyItem;
};
