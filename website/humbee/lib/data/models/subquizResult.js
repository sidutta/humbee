export default (sequelize, DataTypes) => {
  var response = sequelize.define('SubquizResult', {
    score : {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'ResponseType';
      }
    }
  },{
    classMethods : {
      associate : function(models){
        response.belongsTo(models.User,{
          onDelete : 'cascade',
          foreignKey : {
            allowNull : false
          }
        });
        response.belongsTo(models.Subquiz,{
          onDelete : 'cascade',
          foreignKey : {
            allowNull : false
          }
        });
      }
    },
    indexes: [
      {
        fields: ['UserId'],
      }
    ]
  });
  return response;
};
