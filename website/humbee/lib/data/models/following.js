export default (sequelize, DataTypes)	=> {
	var following = sequelize.define('Following', {
		FollowedId: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		FollowerId: {
			type: DataTypes.INTEGER,
			allowNull : true
		},
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'FollowingType';
			}
		},
	});
	return following;
};
