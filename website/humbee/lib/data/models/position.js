export default (sequelize, DataTypes)	=> {
	var position = sequelize.define('Position', {
		agree : {
			type : DataTypes.BOOLEAN,
			allowNull : true
		},
		type: {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'PositionType';
			}
		}
	},
  {
		classMethods : {
			associate : function(models){
				position.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
				position.belongsTo(models.Debate,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		}
	});
	return position;
};
