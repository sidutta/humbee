export default (sequelize, DataTypes)	=> {
	var notification = sequelize.define('Notification', {
		text : {
			type : DataTypes.STRING,
			allowNull : false
		},
		entityType : {
			type : new DataTypes.STRING,
			allowNull : false
		},
		entityId : {
			type : new DataTypes.STRING,
			allowNull : false
		},
		type : {
			type: new DataTypes.VIRTUAL(DataTypes.STRING),
			get() {
				return 'CommentType';
			}
		}
	},{
		classMethods : {
			associate : function(models){
				notification.belongsTo(models.User,{
					onDelete : 'cascade',
					foreignKey : {
						allowNull : false
					}
				});
			}
		}
	});
	return notification;
};
