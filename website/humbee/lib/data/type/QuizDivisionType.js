import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLBoolean,
	GraphQLInt,
	GraphQLList
} from 'graphql';

import { NodeInterface } from '../interface/NodeInterface';

import {
	globalIdField,
	connectionArgs,
	connectionFromArray,
	connectionDefinitions
 } from 'graphql-relay';

import QuestionType from './QuestionType';
import Elapsed from 'elapsed';

export default new GraphQLObjectType({
	name: 'QuizDivision',
	description: 'This represents the quizDivision',
	fields: () => {
		return {
			id: globalIdField('QuizDivision', quizDivision => quizDivision.SubquizId ),
			rank: {
				type: GraphQLInt,
				resolve (quizDivision) {
					return quizDivision.rank;
				}
			},
			QuizId: {
				type: GraphQLString,
				resolve (quizDivision) {
					return quizDivision.QuizId;
				}
			},
			SubquizId: {
				type: GraphQLString,
				resolve (quizDivision) {
					return quizDivision.SubquizId;
				}
			},
		};
	},
	interfaces: () => [NodeInterface]
});
