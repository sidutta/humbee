import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLFloat
} from 'graphql';

import { NodeInterface } from '../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';

export default new GraphQLObjectType({
  name: 'MasterCharacterization',
  description: 'This represents a MasterCharacterization',
  fields: () => {
    return {
      id: globalIdField('MasterCharacterization', masterCharacterization => masterCharacterization.id ),
      index: {
        type: GraphQLInt,
        resolve (masterCharacterization) {
          return masterCharacterization.id;
        }
      },
      characterizationPattern: {
        type: GraphQLString,
        resolve (masterCharacterization) {
          return masterCharacterization.characterizationPattern;
        }
      },
      characterizationPatternWithId: {
        type: GraphQLString,
        resolve (masterCharacterization) {
          return masterCharacterization.dataValues.characterizationPatternWithId;
        }
      },
      subquizScores: {
        type: GraphQLString,
        resolve (masterCharacterization) {
          return masterCharacterization.dataValues.subquizScores;
        }
      },
      masterCharacterization: {
        type: GraphQLString,
        resolve (masterCharacterization) {
          return masterCharacterization.masterCharacterization;
        }
      },
      category: {
        type: GraphQLString,
        resolve (masterCharacterization) {
          return masterCharacterization.category;
        }
      },
      thumbnail: {
        type: GraphQLString,
        resolve (masterCharacterization) {
          return masterCharacterization.thumbnail;
        }
      },
      description: {
        type: GraphQLString,
        resolve (masterCharacterization) {
          return masterCharacterization.description;
        }
      },
      quizId : {
        type : GraphQLInt,
        resolve (masterCharacterization) {
          return masterCharacterization.QuizId;
        }
      },
    };
  },
  interfaces: () => [NodeInterface]
});
