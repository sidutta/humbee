import { GraphQLObjectType } from 'graphql';

import SignupMutation from '../mutations/SignupMutation';
import LoginMutation from '../mutations/LoginMutation';
import FbLoginMutation from '../mutations/FbLoginMutation';
import SetUsernameMutation from '../mutations/SetUsernameMutation';
import CreateReasonMutation from '../mutations/CreateReasonMutation';
import CreateReasonLikeMutation from '../mutations/CreateReasonLikeMutation';
import DeleteReasonMutation from '../mutations/DeleteReasonMutation';
import RefreshDebateMutation from '../mutations/RefreshDebateMutation';
import CreateCommentMutation from '../mutations/CreateCommentMutation';
import CreateCommentLikeMutation from '../mutations/CreateCommentLikeMutation';
import CreateFollowerMutation from '../mutations/CreateFollowerMutation';
import CreatePositionMutation from '../mutations/CreatePositionMutation';
import CreateResponseMutation from '../mutations/CreateResponseMutation';
import SetProfilePictureMutation from '../mutations/SetProfilePictureMutation';
import CreateArticleLikeMutation from '../mutations/article/CreateArticleLikeMutation';
import CreateArticleCommentMutation from '../mutations/article/CreateArticleCommentMutation';
import CreateArticleCommentLikeMutation from '../mutations/article/CreateArticleCommentLikeMutation';
import CreatePictoralPostLikeMutation from '../mutations/pictoralPost/CreatePictoralPostLikeMutation';
import CreatePictoralPostCommentMutation from '../mutations/pictoralPost/CreatePictoralPostCommentMutation';
import CreatePictoralPostCommentLikeMutation from '../mutations/pictoralPost/CreatePictoralPostCommentLikeMutation';
import CreateVideoLikeMutation from '../mutations/video/CreateVideoLikeMutation';
import CreateVideoCommentMutation from '../mutations/video/CreateVideoCommentMutation';
import CreateVideoCommentLikeMutation from '../mutations/video/CreateVideoCommentLikeMutation';
import CreateQuizLikeMutation from '../mutations/quiz/CreateQuizLikeMutation';
import CreateQuizCommentMutation from '../mutations/quiz/CreateQuizCommentMutation';
import CreateQuizCommentLikeMutation from '../mutations/quiz/CreateQuizCommentLikeMutation';
import VerificationCodeMutation from '../mutations/VerificationCodeMutation';
import VerificationCodeCheckMutation from '../mutations/VerificationCodeCheckMutation';
import PasswordResetMutation from '../mutations/PasswordResetMutation';
import SetQuizCompleteMutation from '../mutations/SetQuizCompleteMutation';
import BulkFollowMutation from '../mutations/BulkFollowMutation';
import AddDeviceTokenMutation from '../mutations/AddDeviceTokenMutation';
import InviteForDebateMutation from '../mutations/InviteForDebateMutation';
import AddNotificationMutation from '../mutations/AddNotificationMutation';
import CreateDebateLikeMutation from '../mutations/debate/CreateDebateLikeMutation';

export default new GraphQLObjectType({
	name: 'Mutation',
	fields: () => ({
		signup : SignupMutation,
		login : LoginMutation,
		fbLogin : FbLoginMutation,
		setUsername : SetUsernameMutation,
		createReason : CreateReasonMutation,
		createReasonLike : CreateReasonLikeMutation,
		refreshDebate : RefreshDebateMutation,
		createComment : CreateCommentMutation,
		createCommentLike : CreateCommentLikeMutation,
		createFollower : CreateFollowerMutation,
		createPosition : CreatePositionMutation,
		createResponse : CreateResponseMutation,
		setProfilePicture : SetProfilePictureMutation,
		createArticleLike : CreateArticleLikeMutation,
		createArticleComment : CreateArticleCommentMutation,
		createArticleCommentLike : CreateArticleCommentLikeMutation,
		createPictoralPostLike : CreatePictoralPostLikeMutation,
		createPictoralPostComment : CreatePictoralPostCommentMutation,
		createPictoralPostCommentLike : CreatePictoralPostCommentLikeMutation,
		createVideoLike : CreateVideoLikeMutation,
		createVideoComment : CreateVideoCommentMutation,
		createVideoCommentLike : CreateVideoCommentLikeMutation,
		createQuizLike : CreateQuizLikeMutation,
		createQuizComment : CreateQuizCommentMutation,
		createQuizCommentLike : CreateQuizCommentLikeMutation,
		verificationCode : VerificationCodeMutation,
		verificationCodeCheck : VerificationCodeCheckMutation,
		passwordReset : PasswordResetMutation,
		setQuizComplete : SetQuizCompleteMutation,
		bulkFollow : BulkFollowMutation,
		addNotifcation : AddNotificationMutation,
		addDeviceToken : AddDeviceTokenMutation,
		inviteForDebate : InviteForDebateMutation,
		deleteReason : DeleteReasonMutation,
		createDebateLike : CreateDebateLikeMutation
	})
});
