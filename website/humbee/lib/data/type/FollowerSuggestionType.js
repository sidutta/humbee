import {
  GraphQLObjectType,
  GraphQLString
} from 'graphql';

import { NodeInterface } from '../interface/NodeInterface';

import {
  connectionArgs,
  connectionFromArray,
  connectionDefinitions
} from 'graphql-relay';

import UserType from './UserType';
import { UserConnection } from './UserType';

export default new GraphQLObjectType({
  name: 'FollowerSuggestion',
  description: 'This represents the Follower Suggestion',
  fields: () => {
    return {
      friends: {
        type: UserConnection,
        args : {
          ...connectionArgs
        },
        resolve : (followerSuggestion, {...args}) => {
          return connectionFromArray(followerSuggestion.friends, args);
        }
      },
      strangers: {
        type: UserConnection,
        args : {
          ...connectionArgs
        },
        resolve : (followerSuggestion, {...args}) => {
          // console.log('#@#@!$!@#@');
          // console.log(followerSuggestion);
          return connectionFromArray(followerSuggestion.strangers, args);
        }
      },
      strangersOpposite: {
        type: UserConnection,
        args : {
          ...connectionArgs
        },
        resolve : (followerSuggestion, {...args}) => {
          // console.log('#@#@!$!@#@');
          // console.log(followerSuggestion);
          return connectionFromArray(followerSuggestion.strangersOpposite, args);
        }
      },
    };
  }
});


