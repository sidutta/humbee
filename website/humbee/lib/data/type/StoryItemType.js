import {
	GraphQLObjectType,
	GraphQLString,
  GraphQLInt
} from 'graphql';

import { NodeInterface} from '../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';

export default new GraphQLObjectType({
	name: 'StoryItem',
	description: 'This represents a StoryItem',
	fields: () => {
		return {
			id: globalIdField('StoryItem', storyItem => storyItem.id ),
			title: {
				type: GraphQLString,
				resolve (storyItem) {
					return storyItem.title;
				}
			},
			fullStory: {
				type: GraphQLString,
				resolve (storyItem) {
					return storyItem.fullStory;
				}
			},
      rank: {
        type: GraphQLInt,
        resolve (storyItem) {
          return storyItem.rank;
        }
      },
			imageUrl: {
				type: GraphQLString,
				resolve (storyItem) {
					return storyItem.imageUrl;
				}
			},
      DebateId: {
        type : GraphQLInt,
        resolve(storyItem){
          return storyItem.DebateId;
        }
      },
		};
	},
	interfaces: () => [NodeInterface]
});
