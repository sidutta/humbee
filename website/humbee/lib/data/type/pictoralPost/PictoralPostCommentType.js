import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLBoolean
} from 'graphql';

import { NodeInterface} from '../../interface/NodeInterface';

import {
	globalIdField,
	connectionDefinitions,
	connectionArgs,
	connectionFromArray,
	connectionFromPromisedArray
} from 'graphql-relay';

import PictoralPostCommentLikeType from './PictoralPostCommentLikeType';

import Elapsed from 'elapsed';

const {
	connectionType : PictoralPostCommentLikeConnection
} = connectionDefinitions({
	name : 'PictoralPostCommentLike',
	nodeType : PictoralPostCommentLikeType
});

export default new GraphQLObjectType({
	name: 'PictoralPostComment',
	description: 'This represents a Comment posted by a user on a pictoralPost',
	fields: () => {
		return {
			id: globalIdField('PictoralPostComment', pictoralPostComment => pictoralPostComment.id),
			index: {
        type : GraphQLInt,
        resolve (pictoralPostComment) {
          return pictoralPostComment.id;
        }
      },
			text: {
				type: GraphQLString,
				resolve (pictoralPostComment) {
					return pictoralPostComment.text;
				}
			},
			userId : globalIdField('User', pictoralPostComment => pictoralPostComment.UserId),
			numberOfLikes : {
				type : GraphQLInt,
				resolve: (pictoralPostComment, {...args}, {db}) => {
					return pictoralPostComment.dataValues.numberOfLikes;
				}
			},
			pictoralPostCommentLikes : {
				type : PictoralPostCommentLikeConnection,
				args : {
					...connectionArgs
				},
				resolve : (pictoralPostComment, {...args}, {db}) => {
					let pictoralPostCommentLikesList = db.PictoralPostCommentLike.findAll({
		        where : {
		          PictoralPostCommentId : pictoralPostComment.id
		        }
		      });
					return connectionFromPromisedArray(pictoralPostCommentLikesList, args);
				}
			},
			postedTime : {
				type : GraphQLString,
				resolve(pictoralPostComment) {
					let elapsedTime = new Elapsed(new Date(pictoralPostComment.createdAt), new Date());
					return elapsedTime.optimal;
				}
			},
			liked : {
				type : GraphQLBoolean,
				resolve : (pictoralPostComment, args, {userId, db}) => {
					let count = pictoralPostComment.dataValues.liked;
					if(count=='1') {
						return true;
					} else {
						return false;
					}
				}
			}
		};
	},
	interfaces: () => [NodeInterface]
});
