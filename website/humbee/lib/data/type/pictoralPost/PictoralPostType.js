import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLBoolean,
	GraphQLInt
} from 'graphql';

import { NodeInterface } from '../../interface/NodeInterface';

import {
	globalIdField,
	connectionArgs,
	connectionFromArray,
	connectionDefinitions,
	connectionFromPromisedArray
 } from 'graphql-relay';

import PictoralPostCommentType from './PictoralPostCommentType';
import PictoralPostLikeType from './PictoralPostLikeType';

const {
	connectionType : PictoralPostCommentsConnection
} = connectionDefinitions({
	name : 'PictoralPostComment',
	nodeType : PictoralPostCommentType
});

const {
	connectionType : PictoralPostLikeConnection
} = connectionDefinitions({
	name : 'PictoralPostLike',
	nodeType : PictoralPostLikeType
});

import Elapsed from 'elapsed';

export default new GraphQLObjectType({
	name: 'PictoralPost',
	description: 'This represents the pictoralPost curated by the humbee team',
	fields: () => {
		return {
			id: globalIdField('PictoralPost', pictoralPost => pictoralPost.id ),
			index: {
        type : GraphQLInt,
        resolve (pictoralPost) {
          return pictoralPost.id;
        }
      },
			title: {
				type: GraphQLString,
				resolve (pictoralPost) {
					return pictoralPost.title;
				}
			},
			thumbnail : {
				type : GraphQLString,
				resolve (pictoralPost){
					return pictoralPost.thumbnail;
				}
			},
			url : {
				type : GraphQLString,
				resolve (pictoralPost){
					return pictoralPost.url;
				}
			},
			description : {
				type : GraphQLString,
				resolve (pictoralPost){
					return pictoralPost.description;
				}
			},
			pictoralPostLikes : {
				type : PictoralPostLikeConnection,
				args : {
					...connectionArgs
				},
				resolve : (pictoralPost, {...args}, {db, userId}) => {
					let pictoralPostLikesList = db.PictoralPostLike.findAll({
						where : {
							PictoralPostId : pictoralPost.id
						}
					});
					return connectionFromPromisedArray(pictoralPostLikesList, args);
				}
			},
			pictoralPostComments : {
				type : PictoralPostCommentsConnection,
				args : {
					...connectionArgs
				},
				resolve : (pictoralPost, {...args}, {db, userId}) => {
					let pictoralPostList = db.PictoralPostComment.findAll({
						where : {
							PictoralPostId : pictoralPost.id
						},
						attributes: {
							include: [
								[db.sequelize.literal('(select count(*) from "PictoralPostCommentLikes" where "PictoralPostCommentId" = "PictoralPostComment"."id")'), 'numberOfLikes'],
								[db.sequelize.literal('(select count(*) from "PictoralPostCommentLikes" where "PictoralPostCommentId" = "PictoralPostComment"."id" and "UserId"='+userId+')'), 'liked'],
							]
						},
						order: '"numberOfLikes" desc'
					});
					return connectionFromPromisedArray(pictoralPostList, args);
				}
			},
			numberOfComments : {
				type : GraphQLInt,
				resolve: (pictoralPost, {...args}, {db}) => {
					return pictoralPost.dataValues.numberOfComments;
				}
			},
			postedTime : {
				type : GraphQLString,
				resolve(pictoralPost){
					let elapsedTime = new Elapsed(new Date(pictoralPost.createdAt), new Date());
					return elapsedTime.optimal;
				}
			},
			numberOfLikes : {
				type : GraphQLInt,
				resolve: async (pictoralPost, {...args}, {db}) => {
					if(pictoralPost.dataValues.numberOfLikes == null || pictoralPost.dataValues.numberOfLikes == undefined) {
						let numP = await db.sequelize.query('(select count(*) from "PictoralPostLikes" where "PictoralPostId" = '+pictoralPost.id+')')
						let num = numP[0][0].count;
						return num;
					}
					return pictoralPost.dataValues.numberOfLikes;
				}
			},
			liked : {
				type : GraphQLBoolean,
				resolve : async (pictoralPost, args, { db, userId}) => {
					let count;
					if(pictoralPost.dataValues.liked == null || pictoralPost.dataValues.liked == undefined) {
						let countP = await db.sequelize.query('(select count(*) from "PictoralPostLikes" where "PictoralPostId" = '+pictoralPost.id+' and "UserId"='+userId+')')
						count = countP[0][0].count;
					}
					else {
						count = pictoralPost.dataValues.liked;
					}
					if(count=='1') {
						return true;
					} else {
						return false;
					}
				}
			}
		};
	},
	interfaces: () => [NodeInterface]
});
