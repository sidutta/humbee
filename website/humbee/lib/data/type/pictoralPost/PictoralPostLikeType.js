
import {
	GraphQLObjectType
} from 'graphql';

import { NodeInterface } from '../../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';


export default new GraphQLObjectType({
	name: 'PictoralPostLike',
	description: 'This represents a like by a user for a pictoralPost',
	fields: () => {
		return {
			id: globalIdField('PictoralPostLike', pictoralPostLike => pictoralPostLike.id ),
			userId : globalIdField('User', pictoralPostLike => pictoralPostLike.UserId),
			pictoralPostId : globalIdField('PictoralPost', pictoralPostLike => pictoralPostLike.PictoralPostId)
		};
	},
	interfaces: () => [NodeInterface]
});
