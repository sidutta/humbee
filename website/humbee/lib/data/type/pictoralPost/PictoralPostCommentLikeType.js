import {
	GraphQLObjectType
} from 'graphql';

import { NodeInterface } from '../../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';

export default new GraphQLObjectType({
	name: 'PictoralPostCommentLike',
	description: 'This represents a like by a user for a comment on an pictoralPost',
	fields: () => {
		return {
			id: globalIdField('PictoralPostCommentLike', pictoralPostCommentLike => pictoralPostCommentLike.id ),
			userId : globalIdField('User', pictoralPostCommentLike => pictoralPostCommentLike.UserId),
			pictoralPostCommentId : globalIdField('PictoralPostComment', pictoralPostCommentLike => pictoralPostCommentLike.PictoralPostCommentId)
		};
	},
	interfaces: () => [NodeInterface]
});
