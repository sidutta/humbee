import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLBoolean
} from 'graphql';

import { NodeInterface } from '../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';


export default new GraphQLObjectType({
	name: 'Option',
	description: 'This represents a Option which is part of a question',
	fields: () => {
		return {
			id: globalIdField('Option', option => option.id ),
			text: {
				type: GraphQLString,
				resolve (option) {
					return option.text;
				}
			},
			votes : {
				type: GraphQLInt,
				resolve (option) {
					return option.votes;
				}
			},
			voted : {
				type : GraphQLBoolean,
				resolve: async (option, args, { userId, db }) => {
					let response = await db.Response.findOne({
						where : {
							OptionId : option.id,
							UserId : userId
						}
					});
					if(response){
						return true;
					} else {
						return false;
					}
				}
			}
		};
	},
	interfaces: () => [NodeInterface]
});
