
import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLList,
	GraphQLBoolean
} from 'graphql';

import { NodeInterface } from '../interface/NodeInterface';

import { globalIdField,
          connectionArgs,
          connectionFromArray,
          connectionDefinitions } from 'graphql-relay';

import OptionType from './OptionType';


let QuestionType = new GraphQLObjectType({
	name: 'Question',
	description: 'This represents a Question',
	fields: () => {
		return {
			id: globalIdField('Question', question => question.id ),
			text: {
				type: GraphQLString,
				resolve (question) {
					return question.text;
				}
			},
			answered : {
				type : GraphQLBoolean,
				resolve : async (question, args, { userId, db }) => {
					let response = await db.Response.findOne({
						where : {
							UserId : userId,
							QuestionId : question.id
						}
					});
					if(response){
						return true;
					} else {
						return false;
					}
				}
			},
      rank: {
        type: GraphQLInt,
        resolve (question) {
          return question.rank;
        }
      },
			numberOfOptions: {
				type: GraphQLInt,
				resolve (question) {
					return question.numberOfOptions;
				}
			},
			options : {
				type: new GraphQLList(OptionType),
				resolve(question) {
					return question.Options;
				}
			}
		};
	},
	interfaces: () => [NodeInterface]
});

const {
  connectionType : QuestionsConnection
} = connectionDefinitions({
  name : 'Question',
  nodeType : QuestionType
});

export default QuestionType;
export const QuestionConnection = QuestionsConnection;
