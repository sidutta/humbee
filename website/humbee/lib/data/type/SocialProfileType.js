
import {
	GraphQLObjectType,
	GraphQLString
} from 'graphql';

import { NodeInterface } from '../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';



export default new GraphQLObjectType({
	name: 'SocialProfile',
	description: 'This represents the SocialProfile of a User',
	fields: () => {
		return {
			id: globalIdField('SocialProfile', socialProfile => socialProfile.id ),
			provider: {
				type: GraphQLString,
				resolve (socialProfile) {
					return socialProfile.provider;
				}
			},
			accessToken: {
				type: GraphQLString,
				resolve (socialProfile) {
					return socialProfile.accessToken;
				}
			},
			userID : {
				type : GraphQLString,
				resolve (socialProfile){
					return socialProfile.userID;
				}
			}
		};
	},
	interfaces: () => [NodeInterface]
});
