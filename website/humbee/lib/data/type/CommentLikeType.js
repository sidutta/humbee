
import {
	GraphQLObjectType
} from 'graphql';

import { NodeInterface } from '../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';

export default new GraphQLObjectType({
	name: 'CommentLike',
	description: 'This represents a like by a user for a comment',
	fields: () => {
		return {
			id: globalIdField('CommentLike', commentLike => commentLike.id ),
			userId : globalIdField('User', commentLike => commentLike.UserId),
			commentId : globalIdField('Comment', commentLike => commentLike.CommentId)
		};
	},
	interfaces: () => [NodeInterface]
});
