import {
	GraphQLObjectType,
	GraphQLID,
	GraphQLList,
  GraphQLString,
  GraphQLInt
} from 'graphql';

import sequelize from 'sequelize';

import { NodeField } from '../interface/NodeInterface';

import ViewerType from './ViewerType';
import MasterCharacterizationType from './MasterCharacterizationType';

export default new GraphQLObjectType({
	name: 'Query',
	description: 'Root query object',
	fields: () => {
		return {
			node : NodeField,
			Viewer: {
				type: ViewerType,
				fields : {
					id : GraphQLID,
					debateList : {
						type : GraphQLList
					}
				},
				resolve: () => {
					return {isAdmin : true, error : ''};
				}
			}
		};
	}
});
