
import {
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString,
  GraphQLFloat,
  GraphQLBoolean,
  GraphQLInt,
  GraphQLList
} from 'graphql';

import { NodeInterface } from '../interface/NodeInterface';

import {
  globalIdField,
  connectionArgs,
  connectionFromArray,
  connectionDefinitions
} from 'graphql-relay';

import models from '../models'

import SocialProfileType from './SocialProfileType';
import TopTagType from './TopTagType';
import { NotificationConnection } from './NotificationType';

import { ReasonsConnection } from './ConnectionType';
import { QuizResultConnection } from './quiz/QuizResultType';
import FbFriendsSuggestion from '../../utils/FbFriendsSuggestion';

let UserType = new GraphQLObjectType({
  name: 'User',
  description: 'This represents a User',
  fields: () => {
    return {
      id: globalIdField('User', user => user.id ),
      index: {
        type : GraphQLInt,
        resolve (user) {
          return user.id;
        }
      },
      firstName: {
        type: GraphQLString,
        resolve (user) {
          return user.firstName;
        }
      },
      score : {
        type : GraphQLInt,
        resolve (user){
          return user.score;
        }
      },

      notifications : {
        type : NotificationConnection,
        args : {
          ...connectionArgs
        },
        resolve : async (root, { ...args}) => {
          let notifications = await models.Notification.findAll();
          return connectionFromArray(notifications,args);
        }
      },
      monthlyScore : {
        type : GraphQLInt,
        args : {
          month : {
            type : new GraphQLNonNull(GraphQLString)
          },
          year : {
            type : new GraphQLNonNull(GraphQLString)
          }
        },
        resolve : async ({id}, {month, year}, {db}) => {
          let {value} = await db.MonthlyScore.find({
            where : {
              UserId : id,
              month : month,
              year : year
            }
          });
          return value;
        }
      },
      lastName: {
        type: GraphQLString,
        resolve (user) {
          return user.lastName;
        }
      },
      email: {
        type: GraphQLString,
        resolve (user) {
          return user.email;
        }
      },
      username: {
        type: GraphQLString,
        resolve (user) {
          return user.username;
        }
      },
      password: {
        type: GraphQLString,
        resolve () {
          return null;
        }
      },
      role: {
        type: GraphQLString,
        resolve (user, args, { isUser, userId }) {
          if(!isUser) {
            return "AnonymousUser";
          }
          return user.role;
        }
      },
      profilePic : {
        type : GraphQLString,
        resolve (user) {
          return user.profilePic;
        }
      },
      socialProfile : {
        type : SocialProfileType,
        resolve (user) {
          return user.socialProfile;
        }
      },
      topTags : {
        type : new GraphQLList(TopTagType),
        resolve : async (user, args, {db, isUser, error}) => {
          let tags = await db.sequelize.query('select "ReasonTags"."text", count("ReasonTags"."text") from "Reasons" join "ReasonAndTagsAssociations" on "Reasons"."id" = "ReasonAndTagsAssociations"."ReasonId" join "ReasonTags" on "ReasonTags"."id"="ReasonAndTagsAssociations"."ReasonTagId" where "UserId"=' + user.id + '  and "beAnonymous" = false group by "ReasonTags"."text" order by count desc;')
          return tags[0];
        }
      },
      isFollowing : {
        type : GraphQLBoolean,
        resolve : async (user, args, { userId }) => {
          let follower = await user.getFollowers({
            where : {
              id : userId
            }
          });
          if(follower[0]){
            return true;
          }
          return false;
        }
      },
      reasons : {
        type : ReasonsConnection,
        args : {
          ...connectionArgs
        },
        resolve : async (user, {...args}, {db, userId}) => {
          let includeAnonPosts = (userId==user.id?true:false);
          let reasonsList = await db.Reason.findAll({
            where : {
              UserId : user.id,
              $or: [
                  {
                      beAnonymous:
                      {
                          $eq: includeAnonPosts
                      }
                  },
                  {
                      beAnonymous:
                      {
                          $eq: false
                      }
                  }
              ]
            },
            attributes: {
              include: [
                [db.sequelize.literal('(select count(*) from "ReasonLikes" where "ReasonId" = "Reason"."id")'), 'numberOfLikes'],
                [db.sequelize.literal('(select count(*) from "ReasonLikes" where "ReasonId" = "Reason"."id" and "UserId"='+userId+')'), 'liked'],
                [db.sequelize.literal('(select count(*) from "Comments" where "ReasonId" = "Reason"."id")'), 'numberOfComments']
              ]
            },
            include : [db.ReasonTag]
          });
          let conn = connectionFromArray(reasonsList, args);
          conn.totalCount = reasonsList.length;
          return conn;
        }
      },
      quizzes : {
        type : QuizResultConnection,
        args : {
          ...connectionArgs
        },
        resolve : async (user, {...args}, {db}) =>{
          let quizResultList = await db.QuizResult.findAll({
                                include: [{model: db.Quiz, required:true}, {model: db.MasterCharacterization, required:true}],
                                where: {'UserId' : user.id}
                              })
          let conn = connectionFromArray(quizResultList, args);
          conn.totalCount = quizResultList.length;
          return conn;
        }
      },
      followers : {
        type : UsersConnection,
        args : {
          ...connectionArgs
        },
        resolve : async  (user, {...args}, {db, userId}) =>{
          let followersList = await user.getFollowers();
          let conn = connectionFromArray(followersList, args);
          conn.totalCount = followersList.length;
          return conn;
        }
      },
      similarity : {
        type : GraphQLInt,
        resolve : async  (user, {...args}, {db, userId}) =>{
          return Number(user.dataValues.similarity*100);
        }
      },
      dissimilarity : {
        type : GraphQLInt,
        resolve : async  (user, {...args}, {db, userId}) =>{
          return Number((1-user.dataValues.similarity)*100);
        }
      },
      following : {
        type : UsersConnection,
        args : {
          ...connectionArgs
        },
        resolve : async (user, {...args}, {db, userId}) =>{
          let followingList = await user.getFolloweds();
          let conn = connectionFromArray(followingList, args);
          conn.totalCount = followingList.length;
          return conn;
        }
      },
      followSuggestions : {
        type : UsersConnection,
        args : {
          ...connectionArgs
        },
        resolve :  async (user, {...args}, {db, userId}) => {
          let socialProfile = await db.SocialProfile.findOne({
            where : {
              UserId : userId,
              provider : 'Facebook'
            }
          });
          const {dataValues} = socialProfile;
          let res;
          try{
            res = await FbFriendsSuggestion(dataValues);
          } catch(error){
            console.log(error);
          }
          let suggestionsList = [];
          if(res.friends){
            if(res.friends.data){
              suggestionsList = await Promise.all(res.friends.data.map(({id})  =>  db.User.find({
                include : [{
                  model : db.SocialProfile,
                  where : {
                    userID : id
                  }
                }]
              })));
            }
          }
          return connectionFromArray(suggestionsList, connectionArgs);
        }
      },
      isViewer : {
        type : GraphQLBoolean,
        resolve : (user, args, {userId}) => {
          if(userId == user.id){
            return true;
          } else {
            return false;
          }

        }
      },
      rankInLeaderboard : {
        type : GraphQLInt,
        resolve: async (user, {...connectionArgs}, {db, isUser}) => {
          let position = await db.sequelize.query('select "rank", "score" from (select *, RANK() over(order by score desc) from "Users") as T  where "id" = '+user.id);
          if(position[0][0].score==0) {
            return -1;
          }
          return position[0][0].rank;
        }
      },
      leaderBoard : {
        type : UsersConnection,
        args : {
          ...connectionArgs
        },
        resolve : async (user, {...connectionArgs}, {db, isUser}) => {
          if(isUser){
            let users = await db.User.findAll({
              where : {
                score : {
                  $gt : 0
                }
              },
              order : 'score DESC'
            });
            if(users){
              return connectionFromArray(users, connectionArgs);
            } else {
              return [];
            }
          }
        }
      },
      users : {
        type : UsersConnection,
        args : {
          searchString : {
            type : new GraphQLNonNull(GraphQLString)
          },
          ...connectionArgs
        },
        resolve : async (user, { ...connectionArgs, searchString}, { db, isUser, isAnonymous, error}) => {
          if(isUser||isAnonymous){
            if(searchString){
              let users = await db.User.findAll({
                where : {
                  $or: [
                    {
                      firstName: {
                        $iLike: '%'+ searchString +'%'
                      }
                    },
                    {
                      lastName: {
                        $iLike: '%' + searchString + '%'
                      }
                    },
                    {
                      username : {
                        $iLike : '%' + searchString + '%'
                      }
                    },
                    {
                      $and : [
                        {
                          firstName : {
                            $iLike : '%' + searchString.split(' ')[0] + '%'
                          }
                        },
                        {
                          lastName : {
                            $iLike : '%' + searchString.split(' ')[1] + '%'
                          }
                        }
                      ]
                    },
                    {
                      $and : [
                        {
                          lastName : {
                            $iLike : '%' + searchString.split(' ')[0] + '%'
                          }
                        },
                        {
                          firstName : {
                            $iLike : '%' + searchString.split(' ')[1] + '%'
                          }
                        }
                      ]
                    }
                  ]
                }
              });
              if(users){
                return connectionFromArray(users, connectionArgs);
              } else {
                return [];
              }
            }
          }
        }
      }
    };
  },
  interfaces: () => [NodeInterface]
});

const {
  connectionType : UsersConnection
} = connectionDefinitions({
  name : 'User',
  nodeType : UserType,
  connectionFields: () => ({
    totalCount: {
      type: GraphQLInt,
      resolve: (connection) => {return connection.totalCount}
    }
  })
});

export default UserType;
export const UserConnection = UsersConnection;
