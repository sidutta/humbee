import { GraphQLObjectType } from 'graphql';
import AddNotificationSubscription from '../subscriptions/AddNotificationSubscription';
// import UpdateNotificationSubscription from '../subscriptions/UpdateNotificationSubscription';
// import DeleteNotificationSubscription from '../subscriptions/DeleteNotificationSubscription';


export default new GraphQLObjectType({
	name: 'Subscription',
	fields: () =>  ({
		addNotificationSubscription: AddNotificationSubscription
	})
});
