import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt
} from 'graphql';

import {
  connectionDefinitions,
  connectionArgs,
  connectionFromArray,
  fromGlobalId
} from 'graphql-relay';

import { NodeInterface } from '../interface/NodeInterface';

import ArticleType from './article/ArticleType';
import VideoType from './video/VideoType';

const {
  connectionType : ArticleConnection
} = connectionDefinitions({
  name : 'Article',
  nodeType : ArticleType
});

const {
  connectionType : VideoConnection
} = connectionDefinitions({
  name : 'Video',
  nodeType : VideoType
});

export default new GraphQLObjectType({
  name: 'Recommendation',
  description: 'This represents the Recommendation',
  fields: () => {
    return {
      articleList: {
        type: ArticleConnection,
        args : {
          ...connectionArgs
        },
        resolve : (recommendation, {...args}) => {
          return connectionFromArray(recommendation.articleList, args);
        }
      },
      videoList: {
        type: VideoConnection,
        args : {
          ...connectionArgs
        },
        resolve : (recommendation, {...args}) => {
          return connectionFromArray(recommendation.videoList, args);
        }
      },
      numArticles: {
        type: GraphQLInt,
        resolve (recommendation) {
          return recommendation.numArticles;
        }
      },
      numVideos: {
        type: GraphQLInt,
        resolve (recommendation) {
          return recommendation.numVideos;
        }
      },
    };
  }
});
