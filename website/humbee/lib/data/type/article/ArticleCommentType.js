import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLBoolean
} from 'graphql';

import { NodeInterface} from '../../interface/NodeInterface';

import {
	globalIdField,
	connectionDefinitions,
	connectionArgs,
	connectionFromArray,
	connectionFromPromisedArray
} from 'graphql-relay';

import ArticleCommentLikeType from './ArticleCommentLikeType';

import Elapsed from 'elapsed';

const {
	connectionType : ArticleCommentLikeConnection
} = connectionDefinitions({
	name : 'ArticleCommentLike',
	nodeType : ArticleCommentLikeType
});

export default new GraphQLObjectType({
	name: 'ArticleComment',
	description: 'This represents a Comment posted by a user on a article',
	fields: () => {
		return {
			id: globalIdField('ArticleComment', articleComment => articleComment.id),
			index: {
        type : GraphQLInt,
        resolve (articleComment) {
          return articleComment.id;
        }
      },
			text: {
				type: GraphQLString,
				resolve (articleComment) {
					return articleComment.text;
				}
			},
			userId : globalIdField('User', articleComment => articleComment.UserId),
			numberOfLikes : {
				type : GraphQLInt,
				resolve: (articleComment, {...args}, {db}) => {
					return articleComment.dataValues.numberOfLikes;
				}
			},
			articleCommentLikes : {
				type : ArticleCommentLikeConnection,
				args : {
					...connectionArgs
				},
				resolve : (articleComment, {...args}, {db}) => {
					let articleCommentLikesList = db.ArticleCommentLike.findAll({
		        where : {
		          ArticleCommentId : articleComment.id
		        }
		      });
					return connectionFromPromisedArray(articleCommentLikesList, args);
				}
			},
			postedTime : {
				type : GraphQLString,
				resolve(articleComment) {
					let elapsedTime = new Elapsed(new Date(articleComment.createdAt), new Date());
					return elapsedTime.optimal;
				}
			},
			liked : {
				type : GraphQLBoolean,
				resolve : (articleComment, args, {userId, db}) => {
					let count = articleComment.dataValues.liked;
					if(count=='1') {
						return true;
					} else {
						return false;
					}
				}
			}
		};
	},
	interfaces: () => [NodeInterface]
});
