import {
	GraphQLObjectType
} from 'graphql';

import { NodeInterface } from '../../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';

export default new GraphQLObjectType({
	name: 'ArticleCommentLike',
	description: 'This represents a like by a user for a comment on an article',
	fields: () => {
		return {
			id: globalIdField('ArticleCommentLike', articleCommentLike => articleCommentLike.id ),
			userId : globalIdField('User', articleCommentLike => articleCommentLike.UserId),
			articleCommentId : globalIdField('ArticleComment', articleCommentLike => articleCommentLike.ArticleCommentId)
		};
	},
	interfaces: () => [NodeInterface]
});
