
import {
	GraphQLObjectType
} from 'graphql';

import { NodeInterface } from '../../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';


export default new GraphQLObjectType({
	name: 'ArticleLike',
	description: 'This represents a like by a user for a article',
	fields: () => {
		return {
			id: globalIdField('ArticleLike', articleLike => articleLike.id ),
			userId : globalIdField('User', articleLike => articleLike.UserId),
			articleId : globalIdField('Article', articleLike => articleLike.ArticleId)
		};
	},
	interfaces: () => [NodeInterface]
});
