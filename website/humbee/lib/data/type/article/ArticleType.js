import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLBoolean,
	GraphQLInt
} from 'graphql';

import { NodeInterface } from '../../interface/NodeInterface';

import {
	globalIdField,
	connectionArgs,
	connectionFromArray,
	connectionDefinitions,
	connectionFromPromisedArray
 } from 'graphql-relay';

import ArticleCommentType from './ArticleCommentType';
import ArticleLikeType from './ArticleLikeType';

const {
	connectionType : ArticleCommentsConnection
} = connectionDefinitions({
	name : 'ArticleComment',
	nodeType : ArticleCommentType
});

const {
	connectionType : ArticleLikeConnection
} = connectionDefinitions({
	name : 'ArticleLike',
	nodeType : ArticleLikeType
});

import Elapsed from 'elapsed';

export default new GraphQLObjectType({
	name: 'Article',
	description: 'This represents the article curated by the humbee team',
	fields: () => {
		return {
			id: globalIdField('Article', article => article.id ),
			index: {
        type : GraphQLInt,
        resolve (article) {
          return article.id;
        }
      },
			title: {
				type: GraphQLString,
				resolve (article) {
					return article.title;
				}
			},
			thumbnail : {
				type : GraphQLString,
				resolve (article){
					return article.thumbnail;
				}
			},
			url : {
				type : GraphQLString,
				resolve (article){
					return article.url;
				}
			},
			description : {
				type : GraphQLString,
				resolve (article){
					return article.description;
				}
			},
			articleLikes : {
				type : ArticleLikeConnection,
				args : {
					...connectionArgs
				},
				resolve : (article, {...args}, {db, userId}) => {
					let articleLikesList = db.ArticleLike.findAll({
						where : {
							ArticleId : article.id
						}
					});
					return connectionFromPromisedArray(articleLikesList, args);
				}
			},
			articleComments : {
				type : ArticleCommentsConnection,
				args : {
					...connectionArgs
				},
				resolve : (article, {...args}, {db, userId}) => {
					let articleList = db.ArticleComment.findAll({
						where : {
							ArticleId : article.id
						},
						attributes: {
							include: [
								[db.sequelize.literal('(select count(*) from "ArticleCommentLikes" where "ArticleCommentId" = "ArticleComment"."id")'), 'numberOfLikes'],
								[db.sequelize.literal('(select count(*) from "ArticleCommentLikes" where "ArticleCommentId" = "ArticleComment"."id" and "UserId"='+userId+')'), 'liked'],
							]
						},
						order: '"numberOfLikes" desc'
					});
					return connectionFromPromisedArray(articleList, args);
				}
			},
			numberOfComments : {
				type : GraphQLInt,
				resolve: (article, {...args}, {db}) => {
					return article.dataValues.numberOfComments;
				}
			},
			postedTime : {
				type : GraphQLString,
				resolve(article){
					let elapsedTime = new Elapsed(new Date(article.createdAt), new Date());
					return elapsedTime.optimal;
				}
			},
			numberOfLikes : {
				type : GraphQLInt,
				resolve: async (article, {...args}, {db}) => {
					if(article.dataValues.numberOfLikes == null || article.dataValues.numberOfLikes == undefined) {
						let numP = await db.sequelize.query('(select count(*) from "ArticleLikes" where "ArticleId" = '+article.id+')')
						let num = numP[0][0].count;
						return num;
					}
					return article.dataValues.numberOfLikes;
				}
			},
			liked : {
				type : GraphQLBoolean,
				resolve : async (article, args, { db, userId}) => {
					let count;
					if(article.dataValues.liked == null || article.dataValues.liked == undefined) {
						let countP = await db.sequelize.query('(select count(*) from "ArticleLikes" where "ArticleId" = '+article.id+' and "UserId"='+userId+')')
						count = countP[0][0].count;
					}
					else {
						count = article.dataValues.liked;
					}
					if(count=='1') {
						return true;
					} else {
						return false;
					}
				}
			}
		};
	},
	interfaces: () => [NodeInterface]
});
