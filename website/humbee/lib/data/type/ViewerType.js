import {
	GraphQLObjectType,
	GraphQLList,
  GraphQLString,
  GraphQLInt,
	GraphQLNonNull
} from 'graphql';

import {
  globalIdField,
  connectionDefinitions,
  connectionArgs,
  connectionFromArray,
  connectionFromPromisedArray,
  fromGlobalId,
  toGlobalId
} from 'graphql-relay';

import sequelize from 'sequelize';

import { NodeInterface } from '../interface/NodeInterface';

import DebateType from './DebateType';
import VideoType from './video/VideoType';
import ArticleType from './article/ArticleType';
import PictoralPostType from './pictoralPost/PictoralPostType';
import QuizType from './quiz/QuizType';
import UserType from './UserType';
import NewsFeedItemType from './NewsFeedItemType';
import MasterCharacterizationType from './MasterCharacterizationType';
import RecommendationType from './RecommendationType';
import CommunityType from './CommunityType';
import { UserConnection } from './UserType';

import { NotificationConnection } from './NotificationType';
import FollowerSuggestionType from './FollowerSuggestionType';

import computeUserSimilarity from '../../utils/computeUserSimilarity';
import getNotifications from '../../utils/notification/getNotifications';


const {
	connectionType : NewsFeedItemConnection
} = connectionDefinitions({
	name : 'NewsFeedItem',
	nodeType : NewsFeedItemType
});


// const {
//   connectionType : UserConnection
// } = connectionDefinitions({
//   name : 'User',
//   nodeType : UserType
// });

export default new GraphQLObjectType({
	name: 'Viewer',
	description: 'This represents a Viewer',
	fields: () => {
		return {
			id:   globalIdField('Viewer'),
			users : {
				type : UserConnection,
				args : {
					...connectionArgs
				},
				resolve : async (root, { ...args}, {db, userId}) => {
					let users = await db.User.findAll();
					return connectionFromArray(users,args);
				}
			},
      introQuizId: {
        type : GraphQLString,
        resolve : (root, args, {db, isUser, error}) => {
          return toGlobalId('Quiz', 1);
        }
      },
			newsFeedItems : {
				type : NewsFeedItemConnection,
				args : {
					type : {
						type : new GraphQLNonNull(GraphQLString),
					},
          community : {
            type : GraphQLInt,
          },
					...connectionArgs
				},
				resolve : async (root, {...args, type, community }, {req, res, redis, db, isUser, isAnonymous, error}) => {
					if(isUser||isAnonymous){
						// let newsFeedItems = [];

						if(type == 'debate'){
							return connectionFromPromisedArray(redis.zrange('dcommunity-'+community, 0, -1), args);
						}
						else if(type == 'video'){
							return connectionFromPromisedArray(redis.zrange('vcommunity-'+community, 0, -1), args);
						}
						else if(type == 'article'){
							return connectionFromPromisedArray(redis.zrange('acommunity-'+community, 0, -1), args);
						}
            else if(type == 'pictoralPost'){
              return connectionFromPromisedArray(redis.zrange('pcommunity-'+community, 0, -1), args);
            }
						else if(type == 'quiz'){
							return connectionFromPromisedArray(redis.zrange('qcommunity-'+community, 0, -1), args);
						}
            else {
						  return connectionFromPromisedArray(redis.zrange('community-'+community, 0, -1), args);
						}
					} else {
						return {error : error || 'Authentication Error'};
					}
				}
			},
			debateList : {
				type : new GraphQLList(DebateType),
				resolve :  (root,args,{db, isUser, isAnonymous, error}) => {
					if(isUser||isAnonymous){
						return db.Debate.findAll();
					} else {
						return {error : error || 'Authentication Error'};
					}
				}
			},
			videoList : {
				type : new GraphQLList(VideoType),
				resolve :  (root,args,{db, isUser, isAnonymous, error}) => {
					if(isUser||isAnonymous){
						return db.Video.findAll();
					} else {
						return {error : error || 'Authentication Error'};
					}
				}
			},
      communities : {
        type : new GraphQLList(CommunityType),
        resolve :  (root,args,{db, isUser, isAnonymous, error}) => {
          if(isUser||isAnonymous){
            return db.Community.findAll();
          } else {
            return {error : error || 'Authentication Error'};
          }
        }
      },
			articleList : {
				type : new GraphQLList(ArticleType),
				resolve :  (root,args,{db, isUser, isAnonymous, error}) => {
					if(isUser||isAnonymous){
						return db.Article.findAll();
					} else {
						return {error : error || 'Authentication Error'};
					}
				}
			},
      pictoralPostList : {
        type : new GraphQLList(PictoralPostType),
        resolve :  (root,args,{db, isUser, isAnonymous, error}) => {
          if(isUser||isAnonymous){
            return db.PictoralPost.findAll();
          } else {
            return {error : error || 'Authentication Error'};
          }
        }
      },
			quizList : {
				type : new GraphQLList(QuizType),
				resolve :  (root,args,{db, isUser, isAnonymous, error}) => {
					if(isUser||isAnonymous){
						return db.Quiz.findAll();
					} else {
						return {error : error || 'Authentication Error'};
					}
				}
			},
			user : {
				type : UserType,
				resolve :  (root, args ,{ db, userId })  => {
          if(userId==-1) {
            return {
              id: -1,
              role: 'AnonymousUser',
              firstName: 'Anonymous'
            }
          }
					return db.User.findByPrimary(userId);
				}
			},
      quizResult : {
        type : MasterCharacterizationType,
        args : {
          quizIndex: {
            type: GraphQLString,
            defaultValue: '',
          }
        },
        resolve : (root, args ,{ db, userId, isUser })  => {
          if(!isUser) {
            return {error : error || 'Authentication Error'};
          }
          if(args.quizIndex === '-1') return null;
          const { type, id } = fromGlobalId(args.quizIndex);
          let quizId = id;
          let characterString = "";
          let characterStringWithId = "";
          let subquizScores = "";
          return db.Option.findAll({
            order: '"Question"."SubquizId" ASC',
            attributes: ['Question.SubquizId',
                           sequelize.fn('sum', sequelize.col('weight'))],
            group: ["Question.SubquizId"],
            include : [{model: db.Question, required:true, attributes: [],
                                where: {SubquizId: [sequelize.literal('(select "SubquizId" from "QuizDivisions" where "QuizId"='+quizId+')')]}},
                        {model: db.Response, required:true, attributes: [], where: {'UserId' : userId}}],
            raw: true
          })
          .then( (scores) => {
            let subquizCharacterizationMap = {};
            let promises = [];
            let subquizIdsInOrder = [];
            let first = true;
            for(let score of scores) {
              subquizIdsInOrder.push(score.SubquizId);
              let p = db.OpinionCharacterization.findOne({
                      attributes: ['id', 'characterization', 'code', 'SubquizId'],
                      where: {
                                "SubquizId" : score.SubquizId,
                                "minScore" : {$lte: score.sum},
                                "maxScore" : {$gte: score.sum}
                              }
              })
              .then( (x) => {
                subquizCharacterizationMap[x.SubquizId] = {'characterization': x.characterization, 'code': x.code, 'id': x.id}
              });
              promises.push(p);
              db.SubquizResult
              .findOrCreate({
                where: {SubquizId: score.SubquizId,
                        UserId : userId},
                defaults: {score: score.sum}
              });
              subquizScores = subquizScores + (first?"":",") + "\"" + score.SubquizId.toString() +
                                                          "\":\"" + score.sum + "\"";
              first = false;
            }
            return Promise.all(promises).then( () => {
              for(let i=0; i<subquizIdsInOrder.length; i++) {
                characterString = characterString + subquizIdsInOrder[i].toString() +
                                        ":" +
                                        subquizCharacterizationMap[subquizIdsInOrder[i]].code +
                                        (i==subquizIdsInOrder.length-1?"":"#");
                characterStringWithId = characterStringWithId + "\"" + subquizIdsInOrder[i].toString() +
                                          "\":\"" +
                                          subquizCharacterizationMap[subquizIdsInOrder[i]].id +
                                          (i==subquizIdsInOrder.length-1?"\"":"\",");
              }
              return characterString;
            } )
          })
          .then( () => db.MasterCharacterization.findOne( {
            where : {
              "characterizationPattern" : characterString,
              "QuizId" : quizId
            }
          } ) )
          .then( (x) => {
            db.QuizResult
            .findOrCreate({
              where: {QuizId : quizId,
                      UserId : userId},
              defaults: {MasterCharacterizationId : x.id}
            });
            x.dataValues.characterizationPatternWithId = characterStringWithId;
            x.dataValues.subquizScores = subquizScores;
            return x;
          } );
        }
      },
      recommendationByQuiz : {
        type : RecommendationType,
        args : {
          masterCharacterizationId: {
            type: GraphQLInt,
            defaultValue: -1,
          }
        },
        resolve : async (root,args,{db, isUser, error}) => {
          if(isUser){
            // let characterObjectWithId;
            // try {
            //   characterObjectWithId = JSON.parse("{" + args.characterStringWithId + "}");
            // }
            // catch(e) {
            //   console.log('Error: while trying to parse characterObjectWithId: ', e);
            //   return { articleList: [], videoList: [], numArticles: 0, numVideos: 0 };
            // }
            // let userCharacteristics = [];
            // for (let key in characterObjectWithId) {
            //     userCharacteristics.push(characterObjectWithId[key]);
            // }
            let masterCharacterizationId = args.masterCharacterizationId;
            let recos = await db.RecommendationByOpinion.findAll({
                          where: {"MasterCharacterizationId" : masterCharacterizationId},
                          order: 'rank ASC'
                        });
            let recommendationObject = {};
            let articleIdList = [];
            let videoIdList = [];
            for(let i=0; i<recos.length; i++) {
              if(recos[i].entitytype=='video') {
                videoIdList.push(recos[i].entityId);
              }
              else if(recos[i].entitytype=='article') {
                articleIdList.push(recos[i].entityId);
              }
            }
            let articlePromise = db.Article.findAll({
              where: {"id": articleIdList}
            });
            let videoPromise = db.Video.findAll({
              where: {"id": videoIdList}
            });
            return Promise.all([articlePromise, videoPromise])
            .then( (returnValues) => {
              recommendationObject.articleList = returnValues[0];
              recommendationObject.videoList = returnValues[1];
              recommendationObject.numArticles = returnValues[0].length;
              recommendationObject.numVideos = returnValues[1].length;

              if(returnValues[0].length + returnValues[1].length == 0) {
                console.log('ContentWarning: There are no recommendations for', masterCharacterizationId);
              }

              return recommendationObject;
            } )
          } else {
            return {error : error || 'Authentication Error'};
          }
        }
      },
      followerSuggestion : {
        type : FollowerSuggestionType,
        args : {
          subquizScores: {
            type: GraphQLString,
            defaultValue: '',
          },
          ...connectionArgs
        },
        resolve : async (root,args,{db, isUser, userId, isAnonymous, error}) => {

          if(isUser||isAnonymous){
            let subquizScoresObject = JSON.parse("{"+args.subquizScores+"}");

            let subquizIds = [];

            for(let key in subquizScoresObject) {
              subquizIds.push(key);
            }

            let maxScoreRes = await db.sequelize.query("select sum(weight*weight) from (select sum(weight) as weight, \"SubquizId\" from (select max(weight) as weight, \"QuestionId\", \"SubquizId\" from \"Options\" join \"Questions\" on \"QuestionId\" = \"Questions\".\"id\" where \"SubquizId\" in ("+subquizIds+") group by \"QuestionId\", \"SubquizId\") as M group by \"SubquizId\") as N")
            let maxScore = Math.sqrt(maxScoreRes[0][0].sum);

            let followerSuggestions = {};

            followerSuggestions.strangers = []
            followerSuggestions.strangersOpposite = [];

            followerSuggestions.friends = await db.SubquizResult.findAll({
              where: { $and:
                          {
                             UserId : [sequelize.literal('(select "FollowedId" from "Followings" where "FollowerId"='+userId+')')],
                             SubquizId: subquizIds
                          }
                     },
              include: db.User,
              order: ['UserId', 'SubquizId']
            })
            .then( (scoreResults) => {

              let score = {};
              let scores = [];
              let prevUser = -1;

              for(let i=0; i<scoreResults.length; i++) {
                let scoreResult = scoreResults[i];
                if(scoreResult.UserId != prevUser && prevUser != -1) {
                  scoreResults[i-1].User.dataValues.similarity = computeUserSimilarity(score, subquizScoresObject, maxScore);
                  scores.push({"UserId": prevUser, "scores": score, "similarity": computeUserSimilarity(score, subquizScoresObject, maxScore), "user": scoreResults[i-1].User});
                  score = {};
                }
                score[scoreResult.SubquizId] = scoreResult.score;
                prevUser = scoreResult.UserId;
              }
              if(prevUser != -1) {
                scoreResults[scoreResults.length-1].User.dataValues.similarity = computeUserSimilarity(score, subquizScoresObject, maxScore);
                scores.push({"UserId": prevUser, "scores": score, "similarity": computeUserSimilarity(score, subquizScoresObject, maxScore), "user": scoreResults[scoreResults.length-1].User});
              }
              scores.sort((a, b) => b.similarity - a.similarity);
              scores = scores.map((score) => score.user);
              return scores;
            } );

            await db.SubquizResult.findAll({
              where: { $and:
                        { UserId : {$and: {$notIn: [sequelize.literal('(select "FollowedId" from "Followings" where "FollowerId"='+userId+')')],
                                        $ne: userId}
                                   },
                          SubquizId: subquizIds
                        }
                     },
              include: db.User,
              order: ['UserId', 'SubquizId']
            })
            .then( (scoreResults) => {

              let score = {};
              let scores = [];
              let prevUser = -1;

              for(let i=0; i<scoreResults.length; i++) {
                let scoreResult = scoreResults[i];
                if(scoreResult.UserId != prevUser && prevUser != -1) {
                  scoreResults[i-1].User.dataValues.similarity = computeUserSimilarity(score, subquizScoresObject, maxScore);
                  scores.push({"UserId": prevUser, "scores": score, "similarity": computeUserSimilarity(score, subquizScoresObject, maxScore), "user": scoreResults[i-1].User});
                  score = {};
                }
                score[scoreResult.SubquizId] = scoreResult.score;
                prevUser = scoreResult.UserId;
              }
              if(prevUser != -1) {
                scoreResults[scoreResults.length-1].User.dataValues.similarity = computeUserSimilarity(score, subquizScoresObject, maxScore);
                scores.push({"UserId": prevUser, "scores": score, "similarity": computeUserSimilarity(score, subquizScoresObject, maxScore), "user": scoreResults[scoreResults.length-1].User});
              }
              scores.sort((a, b) => b.similarity - a.similarity);

              scores.map((score) => {
                if(score.similarity>=0.5)
                  followerSuggestions.strangers.push(score.user);
                else {
                  followerSuggestions.strangersOpposite.push(score.user);
                }
              });
            } );

            followerSuggestions.strangersOpposite = followerSuggestions.strangersOpposite.slice().reverse();
            return followerSuggestions;
          } else {
            return {error : error || 'Authentication Error'};
          }
        }
      }
		};
	},
	interfaces: () => [NodeInterface]
});
