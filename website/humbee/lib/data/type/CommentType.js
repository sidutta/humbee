
import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLBoolean
} from 'graphql';

import { NodeInterface} from '../interface/NodeInterface';

import {
	globalIdField,
	connectionDefinitions,
	connectionArgs,
	connectionFromPromisedArray,
	connectionFromArray
} from 'graphql-relay';

import CommentLikeType from './CommentLikeType';

import Elapsed from 'elapsed';

const {
	connectionType : CommentLikeConnection
} = connectionDefinitions({
	name : 'CommentLike',
	nodeType : CommentLikeType
});

export default new GraphQLObjectType({
	name: 'Comment',
	description: 'This represents a Comment posted by a user',
	fields: () => {
		return {
			id: globalIdField('Comment', comment => comment.id ),
			index: {
        type : GraphQLInt,
        resolve (comment) {
          return comment.id;
        }
      },
			text: {
				type: GraphQLString,
				resolve (comment) {
					return comment.text;
				}
			},
			userId : globalIdField('User', comment => comment.UserId),
			numberOfLikes : {
				type : GraphQLInt,
				resolve (comment){
					// if(comment.CommentLikes){
					// 	return comment.CommentLikes.length;
					// } else{
					// 	return 0;
					// }
					return comment.dataValues.numberOfLikes;
				}
			},
			commentLikes : {
				type : CommentLikeConnection,
				args : {
					...connectionArgs
				},
				resolve : (comment, {...args}, {db}) => {
					// let commentLikesList = comment.CommentLikes;
					// return connectionFromArray(commentLikesList, args);
					let commentLikesList = db.CommentLike.findAll({
		        where : {
		          CommentId : comment.id
		        }
		      });
					return connectionFromPromisedArray(commentLikesList, args);
				}
			},
			postedTime : {
				type : GraphQLString,
				resolve(comment){
					let elapsedTime = new Elapsed(new Date(comment.createdAt), new Date());
					return elapsedTime.optimal;
				}
			},
			liked : {
				type : GraphQLBoolean,
				resolve :  (comment, args, {userId}) => {
					// if(comment.CommentLikes){
					// 	let commentLike = comment.CommentLikes.find(obj => (obj.UserId == userId));
					// 	if(commentLike){
					// 		return true;
					// 	} else {
					// 		return false;
					// 	}
					// } else {
					// 	return false;
					// }
					let count = comment.dataValues.liked;
					if(count=='1') {
						return true;
					} else {
						return false;
					}
				}
			}
		};
	},
	interfaces: () => [NodeInterface]
});
