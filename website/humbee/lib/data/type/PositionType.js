
import {
	GraphQLObjectType,
	GraphQLBoolean,
	GraphQLList
} from 'graphql';

import { NodeInterface } from '../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';

import ReasonType from './ReasonType';
import UserType from './UserType';

export default new GraphQLObjectType({
	name: 'Position',
	description: 'This represents a Position taken by a User in a Debate',
	fields: () => {
		return {
			id: globalIdField('Position', position => position.id ),
			agree: {
				type: GraphQLBoolean,
				resolve (position) {
					return position.agree;
				}
			},
			user : {
				type : UserType,
				resolve (position, args, {db}) {
					return db.User.findByPrimary(position.UserId);
					// return { id : position.UserId};
				}
			},
			reasons : {
				type : new GraphQLList(ReasonType),
				resolve(position){
					return position.reason;
				}
			}
		};
	},
	interfaces: () => [NodeInterface]
});
