import { GraphQLObjectType, GraphQLInt } from 'graphql';
import { globalIdField } from 'graphql-relay';

import { NodeInterface } from '../interface/NodeInterface';

let VerificationCodeType = new GraphQLObjectType({
	name: 'VerificationCode',
	description: 'Verification code and UserId mappings.',
	fields: () => {
		return {
			id: globalIdField('VerificationCode', VerificationCode => VerificationCode.id),
			userId : globalIdField('User', comment => comment.UserId),
			code : {
				type: GraphQLInt,
				resolve (verificationCode) {
					return verificationCode.code;
				}
			}
		};
	},
	interfaces: () => [NodeInterface]
});
export default VerificationCodeType;
