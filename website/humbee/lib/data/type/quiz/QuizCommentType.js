
import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLBoolean
} from 'graphql';

import { NodeInterface} from '../../interface/NodeInterface';

import {
	globalIdField,
	connectionDefinitions,
	connectionFromPromisedArray,
	connectionArgs,
	connectionFromArray
} from 'graphql-relay';

import QuizCommentLikeType from './QuizCommentLikeType';

import Elapsed from 'elapsed';

const {
	connectionType : QuizCommentLikeConnection
} = connectionDefinitions({
	name : 'QuizCommentLike',
	nodeType : QuizCommentLikeType
});

export default new GraphQLObjectType({
	name: 'QuizComment',
	description: 'This represents a Comment posted by a user on a quiz',
	fields: () => {
		return {
			id: globalIdField('QuizComment', quizComment => quizComment.id ),
			index: {
        type : GraphQLInt,
        resolve (quizComment) {
          return quizComment.id;
        }
      },
			text: {
				type: GraphQLString,
				resolve (quizComment) {
					return quizComment.text;
				}
			},
			userId : globalIdField('User', quizComment => quizComment.UserId),
			numberOfLikes : {
				type : GraphQLInt,
				resolve (quizComment){
					return quizComment.dataValues.numberOfLikes;
				}
			},
			quizCommentLikes : {
				type : QuizCommentLikeConnection,
				args : {
					...connectionArgs
				},
				resolve : (quizComment, {...args}, {db}) => {
					// let quizCommentLikesList = quizComment.QuizCommentLikes;
					// return connectionFromArray(quizCommentLikesList, args);
					let quizCommentLikesList = db.QuizCommentLike.findAll({
		        where : {
		          QuizCommentId : quizComment.id
		        }
		      });
					return connectionFromPromisedArray(quizCommentLikesList, args);
				}
			},
			postedTime : {
				type : GraphQLString,
				resolve(quizComment){
					let elapsedTime = new Elapsed(new Date(quizComment.createdAt), new Date());
					return elapsedTime.optimal;
				}
			},
			liked : {
				type : GraphQLBoolean,
				resolve :  (quizComment, args, {userId}) => {
					let count = quizComment.dataValues.liked;
					if(count=='1') {
						return true;
					} else {
						return false;
					}
					// let quizCommentLike = quizComment.QuizCommentLikes.find(obj => (obj.UserId == userId));
					// if(quizCommentLike){
					// 	return true;
					// } else {
					// 	return false;
					// }
				}
			}
		};
	},
	interfaces: () => [NodeInterface]
});
