
import {
	GraphQLObjectType
} from 'graphql';

import { NodeInterface } from '../../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';

export default new GraphQLObjectType({
	name: 'QuizCommentLike',
	description: 'This represents a like by a user for a comment on an quiz',
	fields: () => {
		return {
			id: globalIdField('QuizCommentLike', quizCommentLike => quizCommentLike.id ),
			userId : globalIdField('User', quizCommentLike => quizCommentLike.UserId),
			videoCommentId : globalIdField('QuizComment', quizCommentLike => quizCommentLike.VideoCommentId)
		};
	},
	interfaces: () => [NodeInterface]
});
