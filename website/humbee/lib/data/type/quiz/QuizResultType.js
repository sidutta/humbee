import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLBoolean,
	GraphQLInt,
	GraphQLList
} from 'graphql';

import { NodeInterface } from '../../interface/NodeInterface';

import {
	globalIdField,
	connectionArgs,
	connectionFromArray,
	connectionDefinitions
 } from 'graphql-relay';

import QuizType from './QuizType';
import MasterCharacterizationType from '../MasterCharacterizationType';

let QuizResultType = new GraphQLObjectType({
	name: 'QuizResult',
	description: 'This represents the QuizResult',
	fields: () => {
		return {
			id: globalIdField('QuizResult', quizResult => quizResult.id ),
			quizId : globalIdField('Quiz', quizResult => quizResult.QuizId),
			Quiz: {
				type: QuizType,
				resolve (quizResult) {
					return quizResult.Quiz;
				}
			},
			MasterCharacterization: {
				type: MasterCharacterizationType,
				resolve (quizResult) {
					return quizResult.MasterCharacterization;
				}
			},
		};
	},
	interfaces: () => [NodeInterface]
});

const {
	connectionType : QuizResultsConnection
} = connectionDefinitions({
	name : 'QuizResult',
	nodeType : QuizResultType,
  connectionFields: () => ({
    totalCount: {
      type: GraphQLInt,
      resolve: (connection) => {return connection.totalCount}
    }
  })
});

export default QuizResultType;
export const QuizResultConnection = QuizResultsConnection;
