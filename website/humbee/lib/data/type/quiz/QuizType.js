
import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLBoolean,
	GraphQLInt,
	GraphQLList
} from 'graphql';

import { NodeInterface } from '../../interface/NodeInterface';
import {QuestionConnection} from '../QuestionType';
import QuestionType from '../QuestionType';

import {
	globalIdField,
  toGlobalId,
	connectionArgs,
	connectionFromArray,
  connectionFromPromisedArray,
	connectionDefinitions
 } from 'graphql-relay';

import sequelize from 'sequelize';

import SubquizType from '../SubquizType';
import QuizCommentType from './QuizCommentType';
import QuizLikeType from './QuizLikeType';

const {
	connectionType : QuizCommentsConnection
} = connectionDefinitions({
	name : 'QuizComment',
	nodeType : QuizCommentType
});

const {
	connectionType : QuizLikeConnection
} = connectionDefinitions({
	name : 'QuizLike',
	nodeType : QuizLikeType
});

import Elapsed from 'elapsed';

export default new GraphQLObjectType({
	name: 'Quiz',
	description: 'This represents the quiz curated by the humbee team',
	fields: () => {
		return {
			id: globalIdField('Quiz', quiz => quiz.id ),
      index: {
        type: GraphQLInt,
        resolve (quiz) {
          return quiz.id;
        }
      },
			title: {
				type: GraphQLString,
				resolve (quiz) {
					return quiz.title;
				}
			},
      description: {
        type: GraphQLString,
        resolve (quiz) {
          return quiz.description;
        }
      },
			imageUrl : {
				type : GraphQLString,
				resolve (quiz){
					return quiz.imageUrl;
				}
			},
      subquizes : {
        type : new GraphQLList(SubquizType),
        resolve (quiz, args, {db}) {
          return quiz.getSubquizzes();
        }
      },
      subquizIds: {
        type : new GraphQLList(GraphQLString),
        resolve (quiz, args, {db}) {
          let a=  db.QuizDivision.findAll( {
            attributes: ['SubquizId'],
            where: {
              QuizId: quiz.id
            },
            order: 'rank ASC'
          } )
          .then( (subquizes) => {
            return subquizes.map( (subquiz) => toGlobalId('Subquiz', subquiz.SubquizId) );
          } );
          return a;
        }
      },
      Questions: {
        type : QuestionConnection,
        args : {
          ...connectionArgs
        },
        resolve : async (quiz, args, {db}) => {
          let questionList = await db.Question.findAll({
            where: {
              SubquizId: [sequelize.literal('(select "SubquizId" from "QuizDivisions" where "QuizId"='+quiz.id+')')]
            }
          })
          return connectionFromArray(questionList, args);
        }
      },
      numQuestions: {
        type : GraphQLInt,
        resolve : async (quiz, args, {db}) => {
          let questionList = await db.Question.findAll({
            where: {
              SubquizId: [sequelize.literal('(select "SubquizId" from "QuizDivisions" where "QuizId"='+quiz.id+')')]
            }
          })
          return questionList.length;
        }
      },
			quizLikes : {
				type : QuizLikeConnection,
				args : {
					...connectionArgs
				},
				resolve : (quiz, {...args}, { db }) => {
					// let quizLikesList = quiz.QuizLikes;
					// return connectionFromArray(quizLikesList, args);
          let quizLikesList = db.QuizLike.findAll({
            where : {
              QuizId : quiz.id
            }
          });
          return connectionFromPromisedArray(quizLikesList, args);
				}
			},
			quizComments : {
				type : QuizCommentsConnection,
				args : {
					...connectionArgs
				},
				resolve :  (quiz, {...args}, {db, userId}) =>{
					// let quizCommentsList =  quiz.QuizComments;
					// quizCommentsList.sort(( a,b ) => {
					// 	return b.QuizCommentLikes.length - a.QuizCommentLikes.length;
					// });
					// return connectionFromArray(quizCommentsList, args);
          let quizCommentsList = db.QuizComment.findAll({
            where : {
              QuizId : quiz.id
            },
            attributes: { 
              include: [
                [db.sequelize.literal('(select count(*) from "QuizCommentLikes" where "QuizCommentId" = "QuizComment"."id")'), 'numberOfLikes'],
                [db.sequelize.literal('(select count(*) from "QuizCommentLikes" where "QuizCommentId" = "QuizComment"."id" and "UserId"='+userId+')'), 'liked'],
              ] 
            },
            order: '"numberOfLikes" desc'
          });
          return connectionFromPromisedArray(quizCommentsList, args);
				}
			},
      numberOfComments : {
        type : GraphQLInt,
        resolve(quiz){
          // if(quiz.QuizComments){
          //   return quiz.QuizComments.length;
          // } else {
          //   return 0;
          // }
          return quiz.dataValues.numberOfComments;
        }
      },
      likes : {
        type : GraphQLInt,
        resolve : async (quiz, args, {db}) => {
          if(quiz.dataValues.numberOfLikes == null || quiz.dataValues.numberOfLikes == undefined) {
            let numP = await db.sequelize.query('(select count(*) from "QuizLikes" where "QuizId" = '+quiz.id+')')
            let num = numP[0][0].count;
            return num;
          }
          return quiz.dataValues.numberOfLikes;
        }
      },
      liked : {
        type : GraphQLBoolean,
        resolve : async (quiz, args, {db, userId}) => {
          let count;
          if(quiz.dataValues.liked == null || quiz.dataValues.liked == undefined) {
            let countP = await db.sequelize.query('(select count(*) from "QuizLikes" where "QuizId" = '+quiz.id+' and "UserId"='+userId+')')
            count = countP[0][0].count;
          }
          else {
            count = quiz.dataValues.liked;
          }
          if(count=='1') {
            return true;
          } else {
            return false;
          }
        }
      },
      QuizLikes : {
        type : QuizLikeConnection,
        args : {
          ...connectionArgs
        },
        resolve : (quiz, {...args}) => {
          // let quizLikesList = quiz.QuizLikes;
          // return connectionFromArray(quizLikesList, args);
          let quizLikesList = db.QuizLike.findAll({
            where : {
              QuizId : quiz.id
            }
          });
          return connectionFromPromisedArray(reasonLikesList, args);
        }
      },
      QuizComments : {
        type : QuizCommentsConnection,
        args : {
          ...connectionArgs
        },
        resolve :  (quiz, {...args}) =>{
          let quizCommentsList =  quiz.QuizComments;
          quizCommentsList.sort(( a,b ) => {
            return b.QuizCommentLikes.length - a.QuizCommentLikes.length;
          });
          return connectionFromArray(quizCommentsList, args);
        }
      },
			postedTime : {
				type : GraphQLString,
				resolve(quiz){
					let elapsedTime = new Elapsed(new Date(quiz.createdAt), new Date());
					return elapsedTime.optimal;
				}
			},
			numberOfLikes : {
				type : GraphQLInt,
				resolve : async (quiz, args, {db, userId}) => {
					if(quiz.dataValues.numberOfLikes == null || quiz.dataValues.numberOfLikes == undefined) {
            let numP = await db.sequelize.query('(select count(*) from "QuizLikes" where "QuizId" = '+quiz.id+')')
            let num = numP[0][0].count;
            return num;
          }
          return quiz.dataValues.numberOfLikes;
				}
			},
      lastDebateId: {
        type : GraphQLInt,
        resolve(quiz){
          if(quiz.LastDebateId){
            return quiz.LastDebateId;
          } else {
            return -1;
          }
        }
      },
      lastDebateGraphQLId: globalIdField('Debate', quiz => quiz.LastDebateId ),
      completed: {
        type : GraphQLBoolean,
        resolve : (quiz, args, { userId, db }) => {
          console.log(quiz);
          if(quiz.dataValues.completed) {
            return true;
          }
          let ret = db.QuizDivision.findAll( {
            attributes: ['SubquizId'],
            where: {
              QuizId : quiz.id
            }
          } ).then( async (subquizzes) => {
            let subquizIds = subquizzes.map((subquiz) => subquiz.SubquizId);
            let response = await db.SubquizResult.count({
              where : {
                UserId : userId,
                SubquizId : subquizIds
              }
            });
            return ( response >= subquizIds.length );
          } )
          return ret;
        }
      }
		};
	},
	interfaces: () => [NodeInterface]
});
