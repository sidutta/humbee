
import {
	GraphQLObjectType
} from 'graphql';

import { NodeInterface } from '../../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';


export default new GraphQLObjectType({
	name: 'QuizLike',
	description: 'This represents a like by a user for a quiz',
	fields: () => {
		return {
			id: globalIdField('QuizLike', quizLike => quizLike.id ),
			userId : globalIdField('User', quizLike => quizLike.UserId),
			quizId : globalIdField('Quiz', quizLike => quizLike.QuizId)
		};
	},
	interfaces: () => [NodeInterface]
});
