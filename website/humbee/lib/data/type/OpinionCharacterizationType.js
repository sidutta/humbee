import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLFloat
} from 'graphql';

import { NodeInterface } from '../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';

import SubquizType from './SubquizType';

export default new GraphQLObjectType({
  name: 'OpinionCharacterization',
  description: 'This represents a OpinionCharacterization',
  fields: () => {
    return {
      id: globalIdField('OpinionCharacterization', opinionCharacterization => opinionCharacterization.id ),
      index: {
        type: GraphQLInt,
        resolve (opinionCharacterization) {
          return opinionCharacterization.id;
        }
      },
      thumbnail: {
        type: GraphQLString,
        resolve (opinionCharacterization) {
          return opinionCharacterization.thumbnail;
        }
      },
      characterization: {
        type: GraphQLString,
        resolve (opinionCharacterization) {
          return opinionCharacterization.characterization;
        }
      },
      code: {
        type: GraphQLString,
        resolve (opinionCharacterization) {
          return opinionCharacterization.code;
        }
      },
      description: {
        type: GraphQLString,
        resolve (opinionCharacterization) {
          return opinionCharacterization.description;
        }
      },
      minScore: {
        type: GraphQLFloat,
        resolve (opinionCharacterization) {
          return opinionCharacterization.minScore;
        }
      },
      maxScore : {
        type: GraphQLFloat,
        resolve(opinionCharacterization) {
          return opinionCharacterization.maxScore;
        }
      },
      subquiz : {
        type : SubquizType,
        resolve (opinionCharacterization) {
          return opinionCharacterization.getSubquiz();
        }
      },
      subquizId : {
        type : GraphQLInt,
        resolve (opinionCharacterization) {
          return opinionCharacterization.SubquizId;
        }
      },
      noOfUsers : {
        type: GraphQLInt,
        resolve (opinionCharacterization) {
          return opinionCharacterization.noOfUsers;
        }
      },
    };
  },
  interfaces: () => [NodeInterface]
});
