
import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLBoolean
} from 'graphql';

import { NodeInterface} from '../../interface/NodeInterface';

import {
	globalIdField,
	connectionDefinitions,
	connectionArgs,
	connectionFromArray,
	connectionFromPromisedArray
} from 'graphql-relay';

import VideoCommentLikeType from './VideoCommentLikeType';

import Elapsed from 'elapsed';

const {
	connectionType : VideoCommentLikeConnection
} = connectionDefinitions({
	name : 'VideoCommentLike',
	nodeType : VideoCommentLikeType
});

export default new GraphQLObjectType({
	name: 'VideoComment',
	description: 'This represents a Comment posted by a user on a video',
	fields: () => {
		return {
			id: globalIdField('VideoComment', videoComment => videoComment.id ),
			index: {
        type : GraphQLInt,
        resolve (videoComment) {
          return videoComment.id;
        }
      },
			text: {
				type: GraphQLString,
				resolve (videoComment) {
					return videoComment.text;
				}
			},
			userId : globalIdField('User', videoComment => videoComment.UserId),
			numberOfLikes : {
				type : GraphQLInt,
				resolve (videoComment){
					return videoComment.dataValues.numberOfLikes;
				}
			},
			videoCommentLikes : {
				type : VideoCommentLikeConnection,
				args : {
					...connectionArgs
				},
				resolve : (videoComment, {...args}, {db}) => {
					let videoCommentLikesList = db.VideoCommentLike.findAll({
		        where : {
		          VideoCommentId : videoComment.id
		        }
		      });
					return connectionFromPromisedArray(videoCommentLikesList, args);
				}
			},
			postedTime : {
				type : GraphQLString,
				resolve(videoComment){
					let elapsedTime = new Elapsed(new Date(videoComment.createdAt), new Date());
					return elapsedTime.optimal;
				}
			},
			liked : {
				type : GraphQLBoolean,
				resolve : (videoComment, args, {userId, db}) => {
					let count = videoComment.dataValues.liked;
					if(count=='1') {
						return true;
					} else {
						return false;
					}
				}
			}
		};
	},
	interfaces: () => [NodeInterface]
});
