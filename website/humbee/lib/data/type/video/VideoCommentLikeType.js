
import {
	GraphQLObjectType
} from 'graphql';

import { NodeInterface } from '../../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';

export default new GraphQLObjectType({
	name: 'VideoCommentLike',
	description: 'This represents a like by a user for a comment on a video',
	fields: () => {
		return {
			id: globalIdField('VideoCommentLike', videoCommentLike => videoCommentLike.id ),
			userId : globalIdField('User', videoCommentLike => videoCommentLike.UserId),
			videoCommentId : globalIdField('VideoComment', videoCommentLike => videoCommentLike.VideoCommentId)
		};
	},
	interfaces: () => [NodeInterface]
});
