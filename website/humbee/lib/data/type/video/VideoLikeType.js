
import {
	GraphQLObjectType
} from 'graphql';

import { NodeInterface } from '../../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';


export default new GraphQLObjectType({
	name: 'VideoLike',
	description: 'This represents a like by a user for a video',
	fields: () => {
		return {
			id: globalIdField('VideoLike', videoLike => videoLike.id ),
			userId : globalIdField('User', videoLike => videoLike.UserId),
			videoId : globalIdField('Video', videoLike => videoLike.VideoId)
		};
	},
	interfaces: () => [NodeInterface]
});
