
import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLBoolean,
	GraphQLInt
} from 'graphql';

import { NodeInterface } from '../../interface/NodeInterface';

import {
	globalIdField,
	connectionArgs,
	connectionFromArray,
	connectionDefinitions,
	connectionFromPromisedArray
} from 'graphql-relay';

import VideoCommentType from './VideoCommentType';
import VideoLikeType from './VideoLikeType';

const {
	connectionType : VideoCommentsConnection
} = connectionDefinitions({
	name : 'VideoComment',
	nodeType : VideoCommentType
});

const {
	connectionType : VideoLikeConnection
} = connectionDefinitions({
	name : 'VideoLike',
	nodeType : VideoLikeType
});

import Elapsed from 'elapsed';

export default new GraphQLObjectType({
	name: 'Video',
	description: 'This represents the video curated by the humbee team',
	fields: () => {
		return {
			id: globalIdField('Video', video => video.id ),
			index: {
        type : GraphQLInt,
        resolve (video) {
          return video.id;
        }
      },
			title: {
				type: GraphQLString,
				resolve (video) {
					return video.title;
				}
			},
			thumbnail : {
				type : GraphQLString,
				resolve (video){
					return video.thumbnail;
				}
			},
			videoId : {
				type : GraphQLString,
				resolve (video){
					return video.videoId;
				}
			},
			description : {
				type : GraphQLString,
				resolve (video){
					return video.description;
				}
			},
			videoLikes : {
				type : VideoLikeConnection,
				args : {
					...connectionArgs
				},
				resolve : (video, {...args}, {db, userId}) => {
					let videoLikesList = db.VideoLike.findAll({
		        where : {
		          VideoId : video.id
		        }
		      });
					return connectionFromPromisedArray(videoLikesList, args);
				}
			},
			videoComments : {
				type : VideoCommentsConnection,
				args : {
					...connectionArgs
				},
				resolve : (video, {...args}, {db, userId}) => {
					let videoCommentsList = db.VideoComment.findAll({
					  where : {
					    VideoId : video.id
					  },
					  attributes: { 
					  	include: [
					  		[db.sequelize.literal('(select count(*) from "VideoCommentLikes" where "VideoCommentId" = "VideoComment"."id")'), 'numberOfLikes'],
					  		[db.sequelize.literal('(select count(*) from "VideoCommentLikes" where "VideoCommentId" = "VideoComment"."id" and "UserId"='+userId+')'), 'liked'],
					  	] 
					  },
					  order: '"numberOfLikes" desc'
					});
					return connectionFromPromisedArray(videoCommentsList, args);
				}
			},
      numberOfComments : {
        type : GraphQLInt,
        resolve: async (video, {...args}, {db}) => {
					
					if(video.dataValues.numberOfComments == null || video.dataValues.numberOfComments == undefined) {
						let numP = await db.sequelize.query('(select count(*) from "VideoComments" where "VideoId" = '+video.id+')')
						let num = numP[0][0].count;
						return num;
					}
					return video.dataValues.numberOfComments;
				}
      },
			postedTime : {
				type : GraphQLString,
				resolve(video){
					let elapsedTime = new Elapsed(new Date(video.createdAt), new Date());
					return elapsedTime.optimal;
				}
			},
			numberOfLikes : {
				type : GraphQLInt,
				resolve : async (video, args, { db, userId}) => {
					if(video.dataValues.numberOfLikes == null || video.dataValues.numberOfLikes == undefined) {
						let numP = await db.sequelize.query('(select count(*) from "VideoLikes" where "VideoId" = '+video.id+')')
						let num = numP[0][0].count;
						return num;
					}
					return video.dataValues.numberOfLikes;
				}
			},
			liked : {
				type : GraphQLBoolean,
				resolve : async (video, args, { db, userId}) => {
					let count;
					if(video.dataValues.liked == null || video.dataValues.liked == undefined) {
						let countP = await db.sequelize.query('(select count(*) from "VideoLikes" where "VideoId" = '+video.id+' and "UserId"='+userId+')')
						count = countP[0][0].count;
					}
					else {
						count = video.dataValues.liked;
					}
					
					if(count=='1') {
						return true;
					} else {
						return false;
					}
				}
			}
		};
	},
	interfaces: () => [NodeInterface]
});
