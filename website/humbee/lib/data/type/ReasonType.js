import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLBoolean,
	GraphQLInt,
  GraphQLList
} from 'graphql';

import { NodeInterface } from '../interface/NodeInterface';

import {
	globalIdField,
	connectionArgs,
	connectionFromArray,
  connectionFromPromisedArray,
	connectionDefinitions
 } from 'graphql-relay';

import CommentType from './CommentType';
import ReasonLikeType from './ReasonLikeType';

const {
	connectionType : CommentsConnection
} = connectionDefinitions({
	name : 'Comment',
	nodeType : CommentType
});

const {
	connectionType : ReasonLikeConnection
} = connectionDefinitions({
	name : 'ReasonLike',
	nodeType : ReasonLikeType
});

import Elapsed from 'elapsed';

export default new GraphQLObjectType({
	name: 'Reason',
	description: 'This represents the reasons given by a User during the debate',
	fields: () => {
		return {
			id: globalIdField('Reason', reason => reason.id ),
      index: {
        type : GraphQLInt,
        resolve (reason) {
          return reason.id;
        }
      },
      // previousReasonId: {
      //   type : GraphQLInt,
      //   resolve (reason) {
      //     return reason.previousReasonId;
      //   }
      // },
			text: {
				type: GraphQLString,
				resolve (reason) {
					return reason.text;
				}
			},
      ReasonTags: {
        type: new GraphQLList(GraphQLString),
        resolve (reason) {
          let tags = [];
          if(reason.ReasonTags) {
            reason.ReasonTags.map( (tag) => {
              tags.push(tag.text);
            })
          }
          return tags;
        }
      },
      isAuthor : {
        type : GraphQLBoolean,
        resolve : (reason, {...args}, {db, userId}) => {
          return userId == reason.UserId;
        }
      },
			userId : globalIdField('User', reason => {
        if(reason.beAnonymous) {
          return 0;
        }
        return reason.UserId
      }),
			debateId : globalIdField('Debate', reason => {console.log('lll', reason.DebateId, globalIdField('Debate', () => reason.DebateId).resolve()); return reason.DebateId;}),
			agree : {
				type : GraphQLBoolean,
				resolve (reason) {
					return reason.agree;
				}
			},
      beAnonymous : {
        type : GraphQLBoolean,
        resolve (reason) {
          return reason.beAnonymous;
        }
      },
			reasonLikes : {
				type : ReasonLikeConnection,
				args : {
					...connectionArgs
				},
				resolve : (reason, {...args}, {db}) => {
          let reasonLikesList = db.ReasonLike.findAll({
            where : {
              ReasonId : reason.id
            }
          });
          return connectionFromPromisedArray(reasonLikesList, args);
				}
			},
			comments : {
				type : CommentsConnection,
				args : {
					...connectionArgs
				},
        resolve : (reason, {...args}, {db, userId}) => {
          let commentsList = db.Comment.findAll({
            where : {
              ReasonId : reason.id
            },
            attributes: { 
              include: [
                [db.sequelize.literal('(select count(*) from "CommentLikes" where "CommentId" = "Comment"."id")'), 'numberOfLikes'],
                [db.sequelize.literal('(select count(*) from "CommentLikes" where "CommentId" = "Comment"."id" and "UserId"='+userId+')'), 'liked'],
              ] 
            },
            order: '"numberOfLikes" desc'
          });
          return connectionFromPromisedArray(commentsList, args);
        }
			},
			postedTime : {
				type : GraphQLString,
				resolve(reason){
					let elapsedTime = new Elapsed(new Date(reason.createdAt), new Date());
					return elapsedTime.optimal;
				}
			},
			likes : {
				type : GraphQLInt,
        resolve: (reason, {...args}, {db}) => {
          return reason.dataValues.numberOfLikes;
        }
			},
      numberOfComments : {
        type : GraphQLInt,
        resolve: (reason, {...args}, {db}) => {
          return reason.dataValues.numberOfComments;
        }
      },
			liked : {
				type : GraphQLBoolean,
        resolve : (reason, args, { db, userId}) => {
          let count = reason.dataValues.liked;
          if(count=='1') {
            return true;
          } else {
            return false;
          }
        }
			}
		};
	},
	interfaces: () => [NodeInterface]
});
