import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLBoolean,
  GraphQLInt
} from 'graphql';

import {
  globalIdField,
  connectionArgs,
  connectionFromArray,
  connectionDefinitions
 } from 'graphql-relay';

import Elapsed from 'elapsed';

import { NodeInterface} from '../interface/NodeInterface';

export default new GraphQLObjectType({
  name: 'Community',
  description: 'This represents the community curated by the humbee team',
  fields: () => {
    return {
      id: globalIdField('Community', community => community.id ),
      index: {
        type: GraphQLInt,
        resolve (community) {
          return community.id;
        }
      },
      title: {
        type: GraphQLString,
        resolve (community) {
          return community.title;
        }
      },
      imageUrl : {
        type : GraphQLString,
        resolve (community){
          return "";
        }
      },
      url : {
        type : GraphQLString,
        resolve (community){
          return "";
        }
      },
      description : {
        type : GraphQLString,
        resolve (community){
          return "";
        }
      },
      postedTime : {
        type : GraphQLString,
        resolve(community){
          let elapsedTime = new Elapsed(new Date(community.createdAt), new Date());
          return elapsedTime.optimal;
        }
      },
    };
  },
  interfaces: () => [NodeInterface]
});
