import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLBoolean,
  GraphQLInt,
  GraphQLList
} from 'graphql';

import { NodeInterface } from '../interface/NodeInterface';

import QuestionType from './QuestionType';
import QuizDivisionType from './QuizDivisionType';
import OpinionCharacterizationType from './OpinionCharacterizationType';

import {
  globalIdField,
  connectionArgs,
  connectionFromArray,
  connectionDefinitions
 } from 'graphql-relay';

import Elapsed from 'elapsed';

export default new GraphQLObjectType({
  name: 'Subquiz',
  description: 'This represents the subquiz curated by the humbee team',
  fields: () => {
    return {
      id: globalIdField('Subquiz', subquiz => subquiz.id ),
      index: {
        type: GraphQLInt,
        resolve (subquiz) {
          return subquiz.id;
        }
      },
      QuizDivision: {
        type: QuizDivisionType,
        resolve (subquiz, args, {db}) {
          return subquiz.QuizDivision;
        }
      },
      rank: {
        type: GraphQLInt,
        resolve (subquiz, args, {db}) {
          return subquiz.quizDivision.rank;
        }
      },
      title: {
        type: GraphQLString,
        resolve (subquiz) {
          return subquiz.title;
        }
      },
      category: {
        type: GraphQLString,
        resolve (subquiz) {
          return subquiz.category;
        }
      },
      description: {
        type: GraphQLString,
        resolve (subquiz) {
          return subquiz.description;
        }
      },
      imageUrl : {
        type : GraphQLString,
        resolve (subquiz){
          return subquiz.imageUrl;
        }
      },
      questions : {
        type : new GraphQLList(QuestionType),
        resolve (subquiz) {
          return subquiz.getQuestions();
        }
      },
      Questions : {
        type : new GraphQLList(QuestionType),
        resolve (subquiz) {
          return subquiz.Questions;
        }
      },
      opinionCharacterizations : {
        type : new GraphQLList(OpinionCharacterizationType),
        resolve (subquiz, args, {db}) {
          return subquiz.getOpinionCharacterizations();
        }
      },
      postedTime : {
        type : GraphQLString,
        resolve(subquiz){
          let elapsedTime = new Elapsed(new Date(subquiz.createdAt), new Date());
          return elapsedTime.optimal;
        }
      }
    };
  },
  interfaces: () => [NodeInterface]
});
