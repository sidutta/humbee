
import {
	GraphQLObjectType
} from 'graphql';

import { NodeInterface } from '../../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';


export default new GraphQLObjectType({
	name: 'DebateLike',
	description: 'This represents a like by a user for a debate',
	fields: () => {
		return {
			id: globalIdField('DebateLike', debateLike => debateLike.id ),
			userId : globalIdField('User', debateLike => debateLike.UserId),
			debateId : globalIdField('Debate', debateLike => debateLike.DebateId)
		};
	},
	interfaces: () => [NodeInterface]
});
