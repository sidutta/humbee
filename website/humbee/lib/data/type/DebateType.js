
import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLList,
	GraphQLBoolean
} from 'graphql';

import { NodeInterface} from '../interface/NodeInterface';

import {
	globalIdField,
	connectionArgs,
	connectionFromArray,
	connectionFromPromisedArray
 } from 'graphql-relay';

import QuestionType from './QuestionType';
import StoryItemType from './StoryItemType';
import PositionType from './PositionType';
import DebateLikeType from './debate/DebateLikeType';

import { ReasonsConnection } from './ConnectionType';

import { connectionDefinitions } from 'graphql-relay';

const {
	connectionType : DebateLikeConnection
} = connectionDefinitions({
	name : 'DebateLike',
	nodeType : DebateLikeType
});

export default new GraphQLObjectType({
	name: 'Debate',
	description: 'This represents a Debate',
	fields: () => {
		return {
			id: globalIdField('Debate', debate => debate.id ),
      index: {
        type: GraphQLInt,
        resolve (debate) {
          return debate.id;
        }
      },
			title: {
				type: GraphQLString,
				resolve (debate) {
					return debate.title;
				}
			},
			motionShort: {
				type: GraphQLString,
				resolve (debate) {
					return debate.motionShort;
				}
			},
			motionFull: {
				type: GraphQLString,
				resolve (debate) {
					return debate.motionFull;
				}
			},
			agree : {
				type: GraphQLInt,
				resolve : async (debate) => {
					return debate.dataValues.agree;
					// if(debate.type == 'DebateType'){
					// 	const agree = await debate.getPositions({
					// 		where : {
					// 			agree : true
					// 		}
					// 	});
					// 	return agree.length || 0;
					// }
				}
			},
			disagree : {
				type: GraphQLInt,
				resolve : async (debate) => {
					return debate.dataValues.disagree;
					// if(debate.type == 'DebateType'){
					// 	const disagree = await debate.getPositions({
					// 		where : {
					// 			agree : false
					// 		}
					// 	});
					// 	return disagree.length || 0;
					// }
				}
			},
			duration: {
				type: GraphQLInt,
				resolve (debate) {
					return debate.duration;
				}
			},
			imageUrl : {
				type : GraphQLString,
				resolve (debate) {
					return debate.imageUrl;
				}
			},
			// questions : {
			// 	type : new GraphQLList(QuestionType),
			// 	resolve (debate) {
			// 		if(debate.Quiz){
			// 			return debate.Quiz.Questions;
			// 		} else {
			// 			return [];
			// 		}

			// 	}
			// },
      quizId : globalIdField('Quiz', debate => debate.QuizId ),
			storyItems : {
				type : new GraphQLList(StoryItemType),
				resolve : async (debate, {...args}, {db}) => {
					let storyItemsList = await db.StoryItem.findAll({
	          where : {
	            DebateId : debate.id
	          },
	          order: '"rank" asc'
	        });
					return storyItemsList;
				}
			},
			reasons : {
				type : ReasonsConnection,
				args : {
					type : {
						type : GraphQLString
					},
					...connectionArgs
				},
				resolve : async (debate, {...args, type}, {db, userId}) =>{
					let reasonsList;
					if(type=='all') {
						reasonsList = await db.Reason.findAll({
							where : {
								DebateId : debate.id,
								active : true
							},
							attributes: { 
			          include: [
			            [db.sequelize.literal('(select count(*) from "ReasonLikes" where "ReasonId" = "Reason"."id")'), 'numberOfLikes'],
			            [db.sequelize.literal('(select count(*) from "ReasonLikes" where "ReasonId" = "Reason"."id" and "UserId"='+userId+')'), 'liked'],
			            [db.sequelize.literal('(select count(*) from "Comments" where "ReasonId" = "Reason"."id")'), 'numberOfComments']
			          ] 
			        },
							include : [db.ReasonTag]
						});
					}
					else if(type=='agree') {
						reasonsList = await db.Reason.findAll({
							where : {
								DebateId : debate.id,
								agree : true,
								active : true
							},
							attributes: { 
			          include: [
			            [db.sequelize.literal('(select count(*) from "ReasonLikes" where "ReasonId" = "Reason"."id")'), 'numberOfLikes'],
			            [db.sequelize.literal('(select count(*) from "ReasonLikes" where "ReasonId" = "Reason"."id" and "UserId"='+userId+')'), 'liked'],
			            [db.sequelize.literal('(select count(*) from "Comments" where "ReasonId" = "Reason"."id")'), 'numberOfComments']
			          ] 
			        },
							include : [db.ReasonTag]
						});
					}
					else if(type=='disagree') {
						reasonsList = await db.Reason.findAll({
							where : {
								DebateId : debate.id,
								agree : false,
								active : true
							},
							attributes: { 
			          include: [
			            [db.sequelize.literal('(select count(*) from "ReasonLikes" where "ReasonId" = "Reason"."id")'), 'numberOfLikes'],
			            [db.sequelize.literal('(select count(*) from "ReasonLikes" where "ReasonId" = "Reason"."id" and "UserId"='+userId+')'), 'liked'],
			            [db.sequelize.literal('(select count(*) from "Comments" where "ReasonId" = "Reason"."id")'), 'numberOfComments']
			          ] 
			        },
							include : [db.ReasonTag]
						});
					}
					else {
						reasonsList = await db.Reason.findAll({
							where : {
								DebateId : debate.id,
								active : true
							},
							attributes: { 
			          include: [
			            [db.sequelize.literal('(select count(*) from "ReasonLikes" where "ReasonId" = "Reason"."id")'), 'numberOfLikes'],
			            [db.sequelize.literal('(select count(*) from "ReasonLikes" where "ReasonId" = "Reason"."id" and "UserId"='+userId+')'), 'liked'],
			            [db.sequelize.literal('(select count(*) from "Comments" where "ReasonId" = "Reason"."id")'), 'numberOfComments']
			          ] 
			        },
							include : [{model: db.ReasonTag, where:{text:type}}]
						});
					}

					return connectionFromArray(reasonsList,args);
				}
			},
      tags: {
        type : new GraphQLList(GraphQLString),
        resolve : async (debate, {...args}, {db}) => {
          let tags = await db.ReasonTag.findAll({
            attributes: ['text'],
            include : [{model: db.Reason, required:true, where:{DebateId:debate.id, active : true}}]
          });
          return tags.map(tag=>tag.text);
        }
      },
			// position : {
			// 	type : PositionType,
			// 	resolve : async (debate, args, { userId }) => {
			// 		if(debate.type == 'DebateType'){
			// 			let position = await  debate.getPositions({
			// 				where : {
			// 					UserId : userId
			// 				}
			// 			});
			// 			return position[0];
			// 		}
			// 	}
			// },
			userposition : {
				type : GraphQLBoolean,
				resolve : async (debate, args, { userId }) => {
					return debate.dataValues.userposition;
				}
			},
			debateLikes : {
				type : DebateLikeConnection,
				args : {
					...connectionArgs
				},
				resolve : (debate, {...args}, {db, userId}) => {
					let debateLikesList = db.DebateLike.findAll({
						where : {
							DebateId : debate.id
						}
					});
					return connectionFromPromisedArray(debateLikesList, args);
				}
			},
			numberOfLikes : {
				type : GraphQLString,
				resolve: (debate, {...args}, {db}) => {
					return debate.dataValues.numberOfLikes;
				}
			},
			liked : {
				type : GraphQLBoolean,
				resolve : (debate, args, { db, userId}) => {
					let count = debate.dataValues.liked;
					if(count=='1') {
						return true;
					} else {
						return false;
					}
				}
			}
		};
	},
	interfaces: () => [NodeInterface]
});
