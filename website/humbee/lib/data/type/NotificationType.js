import {
	GraphQLObjectType,
	GraphQLString
} from 'graphql';

import { connectionDefinitions } from 'graphql-relay';

import {
	globalIdField
} from 'graphql-relay';

import getNotificationText from '../../utils/notification/getNotificationText';
import getNotificationIcon from '../../utils/notification/getNotificationIcon';

import { NodeInterface } from '../interface/NodeInterface';

const NotificationType =  new GraphQLObjectType({
	name: 'Notification',
	description : 'This represents a notification',
	fields : () =>{
		return {
			id:   globalIdField('Notification', notification => (notification.id)),
			text : {
				type : GraphQLString,
				resolve : (notification) => getNotificationText(notification)
			},
			imageUrl : {
				type : GraphQLString,
				resolve : (notification) => getNotificationIcon(notification)
			}
		};
	},
	interfaces : () => [NodeInterface]
});

export default NotificationType;

const {
	connectionType : NotificationsConnection,
	edgeType: NotificaionsEdge
} = connectionDefinitions({
	name : 'Notification',
	nodeType : NotificationType
});

export const NotificationConnection = NotificationsConnection;
export const NotificationEdge = NotificaionsEdge;
