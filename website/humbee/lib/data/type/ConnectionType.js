import { connectionDefinitions } from 'graphql-relay';
import ReasonType from './ReasonType';
import {
  GraphQLInt
} from 'graphql';

const {
	connectionType: ReasonsConnection,
	edgeType: ReasonsEdge
} = connectionDefinitions({
	name: 'Reason',
	nodeType: ReasonType,
  connectionFields: (a) => ({
    totalCount: {
      type: GraphQLInt,
      resolve: (connection) => {return connection.totalCount}
    }
  })
});

const ConnectionType = {
	ReasonsEdge : ReasonsEdge,
	ReasonsConnection : ReasonsConnection
};

module.exports = ConnectionType;
