
import {
	GraphQLObjectType
} from 'graphql';

import { NodeInterface } from '../interface/NodeInterface';

import { globalIdField } from 'graphql-relay';


export default new GraphQLObjectType({
	name: 'ReasonLike',
	description: 'This represents a like by a user for a reason',
	fields: () => {
		return {
			id: globalIdField('ReasonLike', reasonLike => reasonLike.id ),
			userId : globalIdField('User', reasonLike => reasonLike.UserId),
			reasonId : globalIdField('Reason', reasonLike => reasonLike.ReasonId)
		};
	},
	interfaces: () => [NodeInterface]
});
