import {
	GraphQLObjectType,
	GraphQLString
} from 'graphql';

import {
	globalIdField
} from 'graphql-relay';

import { NodeInterface } from '../interface/NodeInterface';

export default new GraphQLObjectType({
	name: 'NewsFeedItem',
	description : 'This represents an item on the newsfeed',
	fields : (obj) => {
		return {
			id:   globalIdField('NewsFeedItem', obj => {
							let id = obj.substr(1);
							let type = '';
							if(obj[0]=='q') {
								type = 'QuizType';
							}
							else if(obj[0]=='a') {
								type = 'ArticleType';
							}
							else if(obj[0]=='v') {
								type = 'VideoType';
							}
							else if(obj[0]=='d') {
								type = 'DebateType';
							}
							else if(obj[0]=='p') {
								type = 'PictoralPostType';
							}
							return (id + ' ' + type);
						}),
			type : {
				type : GraphQLString,
				resolve : (obj) => {
					let type = '';
					if(obj[0]=='q') {
						type = 'QuizType';
					}
					else if(obj[0]=='a') {
						type = 'ArticleType';
					}
					else if(obj[0]=='v') {
						type = 'VideoType';
					}
					else if(obj[0]=='d') {
						type = 'DebateType';
					}
					else if(obj[0]=='p') {
						type = 'PictoralPostType';
					}
					return type;
				}
			},
			debateId : globalIdField('Debate', obj => {
							let id = obj.substr(1);
							return (id);
						}),
			videoId : globalIdField('Video', obj => {
							let id = obj.substr(1);
							return (id);
						}),
			articleId : globalIdField('Article', obj => {
							let id = obj.substr(1);
							return (id);
						}),
			quizId : globalIdField('Quiz', obj => {
							let id = obj.substr(1);
							return (id);
						}),
			pictoralPostId : globalIdField('PictoralPost', obj => {
							let id = obj.substr(1);
							return (id);
						}),
		};
	},
	interfaces : () => [NodeInterface]
});
