import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean
} from 'graphql';

import {
  globalIdField,
  connectionDefinitions,
  connectionArgs,
  connectionFromPromisedArray,
  connectionFromArray
} from 'graphql-relay';


import Elapsed from 'elapsed';

export default new GraphQLObjectType({
  name: 'TopTag',
  description: 'This represents a top tags posted by a user',
  fields: () => {
    return {
      text: {
        type: GraphQLString,
        resolve (topTag) {
          return topTag.text;
        }
      },
      count : {
        type : GraphQLInt,
        resolve (topTag){
          return topTag.count;
        }
      },
    };
  }
});
