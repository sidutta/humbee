import { subscriptionWithClientId } from 'graphql-relay-subscription';

import { NotificationEdge } from '../type/NotificationType';
import NotificationType from '../type/NotificationType';
import UserType from '../type/UserType';
import getNotifications from '../../utils/notification/getNotifications';

import { cursorForObjectInConnection } from 'graphql-relay';

import models from '../models';

export default subscriptionWithClientId({
	name : 'AddNotificationSubscription',
	outputFields : {
		notification : {
			type : NotificationType,
			resolve : ({notification}) => notification
		},
		notificationEdge : {
			type : NotificationEdge,
			resolve :   ({notifications, notification}) => {
				return {
					cursor : cursorForObjectInConnection(notifications, notification),
					node : notification
				};
			}
		},
		user : {
			type : UserType,
			resolve : ({user})=> {
				console.log(user);
				return user;
			}
		}
	},
	subscribe : (input, context) => {
		context.subscribe('add_notification');
	}
});
