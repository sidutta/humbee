require("babel-core/register");
require("babel-polyfill");

import validateInput from '../../common/validations/signup';
import isEmpty from 'lodash/isEmpty';

export default async function signupValidations(input) {

	const { data, db } = input
	let { errors, isValid } = validateInput(data);

	if(isValid){
		let user = await db.User.find({where : {email : data.email}});
		if(user) {
			errors.email = 'User with email already exists.';
		}

		user =  await db.User.find({where : {username : data.username}});
		if(user) {
			errors.username = 'User with username already exists.';
		}
	}

	return {
		errors,
		isValid : isEmpty(errors)
	};
}
