import validateInput from '../../common/validations/username';
import isEmpty from 'lodash/isEmpty';

export default async function usernameValidations(input) {

	const { data, db } = input
	let { errors, isValid } = validateInput(data);

	if(isValid){
		let user = await db.User.find({where : {username : data.username}});
		if(user) {
			errors.username = 'User with username already exists.';
		}
	}

	return {
		errors,
		isValid : isEmpty(errors)
	};
}
