import { nodeDefinitions, fromGlobalId } from 'graphql-relay';
import ViewerType from '../type/ViewerType';
import UserType from '../type/UserType';
import DebateType from '../type/DebateType';
import OptionType from '../type/OptionType';
import PositionType from '../type/PositionType';
import QuestionType from '../type/QuestionType';
import SocialProfileType from '../type/SocialProfileType';
import ReasonType from '../type/ReasonType';
import CommentType from '../type/CommentType';
import ArticleType from '../type/article/ArticleType';
import ArticleCommentType from '../type/article/ArticleCommentType';
import PictoralPostType from '../type/pictoralPost/PictoralPostType';
import PictoralPostCommentType from '../type/pictoralPost/PictoralPostCommentType';
import VideoType from '../type/video/VideoType';
import VideoCommentType from '../type/video/VideoCommentType';
import NewsFeedItemType from '../type/NewsFeedItemType';
import QuizType from '../type/quiz/QuizType';
import QuizResultType from '../type/quiz/QuizResultType';
import SubquizType from '../type/SubquizType';
import QuizCommentType from '../type/quiz/QuizCommentType';
import VerificationCodeType from '../type/VerificationCodeType';

const { nodeInterface, nodeField } = nodeDefinitions(
	(globalId, { db, userId }) => {
		const { type, id } = fromGlobalId(globalId);
		switch (type) {
		case 'Viewer': {
			return ViewerType;
		}
		case 'NewsFeedItem' :
			return NewsFeedItemType;
		case 'User':
			return db.User.findByPrimary(id);
		case 'Debate':
			return db.Debate.findOne({
				attributes: { 
          include: [
            [db.sequelize.literal('(select count(*) FROM "Positions" WHERE ("DebateId" = "Debate"."id" AND "agree" = true))'), 'agree'],
            [db.sequelize.literal('(select count(*) FROM "Positions" WHERE ("DebateId" = "Debate"."id" AND "agree" = false))'), 'disagree'],
            [db.sequelize.literal('(select "agree" FROM "Positions" WHERE ("DebateId" = "Debate"."id" AND "UserId"='+userId+'))'), 'userposition'],
            [db.sequelize.literal('(select count(*) from "DebateLikes" where "DebateId" = "Debate"."id")'), 'numberOfLikes'],
						[db.sequelize.literal('(select count(*) from "DebateLikes" where "DebateId" = "Debate"."id" and "UserId"='+userId+')'), 'liked']
          ] 
        },
				where : {
					id : id
				}
			});
		case 'Video' : {
			return db.Video.findOne({
				attributes: {
					include: [
						[db.sequelize.literal('(select count(*) from "VideoLikes" where "VideoId" = "Video"."id")'), 'numberOfLikes'],
						[db.sequelize.literal('(select count(*) from "VideoLikes" where "VideoId" = "Video"."id" and "UserId"='+userId+')'), 'liked'],
						[db.sequelize.literal('(select count(*) from "VideoComments" where "VideoId" = "Video"."id")'), 'numberOfComments']
					]
				},
				where : {
					id : id
				}
			});
		}
		case 'Article' : {
			return db.Article.findOne({
				attributes: {
					include: [
						[db.sequelize.literal('(select count(*) from "ArticleLikes" where "ArticleId" = "Article"."id")'), 'numberOfLikes'],
						[db.sequelize.literal('(select count(*) from "ArticleLikes" where "ArticleId" = "Article"."id" and "UserId"='+userId+')'), 'liked'],
						[db.sequelize.literal('(select count(*) from "ArticleComments" where "ArticleId" = "Article"."id")'), 'numberOfComments']
					]
				},
				where : {
					id : id
				}
			});
		}
		case 'PictoralPost' : {
      return db.PictoralPost.findOne({
        attributes: {
          include: [
            [db.sequelize.literal('(select count(*) from "PictoralPostLikes" where "PictoralPostId" = "PictoralPost"."id")'), 'numberOfLikes'],
            [db.sequelize.literal('(select count(*) from "PictoralPostLikes" where "PictoralPostId" = "PictoralPost"."id" and "UserId"='+userId+')'), 'liked'],
            [db.sequelize.literal('(select count(*) from "PictoralPostComments" where "PictoralPostId" = "PictoralPost"."id")'), 'numberOfComments']
          ]
        },
        where : {
          id : id
        }
      });
    }   
		case 'Reason' :{
			return db.Reason.findOne({
				where : {
					id : id
				},
				attributes: { 
          include: [
            [db.sequelize.literal('(select count(*) from "ReasonLikes" where "ReasonId" = "Reason"."id")'), 'numberOfLikes'],
            [db.sequelize.literal('(select count(*) from "ReasonLikes" where "ReasonId" = "Reason"."id" and "UserId"='+userId+')'), 'liked'],
            [db.sequelize.literal('(select count(*) from "Comments" where "ReasonId" = "Reason"."id")'), 'numberOfComments']
          ] 
        },
				include : [db.ReasonTag]
			});
		}
		case 'QuizResult' :{
			return db.QuizResult.findOne({
				where : {
					id : id
				},
				include: [{model: db.Quiz, required:true}, {model: db.MasterCharacterization, required:true}]
			});
		}
		case 'Comment' : {
			return db.Comment.findOne({
				where : {
					id : id
				},
				attributes: { 
          include: [
            [db.sequelize.literal('(select count(*) from "CommentLikes" where "CommentId" = "Comment"."id")'), 'numberOfLikes'],
            [db.sequelize.literal('(select count(*) from "CommentLikes" where "CommentId" = "Comment"."id" and "UserId"='+userId+')'), 'liked'],
          ] 
        },
			});
		}
		case 'VideoComment' : {
			return db.VideoComment.findOne({
				where : {
					id : id
				},
				attributes: { 
			  	include: [
			  		[db.sequelize.literal('(select count(*) from "VideoCommentLikes" where "VideoCommentId" = "VideoComment"."id")'), 'numberOfLikes'],
			  		[db.sequelize.literal('(select count(*) from "VideoCommentLikes" where "VideoCommentId" = "VideoComment"."id" and "UserId"='+userId+')'), 'liked'],
			  	] 
			  },
			});
		}
		case 'ArticleComment' : {
			return db.ArticleComment.findOne({
				where : {
					id : id
				},
				attributes: {
					include: [
						[db.sequelize.literal('(select count(*) from "ArticleCommentLikes" where "ArticleCommentId" = "ArticleComment"."id")'), 'numberOfLikes'],
						[db.sequelize.literal('(select count(*) from "ArticleCommentLikes" where "ArticleCommentId" = "ArticleComment"."id" and "UserId"='+userId+')'), 'liked'],
					]
				},
			});
		}
		case 'PictoralPostComment' : {
      return db.PictoralPostComment.findOne({
        where : {
          id : id
        },
        attributes: {
          include: [
            [db.sequelize.literal('(select count(*) from "PictoralPostCommentLikes" where "PictoralPostCommentId" = "PictoralPostComment"."id")'), 'numberOfLikes'],
            [db.sequelize.literal('(select count(*) from "PictoralPostCommentLikes" where "PictoralPostCommentId" = "PictoralPostComment"."id" and "UserId"='+userId+')'), 'liked'],
          ]
        },
      });
    }
		case 'QuizComment' : {
			return db.QuizComment.findOne({
				where : {
					id : id
				},
				attributes: { 
          include: [
            [db.sequelize.literal('(select count(*) from "QuizCommentLikes" where "QuizCommentId" = "QuizComment"."id")'), 'numberOfLikes'],
            [db.sequelize.literal('(select count(*) from "QuizCommentLikes" where "QuizCommentId" = "QuizComment"."id" and "UserId"='+userId+')'), 'liked'],
          ] 
        },
			});
		}
		case 'Option' :
			return db.Option.findByPrimary(id);
		case 'Position' :
			return db.Position.findByPrimary(id);
		case 'Question' :
			return db.Question.findOne({
				where : {
					id : id
				},
				include : [db.Option]
			});
		case 'Quiz' : {
			return db.Quiz.findOne({
				where : {
					id : id
				},
				attributes: { 
          include: [
            [db.sequelize.literal('(select count(*) from "QuizLikes" where "QuizId" = "Quiz"."id")'), 'numberOfLikes'],
            [db.sequelize.literal('(select count(*) from "QuizLikes" where "QuizId" = "Quiz"."id" and "UserId"='+userId+')'), 'liked'],
            [db.sequelize.literal('(select count(*) from "QuizComments" where "QuizId" = "Quiz"."id")'), 'numberOfComments']
          ] 
        },
				include : [
					db.Subquiz
				]
			});
		}
		case 'Subquiz' : {
			return db.Subquiz.findOne({
				where : {
					id : id
				},
				include : [db.Question
				]
			});
		}
		case 'SocialProfile' :
			return db.SocialProfile.findByPrimary(id);
		case 'VerificationCode':
			return db.VerificationCode.findByPrimary(id);
		default:
			return null;
		}
	},
	obj => {
		switch (obj.type) {
		case 'UserType':
			return UserType;
		case 'DebateType' :
			return DebateType;
		case 'OptionType' :
			return OptionType;
		case 'PositionType' :
			return PositionType;
		case 'QuestionType' :
			return QuestionType;
		case 'QuizType' :
			return QuizType;
		case 'QuizResultType' :
			return QuizResultType;
		case 'SubquizType' :
			return SubquizType;
		case 'SocialProfileType' :
			return SocialProfileType;
		case 'ReasonType' :
			return ReasonType;
		case 'CommentType' :
			return CommentType;
		case 'VideoType' :
			return VideoType;
		case 'ArticleType' :
			return ArticleType;
		case 'PictoralPostType' :
			return PictoralPostType;
		case 'VideoCommentType' :
			return VideoCommentType;
		case 'ArticleCommentType' :
			return ArticleCommentType;
		case 'PictoralPostCommentType':
			return PictoralPostCommentType;
		case 'QuizCommentType' :
			return QuizCommentType;
		case 'VerificationCodeType' :
			return VerificationCodeType;
		default: {
			console.log('Default type being returned from node interface!!!');
			return obj;
		}
		}
	}
);

export const NodeInterface = nodeInterface;
export const NodeField = nodeField;
