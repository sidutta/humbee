import {
	GraphQLNonNull,
	GraphQLString,
	GraphQLBoolean
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';


import QuestionType from '../type/QuestionType';


import { GraphQLError } from 'graphql/error';

export default mutationWithClientMutationId({
	name: 'CreateResponse',
	inputFields: {
		optionId: {
			type: new GraphQLNonNull(GraphQLString)
		},
		questionId : {
			type : new GraphQLNonNull(GraphQLString)
		},
		edit : {
			type : GraphQLBoolean
		}
	},
	outputFields: {
		question : {
			type : QuestionType,
			resolve :  ({id},args,{db}) => {
				return db.Question.findOne({
					where : {
						id : id
					},
					include : [db.Option]
				});
			}
		}
	},
	mutateAndGetPayload: 	async (data,{db, userId}) => {
		const { optionId, questionId, edit } = data;
		const { id } = fromGlobalId(optionId);
		let qid = fromGlobalId(questionId).id;
		// console.log(id, qid, questionId)
		// let newOption = await db.Option.findByPrimary(id);

		if(edit) {
			let optionId = await db.sequelize.query("select \"OptionId\" from \"Responses\" where \"QuestionId\" = "+qid+" and \"UserId\" = "+userId)
			if(id != optionId[0][0].OptionId) {
				// await db.Option.findByPrimary(optionId[0][0].OptionId)
				// .then(async (option)=>{
				//  	await option.updateAttributes({ votes : (option.votes - 1)});
				// })
				await db.sequelize.query("update \"Options\" set \"votes\"=\"votes\"-1 where \"id\"="+optionId[0][0].OptionId)
				await db.Response.destroy({
			    where:{
			      UserId : userId,
						QuestionId : qid
			    }
				})
			}
			else {
				return {id : qid};
			}
		}

		await db.Response.findOrCreate({
	    where:{
	      UserId : userId,
				QuestionId : qid
	    }, 
	    defaults: {
	    	UserId : userId,
				QuestionId : qid,
				OptionId : id
	    }
		}).then(async function(tag){
	    let created  = tag[1];
	    tag = tag[0]; 
	    if( created ){
	    	await db.sequelize.query("update \"Options\" set \"votes\"=\"votes\"+1 where \"id\"="+id)
	    	// await newOption.updateAttributes({ votes : (newOption.votes + 1)});
	    }
		});

		return {id : qid};
	}
});
