import {
	GraphQLString,
	GraphQLNonNull
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';

export default mutationWithClientMutationId({
	name: 'AddDeviceToken',
	inputFields: {
		token: {
			type: new GraphQLNonNull(GraphQLString)
		},
		deviceType : {
			type : new GraphQLNonNull(GraphQLString)
		}
	},
	outputFields: {
		success : {
			type : GraphQLString,
			resolve :  ({id}) => {
				if(id){
					return 'Success';
				}
			}
		}
	},
	mutateAndGetPayload:  ({token, deviceType},{db, userId}) => {
		return db.DeviceToken.findOrCreate({
			where : {
				UserId : userId,
				token,
				deviceType
			}
		});
	}
});
