import {
	GraphQLNonNull,
	GraphQLString,
	GraphQLBoolean,
	GraphQLList,
  GraphQLInt
} from 'graphql';

import isEmpty from 'lodash/isEmpty';
import updateScore from '../../utils/updateScore';

import {
	mutationWithClientMutationId,
	fromGlobalId,
  toGlobalId,
	cursorForObjectInConnection,
	connectionFromArray,
	connectionArgs
} from 'graphql-relay';

import {
	ReasonsEdge
} from '../type/ConnectionType';

import DebateType from '../type/DebateType';

import { GraphQLError } from 'graphql/error';

import Promise from 'bluebird';

export default mutationWithClientMutationId({
	name: 'CreateReason',
	inputFields: {
		reason: {
			type: new GraphQLNonNull(GraphQLString)
		},
		agree:{
			type:  GraphQLBoolean
		},
    beAnonymous: {
      type:  GraphQLBoolean
    },
		debateId : {
			type : new GraphQLNonNull(GraphQLString)
		},
    tags : {
      type : new GraphQLList(GraphQLString)
    },
    edit : {
      type : GraphQLBoolean
    },
    previousReasonId : {
      type : new GraphQLNonNull(GraphQLInt)
    }
	},
	outputFields: {
    reasonId : {
      type : new GraphQLNonNull(GraphQLString),
      resolve : async ({reasonId}, args, {db}) => {
        return reasonId;
      }
    },
		debate : {
			type : DebateType,
			resolve : ({id},args,{db}) => {
				return db.Debate.findByPrimary(id);
			}
		},
		reason : {
			type : ReasonsEdge,
			resolve : async ({newReason, reasonsList}, args, {db}) => {

				return {
					cursor: cursorForObjectInConnection(reasonsList, newReason),
					node: newReason
				};
			}
		}
	},
	mutateAndGetPayload: async	(data, {db, userId, isUser}) => {
    const { reason, agree, debateId, tags, beAnonymous, edit, previousReasonId } = data;

    let tagPromises = [];
    for (let i = 0; i < tags.length; ++i) {
      tagPromises.push(db.ReasonTag.findOrCreate({
        where: {
          text: tags[i]
        },
        defaults: {
          text: tags[i]
        }
      }));
    }

    if(edit) {
      await db.Reason.find( {
        where: {
          id: previousReasonId
        }
      } )
      .then( async (reason) => {
        await reason.update(
          {
            active: false
          }
        );
      })
    }

		const {id} = fromGlobalId(debateId);
		let newReason;
		if(isUser){
			newReason = db.Reason.create({
				text : reason,
        agree : agree,
        DebateId : id,
        UserId : userId,
        beAnonymous : beAnonymous,
        previousReasonId : previousReasonId,
        active : true
			})
      .catch(function (err) {
        console.log(err);
      });
      if(!beAnonymous && !edit)
  			updateScore({userId, db, points	: 10});
		}
    else {
			throw new GraphQLError('You dont have the required permissions');
		}

    return Promise.all([newReason, ...tagPromises])
    .then( (results) => {
      let assocs = [];
      for (let i = 1; i < results.length; ++i) {
        assocs.push(db.ReasonAndTagsAssociation.create({
          ReasonId: results[0].id,
          ReasonTagId: results[i][0].id
        }));
      }
      return assocs;
    })
    .then( (assocs) => {

      let reasonsList = db.Reason.findAll({
        where : {
          DebateId : id,
          active : true
        },
        include : [db.ReasonTag]
      });

      return {reasonsList, newReason, id, reasonId: toGlobalId('Reason', previousReasonId)};
    })

		// newReason = reasonsList.find(obj => (obj.id == reasonId));

	}
});
