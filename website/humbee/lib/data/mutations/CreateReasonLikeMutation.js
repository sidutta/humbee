import {
	GraphQLNonNull,
	GraphQLBoolean,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId,
  toGlobalId
} from 'graphql-relay';

import updateScore from '../../utils/updateScore';


import ReasonType from '../type/ReasonType';
import Promise from 'bluebird';

import { GraphQLError } from 'graphql/error';

export default mutationWithClientMutationId({
	name: 'CreateReasonLike',
	inputFields: {
		reasonId : {
			type : new GraphQLNonNull(GraphQLString)
		},
		liked : {
			type : GraphQLBoolean
		},
    debateId : {
      type : new GraphQLNonNull(GraphQLString)
    },
    userId : {
      type : new GraphQLNonNull(GraphQLString)
    }
	},
	outputFields: {
		reason : {
			type : ReasonType,
			resolve : ({id},args,{db, userId}) => {
				return db.Reason.findOne({
					where : {
						id : id
					},
					attributes: [
						"id",
						[db.sequelize.literal('(select count(*) from "ReasonLikes" where "ReasonId" = "Reason"."id")'), 'numberOfLikes'],
		        [db.sequelize.literal('(select count(*) from "ReasonLikes" where "ReasonId" = "Reason"."id" and "UserId"='+userId+')'), 'liked'],
					]
				});
			}
		}
	},
	mutateAndGetPayload: async	(data,{db, userId}) => {
		const { reasonId, debateId } = data;
    let userIdArg = data.userId;
		const { id } = fromGlobalId(reasonId);
    const debateIdInt = fromGlobalId(debateId);
    const userIdInt = fromGlobalId(userIdArg);

		let likePromise = db.ReasonLike.findOne({
			where : {
				UserId : userId,
				ReasonId : id
			}
		});
    let reasonPromise = db.Reason.find({
			where : {
        id : id
			}
		});
		let promiseOfLikerPromise = db.Position.find({
			where : {
				UserId : userId,
				DebateId : debateIdInt.id
			}
		});

		let like = await likePromise;
    let reason = await reasonPromise;
		let positionOfLiker = await promiseOfLikerPromise;

		let points = 5;

		if(positionOfLiker && positionOfLiker.agree!=undefined &&
				positionOfLiker.agree!=null && (reason.agree != positionOfLiker.agree)) {
					points = 10;
		}

		if(like){
			await db.ReasonLike.destroy({
				where : {
					UserId : userId,
					ReasonId : id
				}
			});
      if(userIdInt.id!=0)
			 updateScore({userId : userIdInt.id, db, points : -points});
			return { id : id };
		}
    else if(!like){
			await db.ReasonLike.create({
				UserId : userId,
				ReasonId : id
			});
      if(userIdInt.id!=0)
			 updateScore({userId : userIdInt.id, db, points : points});
			return {id : id};
		}
	}
});
