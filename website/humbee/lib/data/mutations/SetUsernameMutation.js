import {GraphQLNonNull, GraphQLString} from 'graphql';
import {mutationWithClientMutationId} from 'graphql-relay';
import jwt from 'jsonwebtoken';
import config from '../../config';

import { GraphQLError } from 'graphql/error';
import usernameValidations from '../validations/username';

export default mutationWithClientMutationId({
	name: 'SetUsername',
	inputFields: {
    username : {
      type: new GraphQLNonNull(GraphQLString)
    },
    id : {
      type: new GraphQLNonNull(GraphQLString)
    }
	},
	outputFields: {
		token: {
			type: GraphQLString,
			resolve: payload => {
				return payload.token;
			}
		}
	},
	mutateAndGetPayload: async (data,{db}) => {
    const { username, id } = data;
    const { errors, isValid } = await usernameValidations({data, db});

		if(!isValid){
			throw new GraphQLError(errors);
		}

		let user = await db.User.findOne({ where: { id: id }});
		user = await user.updateAttributes({username : username});

    if(user){
      const { id, username, role, firstName, lastName, email, profilePic} = user.dataValues;
			const token = jwt.sign({
					id : id,
					username : username,
					role : role,
					firstName : firstName,
					lastName : lastName,
					email : email,
					profilePic : profilePic
				}, config.jwtSecret);
      return {token : token};
    } else {
      throw new GraphQLError('Invalid Credentials');
    }
	}
});
