import {
	GraphQLNonNull,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';


import VideoType from '../../type/video/VideoType';


import { GraphQLError } from 'graphql/error';

export default mutationWithClientMutationId({
	name: 'CreateVideoComment',
	inputFields: {
		comment: {
			type: new GraphQLNonNull(GraphQLString)
		},
		videoId : {
			type : new GraphQLNonNull(GraphQLString)
		}
	},
	outputFields: {
		video : {
			type : VideoType,
			resolve : ({id},args,{db}) => db.Video.findOne({
				where : {
					id : id
				},
				attributes: { 
          include: [
            [db.sequelize.literal('(select count(*) from "VideoComments" where "VideoId" = "Video"."id")'), 'numberOfComments']
          ] 
        }
			})
		}
	},
	mutateAndGetPayload: 	async ({comment, videoId},{db, userId}) => {
		const { id } = fromGlobalId(videoId);
		if(!comment){
			throw new GraphQLError('Comment cannot be empty');
		}
		await db.VideoComment.create({
			text : comment,
			UserId : userId,
			VideoId : id
		});
		return {id};
	}
});
