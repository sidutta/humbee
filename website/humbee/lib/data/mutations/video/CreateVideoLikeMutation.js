import {
	GraphQLNonNull,
	GraphQLBoolean,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';

import VideoType from '../../type/video/VideoType';

export default mutationWithClientMutationId({
	name: 'CreateVideoLike',
	inputFields: {
		videoId : {
			type : new GraphQLNonNull(GraphQLString)
		},
		liked : {
			type : GraphQLBoolean
		}
	},
	outputFields: {
		video : {
			type : VideoType,
			resolve : ({id, userId},args,{db}) => db.Video.findOne({
				attributes: [
					"id",
          [db.sequelize.literal('(select count(*) from "VideoLikes" where "VideoId" = "Video"."id")'), 'numberOfLikes'],
          [db.sequelize.literal('(select count(*) from "VideoLikes" where "VideoId" = "Video"."id" and "UserId"='+userId+')'), 'liked'],
        ],
				where : {
					id : id
				}
			})
		}
	},
	mutateAndGetPayload: async	({videoId, liked},{db, userId}) => {
		const { id } = fromGlobalId(videoId);
		let like = await db.VideoLike.findOne({
			where : {
				UserId : userId,
				VideoId : id
			}
		});
		if(like){
			await db.VideoLike.destroy({
				where : {
					UserId : userId,
					VideoId : id
				}
			});
		} else if(!like){
			await db.VideoLike.create({
				UserId : userId,
				VideoId : id
			});
		}
		return { id : id, userId : userId };
	}
});
