import {
	GraphQLNonNull,
	GraphQLBoolean,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';


import VideoCommentType from '../../type/video/VideoCommentType';



export default mutationWithClientMutationId({
	name: 'CreateVideoCommentLike',
	inputFields: {
		videoCommentId : {
			type : new GraphQLNonNull(GraphQLString)
		},
		liked : {
			type : GraphQLBoolean
		}
	},
	outputFields: {
		videoComment : {
			type : VideoCommentType,
			resolve : ({id, userId}, args, {db}) => db.VideoComment.findOne({
				where : {
					id : id
				},
				attributes: [
		  		"id",
		  		[db.sequelize.literal('(select count(*) from "VideoCommentLikes" where "VideoCommentId" = "VideoComment"."id")'), 'numberOfLikes'],
		  		[db.sequelize.literal('(select count(*) from "VideoCommentLikes" where "VideoCommentId" = "VideoComment"."id" and "UserId"='+userId+')'), 'liked'],
			  ], 
			})
		}
	},
	mutateAndGetPayload: async	({videoCommentId, liked},{db, userId}) => {
		const { id } = fromGlobalId(videoCommentId);
		let like = await db.VideoCommentLike.findOne({
			where : {
				UserId : userId,
				VideoCommentId : id
			}
		});
		if(like){
			await db.VideoCommentLike.destroy({
				where : {
					UserId : userId,
					VideoCommentId : id
				}
			});
		} else if(!like){
			await db.VideoCommentLike.create({
				UserId : userId,
				VideoCommentId : id
			});
		}
		return { id : id, userId : userId };
	}
});
