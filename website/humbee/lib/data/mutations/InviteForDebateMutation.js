import {
	GraphQLString,
	GraphQLList,
	GraphQLNonNull
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';

import sendInvite from '../../utils/sendInvite';


export default mutationWithClientMutationId({
	name: 'InviteForDebate',
	inputFields: {
		inviteeList : {
			type : new GraphQLList(GraphQLString)
		},
		debateId : {
			type : new GraphQLNonNull(GraphQLString)
		},
		debateTitle : {
			type : new GraphQLNonNull(GraphQLString)
		}
	},
	outputFields: {
		success : {
			type : GraphQLString,
			resolve :  () => {
				return 'Success';
			}
		}
	},
	mutateAndGetPayload:  ({inviteeList, debateTitle, debateId},{db, firstName, lastName}) => {
		console.log(firstName);
		const inviteeIdList = inviteeList.map((id) => fromGlobalId(id).id);
		if(inviteeIdList){
			inviteeIdList.forEach((inviteeId) => {
				sendInvite({inviteeId, firstName, lastName, debateId, debateTitle , db });
			});
		}
		return {};
	}
});
