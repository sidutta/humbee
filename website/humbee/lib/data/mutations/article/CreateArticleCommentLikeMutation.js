import {
	GraphQLNonNull,
	GraphQLBoolean,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';


import ArticleCommentType from '../../type/article/ArticleCommentType';

export default mutationWithClientMutationId({
	name: 'CreateArticleCommentLike',
	inputFields: {
		articleCommentId : {
			type : new GraphQLNonNull(GraphQLString)
		},
		liked : {
			type : GraphQLBoolean
		}
	},
	outputFields: {
		articleComment : {
			type : ArticleCommentType,
			resolve : ({id, userId}, args, {db}) => db.ArticleComment.findOne({
				where : {
					id : id
				},
				attributes: [
		  		"id",
		  		[db.sequelize.literal('(select count(*) from "ArticleCommentLikes" where "ArticleCommentId" = "ArticleComment"."id")'), 'numberOfLikes'],
		  		[db.sequelize.literal('(select count(*) from "ArticleCommentLikes" where "ArticleCommentId" = "ArticleComment"."id" and "UserId"='+userId+')'), 'liked'],
			  ], 
			})
		}
	},
	mutateAndGetPayload: async	({articleCommentId, liked},{db, userId}) => {
		const { id } = fromGlobalId(articleCommentId);
		let like = await db.ArticleCommentLike.findOne({
			where : {
				UserId : userId,
				ArticleCommentId : id
			}
		});
		if(like){
			await db.ArticleCommentLike.destroy({
				where : {
					UserId : userId,
					ArticleCommentId : id
				}
			});
		} else if(!like){
			await db.ArticleCommentLike.create({
				UserId : userId,
				ArticleCommentId : id
			});
		}
		return { id : id, userId : userId };
	}
});
