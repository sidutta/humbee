import {
	GraphQLNonNull,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';


import ArticleType from '../../type/article/ArticleType';


import { GraphQLError } from 'graphql/error';

export default mutationWithClientMutationId({
	name: 'CreateArticleComment',
	inputFields: {
		comment: {
			type: new GraphQLNonNull(GraphQLString)
		},
		articleId : {
			type : new GraphQLNonNull(GraphQLString)
		}
	},
	outputFields: {
		article : {
			type : ArticleType,
			resolve : ({id},args,{db}) => db.Article.findOne({
				where : {
					id : id
				},
				attributes: { 
          include: [
            [db.sequelize.literal('(select count(*) from "ArticleComments" where "ArticleId" = "Article"."id")'), 'numberOfComments']
          ] 
        }
			})
		}
	},
	mutateAndGetPayload: 	async ({comment, articleId},{db, userId}) => {
		const { id } = fromGlobalId(articleId);
		if(!comment){
			throw new GraphQLError('Comment cannot be empty');
		}
		await db.ArticleComment.create({
			text : comment,
			UserId : userId,
			ArticleId : id
		});
		return {id};
	}
});
