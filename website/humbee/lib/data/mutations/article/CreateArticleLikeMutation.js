import {
	GraphQLNonNull,
	GraphQLBoolean,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';


import ArticleType from '../../type/article/ArticleType';


export default mutationWithClientMutationId({
	name: 'CreateArticleLike',
	inputFields: {
		articleId : {
			type : new GraphQLNonNull(GraphQLString)
		},
		liked : {
			type : GraphQLBoolean
		}
	},
	outputFields: {
		article : {
			type : ArticleType,
			resolve : ({id, userId},args,{db}) => db.Article.findOne({
				attributes: [
					"id",
          [db.sequelize.literal('(select count(*) from "ArticleLikes" where "ArticleId" = "Article"."id")'), 'numberOfLikes'],
          [db.sequelize.literal('(select count(*) from "ArticleLikes" where "ArticleId" = "Article"."id" and "UserId"='+userId+')'), 'liked'],
        ],
				where : {
					id : id
				}
			})
		}
	},
	mutateAndGetPayload: async	({articleId, liked},{db, userId}) => {
		const { id } = fromGlobalId(articleId);
		let like = await db.ArticleLike.findOne({
			where : {
				UserId : userId,
				ArticleId : id
			}
		});
		if(like) {
			await db.ArticleLike.destroy({
				where : {
					UserId : userId,
					ArticleId : id
				}
			});
		} else if(!like){
			await db.ArticleLike.create({
				UserId : userId,
				ArticleId : id
			});
		}
		return { id : id, userId : userId };
	}
});
