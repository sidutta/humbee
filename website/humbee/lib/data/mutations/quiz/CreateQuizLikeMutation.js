import {
	GraphQLNonNull,
	GraphQLBoolean,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';

import QuizType from '../../type/quiz/QuizType';

export default mutationWithClientMutationId({
	name: 'CreateQuizLike',
	inputFields: {
		quizId : {
			type : new GraphQLNonNull(GraphQLString)
		},
		liked : {
			type : GraphQLBoolean
		}
	},
	outputFields: {
		quiz : {
			type : QuizType,
			resolve : ({id},args,{db, userId}) => db.Quiz.findOne({
				where : {
					id : id
				},
				attributes: [
					"id",
          [db.sequelize.literal('(select count(*) from "QuizLikes" where "QuizId" = "Quiz"."id")'), 'numberOfLikes'],
          [db.sequelize.literal('(select count(*) from "QuizLikes" where "QuizId" = "Quiz"."id" and "UserId"='+userId+')'), 'liked'],
        ],
			})
		}
	},
	mutateAndGetPayload: async	({quizId, liked},{db, userId}) => {
		const { id } = fromGlobalId(quizId);
		let like = await db.QuizLike.findOne({
			where : {
				UserId : userId,
				QuizId : id
			}
		});
		if(like){
			await db.QuizLike.destroy({
				where : {
					UserId : userId,
					QuizId : id
				}
			});
		} 
		else if(!like){
			await db.QuizLike.create({
				UserId : userId,
				QuizId : id
			});
		}
		return { id : id };
	}
});
