import {
	GraphQLNonNull,
	GraphQLBoolean,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';


import QuizCommentType from '../../type/quiz/QuizCommentType';



export default mutationWithClientMutationId({
	name: 'CreateQuizCommentLike',
	inputFields: {
		quizCommentId : {
			type : new GraphQLNonNull(GraphQLString)
		},
		liked : {
			type : GraphQLBoolean
		}
	},
	outputFields: {
		quizComment : {
			type : QuizCommentType,
			resolve : ({id, userId}, args, {db}) => db.QuizComment.findOne({
				where : {
					id : id
				},
				attributes: [
		  		"id",
		  		[db.sequelize.literal('(select count(*) from "QuizCommentLikes" where "QuizCommentId" = "QuizComment"."id")'), 'numberOfLikes'],
		  		[db.sequelize.literal('(select count(*) from "QuizCommentLikes" where "QuizCommentId" = "QuizComment"."id" and "UserId"='+userId+')'), 'liked'],
			  ], 
			})
		}
	},
	mutateAndGetPayload: async	({quizCommentId, liked},{db, userId}) => {
		const { id } = fromGlobalId(quizCommentId);
		let like = await db.QuizCommentLike.findOne({
			where : {
				UserId : userId,
				QuizCommentId : id
			}
		});
		if(like){
			await db.QuizCommentLike.destroy({
				where : {
					UserId : userId,
					QuizCommentId : id
				}
			});
		} 
		else if(!like){
			await db.QuizCommentLike.create({
				UserId : userId,
				QuizCommentId : id
			});
		}
		return { id : id, userId : userId };
	}
});
