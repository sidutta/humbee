import {
	GraphQLNonNull,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';


import QuizType from '../../type/quiz/QuizType';


import { GraphQLError } from 'graphql/error';

export default mutationWithClientMutationId({
	name: 'CreateQuizComment',
	inputFields: {
		comment: {
			type: new GraphQLNonNull(GraphQLString)
		},
		quizId : {
			type : new GraphQLNonNull(GraphQLString)
		}
	},
	outputFields: {
		quiz : {
			type : QuizType,
			resolve : ({id},args,{db}) => db.Quiz.findOne({
				where : {
					id : id
				},
				attributes: { 
          include: [
            [db.sequelize.literal('(select count(*) from "QuizComments" where "QuizId" = "Quiz"."id")'), 'numberOfComments']
          ] 
        }
			})
		}
	},
	mutateAndGetPayload: 	async ({comment, quizId},{db, userId}) => {
		const { id } = fromGlobalId(quizId);
		if(!comment){
			throw new GraphQLError('Comment cannot be empty');
		}
		await db.QuizComment.create({
			text : comment,
			UserId : userId,
			QuizId : id
		});
		return {id};
	}
});
