import {
	GraphQLNonNull,
	GraphQLBoolean,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';


import PictoralPostType from '../../type/pictoralPost/PictoralPostType';


export default mutationWithClientMutationId({
	name: 'CreatePictoralPostLike',
	inputFields: {
		pictoralPostId : {
			type : new GraphQLNonNull(GraphQLString)
		},
		liked : {
			type : GraphQLBoolean
		}
	},
	outputFields: {
		pictoralPost : {
			type : PictoralPostType,
			resolve : ({id, userId},args,{db}) => db.PictoralPost.findOne({
				attributes: [
					"id",
          [db.sequelize.literal('(select count(*) from "PictoralPostLikes" where "PictoralPostId" = "PictoralPost"."id")'), 'numberOfLikes'],
          [db.sequelize.literal('(select count(*) from "PictoralPostLikes" where "PictoralPostId" = "PictoralPost"."id" and "UserId"='+userId+')'), 'liked'],
        ],
				where : {
					id : id
				}
			})
		}
	},
	mutateAndGetPayload: async	({pictoralPostId, liked},{db, userId}) => {
		const { id } = fromGlobalId(pictoralPostId);
		let like = await db.PictoralPostLike.findOne({
			where : {
				UserId : userId,
				PictoralPostId : id
			}
		});
		if(like) {
			await db.PictoralPostLike.destroy({
				where : {
					UserId : userId,
					PictoralPostId : id
				}
			});
		} else if(!like){
			await db.PictoralPostLike.create({
				UserId : userId,
				PictoralPostId : id
			});
		}
		return { id : id, userId : userId };
	}
});
