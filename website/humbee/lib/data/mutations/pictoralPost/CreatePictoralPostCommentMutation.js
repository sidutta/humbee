import {
	GraphQLNonNull,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';


import PictoralPostType from '../../type/pictoralPost/PictoralPostType';


import { GraphQLError } from 'graphql/error';

export default mutationWithClientMutationId({
	name: 'CreatePictoralPostComment',
	inputFields: {
		comment: {
			type: new GraphQLNonNull(GraphQLString)
		},
		pictoralPostId : {
			type : new GraphQLNonNull(GraphQLString)
		}
	},
	outputFields: {
		pictoralPost : {
			type : PictoralPostType,
			resolve : ({id},args,{db}) => db.PictoralPost.findOne({
				where : {
					id : id
				},
				attributes: { 
          include: [
            [db.sequelize.literal('(select count(*) from "PictoralPostComments" where "PictoralPostId" = "PictoralPost"."id")'), 'numberOfComments']
          ] 
        }
			})
		}
	},
	mutateAndGetPayload: 	async ({comment, pictoralPostId},{db, userId}) => {
		const { id } = fromGlobalId(pictoralPostId);
		if(!comment){
			throw new GraphQLError('Comment cannot be empty');
		}
		await db.PictoralPostComment.create({
			text : comment,
			UserId : userId,
			PictoralPostId : id
		});
		return {id};
	}
});
