import {
	GraphQLNonNull,
	GraphQLBoolean,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';


import PictoralPostCommentType from '../../type/pictoralPost/PictoralPostCommentType';

export default mutationWithClientMutationId({
	name: 'CreatePictoralPostCommentLike',
	inputFields: {
		pictoralPostCommentId : {
			type : new GraphQLNonNull(GraphQLString)
		},
		liked : {
			type : GraphQLBoolean
		}
	},
	outputFields: {
		pictoralPostComment : {
			type : PictoralPostCommentType,
			resolve : ({id, userId}, args, {db}) => db.PictoralPostComment.findOne({
				where : {
					id : id
				},
				attributes: [
		  		"id",
		  		[db.sequelize.literal('(select count(*) from "PictoralPostCommentLikes" where "PictoralPostCommentId" = "PictoralPostComment"."id")'), 'numberOfLikes'],
		  		[db.sequelize.literal('(select count(*) from "PictoralPostCommentLikes" where "PictoralPostCommentId" = "PictoralPostComment"."id" and "UserId"='+userId+')'), 'liked'],
			  ], 
			})
		}
	},
	mutateAndGetPayload: async	({pictoralPostCommentId, liked},{db, userId}) => {
		const { id } = fromGlobalId(pictoralPostCommentId);
		let like = await db.PictoralPostCommentLike.findOne({
			where : {
				UserId : userId,
				PictoralPostCommentId : id
			}
		});
		if(like){
			await db.PictoralPostCommentLike.destroy({
				where : {
					UserId : userId,
					PictoralPostCommentId : id
				}
			});
		} else if(!like){
			await db.PictoralPostCommentLike.create({
				UserId : userId,
				PictoralPostCommentId : id
			});
		}
		return { id : id, userId : userId };
	}
});
