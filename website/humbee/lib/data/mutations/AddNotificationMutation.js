import {
	GraphQLString,
	GraphQLNonNull
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';

import NotificationType from '../type/NotificationType';

import ViewerType from '../type/ViewerType';

import { NotificationEdge } from '../type/NotificationType';

import { cursorForObjectInConnection } from 'graphql-relay';

import notifyChange from '../../utils/subscription/notifyChange';

export default mutationWithClientMutationId({
	name: 'AddNotification',
	inputFields: {
		userId: {
			type: new GraphQLNonNull(GraphQLString)
		}
	},
	outputFields: {
		success : {
			type : GraphQLString,
			resolve :  () => {
				return 'Success';
			}
		}
	},
	mutateAndGetPayload: async ({userId},{db}) => {
		let notification = await db.Notification.create({
			text : 'random text',
			entityType : 'some type',
			entityId  : 'some ID',
			UserId : 23
		});

		const { id } = fromGlobalId(userId)

		let notifications = await db.Notification.findAll();
		let newNotification = notifications.find(obj => (obj.id == notification.id));
		let user = await db.User.findByPrimary(id);
		notifyChange('add_notification', {notification : newNotification, notifications, user});
		return { notification : newNotification, notifications};
	}
});
