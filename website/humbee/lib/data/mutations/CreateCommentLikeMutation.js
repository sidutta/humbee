import {
	GraphQLNonNull,
	GraphQLBoolean,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';

import updateScore from '../../utils/updateScore';

import CommentType from '../type/CommentType';


import { GraphQLError } from 'graphql/error';

export default mutationWithClientMutationId({
	name: 'CreateCommentLike',
	inputFields: {
		commentId : {
			type : new GraphQLNonNull(GraphQLString)
		},
		liked : {
			type : GraphQLBoolean
		}
	},
	outputFields: {
		comment : {
			type : CommentType,
			resolve : ({id}, args, {db, userId}) => db.Comment.findOne({
				where : {
					id : id
				},
				attributes: [
		  		"id",
		  		[db.sequelize.literal('(select count(*) from "CommentLikes" where "CommentId" = "Comment"."id")'), 'numberOfLikes'],
		  		[db.sequelize.literal('(select count(*) from "CommentLikes" where "CommentId" = "Comment"."id" and "UserId"='+userId+')'), 'liked'],
			  ], 
			})
		}
	},
	mutateAndGetPayload: async	(data,{db, userId}) => {
		const { commentId, liked } = data;
		const { id } = fromGlobalId(commentId);
		let like = await db.CommentLike.findOne({
			where : {
				UserId : userId,
				CommentId : id
			}
		});
		let comment = await db.Comment.findByPrimary(id);
		if(like){
			await db.CommentLike.destroy({
				where : {
					UserId : userId,
					CommentId : id
				}
			});
			updateScore({userId : comment.UserId, db, points : -3});
			return { id : id };
		} 
		else if(!like){
			await db.CommentLike.create({
				UserId : userId,
				CommentId : id
			});
			updateScore({userId : comment.UserId, db, points : 3});
			return {id : id};
		}

	}
});
