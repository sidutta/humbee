import {GraphQLNonNull, GraphQLString} from 'graphql';
import {mutationWithClientMutationId} from 'graphql-relay';
import UserType from '../type/UserType';

import FbLogin from '../../utils/FbLogin';

import jwt from 'jsonwebtoken';

import {jwtSecret, FB} from '../../config';

import { GraphQLError } from 'graphql/error';

import randomstring from 'randomstring';

import bcrypt from 'bcrypt';

export default mutationWithClientMutationId({
	name: 'FbLogin',
	inputFields: {
		accessToken: {
			type: new GraphQLNonNull(GraphQLString)
		},
		userID : {
			type: new GraphQLNonNull(GraphQLString)
		}
	},
	outputFields: {
		token: {
			type: GraphQLString,
			resolve: payload => {
				 if(payload.token){
					 return payload.token;
				 }
			}
		},
		id : {
			type : GraphQLString,
			resolve : payload => {
				if(payload.dataValues){
					return payload.dataValues.id;
				}
			}
		}
	},
	mutateAndGetPayload: async	(data,{db}) => {
		const { accessToken, userID } = data;
		let response;
		let token;
		try{
			response = await	FbLogin(data);
		} catch(error){
			console.log(error);
			throw new GraphQLError('Oops! Something went wrong!! Please Try again');
		}

		let user = await db.User.findOne({where : {email : response.email}});

		if(user){
			const { id, username, role, firstName, lastName, email, profilePic } = user.dataValues;
			token = jwt.sign({
				id : id,
				username : username,
				role : role,
				firstName : firstName,
				lastName : lastName,
				email : email,
				profilePic : profilePic
			},jwtSecret);
			return {token : token};
		} else {
			const { first_name, last_name, email } = response;
			const profilePic = 'https://graph.facebook.com/' + userID + '/picture?type=large';
			const hashedPassword = bcrypt.hashSync(randomstring.generate(12), 10);
			return db.User.create({
				firstName: first_name,
				lastName: last_name,
				email: email.toLowerCase(),
				password : hashedPassword,
				role : 'User',
				profilePic : profilePic,
				SocialProfiles : [{
					provider : 'Facebook',
					accessToken : accessToken,
					userID : userID
				}]
			}, {
				include : db.SocialProfile
			});
		}
	}
});
