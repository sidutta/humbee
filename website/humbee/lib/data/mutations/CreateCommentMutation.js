import {
	GraphQLNonNull,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';


import ReasonType from '../type/ReasonType';


import { GraphQLError } from 'graphql/error';

export default mutationWithClientMutationId({
	name: 'CreateComment',
	inputFields: {
		comment: {
			type: new GraphQLNonNull(GraphQLString)
		},
		reasonId : {
			type : new GraphQLNonNull(GraphQLString)
		}
	},
	outputFields: {
		reason : {
			type : ReasonType,
			resolve : async ({id},args,{db}) => {
				let reason = await db.Reason.findOne({
					where : {
						id : id
					},
					attributes: { 
	          include: [
	            [db.sequelize.literal('(select count(*) from "Comments" where "ReasonId" = "Reason"."id")'), 'numberOfComments']
	          ] 
	        }
				});

				return reason;
			}
		}
	},
	mutateAndGetPayload: 	async (data,{db, userId}) => {
		const { comment, reasonId } = data;
		const { id } = fromGlobalId(reasonId);

		if(!comment){
			throw new GraphQLError('Cannot be empty');
		}
		await db.Comment.create({
			text : comment,
			UserId : userId,
			ReasonId : id,
      beAnonymous: false
		});
		return { id : id };
	}
});
