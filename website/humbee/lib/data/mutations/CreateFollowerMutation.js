import {
	GraphQLNonNull,
	GraphQLString,
	GraphQLBoolean
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';


import UserType from '../type/UserType';


import { GraphQLError } from 'graphql/error';

export default mutationWithClientMutationId({
	name: 'CreateFollower',
	inputFields: {
		followingId : {
			type : new GraphQLNonNull(GraphQLString)
		},
		isFollowing : {
			type : new GraphQLNonNull(GraphQLBoolean)
		}
	},
	outputFields: {
		user : {
			type : UserType,
			resolve :  (followed) => {
				return followed;
			}
		}
	},
	mutateAndGetPayload: async (data,{db, userId}) => {
		const { followingId } = data;
		const { id } = fromGlobalId(followingId);
		let followed = await db.User.findByPrimary(id);
		let isFollowing = await db.Following.count({ where: { FollowedId: id, FollowerId: userId } });
		if(isFollowing){
			await followed.removeFollower(userId);
		} else {
			await followed.addFollower(userId);
		}
		return followed;
	}
});
