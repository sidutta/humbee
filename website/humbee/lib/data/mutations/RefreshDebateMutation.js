import {
	GraphQLNonNull,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';

import DebateType from '../type/DebateType';


export default mutationWithClientMutationId({
	name: 'RefreshDebate',
	inputFields: {
		debateId : {
			type : new GraphQLNonNull(GraphQLString)
		}
	},
	outputFields: {
		debate : {
			type : DebateType,
			resolve : ({debate}) => {
				return debate;
			}
		}
	},
	mutateAndGetPayload: 	(data,{db}) => {
		const {debateId} = data;
		const {id} = fromGlobalId(debateId);
		return {debate : db.Debate.findByPrimary(id)};
	}
});
