import {
	GraphQLNonNull,
	GraphQLBoolean,
	GraphQLString
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';


import DebateType from '../../type/DebateType';


export default mutationWithClientMutationId({
	name: 'CreateDebateLike',
	inputFields: {
		debateId : {
			type : new GraphQLNonNull(GraphQLString)
		},
		liked : {
			type : GraphQLBoolean
		}
	},
	outputFields: {
		debate : {
			type : DebateType,
			resolve : ({id, userId},args,{db}) => db.Debate.findOne({
				attributes: [
					"id",
          [db.sequelize.literal('(select count(*) from "DebateLikes" where "DebateId" = "Debate"."id")'), 'numberOfLikes'],
          [db.sequelize.literal('(select count(*) from "DebateLikes" where "DebateId" = "Debate"."id" and "UserId"='+userId+')'), 'liked'],
        ],
				where : {
					id : id
				}
			})
		}
	},
	mutateAndGetPayload: async	({debateId, liked},{db, userId}) => {
		const { id } = fromGlobalId(debateId);
		let like = await db.DebateLike.findOne({
			where : {
				UserId : userId,
				DebateId : id
			}
		});
		if(like) {
			await db.DebateLike.destroy({
				where : {
					UserId : userId,
					DebateId : id
				}
			});
		} else if(!like){
			await db.DebateLike.create({
				UserId : userId,
				DebateId : id
			});
		}
		return { id : id, userId : userId };
	}
});
