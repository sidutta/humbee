import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLBoolean
} from 'graphql';

import {
  mutationWithClientMutationId,
  fromGlobalId
} from 'graphql-relay';

import QuizType from '../type/quiz/QuizType';

import { GraphQLError } from 'graphql/error';

export default mutationWithClientMutationId({
  name: 'SetQuizComplete',
  inputFields: {
    completed: {
      type: GraphQLBoolean
    },
    quizId : {
      type : new GraphQLNonNull(GraphQLString)
    }
  },
  outputFields: {
    quiz : {
      type : QuizType,
      resolve :  (quiz) => quiz
    }
  },
  mutateAndGetPayload:  async (data,{db, userId}) => {
    const { quizId } = data;
    const { id } = fromGlobalId(quizId);
    let quiz = await db.Quiz.findByPrimary(id);
    quiz.dataValues.completed = true;
    return quiz;
  }
});
