import {GraphQLNonNull, GraphQLBoolean, GraphQLString, GraphQLInt} from 'graphql';
import { mutationWithClientMutationId } from 'graphql-relay';
import { GraphQLError } from 'graphql/error';

export default mutationWithClientMutationId({
	name: 'VerificationCodeCheck',
	inputFields: {
		identifier : {
			type: new GraphQLNonNull(GraphQLString)
		}, claimedCode : {
			type: new GraphQLNonNull(GraphQLInt)
		}
	},
	outputFields: {
		verificationResult: {
			type: GraphQLBoolean,
			resolve: payload => {
				if(payload.verificationResult) {
					return payload.verificationResult;
				}
			}
		}
	},
	mutateAndGetPayload: async (data,{db}) => {
		const { identifier, claimedCode } = data;
		let user = await db.User.find({
			where: {
				$or : {
      		email : identifier,
      		username : identifier
    		}
			}
		});

		if(user) {
			const { id, email } = user.dataValues;
			// Currently id == UserId.
			// Change to allow multiple verification-codes to be valid (not only the latest one).
			let existingVerificationCode = await db.VerificationCode.find({
				where : {
					id : id
				}
			});

			if(existingVerificationCode) {
				const {code, updatedAt} = existingVerificationCode.dataValues;

				if(code == claimedCode) {
					// Verification-codes older than 5 minutes are not valid.
	        if(((new Date().getTime()) - Date.parse(updatedAt)) > 300000) {
	          throw new GraphQLError('Verification-code expired.');
	        }
					return {verificationResult : true};
				} else {
					throw new GraphQLError('Verification-code is either wrong or not the latest one.');
				}
			} else {
				throw new GraphQLError('Verification code for given identifier doesn\'t exist.');
			}
		} else {
			throw new GraphQLError('Invalid identifier.');
		}
	}
});
