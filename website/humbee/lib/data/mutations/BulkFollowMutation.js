import {
	GraphQLString,
	GraphQLList,
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';



export default mutationWithClientMutationId({
	name: 'BulkFollow',
	inputFields: {
		followingList : {
			type : new GraphQLList(GraphQLString)
		}
	},
	outputFields: {
		success : {
			type : GraphQLString,
			resolve :  () => {
				return 'Success';
			}
		}
	},
	mutateAndGetPayload: async ({followingList},{db, userId}) => {
		const followingIdList = followingList.map((userId) =>{
			let { id } = fromGlobalId(userId);
			return id;
		});
		const follower = await db.User.findByPrimary(userId);
		if(followingIdList){
			await follower.addFolloweds(followingIdList);
		}
		return follower;
	}
});
