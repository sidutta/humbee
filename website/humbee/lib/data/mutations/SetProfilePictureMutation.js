import {GraphQLNonNull, GraphQLString, GraphQLInt} from 'graphql';
import {mutationWithClientMutationId} from 'graphql-relay';
import jwt from 'jsonwebtoken';
import config from '../../config';

import { GraphQLError } from 'graphql/error';
import imageUploader from '../../utils/imageUploader';

export default mutationWithClientMutationId({
	name: 'SetProfilePicture',
	inputFields: {
		image : {
			type: new GraphQLNonNull(GraphQLString)
		},
		id : {
			type: new GraphQLNonNull(GraphQLInt)
		},

	},
	outputFields: {
		token: {
			type: GraphQLString,
			resolve: ({token}) => {
				return token;
			}
		}
	},
	mutateAndGetPayload: async (data,{db}) => {
		const { image, id } = data;
		let user = await db.User.findOne({ where: { id: id }});
		let profilePic;
		if(image == 'skip'){
			profilePic = 'https://storage.googleapis.com/humbee_images/cartoon-bee-1.jpg';
		}else if(user.dataValues){
			try{
				profilePic =  await imageUploader({data_uri : image, filename : user.dataValues.username , filetype : 'image/jpeg' });
			} catch(error){
				throw new GraphQLError({form : 'Oops! Something went wrong!! Please Try again'});
			}
		} else {
			throw new GraphQLError('Invalid user');
		}
		if(profilePic){
			user =  await user.updateAttributes({profilePic : profilePic});
			const { id, username, role, firstName, lastName, email} = user.dataValues;
			const token = jwt.sign({
				id : id,
				username : username,
				role : role,
				firstName : firstName,
				lastName : lastName,
				email : email,
				profilePic : profilePic
			}, config.jwtSecret);
			return {token : token};
		} else {
			throw new GraphQLError('Invalid Credentials');
		}
	}
});
