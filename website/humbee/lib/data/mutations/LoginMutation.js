import {GraphQLNonNull, GraphQLString} from 'graphql';
import {mutationWithClientMutationId} from 'graphql-relay';
import UserType from '../type/UserType';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import config from '../../config';

import { GraphQLError } from 'graphql/error';
import signupValidations from '../validations/signup';

export default mutationWithClientMutationId({
	name: 'Login',
	inputFields: {
    identifier: {
      type: new GraphQLNonNull(GraphQLString)
    },
    inputPassword:{
      type: new GraphQLNonNull(GraphQLString)
    }
	},
	outputFields: {
		token: {
			type: GraphQLString,
			resolve: payload => {
				return payload.token;
			}
		}
	},
	mutateAndGetPayload: async (data,{db}) => {
    const { identifier, inputPassword } = data;
    let user = await db.User.find({where: {$or : {
      email : identifier,
      username : identifier
    }}});

    if(user){
      const { password, id, username, role, firstName, lastName, email, profilePic } = user.dataValues;
      if(bcrypt.compareSync(inputPassword, password)){

				const token = jwt.sign({
						id : id,
						username : username,
						role : role,
						firstName : firstName,
						lastName : lastName,
						email : email,
						profilePic : profilePic
					}, config.jwtSecret);

        return {token : token};

      } else {
        throw new GraphQLError('Invalid Credentials');
      }
    } else {
      throw new GraphQLError('Invalid Credentials');
    }
	}
});
