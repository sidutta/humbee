import {GraphQLNonNull, GraphQLString, GraphQLInt} from 'graphql';
import { mutationWithClientMutationId } from 'graphql-relay';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import config from '../../config';

import { GraphQLError } from 'graphql/error';
import signupValidations from '../validations/signup';

export default mutationWithClientMutationId({
	name: 'Signup',
	inputFields: {
		email: {
			type: new GraphQLNonNull(GraphQLString)
		},
		password: {
			type: new GraphQLNonNull(GraphQLString)
		},
		firstName: {
			type: new GraphQLNonNull(GraphQLString)
		},
		lastName: {
			type: GraphQLString
		},
		username:{
			type: new GraphQLNonNull(GraphQLString)
		},
    client: {
      type: GraphQLString
    }
	},
	outputFields: {
		id: {
      type: GraphQLInt,
      resolve: (user) => {
        return user.dataValues.id;
      }
    }
	},
	mutateAndGetPayload: async (data,{db, res, req, next}) => {
		const { email, password, firstName, lastName, username, client } = data;
		const { errors, isValid } = await signupValidations({data, db});

		if(!isValid){
			throw new GraphQLError(errors);
		}

    const role = 'User';
    let emailInLowerCase = email.toLowerCase();

		const hashedPassword = bcrypt.hashSync(password, 10);
		return db.User.create({
			firstName: firstName,
			lastName: lastName,
			email: emailInLowerCase,
			password : hashedPassword,
			username : username,
			role : role
		})
    .then( (user) => {
      const token = jwt.sign({
        id : user.dataValues.id,
        username : username,
        role : role,
        firstName : firstName,
        lastName : lastName,
        email : emailInLowerCase,
      }, config.jwtSecret);
      res.header('Access-Control-Allow-Credentials', true);
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
      res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
      res.cookie( 'accessToken', token, { maxAge: 86400000, path: '/', httpOnly: true, secure: false } );
      // res.json( { success : true } );
      return user;
    });
	}
});
