import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLBoolean,
  GraphQLList
} from 'graphql';

import isEmpty from 'lodash/isEmpty';
import updateScore from '../../utils/updateScore';

import {
  mutationWithClientMutationId,
  fromGlobalId,
  cursorForObjectInConnection,
  connectionFromArray,
  connectionArgs
} from 'graphql-relay';

import {
  ReasonsEdge
} from '../type/ConnectionType';

import DebateType from '../type/DebateType';

import { GraphQLError } from 'graphql/error';

import Promise from 'bluebird';

export default mutationWithClientMutationId({
  name: 'DeleteReason',
  inputFields: {
    reasonId: {
      type: new GraphQLNonNull(GraphQLString)
    },
    debateId: {
      type: new GraphQLNonNull(GraphQLString)
    }
  },
  outputFields: {
    debate : {
      type : DebateType,
      resolve : ({debateIndex},args,{db}) => {
        return db.Debate.findByPrimary(debateIndex);
      }
    },
    reasonId : {
      type : new GraphQLNonNull(GraphQLString),
      resolve : async ({reasonId}, args, {db}) => {
        return reasonId;
      }
    }
  },
  mutateAndGetPayload: async  (data, {db, userId, isUser}) => {
    const { reasonId, debateId } = data;
    const reasonIndex = fromGlobalId(reasonId).id;
    const debateIndex = fromGlobalId(debateId).id;

    await db.Reason.find( {
      where: {
        id: reasonIndex
      }
    } )
    .then( async (reason) => {
      await reason.update(
        {
          active: false
        }
      );
    })

    return {reasonId: reasonId, debateIndex: debateIndex};
  }
});
