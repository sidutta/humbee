import {GraphQLNonNull, GraphQLString, GraphQLBoolean} from 'graphql';
import { mutationWithClientMutationId } from 'graphql-relay';
import { GraphQLError } from 'graphql/error';

import bcrypt from 'bcrypt';

import UserType from '../type/UserType';

import sendEmail from '../../common/email/email';

export default mutationWithClientMutationId({
	name: 'PasswordReset',
	inputFields: {
		identifier : {
			type: new GraphQLNonNull(GraphQLString)
		},
		password : {
			type: new GraphQLNonNull(GraphQLString)
		}
	},
	outputFields: {
		passwordResetResult : {
			type: GraphQLBoolean,
			resolve: payload => {
				if(payload.passwordResetResult) {
					return payload.passwordResetResult;
				}
			}
		}
	},
	mutateAndGetPayload: async (data,{db}) => {
		const { identifier, password } = data;

		// TODO have a check that password doesn't match any of the previous ones.
		const hashedPassword = bcrypt.hashSync(password, 10);
		let userUpdationResult = await db.User.update({
				password : hashedPassword
			},
			{
			where: {
				$or : {
      		email : identifier,
      		username : identifier
    		}
			},
			returning : true
		});

    /*
     * Sending email is not necessary in forget-password case.
     * But this mutation can be used for casual password change as well
     * where sending email is necessary.
     */
    if(userUpdationResult[0] == 1) {
      const {email} = userUpdationResult[1][0].dataValues;
			let emailSubject = "Password changed";
			let htmlContent = "<p>Password for your account has been successfully changed.</p>";
      sendEmail(email, emailSubject, htmlContent);
  		return {passwordResetResult : true};
    } else {
        throw new GraphQLError('Oops something went wrong!');
    }
	}
});
