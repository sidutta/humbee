import {GraphQLNonNull, GraphQLString} from 'graphql';
import { mutationWithClientMutationId } from 'graphql-relay';
import { GraphQLError } from 'graphql/error';

import UserType from '../type/UserType';
import VerificationCodeType from '../type/VerificationCodeType';

import sendEmail from '../../common/email/email';

export default mutationWithClientMutationId({
	name: 'VerificationCode',
	inputFields: {
		identifier : {
			type: new GraphQLNonNull(GraphQLString)
		}
	},
	outputFields: {
		maskedEmail: {
			type: GraphQLString,
			resolve: payload => {
				if(payload.maskedEmail) {
					return payload.maskedEmail;
				}
			}
		}
	},
	mutateAndGetPayload: async (data,{db}) => {
		const { identifier } = data;
		let user = await db.User.find({
			where: {
				$or : {
      		email : identifier,
      		username : identifier
    		}
			}
		});

		if(user) {
			const { id, email } = user.dataValues;

      /*
       * TODO
			 * Is it safe to save verification-code as it is in database?
			 * http://security.stackexchange.com/questions/52800/why-gmails-phone-verification-code-not-encrypted
      */

      // min and max to define n-digit code.
			var min = 10000000;
			var max = 99999999;
			var randomCode = Math.floor(Math.random() * (max - min + 1)) + min;

      // Generate only 1 verification-code per 5 seconds for a given user to prevent un-necessary mails.
      let existingVerificationCode = await db.VerificationCode.find({
        where : {
					id : id
				}
      });

      if(existingVerificationCode) {
        const {updatedAt} = existingVerificationCode.dataValues;
        if(((new Date().getTime()) - Date.parse(updatedAt)) < 5000) {
          throw new GraphQLError('Can\'t re-generate verification-code within 5 seconds.');
        }
      }

      // This invalidates previous verification-code.
			let successfulUpdation = db.VerificationCode.upsert({
					id : id,
					code : randomCode,
					UserId : id
			}, {
				where : {
					id : id
				}
			});

			let emailSubject = "Verification code";
			let htmlContent =  "<p>Verification code for you request is : <b>" + randomCode + "</b></p>";
			sendEmail(email, emailSubject, htmlContent);

      let indexOfAtTheRate = email.indexOf('@');
      return { maskedEmail : (email.charAt(0) + "*".repeat(indexOfAtTheRate - 2) + email.charAt(indexOfAtTheRate - 1) + email.substring(indexOfAtTheRate))};
		} else {
			throw new GraphQLError('Invalid identifier.');
		}
	}
});
