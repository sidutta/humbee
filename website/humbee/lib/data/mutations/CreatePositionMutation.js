import {
	GraphQLNonNull,
	GraphQLString,
  GraphQLBoolean
} from 'graphql';

import {
	mutationWithClientMutationId,
	fromGlobalId
} from 'graphql-relay';

import DebateType from '../type/DebateType';

import { GraphQLError } from 'graphql/error';

export default mutationWithClientMutationId({
	name: 'CreatePosition',
	inputFields: {
		agree: {
			type: new GraphQLNonNull(GraphQLBoolean)
		},
		debateId : {
			type : new GraphQLNonNull(GraphQLString)
		}
	},
	outputFields: {
		debate : {
			type : DebateType,
			resolve :  ({id},args,{db, userId}) => {
				return db.Debate.findOne({
					where : {
						id : id
					},
					attributes: [
						"id", 
						[db.sequelize.literal('(select count(*) FROM "Positions" WHERE ("DebateId" = "Debate"."id" AND "agree" = true))'), 'agree'],
            [db.sequelize.literal('(select count(*) FROM "Positions" WHERE ("DebateId" = "Debate"."id" AND "agree" = false))'), 'disagree'],
            [db.sequelize.literal('(select "agree" FROM "Positions" WHERE ("DebateId" = "Debate"."id" AND "UserId"='+userId+'))'), 'userposition']
					]
				});
			}
		}
	},
	mutateAndGetPayload: 	async (data,{db, userId}) => {
		const { agree, debateId } = data;
		const { id } = fromGlobalId(debateId);
		await db.Position.create({
			agree : agree,
			UserId : userId,
			DebateId : id
		});
		return { id : id };
	}
});
