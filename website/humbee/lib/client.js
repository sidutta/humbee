//import register from 'babel-core/register';
//import polyfill from 'babel-polyfill';

import IsomorphicRelay from 'isomorphic-relay';
import IsomorphicRouter from 'isomorphic-relay-router';
import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory, match, Router } from 'react-router';
import Relay from 'react-relay';
import routes from './routes';
import { RelayNetworkLayer, urlMiddleware } from 'react-relay-network-layer';

if(process.env.NODE_ENV !== 'production') {
  React.Perf = require('react-addons-perf');
}

const environment = new Relay.Environment();

// environment.injectNetworkLayer(new Relay.DefaultNetworkLayer('/graphql', { credentials: 'same-origin' }));
// Relay.injectNetworkLayer(new Relay.DefaultNetworkLayer('/graphql', { credentials: 'same-origin' }));

// environment.injectNetworkLayer(
//   new RelayNetworkLayer([
//     urlMiddleware({
//       url : (req) => '/ggraphql2'
//     }),

//     next => req => {
//       console.log('#---------#')
//       console.log(req.headers)
//       console.log('#---------#')
//       req.credentials = 'same-origin' // provide CORS policy to XHR request in fetch method
//       return next( req )
//     }
//   ], { disableBatchQuery : true})
// );

const data = JSON.parse(document.getElementById('preloadedData').textContent);

const nwlayer =  new RelayNetworkLayer(
  [
    urlMiddleware( {
      url: '/graphql' // GraphQL Server is relative to main server in directory graphql
    } ),
    next => req => {
      // alert('a');
      req.headers[ 'UserToken2' ] = 'dewf' // Provide token for server to prevent CSRF
      req.credentials = 'include' // provide CORS policy to XHR request in fetch method
      return next( req )
    },
    next => req => {
      return next( req )
        .then( res => {
          if( res.json.error ) {
            alert( res.json.error ) // TODO x5000 Transfer error to server, possibly
            if( res.json.error == 'Authentication Failed' ) {
              // When authentication fails, alert user and log out
              var loc = window.location
              var host = loc.protocol + "//" + loc.hostname + ":" + loc.port

              postXHR( host + '/auth/logout', {},
                () => {
                  alert( "Your account could not be found. You have been logged out." )
                  location.replace( location.href )
                },
                () => {
                  alert( "Your account could not be found. An attempt has been made to log you out, which did not succeed." )
                  location.replace( location.href )
                }
              )
            }
          } else if( res.json.errors )
            alert( 'GraphQL errors occurred! TODO: x2000 provide error handling' )
          return res
        } )
    }
  ], {
    disableBatchQuery: true
  }
)

Relay.injectNetworkLayer(nwlayer)
environment.injectNetworkLayer(nwlayer)

IsomorphicRelay.injectPreparedData(environment, data);

const rootElement = document.getElementById('root');

match({ routes, history: browserHistory }, (error, redirectLocation, renderProps) => {
  IsomorphicRouter.prepareInitialRender(environment, renderProps).then(props => {
    ReactDOM.render(<Router {...props} />, rootElement);
  });
});


