import bcrypt from 'bcrypt';
import bodyParser from 'body-parser';
import express from 'express';
import jwt from 'jsonwebtoken';

import config from './config';
import models from './data/models';
import signupValidations from './data/validations/signup';

// Read environment
// require( 'dotenv' ).load( );

let auth = express( );
auth.use( bodyParser.json() );

auth.post('/login', async (req, res, next) => {
  const identifier = req.body.email.toLowerCase( );
  const inputPassword = req.body.password;

  let user = await models.User.find({where: {$or : {
    email : identifier,
    username : identifier
  }}});

  if(user) {
    const { password, id, username, role, firstName, lastName, email, profilePic } = user.dataValues;
    if(bcrypt.compareSync(inputPassword, password)) {
      const token = jwt.sign({
        id : id,
        username : username,
        role : role,
        firstName : firstName,
        lastName : lastName,
        email : email,
      }, config.jwtSecret);

      // return {token : token};
      res.cookie( 'accessToken', token, { httpOnly: true } );
      res.json( { success : true } );
    }
    else {
      return res.json( { errors : 'Invalid Credentials', success: false } );
    }
  }
  else {
    return res.json( { errors : 'Invalid Credentials', success: false } );
  }
});

auth.post('/createuser', async (req, res, next) => {
  const email = req.body.email.toLowerCase( );
  const password = req.body.password;
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const username = req.body.username;
  const client = req.body.client;
  const role = 'User';

  const { errors, isValid } = await signupValidations({data: {email, username, password, firstName}, db: models});
  if(errors.email) {
    return res.json( { errors : errors.email, success: false } );
  }
  if(errors.username) {
    return res.json( { errors : errors.username, success: false } );
  }
  if(errors.firstName) {
    return res.json( { errors : errors.firstName, success: false } );
  }
  if(errors.password) {
    return res.json( { errors : errors.password, success: false } );
  }

  const hashedPassword = bcrypt.hashSync(password, 10);
  let user = await models.User.create({
              firstName: firstName,
              lastName: lastName,
              email: email,
              password : hashedPassword,
              username : username,
              role : role
            })

  const token = jwt.sign({
    id : user.dataValues.id,
    username : username,
    role : role,
    firstName : firstName,
    lastName : lastName,
    email : email,
  }, config.jwtSecret);
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
  res.cookie( 'accessToken', token, { maxAge: 86400000, path: '/', httpOnly: true, secure: false } );
  res.json( { success : true } );
});

auth.post('/logout', (req, res, next) =>
{
  res.cookie( 'accessToken', '', { httpOnly: true, expires: new Date( 1 ) } );
  res.json( { success : true } );
} );

export default auth;
