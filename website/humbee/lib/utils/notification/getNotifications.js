import models from '../../data/models';

export default function getNotifications(){
	return models.Notification.findAll();
}
