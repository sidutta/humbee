import googleCloud from 'google-cloud';
import credentials from './gcloud_key.json';

export default function ImageUploader(image){

	return new Promise(
		function (resolve, reject){
			const { data_uri, filename, filetype } = image;

			//define the bucket in google cloud storage
			let bucket = googleCloud({
				projectId : 'humbee-1470729034137',
				credentials : credentials
			}).storage()
				.bucket('humbee_images');

			//create a buffer from the image data sent by the user
			let buffer = new Buffer(data_uri.replace(/^data:image\/\w+;base64,/, ''),'base64');

			//set a name for the image
			let gcsname = Date.now() + filename;
			//create a file with the above name
			let file = bucket.file(gcsname);

			//set the file type
			let stream = file.createWriteStream({
				metadata: {
					contentType: filetype
				}
			});
			stream.on('error', (error) => {
				reject(error);
			});
			stream.on('finish',  () => {
				let publicUrl = 'https://storage.googleapis.com/humbee_images/' + gcsname;
				resolve(publicUrl);
			});
			stream.end(buffer);
		});
}
