export default function addNotifier(cb) {
	console.log(GLOBAL.notifiers);
	GLOBAL.notifiers.push(cb);
	return () => {
		const index = GLOBAL.notifiers.indexOf(cb);
		if (index !== -1) {
			GLOBAL.notifiers.splice(index, 1);
		}
	};
}
