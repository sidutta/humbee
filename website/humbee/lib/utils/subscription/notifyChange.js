export default function notifyChange(topic, data) {
	setTimeout(() => {
		GLOBAL.notifiers.forEach(notifier => notifier({ topic, data }));
	}, 100);
}
