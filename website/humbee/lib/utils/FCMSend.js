import FCM from 'fcm-node';

export default function FCMSend({recepient, title, body, data}){
	let serverKey = 'AAAAc37nnfU:APA91bGWY1zfSQgiHFkgBPWonrnX2IIn2oLpkf5iIpPSaccFcNCFqxEfoNmWTFts10lsvv3ZuwZD_R_Z_eVylJWeBLpcKVkUaAGxCmb37Mc995edN7CqpHjRXFuyBjkHKH70Crc2eC2F';
	let fcm = new FCM(serverKey);

	let message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
		to: recepient,
		notification: {
			title: title,
			body: body
		},
		data : data
	};

	fcm.send(message, (err, response) => {
		if (err) {
				console.log('Something went wrong!');
		} else {
				console.log('Successfully sent with response: ', response);
		}
	});

}
