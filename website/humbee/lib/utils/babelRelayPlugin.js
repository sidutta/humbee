// inside that file

var babelRelayPlugin = require('babel-relay-plugin');
var schema = require('../../data/schema.json');

module.exports = babelRelayPlugin(schema.data, {
	abortOnError: true,
});
