
export default function updateScore({points, userId, db}){
	return db.User.update({
		score : db.sequelize.literal('score +' + points)
	},{
		where : {
			id : userId
		}
	});
}
