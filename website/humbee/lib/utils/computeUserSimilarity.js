export default function computeUserSimilarity(score1, score2, maxScore) {

  /* dot product */
  // let vec1 = [];
  // let vec2 = [];
  // let sum = 0;
  // let mag1 = 0;
  // let mag2 = 0;
  // for(let key in score2) {
  //   vec2.push(score2[key]);
  //   vec1.push(score1[key]);
  //   sum = sum + (score1[key] * score2[key]);
  //   mag1 = mag1 + (score1[key] * score1[key]);
  //   mag2 = mag2 + (score2[key] * score2[key]);
  // }
  // mag1 = Math.sqrt(mag1);
  // mag2 = Math.sqrt(mag2);
  // console.log(vec1, vec2)
  // console.log(mag1, mag2)

  // if(mag1==0 || mag2==0) return 0;
  // return sum/(mag1*mag2);

  /* euclidean distance */
  // let vec1 = [];
  // let vec2 = [];
  let sum = 0;
  for(let key in score2) {
    sum = sum + ((score1[key] - score2[key]) * (score1[key] - score2[key]));
  }
  sum = Math.sqrt(sum);
  if(!maxScore) maxScore = 1;
  let dissimilarityFraction = sum/maxScore;
  return 1 - dissimilarityFraction;
  // return 1/(1+sum); // (based on http://stats.stackexchange.com/questions/53068/euclidean-distance-score-and-similarity)
}
