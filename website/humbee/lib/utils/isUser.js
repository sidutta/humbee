import jwt from 'jsonwebtoken';
import config from '../config';

export default  function isUser(accessToken, req, res) {

  if(accessToken == null && req && req.headers) {
    if(req.headers['authorization']) {
      accessToken = req.headers['authorization'].split(' ')[1];
    }
  }

  if(accessToken == null && req && req.cookies) {
    if(req.cookies.accessToken) {
      accessToken = req.cookies.accessToken;
    }
  }

  let decodedUser;
  try {
    decodedUser = jwt.verify(accessToken, config.jwtSecret);
  }
  catch(error){
    return {
      isUser : false,
      isAnonymous: true,
      error : 'Invalid access token',
      userId : -1
    };
  }

	const { role, id} = decodedUser;
	if(role == 'User' || role == 'Admin'){
		return {
			isUser : true,
      isAnonymous: true,
			error : '',
			userId : id
		};
	}
  else {
		return {
			isUser : false,
      isAnonymous: true,
			error : 'You do not have access to this page. Please contact the administrator.',
			userId : -1
		};
	}
}
