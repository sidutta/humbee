import FCMSend from './FCMSend';
import {
	fromGlobalId
} from 'graphql-relay';

export default function sendInvite({ inviteeId, firstName, lastName, debateTitle, debateId,db }){

	const { id } = fromGlobalId(debateId);

	db.Position.find({
		where : {
			UserId : inviteeId,
			DebateId : id
		}
	}).then((position) => {
		if(!position){
			db.DeviceToken.findAll({
				where : {
					UserId : inviteeId
				}
			}).then((tokens) => {
				tokens.forEach((token) => FCMSend({
					recepient : token.dataValues.token,
					title : 'Debate invitation',
					body : firstName + ' ' + lastName + ' has invited you to the debate : ' + debateTitle,
					data : {
						debateId : debateId,
						notificationType : 'DebateInvitation'
					}
				}));
			});
		}
	});
}
