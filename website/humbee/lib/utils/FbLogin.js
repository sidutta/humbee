import jwt from 'jsonwebtoken';

import {jwtSecret, FB} from '../config';

export default function FbLogin(input) {

	return new Promise(
		(resolve, reject) => {
			const { accessToken, userID } = input;
			FB.setAccessToken(accessToken);
			FB.api(userID, { fields: [ 'id', 'name', 'email', 'picture', 'first_name', 'last_name']}, (res) => {
				if(!res || res.error) {
					reject(res.error);
				}
				resolve(res);
			});
		}
	);
}

// let b = {};
// b.accessToken = "EAAXRDkgdqf0BAFvJYIf1ZBh98o70faGr2fiqQQECwU3MYD3vyrd6u4RtHVgfnRull3jN5HPJeNAjAiiHhmlQ7IpJKEWbiSklxn2DWYjLZA3SdbQIOv6IUAkxibWVwfK1PVa1XAUh9mQILJXSFkN69L3ksj2MZC35i1nCp08zITnlT49NAwwHodfJOpqDzAiWLzPfS83a5HS4fZB4L8WX"
// b.userID = "10211001080975572"
// FbLogin(b)
// .then((a)=>console.log(a))
// .catch(b=>console.log(b))