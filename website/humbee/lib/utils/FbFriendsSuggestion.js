import {FB} from '../config';

export default function FbFriendsSuggestion(input) {

	return new Promise(
		(resolve, reject) => {
			const { accessToken, userID } = input;
			FB.setAccessToken(accessToken);
			FB.api(userID, { fields: [ 'friends{id}']}, (res) => {
				if(!res || res.error) {
					reject(res.error);
				}
				resolve(res);
			});
		}
	);
}
