'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class homeRoute extends Route {
  static paramDefinitions = {
    //
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      Viewer {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'homeRoute';
}
