'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class questionRoute extends Route {
  static routeParams = {
    questionId : ''
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      node(id : $questionId) {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'questionRoute';
}
