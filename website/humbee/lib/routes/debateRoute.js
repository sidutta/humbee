'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class debateRoute extends Route {
  static routeParams = {
    debateId : ''
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      node(id : $debateId) {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'debateRoute';
}
