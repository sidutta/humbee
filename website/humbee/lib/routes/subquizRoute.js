'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class subquizRoute extends Route {
  static routeParams = {
    subquizId : ''
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      node(id : $subquizId) {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'subquizRoute';
}
