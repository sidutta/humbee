'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class reasonCardRoute extends Route {
  static routeParams = {
    reasonId : ''
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      node(id : $reasonId) {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'reasonCardRoute';
}
