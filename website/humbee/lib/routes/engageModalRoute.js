'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class engageModaRoute extends Route {
  static routeParams = {
    reasonId : ''
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      node(id : $reasonId) {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'engageModaRoute';
}
