'use strict';

import Relay, {
  Route,
} from 'react-relay';

export default class quizResultRoute extends Route {
  static routeParams = {
    quizId : '1'
  };
  static queries = {
    viewer: (Component) => Relay.QL`query {
      Viewer {
        ${Component.getFragment('viewer')}
      }
    }`
  };
  static routeName = 'quizResultRoute';
}
