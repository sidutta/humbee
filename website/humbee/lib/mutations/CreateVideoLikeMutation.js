import Relay from 'react-relay';

export default class CreateVideoLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createVideoLike }
		`;
	}

	getVariables () {
		return {
			videoId : this.props.videoId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateVideoLikePayload {
				video
			}
		`;
	}

	getOptimisticResponse(){
		let { liked } = this.props;
		return {
			video : {
				liked : !liked
			}
		};
	}
	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					video : this.props.videoId
				}
			}];
	}
}
