import RelayMutationType from 'react-relay';
import Relay from 'react-relay';

export default class SetUsernameMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { setUsername }
		`;
	}

	getVariables () {
		return {
			id: this.props.id,
			username : this.props.username
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on SetUsernamePayload {
				token
			}
		`;
	}

	getConfigs() {
	return [{
		type: 'REQUIRED_CHILDREN',
		children: [
			Relay.QL`
				fragment on SetUsernamePayload {
					token
				}
			`,
		]
	}];
}

	static fragments = {
  };
}
