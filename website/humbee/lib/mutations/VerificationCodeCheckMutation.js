import RelayMutationType from 'react-relay';
import Relay from 'react-relay';

export default class VerificationCodeCheckMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { verificationCodeCheck }
		`;
	}

	getVariables () {
		return {
			identifier: this.props.identifier,
			claimedCode : this.props.claimedCode
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on VerificationCodeCheckPayload {
				verificationResult
			}
		`;
	}

	getConfigs() {
		return [{
			type: 'REQUIRED_CHILDREN',
			children: [
				Relay.QL`
					fragment on VerificationCodeCheckPayload {
						verificationResult
					}
				`,
			]
		}];
	}

	static fragments = {
  };
}
