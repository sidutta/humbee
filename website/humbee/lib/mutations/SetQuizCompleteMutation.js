import Relay from 'react-relay';

export default class SetQuizCompleteMutation extends Relay.Mutation {

  getMutation () {
    return Relay.QL`
    mutation { setQuizComplete }
    `;
  }

  getVariables () {
    return {
      completed : this.props.completed,
      quizId : this.props.quizId
    };
  }

  getFatQuery () {
    return Relay.QL`
      fragment on SetQuizCompletePayload {
        quiz {
          completed
        }
      }
    `;
  }

  getOptimisticResponse() {
    return {
      quiz : {
        completed : true
      }
    };
  }

  getConfigs() {
    return [
      {
        type : 'FIELDS_CHANGE',
        fieldIDs : {
          quiz : this.props.quizId
        }
      }];
  }
}
