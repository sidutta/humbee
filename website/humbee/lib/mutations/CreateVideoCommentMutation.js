import Relay from 'react-relay';

export default class CreateVideoCommentMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createVideoComment }
		`;
	}

	getVariables () {
		return {
			comment: this.props.comment,
			videoId : this.props.videoId
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateVideoCommentPayload {
				video
			}
		`;
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					video : this.props.videoId
				}
			}];
	}
}
