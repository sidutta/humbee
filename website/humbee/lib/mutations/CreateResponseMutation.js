import Relay from 'react-relay';

export default class CreateResponseMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createResponse }
		`;
	}

	getVariables () {
		return {
			optionId : this.props.optionId,
			questionId : this.props.questionId
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateResponsePayload {
				question
			}
		`;
	}

	getOptimisticResponse(){
		return {
			question : {
				answered : true
			}
		};
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					question : this.props.questionId
				}
			}];
	}
}
