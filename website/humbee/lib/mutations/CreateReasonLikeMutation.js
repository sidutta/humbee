import Relay from 'react-relay';

export default class CreateReasonMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createReasonLike }
		`;
	}

	getVariables () {
		return {
			reasonId : this.props.reasonId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateReasonLikePayload {
				reason
			}
		`;
	}

	getOptimisticResponse(){
		let { liked } = this.props;
		return {
			reason : {
				liked : !liked
			}
		};
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					reason : this.props.reasonId
				}
			}];
	}
}
