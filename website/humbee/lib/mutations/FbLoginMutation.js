import Relay from 'react-relay';

export default class FbLoginMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { fbLogin }
		`;
	}

	getVariables () {
		return {
			accessToken: this.props.accessToken,
			userID : this.props.userID
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on FbLoginPayload {
				token,
				id
			}
		`;
	}

	getConfigs() {
		return [{
			type: 'REQUIRED_CHILDREN',
			children: [
				Relay.QL`
					fragment on FbLoginPayload {
						token,
						id
					}
				`,
			]
		}];
	}
}
