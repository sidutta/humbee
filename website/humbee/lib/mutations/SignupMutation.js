import RelayMutationType from 'react-relay';
import Relay from 'react-relay';

export default class SignupMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		  mutation { signup }
		`;
	}

	getVariables () {
		return {
			email: this.props.email,
			password: this.props.password,
			firstName: this.props.firstName,
			lastName: this.props.lastName,
			username : this.props.username,
      client : this.props.client
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on SignupPayload {
				id
			}
		`;
	}

	getConfigs() {
	return [{
		type: 'REQUIRED_CHILDREN',
		children: [
			Relay.QL`
				fragment on SignupPayload {
					id
				}
			`,
		]
	}];
}

	static fragments = {
  };
}
