import Relay from 'react-relay';

export default class CreateReasonMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createReason }
		`;
	}

	getVariables () {
		return {
			reason: this.props.reason,
			agree : this.props.agree,
			debateId : this.props.debateId
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateReasonPayload {
				reason,
				debate{
					reasons
				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					debate : this.props.debateId
				}
			}];
	}
}
