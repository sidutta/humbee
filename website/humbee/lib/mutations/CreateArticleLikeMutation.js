import Relay from 'react-relay';

export default class CreateArticleLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createArticleLike }
		`;
	}

	getVariables () {
		return {
			articleId : this.props.articleId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateArticleLikePayload {
				article
			}
		`;
	}

	getOptimisticResponse(){
		let { liked } = this.props;
		return {
			article : {
				liked : !liked
			}
		};
	}
	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					article : this.props.articleId
				}
			}];
	}
}
