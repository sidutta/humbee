import Relay from 'react-relay';

export default class CreateQuizCommentLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createQuizCommentLike }
		`;
	}

	getVariables () {
		return {
			quizCommentId : this.props.quizCommentId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateQuizCommentLikePayload {
				quizComment
			}
		`;
	}

	getOptimisticResponse(){
		let { liked } = this.props;
		return {
			quizComment : {
				liked : !liked
			}
		};
	}
	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					quizComment : this.props.quizCommentId
				}
			}];
	}
}
