import Relay from 'react-relay';

export default class CreateArticleCommentLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createArticleCommentLike }
		`;
	}

	getVariables () {
		return {
			articleCommentId : this.props.articleCommentId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateArticleCommentLikePayload {
				articleComment
			}
		`;
	}

	getOptimisticResponse(){
		let { liked } = this.props;
		return {
			articleComment : {
				liked : !liked
			}
		};
	}
	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					articleComment : this.props.articleCommentId
				}
			}];
	}
}
