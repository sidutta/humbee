import RelayMutationType from 'react-relay';
import Relay from 'react-relay';

export default class LoginMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { login }
		`;
	}

	getVariables () {
		return {
			identifier: this.props.identifier,
			inputPassword : this.props.inputPassword
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on LoginPayload {
				token
			}
		`;
	}

	getConfigs() {
	return [{
		type: 'REQUIRED_CHILDREN',
		children: [
			Relay.QL`
				fragment on LoginPayload {
					token
				}
			`,
		]
	}];
}

	static fragments = {
  };
}
