import Relay from 'react-relay';

export default class CreateCommentLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createCommentLike }
		`;
	}

	getVariables () {
		return {
			commentId : this.props.commentId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateCommentLikePayload {
				comment
			}
		`;
	}

	getOptimisticResponse(){
		let { liked } = this.props;
		return {
			comment : {
				liked : !liked
			}
		};
	}
	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					comment : this.props.commentId
				}
			}];
	}
}
