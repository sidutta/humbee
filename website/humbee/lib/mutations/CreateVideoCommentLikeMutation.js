import Relay from 'react-relay';

export default class CreateVideoCommentLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createVideoCommentLike }
		`;
	}

	getVariables () {
		return {
			videoCommentId : this.props.videoCommentId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateVideoCommentLikePayload {
				videoComment
			}
		`;
	}

	getOptimisticResponse(){
		let { liked } = this.props;
		return {
			videoComment : {
				liked : !liked
			}
		};
	}
	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					videoComment : this.props.videoCommentId
				}
			}];
	}
}
