import Relay from 'react-relay';

export default class CreateQuizLikeMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createQuizLike }
		`;
	}

	getVariables () {
		return {
			quizId : this.props.quizId,
			liked : this.props.liked
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreateQuizLikePayload {
				quiz
			}
		`;
	}

	getOptimisticResponse(){
		let { liked } = this.props;
		return {
			quiz : {
				liked : !liked
			}
		};
	}
	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					quiz : this.props.quizId
				}
			}];
	}
}
