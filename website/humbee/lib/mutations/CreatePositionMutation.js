import Relay from 'react-relay';

export default class CreatePositionMutation extends Relay.Mutation {

	getMutation () {
		return Relay.QL`
		mutation { createPosition }
		`;
	}

	getVariables () {
		return {
			agree: this.props.agree,
			debateId : this.props.debateId
		};
	}

	getFatQuery () {
		return Relay.QL`
			fragment on CreatePositionPayload {
				debate
			}
		`;
	}

	getConfigs() {
		return [
			{
				type : 'FIELDS_CHANGE',
				fieldIDs : {
					debate : this.props.debateId
				}
			}];
	}
}
