import React from 'react';
import Relay from 'react-relay';

import Leaderboard from '../../common/Leaderboard';
import HumbeeHelpBox from '../../common/HumbeeHelpBox';
import CelebrityComment from './CelebrityComment';
import ReasonCardContainer from '../../Containers/ReasonCardContainer';

class Debate extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    const { reasons } = this.props.viewer;
    const { hasNextPage } = reasons.pageInfo;
    const reasonCards = reasons.edges.map( (item, index) => {
      return <ReasonCardContainer reasonId={item.node.id} {...item.node} key={index} />;
      // switch(node.type) {
      //   case 'DebateType':
      //     return <DebateCardContainer debateId={node.debateId} key={index} />;
      // }
    } );

    return (
      <div>
        <div className="row">
          <div className="col-xs-hidden col-sm-hidden col-md-2 col-lg-2"> </div>
          <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <CelebrityComment />
          </div>
        </div>
        {reasonCards}
      </div>
    );

  }
}

export default Relay.createContainer(Debate, {
  initialVariables : {
    pageSize : 100
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Debate {
        motionFull,
        reasons(first : $pageSize){
          pageInfo{
            hasNextPage
          },
          edges{
            node{
              id,
              text,
              agree,
              userId,
              postedTime,
              likes,
              liked
            }
          }
        }
      }
    `
  }
});
