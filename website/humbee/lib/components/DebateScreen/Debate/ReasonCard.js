import React from 'react';
import Relay from 'react-relay';

import { InputGroup, Button, FormControl } from 'react-bootstrap';

import Leaderboard from '../../common/Leaderboard';
import HumbeeHelpBox from '../../common/HumbeeHelpBox';

import CommentCardContainer from '../../Containers/CommentCardContainer';
import CreateReasonLikeMutation from '../../../mutations/CreateReasonLikeMutation';
import CreateCommentMutation from '../../../mutations/CreateCommentMutation';

class ReasonCard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
    }
    this.like = this.like.bind(this);
    this.onCommentTextChange = this.onCommentTextChange.bind(this);
    this.postComment = this.postComment.bind(this);
  }

  like() {
    const { id, liked } = this.props;
    Relay.Store.commitUpdate(
      new CreateReasonLikeMutation({
        liked : liked,
        reasonId : id
      })
    );
  }

  onCommentTextChange(e) {
    this.commentText = e.target.value;
  }

  postComment() {
    this.setState({isLoading: true});
    Relay.Store.commitUpdate(
      new CreateCommentMutation({
        comment : this.commentText,
        reasonId : this.props.id
      }),
      {
        onFailure: () => {
          alert('Oops something went wrong, please try again!!');
          this.setState({isLoading: false});
        },
        onSuccess: () => {
          this.setState({isLoading: false});
          this.commentText = '';
        }
      }
    );
  }

  render() {
    const {id, text, postedTime, likes, liked, position, agree, userId, showUser, seeDebate, debateId} = this.props;
    const { edges, pageInfo } = this.props.viewer.comments;
    const { hasNextPage } = pageInfo;

    let likeStmt;
    switch(likes) {
      case 0: {
        if(liked) {
          likeStmt = "Show your support by liking!";
        }
        else {
          likeStmt = "Show your support by liking!";
        }
        break;
      }
      case 1: {
        if(liked) {
          likeStmt =
            <a href="#" data-toggle="modal" data-target="#like_list">
              You liked this reason!
            </a>
        }
        else {
          likeStmt =
            <a href="#" data-toggle="modal" data-target="#like_list">
              1 person likes this reason
            </a>
        }
        break;
      }
      default: {
        if(liked) {
          likeStmt =
            <a href="#" data-toggle="modal" data-target="#like_list">
              You and {likes - 1} others like this reason!
            </a>
        }
        else {
          likeStmt =
            <a href="#" data-toggle="modal" data-target="#like_list">
              {likes} people like this reason!
            </a>
        }
        break;
      }
    }

    const comments = edges.map((comment) => {
      return <CommentCardContainer key={comment.node.id} commentId={comment.node.id} />;
    });

    return (
      <div style={styles.debate_for_container} id={id}>
        <div style={styles.top_box_2}>
          <div style={styles.name_photo}>
            <InputGroup>
              <InputGroup.Addon>
                <img src="https://instagram.fdel1-2.fna.fbcdn.net/t51.2885-19/11849450_441929729320015_1507482248_a.jpg" alt="{firstName lastName}" className="image-rounded" height="50" width="50" id="profile-pic-small" />
              </InputGroup.Addon>
              <div className="user-name">
                <a href>Vivek</a>
              </div>
            </InputGroup>
          </div>
          <div className="share-box">
            <a href="#">
              <span className="material-icons share-icon-2">share</span>
            </a>
          </div>
        </div>
        <div className="reason">
          {text}
        </div>
        <div style={styles.comment_box}></div>
        <div className="like-statement" style={{justifyContent:'space-between'}}>
            {likeStmt}
          <div>
            Updated {postedTime} ago
          </div>
        </div>
        <div className="like-comment">
          <div className="seperator"></div>
          <div className="like-comment-icon">
            <a href="#" onClick={this.like}>
              <span className="material-icons action-icon">thumb_up</span>
            </a>
            <a href="#">
              <span className="material-icons action-icon">comment</span>
            </a>
          </div>
          <div className="comment-statement">
            <a href="#"> {edges.length} comments </a>
          </div>
        </div>
        {comments}
        <div className="comment-box">
          <div className="input-group">
            <span className="input-group-addon" id="input-group-addon-comment">
              <img src="https://instagram.fdel1-2.fna.fbcdn.net/t51.2885-19/s150x150/14550179_1403190083043681_6137863829146566656_a.jpg" alt="{firstName lastName}" className="image-rounded" height="40" width="40" id="profile-pic-make-comment"/>
            </span>
            <FormControl onChange={this.onCommentTextChange} id="usr" componentClass="textarea" placeholder="Write a comment" rows="2" />
            <Button onClick={this.postComment}>Post</Button>
          </div>
        </div>
      </div>
    );
  }
}

const styles = {
  "debate_for_container": {
    "display": "flex",
    "height": "auto",
    "WebkitFlexDirection": "column",
    "flexDirection": "column",
    "backgroundColor": "#E8F5E9",
    "flexWrap": "wrap",
    "borderRadius": "5px",
    "marginRight": "100px",
    "marginBottom": "15px",
    "marginLeft": "5px",
    "borderTop": "20px solid #E8F5E9",
    "borderLeft": "20px solid #E0E0E0",
    "boxShadow": "none"
  },
  "debate_against_container": {
    "display": "flex",
    "height": "auto",
    "WebkitFlexDirection": "column",
    "flexDirection": "column",
    "backgroundColor": "#FFEBEE",
    "flexWrap": "wrap",
    "borderRadius": "5px",
    "marginLeft": "100px",
    "marginRight": "5px",
    "marginBottom": "15px",
    "borderTop": "20px solid #FFEBEE",
    "borderRight": "20px solid #E0E0E0",
    "boxShadow": "none",
    "marginBottom": "15px",
    "marginTop": "15px"
  },
  "comment_box": {
    "WebkitFlex": "1",
    "flex": "1!important",
    "WebkitFlexDirection": "row",
    "flexDirection": "row",
    "display": "flex",
    "backgroundColor": "#F1F8E9"
  },
  "comment_box_against": {
    "WebkitFlex": "1",
    "flex": "1",
    "WebkitFlexDirection": "row",
    "flexDirection": "row",
    "display": "flex",
    "backgroundColor": "#FBE9E7"
  },
  "top_box_2": {
    "WebkitFlex": "1",
    "flex": "1",
    "WebkitFlexDirection": "row",
    "flexDirection": "row",
    "display": "flex"
  },
  "name_photo": {
    "WebkitFlex": "3!important",
    "flex": "3!important",
    "display": "flex!important",
    "alignItems": "flex-start",
    "justifyContent": "flex-start"
  }
}

export default Relay.createContainer(ReasonCard, {
  initialVariables : {
    pageSize : 100
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Reason {
        id,
        likes,
        comments(first : $pageSize){
          pageInfo{
            hasNextPage
          },
          edges{
            node{
              id
            }
          }
        }
      }
    `
  }
});
