import React from 'react';
import Relay from 'react-relay';

import { InputGroup } from 'react-bootstrap';

import Leaderboard from '../../common/Leaderboard';
import HumbeeHelpBox from '../../common/HumbeeHelpBox';
import CreateCommentLikeMutation from '../../../mutations/CreateCommentLikeMutation';

class CommentCard extends React.Component {

  constructor(props) {
    super(props);
    this.like = this.like.bind(this);
  }

  like() {
    const { id, liked } = this.props.viewer;
    Relay.Store.commitUpdate(
      new CreateCommentLikeMutation({
        liked : liked,
        commentId : id
      })
    );
  }

  render() {
    const { numberOfLikes, text, postedTime, userId, liked } = this.props.viewer;
    return (
      <div>
        <div style={styles.comment_box}>
          <div className="comment-pic">
            <img src="https://instagram.fdel1-2.fna.fbcdn.net/t51.2885-19/s150x150/14550179_1403190083043681_6137863829146566656_a.jpg" alt="{firstName lastName}" className="image-rounded" height="50" width="50" id="profile-pic-comment" />
          </div>
          <div className="comment-line">
            <div className="input-group">
              <div className="user-name-comment">
                <a href>Prateek</a>
              </div>
              <div>{text}</div>
            </div>
          </div>
        </div>
        <div className="comment-like-reply">
          <div onClick={this.like}>
            <span className="material-icons comment-action-icon">thumb_up</span>
          </div>
          <div className="like-statement-comment">
            <a href="#">{numberOfLikes} likes</a>
          </div>
          <div className="like-statement-comment">
            Posted {postedTime} ago
          </div>
        </div>
      </div>
    );
  }
}

const styles = {
  "comment_box": {
    "WebkitFlex": "1",
    "flex": "1!important",
    "WebkitFlexDirection": "row",
    "flexDirection": "row",
    "display": "flex",
    "backgroundColor": "#F1F8E9"
  },
  "comment_box_against": {
    "WebkitFlex": "1",
    "flex": "1",
    "WebkitFlexDirection": "row",
    "flexDirection": "row",
    "display": "flex",
    "backgroundColor": "#FBE9E7"
  }
}

export default Relay.createContainer(CommentCard, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Comment {
        id,
        text,
        userId,
        postedTime,
        numberOfLikes,
        liked
      }
    `
  }
});
