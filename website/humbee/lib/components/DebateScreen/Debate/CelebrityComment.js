import React from 'react';
import Relay from 'react-relay';

import Leaderboard from '../../common/Leaderboard';
import HumbeeHelpBox from '../../common/HumbeeHelpBox';

class CelebrityComment extends React.Component {
  render() {
    return (
      <div className="flex-container-discuss" id="celeb-discuss-container">
        <div className="top-box">
          <div className="celeb-discuss-item1">
            <img src="https://pbs.twimg.com/profile_images/718314968102367232/ypY1GPCQ.jpg" alt="PictureOfceleb-discussrity" className="image-rounded" height="70" width="70" id="celeb-discuss-pic"/>
          </div>
          <div className="celeb-discuss-item2">
            <a href="#">
              <span className="material-icons share-icon">share</span>
            </a>
          </div>
        </div>
        <div className="celeb-discuss-comment">
          "sddhsjd dsgd dghd gdfdfdfada jkkhadjsgd dgshsyafgashfgad fhgdhafgahfadf sgdahsdeyd jgdhsadgyadg hsdghsdg dgasd hgdhsfdsd sfdgsdfasgdfasdaD gfgdgfdsfds gftrerewe fddsaszfghjj vcfdfdd hggfrde gfgdes gfdfsawa gfgderawa fddwawa hgghftreraz bvgfhf jhyth trfh"
        </div>
        <div className="celeb-discuss-name">
          -PM Narendra Modi
        </div>
        <div className="celeb-discuss-source">
          Source:Twitter
        </div>
      </div>
    );
  }
}

export default Relay.createContainer(CelebrityComment, {
  fragments: {
    // viewer: () => Relay.QL`
    //   fragment on Debate {
    //     motionFull,
    //     reasons(first : $pageSize){
    //       pageInfo{
    //         hasNextPage
    //       },
    //       edges{
    //         node{
    //           id,
    //           text,
    //           agree,
    //           userId,
    //           postedTime,
    //           likes,
    //           liked
    //         }
    //       }
    //     }
    //   }
    // `
  }
});
