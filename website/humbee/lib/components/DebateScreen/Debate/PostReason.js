import React from 'react';
import Relay from 'react-relay';

import { DropdownButton, MenuItem, FormControl, Alert } from 'react-bootstrap';

import Leaderboard from '../../common/Leaderboard';
import HumbeeHelpBox from '../../common/HumbeeHelpBox';
import CelebrityComment from './CelebrityComment';
import ReasonCard from './ReasonCard';
import CreateReasonMutation from '../../../mutations/CreateReasonMutation';

export default class PostReason extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
      agree : null,
      warning : ''
    }
    this.createReason = this.createReason.bind(this);
    this.changePosition = this.changePosition.bind(this);
    this.onReasonTextChange = this.onReasonTextChange.bind(this);
  }

  createReason() {
    const { agree } = this.state;
    const reason = this.reasonText;
    if(agree == null) {
      this.setState({warning: 'Please take a position!'});
    }
    else if(reason==null || reason=='') {
      this.setState({warning: 'Reason can\'t be empty!'});
    }
    else {
      this.setState({warning: ''});
      this.setState({isLoading : true});
    }

    Relay.Store.commitUpdate(
      new CreateReasonMutation({
        agree : agree,
        reason : reason,
        debateId : this.props.debateId
      }),
      {
        onFailure: () => {
          alert('Oops something went wrong, please try again!!');
          this.setState({isLoading : false});
        },
        onSuccess: () => {
          this.reasonText = '';
          this.setState({isLoading : false});
          $('#myModal').modal('toggle');
        }
      }
    );

  }

  changePosition(eventKey) {
    if(eventKey==1) {
      this.setState({agree: true});
    }
    else if(eventKey==2) {
      this.setState({agree: false});
    }
  }

  onReasonTextChange(e) {
    this.reasonText = e.target.value;
  }

  render() {

    let buttonStyle = (this.state.agree == null ? 'default'
                          :
                          (this.state.agree == true ? 'success' : 'danger')
                        );

    let dropDownTitle = (this.state.agree == null ? 'Your side'
                          :
                          (this.state.agree == true ? 'Agree' : 'Disagree')
                        );

    let reasonBoxPlaceholder = (this.state.agree == null ? 'Tell us your reason for why you agree/disagree...'
                                  :
                                  (this.state.agree == true ? 'Tell us your reason for why you agree...'
                                    : 'Tell us your reason for why you disagree...'
                                  )
                                );

    return (
      <div className="container">
        <div id="myModal" className="modal fade" role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" id="close-box" disabled={this.state.isLoading}>&times;</button>
                <h4 className="modal-title">Speak your Mind</h4>
              </div>
              <div className="modal-body">
                <div className="input-group">
                  <span className="sub-heading">Your Position:</span>
                  <DropdownButton bsStyle={buttonStyle} title={dropDownTitle} onSelect={this.changePosition}>
                    <MenuItem eventKey="1">Agree</MenuItem>
                    <MenuItem eventKey="2">Disagree</MenuItem>
                  </DropdownButton>
                </div>
                <div className="input-group">
                  <span className="input-group-addon" id="pic-box">
                  </span>
                  <FormControl onChange={this.onReasonTextChange} componentClass="textarea" placeholder={reasonBoxPlaceholder} rows="3" />
                </div>
                {
                  (this.state.warning )
                  ?
                  <Alert bsStyle="warning">
                    <strong>{this.state.warning}</strong>
                  </Alert>
                  :
                  <div />
                }
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal" id="close_button" disabled={this.state.isLoading}>Close</button>
                <button type="button" className="btn btn-primary" id="post_button" onClick={this.createReason} disabled={this.state.isLoading}>Post</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
