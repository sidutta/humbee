import React from 'react';
import Relay from 'react-relay';

import CreateResponseMutation from '../../../mutations/CreateResponseMutation';

export default class Options extends React.Component {

  constructor(props){
    super(props);
    this.markAsAnswered = this.markAsAnswered.bind(this);
  }

  markAsAnswered(optionId) {
    let questionId = this.props.viewer.id;
    Relay.Store.commitUpdate(
      new CreateResponseMutation({
        optionId : optionId,
        questionId : questionId
      })
    );
  }

  render() {
    const { text } = this.props.viewer;

    let options = this.props.viewer.options;
    let numberOfOptions = options.length;

    let optionSection;

    let randomNum = (Math.floor(Math.random() * 100))%2;

    switch(numberOfOptions) {
      case 2: {
        optionSection =
          <div className="flex-container" id="opinions-container">
            <div className="item1"></div>
            <div className="question-box">{text}</div>
            <div className="item3"></div>
            <div className="option-box">
              <div className="part1">
                <button type="button" className="btn btn-primary" id="agree_button" onClick={() => this.markAsAnswered(options[0].id)}>{options[0].text}</button>
              </div>
              <div className="part2">
                <button type="button" className="btn btn-primary" id="disagree_button" onClick={() => this.markAsAnswered(options[1].id)}>{options[1].text}</button>
              </div>
            </div>
          </div>
        break;
      }
      case 3: {
        if(randomNum==0) {
          optionSection =
            <div className="flex-container" id="opinions-container">
              <div className="item1"></div>
              <div className="question-box-3">{text}</div>
              <div className="option-box">
                <div className="part1">
                  <button type="button" className="btn btn-primary" id="option_button" onClick={() => this.markAsAnswered(options[0].id)}>{options[0].text}</button>
                </div>
                <div className="part2">
                  <button type="button" className="btn btn-primary" id="option_button" onClick={() => this.markAsAnswered(options[1].id)}>{options[1].text}</button>
                </div>
              </div>
              <div className="part3">
                <button type="button" className="btn btn-primary" id="option_button" onClick={() => this.markAsAnswered(options[2].id)}>{options[2].text}</button>
              </div>
            </div>
        }
        else {
          optionSection =
            <div className="flex-container" id="opinions-container">
              <div className="question-box">{text}</div>
              <div className="part1">
                <button type="button" className="btn btn-primary" id="option_big_button" onClick={() => this.markAsAnswered(options[0].id)}>{options[0].text}</button>
              </div>
              <div className="part2">
                <button type="button" className="btn btn-primary" id="option_big_button" onClick={() => this.markAsAnswered(options[1].id)}>{options[1].text}</button>
              </div>
              <div className="part3">
                <button type="button" className="btn btn-primary" id="option_big_button" onClick={() => this.markAsAnswered(options[2].id)}>{options[2].text}</button>
              </div>
            </div>
        }
        break;
      }
      case 4: {
        if(randomNum==0) {
          optionSection =
            <div className="flex-container" id="opinions-container">
              <div className="item1"></div>
              <div className="question-box-3">{text}</div>
              <div className="option-box">
                <div className="part1">
                  <button type="button" className="btn btn-primary" id="option_button" onClick={() => this.markAsAnswered(options[0].id)}>{options[0].text}</button>
                </div>
                <div className="part2">
                  <button type="button" className="btn btn-primary" id="option_button" onClick={() => this.markAsAnswered(options[1].id)}>{options[1].text}</button>
                </div>
              </div>
              <div className="option-box-2">
                <div className="part3">
                  <button type="button" className="btn btn-primary" id="option_button" onClick={() => this.markAsAnswered(options[2].id)}>{options[2].text}</button>
                </div>
                <div className="part4">
                  <button type="button" className="btn btn-primary" id="option_button" onClick={() => this.markAsAnswered(options[3].id)}>{options[3].text}</button>
                </div>
              </div>
            </div>
        }
        else {
          optionSection =
            <div className="flex-container" id="opinions-container">
              <div className="question-box">{text}</div>
              <div className="part1">
                <button type="button" className="btn btn-primary" id="option_big_button" onClick={() => this.markAsAnswered(options[0].id)}>{options[0].text}</button>
              </div>
              <div className="part2">
                <button type="button" className="btn btn-primary" id="option_big_button" onClick={() => this.markAsAnswered(options[1].id)}>{options[1].text}</button>
              </div>
              <div className="part3">
                <button type="button" className="btn btn-primary" id="option_big_button" onClick={() => this.markAsAnswered(options[2].id)}>{options[2].text}</button>
              </div>
              <div className="part4">
                <button type="button" className="btn btn-primary" id="option_big_button" onClick={() => this.markAsAnswered(options[3].id)}>{options[3].text}</button>
              </div>
            </div>
        }
        break;
      }
      default: {
        let optionRows = options.map( (option, index) =>
                                        <div className="part1">
                                          <button type="button" key={index} className="btn btn-primary" id="option_big_button">{option.text}</button>
                                        </div>
                                    );
        optionSection =
            optionSection =
            <div className="flex-container" id="opinions-container">
              <div className="question-box">{text}</div>
              {optionRows}
            </div>
        break;
      }
    }

    return (
      optionSection
    );
  }
}
