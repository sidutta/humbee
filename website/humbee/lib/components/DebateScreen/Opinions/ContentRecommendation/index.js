import React from 'react';

import HumbeeHelpBox from '../../../common/HumbeeHelpBox';

import LeaderboardContainer from '../../../Containers/LeaderboardContainer';

class ShowcaseLayout extends React.Component {
  render() {
    return (
      <div className="row">
        <div className="col-xs-hidden col-sm-hidden col-md-3 col-lg-3">
        </div>
        <div className="col-xs-12 col-sm-8 col-md-6 col-lg-6" id="main-grid-ds">
          <div>
            <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4" id="main-grid-ds">
              <div>
                dwdwd
              </div>
            </div>
            <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4" id="main-grid-ds">
              <div>
                defe
              </div>
            </div>
            <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4" id="main-grid-ds">
              <div>
                defe
              </div>
            </div>
          </div>
        </div>
        <div className="col-xs-hidden col-sm-hidden col-md-3 col-lg-3">
          <LeaderboardContainer />
          <HumbeeHelpBox />
        </div>
      </div>
    );
  }
}

module.exports = ShowcaseLayout;

function generateLayout() {
  return _.map(_.range(0, 25), function (item, i) {
    var y = Math.ceil(Math.random() * 4) + 1;
    return {
      x: _.random(0, 5) * 2 % 12,
      y: Math.floor(i / 6) * y,
      w: 2,
      h: y,
      i: i.toString(),
      static: Math.random() < 0.05
    };
  });
}
