import React from 'react';
import Relay from 'react-relay';

export default class Results extends React.Component {

  render() {
    const { text } = this.props.viewer;

    let options = this.props.viewer.options;
    let numberOfOptions = options.length;

    let totalVotes = 0;

    let indexOfUnsure = -1;

    for(let i=0; i<options.length; i++) {
      if(options[i].text == "unsure"||options[i].text == "Unsure") {
        indexOfUnsure = i;
      }
      else {
        totalVotes = totalVotes + options[i].votes;
      }
    }

    totalVotes = (totalVotes == 0) ? 1 : totalVotes;

    let optionSection;

    switch(numberOfOptions) {
      case 2: {
        optionSection =
          <div className="flex-container" id="opinions-container">
            <div className="item1"></div>
            <div className="question-box">{text}</div>
            <div className="item3"></div>
            <div className="item1"></div>
            <div className="question-box-result">
              <div className="progress">
                <div className="progress-bar progress-bar-success" style={{"width": "75%"}}></div>
                <div className="progress-bar progress-bar-danger" style={{"width": "25%"}}></div>
              </div>
              <div className="label-agree-disagree">
                <div className="agree-label">{((options[0].votes/totalVotes)*100) + '% agree'}</div>
                <div className="disagree-label">{((options[1].votes/totalVotes)*100) + '% disagree'}</div>
              </div>
            </div>
            <div className="item3"></div>
          </div>
        break;
      }
      case 3: {
        optionSection =
          <div className="flex-container" id="opinions-container">
            <div className="item1"></div>
            <div className="question-box-3">{text}</div>
            <div className="item1">
              <div className="gq-1">{options[0].text}</div>
              <div className="gq-2">
                <div className="progress-bar progress-bar-success" style={{"width": "60%"}} id="follow-bar-1"> </div>
                <div className="progress-bar progress-bar-danger" style={{"width": "40%"}} id="follow-bar-2"> </div>
                <div className="similarity-label">{((options[0].votes/totalVotes)*100) + '%'}</div>
              </div>
              <div className="gq-3"> </div>
            </div>
            <div className="item3">
              <div className="gq-1">{options[1].text}</div>
              <div className="gq-2">
                <div className="progress-bar progress-bar-success" style={{"width": "30%"}} id="follow-bar-1"> </div>
                <div className="progress-bar progress-bar-danger" style={{"width": "70%"}} id="follow-bar-2"> </div>
                <div className="similarity-label">{((options[1].votes/totalVotes)*100) + '%'}</div>
              </div>
              <div className="gq-3"> </div>
            </div>
            <div className="item3">
              <div className="gq-1">{options[2].text}</div>
              <div className="gq-2">
                <div className="progress-bar progress-bar-success" style={{"width": "80%"}} id="follow-bar-1"> </div>
                <div className="progress-bar progress-bar-danger" style={{"width": "20%"}} id="follow-bar-2"> </div>
                <div className="similarity-label">{((options[2].votes/totalVotes)*100) + '%'}</div>
              </div>
              <div className="gq-3"> </div>
            </div>
          </div>
        break;
      }
      case 4: {
        optionSection =
          <div className="flex-container" id="opinions-container">
            <div className="item1"></div>
            <div className="question-box-3">{text}</div>
            <div className="item1">
              <div className="gq-1">{options[0].text}</div>
              <div className="gq-2">
                <div className="progress-bar progress-bar-success" style={{"width": "60%"}} id="follow-bar-1"> </div>
                <div className="progress-bar progress-bar-danger" style={{"width": "40%"}} id="follow-bar-2"> </div>
                <div className="similarity-label">{((options[0].votes/totalVotes)*100) + '%'}</div>
              </div>
              <div className="gq-3"> </div>
            </div>
            <div className="item3">
              <div className="gq-1">{options[1].text}</div>
              <div className="gq-2">
                <div className="progress-bar progress-bar-success" style={{"width": "30%"}} id="follow-bar-1"> </div>
                <div className="progress-bar progress-bar-danger" style={{"width": "70%"}} id="follow-bar-2"> </div>
                <div className="similarity-label">{((options[1].votes/totalVotes)*100) + '%'}</div>
              </div>
              <div className="gq-3"> </div>
            </div>
            <div className="item3">
              <div className="gq-1">{options[2].text}</div>
              <div className="gq-2">
                <div className="progress-bar progress-bar-success" style={{"width": "80%"}} id="follow-bar-1"> </div>
                <div className="progress-bar progress-bar-danger" style={{"width": "20%"}} id="follow-bar-2"> </div>
                <div className="similarity-label">{((options[2].votes/totalVotes)*100) + '%'}</div>
              </div>
              <div className="gq-3"> </div>
            </div>
            <div className="item3">
              <div className="gq-1">{options[3].text}</div>
              <div className="gq-2">
                <div className="progress-bar progress-bar-success" style={{"width": "40%"}} id="follow-bar-1"> </div>
                <div className="progress-bar progress-bar-danger" style={{"width": "60%"}} id="follow-bar-2"> </div>
                <div className="similarity-label">{((options[3].votes/totalVotes)*100) + '%'}</div>
              </div>
              <div className="gq-3"> </div>
            </div>
          </div>
        break;
      }
      default: {
        let optionRows = options.map( (option, index) =>
                                        <div className="item3">
                                          <div className="gq-1">{option.text}</div>
                                          <div className="gq-2">
                                            <div className="progress-bar progress-bar-success" style={{"width": "40%"}} id="follow-bar-1"> </div>
                                            <div className="progress-bar progress-bar-danger" style={{"width": "60%"}} id="follow-bar-2"> </div>
                                            <div className="similarity-label">{((option.votes/totalVotes)*100) + '%'}</div>
                                          </div>
                                          <div className="gq-3"> </div>
                                        </div>
                                    );
        optionSection =
            optionSection =
                    <div className="flex-container" id="opinions-container">
                      <div className="item1"></div>
                      <div className="question-box-3">{text}</div>
                      {optionRows}
                    </div>
        break;
      }
    }

    return (
      optionSection
    );
  }
}
