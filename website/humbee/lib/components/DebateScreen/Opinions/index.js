import React from 'react';
import Relay from 'react-relay';

import QuestionScreenContainer from '../../Containers/QuestionScreenContainer';

class Opinions extends React.Component {

  render() {
    const { Questions, numQuestions } = this.props.viewer;
    let questionList = Questions.edges.map( (question, index, questions) =>
                                               <QuestionScreenContainer
                                                 questionId={question.node.id}
                                                 key={index}
                                               />
                                          );

    return (
      <div>
        {questionList}
      </div>
    );
  }
}

export default Relay.createContainer(Opinions, {
  initialVariables : {
    pageSize : 10
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Quiz {
        id,
        title,
        description,
        lastDebateId,
        numQuestions,
        completed,
        Questions(first : $pageSize) {
          pageInfo{
            hasNextPage
          },
          edges{node{
          id,
          answered
          }}
        }
      }
    `
  }
});
