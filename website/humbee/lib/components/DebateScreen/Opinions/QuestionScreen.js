import React from 'react';
import Relay from 'react-relay';

import Options from './Options';
import Results from './Results';

class Opinions extends React.Component {

  render() {
    const { answered } = this.props.viewer;



    return (
      answered ?
      <Results viewer={this.props.viewer} />
      :
      <Options viewer={this.props.viewer} />
    );
  }
}

export default Relay.createContainer(Opinions, {
  initialVariables : {
    pageSize : 10
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Question {
        id,
        text,
        numberOfOptions,
        answered,
        options {
          id,
          text,
          votes,
          voted
        }
      }
    `
  }
});
