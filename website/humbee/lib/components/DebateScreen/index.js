import React from 'react';
import Relay from 'react-relay';
import { Link } from 'react-router'

import { LinkContainer } from 'react-router-bootstrap';

import HumbeeHelpBox from '../common/HumbeeHelpBox';

import PostReason from './Debate/PostReason';
import FullStoryContainer from '../Containers/FullStoryContainer';
import DebateContainer from '../Containers/DebateContainer';
import OpinionsContainer from '../Containers/OpinionsContainer';
import LeaderboardContainer from '../Containers/LeaderboardContainer';

class DebateScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      tabIndex : this.props.tabIndex || 0
    }
    this.switchTab = this.switchTab.bind(this);
  }

  switchTab(tabIndex) {
    this.setState({tabIndex});
  }

  render() {
    const { tabIndex } = this.state;
    let fsTabClass = "full-story-ds";
    let opinionTabClass = "opinions-ds";
    let discussTabClass = "debate-ds";
    let debateBody;
    let debateId = this.props.params.debateId;
    let quizId = this.props.viewer.quizId;
    // let quizId = this.props.location.query.quizId;
    switch(tabIndex) {
      case 0: {
        fsTabClass = fsTabClass + " selected-tab-ds";
        debateBody = <FullStoryContainer debateId={debateId} />;
        break;
      }
      case 1: {
        opinionTabClass = opinionTabClass + " selected-tab-ds";
        debateBody = <OpinionsContainer quizId={quizId} />;
        break;
      }
      case 2: {
        discussTabClass = discussTabClass + " selected-tab-ds";
        debateBody = <DebateContainer debateId={debateId} />;
        break;
      }
    }
    return (
      <div className="row">
        <div className="col-xs-hidden col-sm-hidden col-md-3 col-lg-3">
        </div>
        <div className="col-xs-12 col-sm-8 col-md-6 col-lg-6" id="main-grid-ds">
          <div style={{textAlign:'center', marginTop: 5, marginBottom: 5}}>
            {this.props.viewer.title}: {this.props.viewer.motionShort}
          </div>
          <div className="flex-container-ds">
            <Link to={'/debate/'+this.props.debateId+'/fullStory'} className={fsTabClass} style={{"textDecoration":"none"}} onClick={() => this.switchTab(0)}>
              Full Story
            </Link>
            <Link to={'/debate/'+this.props.debateId+'/opinion'} className={opinionTabClass} style={{"textDecoration":"none"}} onClick={() => this.switchTab(1)}>
              Opinions
            </Link>
            <Link to={'/debate/'+this.props.debateId+'/discuss'} className={discussTabClass} style={{"textDecoration":"none"}} onClick={() => this.switchTab(2)}>
              Debate
            </Link>
          </div>
          {debateBody}
        </div>
        <div className="col-xs-hidden col-sm-hidden col-md-3 col-lg-3">
          <LeaderboardContainer />
          <HumbeeHelpBox />
        </div>
        <div className="container">
          <button type="button" id="fabButton" className="btn btn-primary btn-fab" data-toggle="modal" data-target="#myModal">
            <i className="material-icons">edit</i>
          </button>
          <PostReason debateId={debateId} />
        </div>
      </div>
    );
  }
}

export default Relay.createContainer(DebateScreen, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Debate {
        title,
        quizId
        motionShort,
        motionFull,
      }
    `
  }
});
