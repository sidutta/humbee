import React from 'react';
import Relay from 'react-relay';

import StoryItemCard from './StoryItemCard';

class FullStory extends React.Component {
  render() {
    const { storyItems } = this.props.viewer;
    const storyItemCards = storyItems.map((storyItem, index) => (
      <StoryItemCard
        key={index}
        {...storyItem}
      />
    ));
    return (
      <section id="cd-timeline-fs" className="cd-container">
        {storyItemCards}
      </section>
    );
  }
}

export default Relay.createContainer(FullStory, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Debate {
        storyItems{
          id,
          title,
          imageUrl,
          fullStory
        }
      }
    `
  }
});
