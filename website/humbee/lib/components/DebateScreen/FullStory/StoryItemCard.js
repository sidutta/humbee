import React from 'react';
import Relay from 'react-relay';

export default class StoryItemCard extends React.Component {
  render() {
    const { imageUrl, fullStory, title } = this.props;
    return (
      <div className="cd-timeline-fs-block">
        <div className="cd-timeline-fs-img cd-picture">
          <span className="material-icons time-icon">event</span>
        </div>
        <div className="cd-timeline-fs-content">
          <h2>{title}</h2>
          {
            imageUrl
            ?
            <div className="flex-container-fs" id="pic-box">
              <img src={imageUrl} alt="{picture}" style={{"width":"447px", "height":"200px"}} />
            </div>
            :
            <div />
          }
          <p>{fullStory}</p>
          <span className="cd-date-fs">Jan 14</span>
        </div>
      </div>
    );
  }
}

