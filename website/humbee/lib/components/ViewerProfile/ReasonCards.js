import React from 'react';
import Relay from 'react-relay';

import Waypoint from 'react-waypoint';

class ReasonCards extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      isLoading: false,
      reasons : 10
    };
    this._loadMoreItems = this._loadMoreItems.bind(this);
    this._renderWaypoint = this._renderWaypoint.bind(this);
  }

  _loadMoreItems() {
    let {reasons}  = this.state;
    reasons = reasons + 10;
    this.props.relay.setVariables({reasons});
    this.setState({reasons : reasons, isLoading : true});
  }

  _renderWaypoint() {
    const { reasons } = this.props.viewer.user;
    const { hasNextPage } = reasons.pageInfo;
    if (!this.state.isLoading && hasNextPage) {
      return (
        <Waypoint
          onEnter={this._loadMoreItems}
          threshold={2.0}
        />
      );
    }
  }

  render(){
    console.log(this.props.viewer)
    const { reasons } = this.props.viewer.user;
    const { hasNextPage } = reasons.pageInfo;
    const cards = reasons.edges.map( (item, index) => {
      const { node } = item;
      console.log(node);
      return (
        <div class="row">
          <div class="col-md-12">
            <h1>ewf</h1>
            <hr />
            <p>
              {node.text}
            </p>
            <div>
              <a class="btn btn-primary">Click me</a>
              <a class="btn btn-primary">Click me</a>
              <a class="btn btn-primary pull-right" style={{"marginRight":"20px"}}>Click me</a>
              <a class="btn btn-primary pull-right" style={{"marginRight":"20px"}}>Click me</a>
            </div>
          </div>
        </div>
      )
    } );
    return(
      <div class="row">
        {cards}
      </div>
    );
  }
}

export default Relay.createContainer(ReasonCards, {
  initialVariables : {
    reasons : 10
  },
  fragments : {
    viewer : () => Relay.QL`
      fragment on Viewer {
        user {
          reasons(first : $reasons) {
            edges{
              node{
                id,
                text,
                agree,
                userId,
                postedTime,
                likes,
                liked,
                debateId,
              }
            }
            pageInfo{
              hasNextPage
            }
          }
        }
      }
    `
  }
});
