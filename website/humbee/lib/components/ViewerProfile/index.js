import React from 'react';
import Relay from 'react-relay';
import { Link } from 'react-router'

class ViewerProfile extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      tabIndex : this.props.tabIndex || 0
    }
    this.switchTab = this.switchTab.bind(this);
  }

  switchTab(tabIndex) {
    this.setState({tabIndex});
  }

  render() {
    // const { tabIndex } = this.state;
    let reasonsTabClass = "";
    let quizzesTabClass = "";
    let followersTabClass = "";
    let followingsTabClass = "";

    const { user } = this.props.viewer;
    console.log(user)
    return (
      <div className="row">
        <div className="col-md-2"></div>
        <div className="col-md-6">
          <div className="row">
            <div className="col-md-3 col-lg-3">
              <img src="https://storage.googleapis.com/humbee_images/cartoon-bee-1.jpg" className="img-responsive" />
            </div>
            <div className="col-md-9 col-lg-9">
              <h2 className="">{user.firstName} {user.lastName}</h2>
              <h4 className="">{user.username}</h4>
              <div className="">
                <a className="btn btn-primary">Follow</a>
                <a><i className="fa fa-fw fa-instagram fa-lg"></i></a>
                <a><i className="fa fa-fw fa-lg fa-twitter"></i></a>
                <a><i className="fa fa-facebook fa-fw fa-lg"></i></a>
                <a><i className="fa fa-fw fa-github fa-lg"></i></a>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <ul className="nav nav-pills">
                <li>
                  <Link to={'/profile/'+this.props.viewer.user.id} className={reasonsTabClass}>
                    Reasons
                  </Link>
                </li>
                <li>
                  <Link to={'/profile/'+this.props.viewer.user.id+'/quizzes'} className={reasonsTabClass}>
                    Quizzes
                  </Link>
                </li>
                <li>
                  <Link to={'/profile/'+this.props.viewer.user.id+'/followers'} className={reasonsTabClass}>
                    Followers
                  </Link>
                </li>
                <li>
                  <Link to={'/profile/'+this.props.viewer.user.id+'/followings'} className={reasonsTabClass}>
                    Followings
                  </Link>
                </li>
              </ul>
            </div>
          </div>
          <div className="row">
            {this.props.children}
          </div>
        </div>
        <div className="col-md-2">text</div>
        <div className="col-md-2"></div>
      </div>
    );
  }
}

export default Relay.createContainer(ViewerProfile, {
  fragments: {
    viewer : () => Relay.QL`
      fragment on Viewer {
        user {
          id,
          firstName,
          lastName,
          username,
          profilePic,
        }
      }
    `
  },
});
