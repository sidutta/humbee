import React from 'react';
import Relay from 'react-relay';

import HumbeeHelpBox from '../common/HumbeeHelpBox';

import LeaderboardContainer from '../Containers/LeaderboardContainer';
import OpinionsContainer from '../Containers/OpinionsContainer';

class QuizScreen extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    let quizId = this.props.quizId;

    return (
      <div className="row">
        <div className="col-xs-hidden col-sm-hidden col-md-3 col-lg-3">
        </div>
        <div className="col-xs-12 col-sm-8 col-md-6 col-lg-6" id="main-grid-ds">
          <OpinionsContainer quizId={quizId} />
        </div>
        <div className="col-xs-hidden col-sm-hidden col-md-3 col-lg-3">
          <LeaderboardContainer />
          <HumbeeHelpBox />
        </div>
        <div className="container">
          <button type="button" id="fabButton" className="btn btn-primary btn-fab" data-toggle="modal" data-target="#myModal">
            <i className="material-icons">edit</i>
          </button>
        </div>
      </div>
    );
  }
}

export default Relay.createContainer(QuizScreen, {
  fragments: {
    // viewer: () => Relay.QL`
    //   fragment on Quiz {
    //     id
    //   }
    // `
  }
});
