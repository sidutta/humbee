import React from 'react';
import Relay from 'react-relay';

class TakeQuizBox extends React.Component {
  render() {
    return (
      <div className="flex-container" id="gq-container">
        <div className="gq-heading-box">
          <div>Politcal Similarity Quiz</div>
          <div className="seperator"></div>
        </div>
        <div className="Similarity-bar">
          <div className="flex-container" id="gq-container-2">
            <div className="gq-1"> </div>
            <div className="gq-2">
              <div className="progress-bar progress-bar-success" style={{"width":"100%"}} id="gq-bar"> </div>
            </div>
            <div className="gq-3">
            </div>
          </div>
        </div>
        <div className="gq-text">
          <div>Take this Quiz to know how similar your political views are with your friends</div>
        </div>
        <div className="gq-button-bar">
          <button type="button" className="btn btn-primary" id="gq_button">Unlock Similarity with Friends</button>
        </div>
      </div>
    );
  }
}

export default Relay.createContainer(TakeQuizBox, {
  fragments: {
    // viewer: () => Relay.QL`
    // fragment on User {
    //   id
    // }
    // `,
  },
});
