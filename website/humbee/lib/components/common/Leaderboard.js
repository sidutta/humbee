import React from 'react';
import Relay from 'react-relay';

import LeaderboardUserCard from './LeaderboardUserCard';

class Leaderboard extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      pageSize : 10
    };
    this.onEndReached = this.onEndReached.bind(this);
  }

  onEndReached(){
    let {pageSize}  = this.state;
    pageSize = pageSize + 10;
    this.props.relay.setVariables({pageSize});
    this.setState({pageSize});
  }

  render() {

    const { viewer } = this.props;
    const { pageInfo, edges } = viewer.user.leaderBoard;
    const { hasNextPage } = pageInfo;

    const userCards = edges.map( (user, index) =>
                                   <LeaderboardUserCard user={user.node} />
                               );

    return (
      <div className="flex-container" id="leader-container">
        <div className="leader-heading-box">
          <div>The Leaderboard</div>
          <div className="seperator"> </div>
        </div>
        <table>
          <tbody>
            {userCards}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Relay.createContainer(Leaderboard, {
  initialVariables : {
    pageSize : 10
  },
  fragments: {
    viewer : () => Relay.QL`
      fragment on Viewer {
        user {
          leaderBoard(first : $pageSize) {
            pageInfo {
              hasNextPage
            },
            edges {
              node {
                firstName,
                lastName,
                username,
                profilePic,
                id,
                isFollowing,
                score
              }
            }
          }
        }
      }
    `
  },
});
