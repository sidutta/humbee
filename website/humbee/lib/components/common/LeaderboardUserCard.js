import React from 'react';
import Relay from 'react-relay';

import CreateFollowerMutation from '../../mutations/CreateFollowerMutation';

export default class LeaderboardUserCard extends React.Component {

  constructor(props){
    super(props);
    this.openUserProfile = this.openUserProfile.bind(this);
    this.followUser = this.followUser.bind(this);
  }

  openUserProfile(){
    const { navigator, id } = this.props;
    navigator.push({
      component : UserProfileContainer,
      passProps : {
        userId : id
      }
    });
  }

  followUser(id, isFollowing){
    Relay.Store.commitUpdate(
      new CreateFollowerMutation({
        followingId : id,
        isFollowing : isFollowing
      }),{
        onFailure : () => {
          alert('Oops something went wrong. Please try again.');
        }
      }
    );
  }

  render() {

    const { firstName, lastName, profilePic, username, score, id, isFollowing } = this.props.user;

    return (
      <tr>
        <td>
          <div className="rank"> 1 </div>
        </td>
        <td>
          <div className="input-group">
            <span className="input-group-addon">
              <img src={this.profilePic} alt="{firstName lastName}" className="image-rounded" height="50" width="50" id="profile-pic-small"/>
            </span>
            <div className="leader-name">
              <a onClick={this.openUserProfile}>{firstName} {lastName}</a>
            </div>
          </div>
        </td>
        <td>
          <div className="points">{score}</div>
        </td>
        <td>
          <button type="button" className="btn btn-primary" id="follow-button" onClick={ () => this.followUser(id, isFollowing) }>Follow</button>
        </td>
      </tr>
    );
  }

}
