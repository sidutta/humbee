import React from 'react';
import Relay from 'react-relay';

class HumbeeHelpBox extends React.Component {
  render() {
    return (
      <div className="flex-container" id="others-container">
        <div className="others-heading-box">
          <div>Humbee Help</div>
          <div className="seperator"></div>
        </div>
        <ul className="others-list">
          <li> <a href="#"> Support </a> </li>
          <li> <a href="#"> Privacy Policy </a> </li>
          <li> <a href="#"> Community Guidelines </a> </li>
          <li> <a href="#"> Terms of Service </a> </li>
          <li> <a href="#"> Blog </a> </li>
          <li> <a href="#"> Jobs </a> </li>
        </ul>
      </div>
    );
  }
}

export default Relay.createContainer(HumbeeHelpBox, {
  fragments: {
    // viewer: () => Relay.QL`
    // fragment on User {
    //   id
    // }
    // `,
  },
});
