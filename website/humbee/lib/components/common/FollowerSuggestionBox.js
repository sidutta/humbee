import React from 'react';
import Relay from 'react-relay';

class FollowerSuggestionBox extends React.Component {
  render() {
    return (
      <div className="flex-container" id="follow-container">
        <div className="follow-heading-box">
          <div>People you can follow</div>
          <div className="seperator"></div>
        </div>
        <div className="friend-list">
          <div className="frnd-pic-name">
            <div className="input-group">
              <span className="input-group-addon">
                <img src="https://instagram.fdel1-2.fna.fbcdn.net/t51.2885-19/11849450_441929729320015_1507482248_a.jpg" alt="{firstName lastName}" className="image-rounded" height="50" width="50" id="profile-pic-small" />
              </span>
              <div className="frnd-name">Vivek Nautiyal</div>
            </div>
          </div>
          <div className="follow-button-box">
            <button type="button" className="btn btn-primary" id="follow-button">Follow</button>
          </div>
        </div>
        <div className="Similarity-bar">
          <div className="flex-container" id="follow-container-2">
            <div className="gq-1"> </div>
            <div className="gq-2">
              <div className="progress-bar progress-bar-success" style={{"width":"60%"}} id="follow-bar-1"> </div>
              <div className="progress-bar progress-bar-danger" style={{"width":"40%"}} id="follow-bar-2"> </div>
            </div>
            <div className="gq-3"> </div>
          </div>
          <div className="agree-label" id="similarity-label"> 60% Similar</div>
          <div className="seperator"></div>
        </div>
        <div className="friend-list">
          <div className="frnd-pic-name">
            <div className="input-group">
              <span className="input-group-addon">
                <img src=" https://instagram.fdel1-2.fna.fbcdn.net/t51.2885-19/s150x150/14032955_586240608227187_521805580_a.jpg" alt="{firstName lastName}" className="image-rounded" height="50" width="50" id="profile-pic-small"/>
              </span>
              <div className="frnd-name">Nivedita Soni</div>
            </div>
          </div>
          <div className="follow-button-box">
            <button type="button" className="btn btn-primary" id="follow-button">Follow</button>
          </div>
        </div>
        <div className="Similarity-bar">
          <div className="flex-container" id="follow-container-2">
            <div className="gq-1"> </div>
            <div className="gq-2">
              <div className="progress-bar progress-bar-success" style={{"width":"95%"}} id="follow-bar-1"> </div>
              <div className="progress-bar progress-bar-danger" style={{"width":"5%"}} id="follow-bar-2"> </div>
            </div>
            <div className="gq-3"> </div>
          </div>
          <div className="agree-label" id="similarity-label"> 95% Similar</div>
          <div className="seperator"></div>
        </div>
        <div className="friend-list">
          <div className="frnd-pic-name">
            <div className="input-group">
              <span className="input-group-addon">
                <img src=" https://instagram.fdel1-2.fna.fbcdn.net/t51.2885-19/s150x150/14031673_285197298521901_2027709228_a.jpg" alt="{firstName lastName}" className="image-rounded" height="50" width="50" id="profile-pic-small"/>
              </span>
              <div className="frnd-name">Aushim Krishan</div>
            </div>
          </div>
          <div className="follow-button-box">
            <button type="button" className="btn btn-primary" id="follow-button">Follow</button>
          </div>
        </div>
        <div className="Similarity-bar">
          <div className="flex-container" id="follow-container-2">
            <div className="gq-1"> </div>
            <div className="gq-2">
              <div className="progress-bar progress-bar-success" style={{"width":"85%"}} id="follow-bar-1"> </div>
              <div className="progress-bar progress-bar-danger" style={{"width":"15%"}} id="follow-bar-2"> </div>
            </div>
            <div className="gq-3"> </div>
          </div>
          <div className="agree-label" id="similarity-label"> 85% Similar</div>
          <div className="seperator"></div>
        </div>
        <div className="friend-list">
          <div className="frnd-pic-name">
            <div className="input-group">
              <span className="input-group-addon">
                <img src="https://instagram.fdel1-2.fna.fbcdn.net/t51.2885-19/s150x150/14550179_1403190083043681_6137863829146566656_a.jpg" alt="{firstName lastName}" className="image-rounded" height="50" width="50" id="profile-pic-small"/>
              </span>
              <div className="frnd-name">Prateek Agarwal</div>
            </div>
          </div>
          <div className="follow-button-box">
            <button type="button" className="btn btn-primary" id="follow-button">Follow</button>
          </div>
        </div>
        <div className="Similarity-bar">
          <div className="flex-container" id="follow-container-2">
            <div className="gq-1"> </div>
            <div className="gq-2">
              <div className="progress-bar progress-bar-success" style={{"width":"30%"}} id="follow-bar-1"> </div>
              <div className="progress-bar progress-bar-danger" style={{"width":"70%"}} id="follow-bar-2"> </div>
            </div>
            <div className="gq-3"> </div>
          </div>
          <div className="agree-label" id="similarity-label"> 30% Similar</div>
        </div>
      </div>
    );
  }
}

export default Relay.createContainer(FollowerSuggestionBox, {
  fragments: {
    // viewer: () => Relay.QL`
    // fragment on User {
    //   id
    // }
    // `,
  },
});
