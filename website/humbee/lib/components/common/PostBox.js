import React from 'react';
import Relay from 'react-relay';

class PostBox extends React.Component {
  render() {
    return (
      <div className="container">
        <button type="button" id="fabButton" className="btn btn-primary btn-fab" data-toggle="modal" data-target="#myModal">
          <i className="material-icons">edit</i>
        </button>
        <div id="myModal" className="modal fade" role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" id="close-box">&times;</button>
                <h4 className="modal-title">Speak your Mind</h4>
              </div>
              <div className="modal-body">
                <div className="input-group">
                  <span className="input-group-addon" id="pic-box">
                      <img src="https://scontent.fdel1-1.fna.fbcdn.net/v/t1.0-9/14368748_1114246878659672_5337314871624884662_n.jpg?oh=7e434cdc0b2cc86f3ffa94869e97b4d8&oe=5898303B" alt="{firstName lastName}" className="image-rounded" height="60" width="60" id="profile-pic-small"/>
                  </span>
                  <textarea className="form-control" placeholder="Speak your mind on issues that matter to you...." rows="3"></textarea>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal" id="close_button">Close</button>
                <button type="button" className="btn btn-primary" id="post_button">Post</button>
              </div>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default Relay.createContainer(PostBox, {
  fragments: {
    // viewer: () => Relay.QL`
    // fragment on Viewer {
    //   id
    // }
    // `,
  },
});
