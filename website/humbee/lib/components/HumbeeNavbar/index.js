import React from 'react';
import Relay from 'react-relay';

import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router'
import { LinkContainer } from 'react-router-bootstrap';

import { postXHR } from '../../utils/XHR';

class HumbeeNavbar extends React.Component {

  constructor(props) {
    super(props);
    this.onLogout = this.onLogout.bind(this);
    // this.onSeeProfile = this.onSeeProfile.bind(this);
  }

  onLogout(e) {
    e.preventDefault();
    var loc = window.location
    var host = loc.protocol + "//" + loc.hostname + ":" + loc.port

    postXHR( host + '/auth/logout', {},
      () => {
        location.replace( location.href )
      },
      () => {
        location.replace( location.href )
      }
    )
  }

  // onSeeProfile(e) {
  //   e.preventDefault();
  //   var loc = window.location
  //   var host = loc.protocol + "//" + loc.hostname + ":" + loc.port

  //   postXHR( host + '/auth/logout', {},
  //     () => {
  //       location.replace( location.href )
  //     },
  //     () => {
  //       location.replace( location.href )
  //     }
  //   )
  // }

  render() {
    let { user } = this.props.viewer;
    let isAnon = (user.role=='AnonymousUser');
    let logoutOrLoginSection = [];
    if(isAnon) {
      logoutOrLoginSection = [
        <li>
          <a onClick={this.onLogout}>
            <span className="material-icons">account_circle</span>
            Login
          </a>
        </li>
      ]
    }
    else {
      logoutOrLoginSection = [
        <li>
          <Link to={'/profile/'+this.props.viewer.user.id}>
            <span className="material-icons">account_circle</span>
            {user.firstName}
          </Link>
        </li>,
        <li>
          <a onClick={this.onLogout}>
            <span className="material-icons">power_settings_new</span>
            Log Out
          </a>
        </li>
      ]
    }

    return (
      <Navbar fluid fixedTop style={{"backgroundColor":"#6B2F55!important","borderRadius":"0px!important"}}>
        <Navbar.Header>
          <LinkContainer to={{ pathname: '/' }}>
            <Navbar.Brand>
              HumBee
            </Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <li>
              <a href="#">
                <span className="material-icons">record_voice_over</span>
                Debates
              </a>
            </li>

            <li>
              <a href="#">
                <span className="material-icons">featured_video</span>
                Videos
              </a>
            </li>

            <li>
              <a href="#">
                <i className="material-icons">lightbulb_outline</i>
                Quiz
              </a>
            </li>
          </Nav>
          <Nav pullRight>
            <li>
              <input className="input" type="search" placeholder="Search" />
            </li>
            {logoutOrLoginSection}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default Relay.createContainer(HumbeeNavbar, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        user {
          id,
          firstName,
          role
        }
      }
    `,
  }
});
