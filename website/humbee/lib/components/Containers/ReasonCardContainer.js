'use strict';


import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import ReasonCard from '../DebateScreen/Debate/ReasonCard';

import reasonCardRoute from '../../routes/reasonCardRoute';

export default class ReasonCardContainer extends Component {
	render() {
		const { reasonId } = this.props;
		return (
			<RootContainer
				Component={ReasonCard}
				route={new reasonCardRoute({reasonId})}
				renderFetched={(data) => <ReasonCard {...this.props} {...data} />}
			/>
		);
	}
}
