'use strict';


import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';
import { ActivityIndicator } from 'react-native';
import colors from '../../config/colors';

import UserCard from '../Common/UserCard';

import userRoute from '../../routes/userRoute';

export default class UserCardContainer extends Component {
	render() {
		const { userId } = this.props;
		return (
			<RootContainer
				Component={UserCard}
				route={new userRoute({userId})}
				renderFetched={(data) => <UserCard {...this.props} {...data} />}
				renderLoading={() => <ActivityIndicator
																animating={true}
																color={colors.humbeeGreen}
																style={{
																	alignItems: 'center',
																	justifyContent: 'center',
																	padding: 40,
																	height: 200}}
																size="large"/>}/>
		);
	}
}
