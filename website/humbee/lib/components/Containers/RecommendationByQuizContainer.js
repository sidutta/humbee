'use strict';

import React, { Component } from 'react';
import {
  RootContainer
} from 'react-relay';

import RecommendationByQuiz from '../DebateScreen/Opinions/RecommendationByQuiz';

import recommendationByQuizRoute from '../../routes/recommendationByQuizRoute';

export default class RecommendationByQuizCardContainer extends Component {
  render() {
    const { masterCharacterizationId } = this.props;
    return (
      <RootContainer
        Component={RecommendationByQuiz}
        route={new recommendationByQuizRoute({masterCharacterizationId})}
        renderFetched={(data) => <RecommendationByQuiz {...this.props} {...data} />}
      />
    );
  }
}
