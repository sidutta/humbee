'use strict';

import React, { Component } from 'react';
import { RootContainer } from 'react-relay';

import ArticleCard from '../Home/article/ArticleCard';

import articleRoute from '../../routes/articleRoute';

export default class ArticleCardContainer extends Component {
	render() {
		const { articleId } = this.props;
		return (
			<RootContainer
				Component={ArticleCard}
				route={new articleRoute({articleId})}
				renderFetched={(data) => <ArticleCard {...this.props} {...data} />}
      />
		);
	}
}
