'use strict';

import React, { Component } from 'react';
import { RootContainer } from 'react-relay';

import Leaderboard from '../common/Leaderboard';

import homeRoute from '../../routes/homeRoute';

export default class LeaderboardContainer extends Component {
	render() {
		return (
			<RootContainer
				Component={Leaderboard}
				route={new homeRoute()}
				renderFetched={(data) => <Leaderboard {...this.props} {...data} />}
			/>
		);
	}
}
