'use strict';

import React, { Component } from 'react';
import { RootContainer } from 'react-relay';

import Opinions from '../DebateScreen/Opinions';

import quizRoute from '../../routes/quizRoute';

export default class OpinionsContainer extends Component {
  render() {
    let quizId = this.props.quizId;

    return (
      <RootContainer
        Component={Opinions}
        route={new quizRoute({quizId})}
        renderFetched={(data) => <Opinions {...this.props} {...data} />}
      />
    );
  }
}
