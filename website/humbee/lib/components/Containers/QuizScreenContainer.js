'use strict';

import React, { Component } from 'react';
import { RootContainer } from 'react-relay';

import QuizScreen from '../QuizScreen';

import quizRoute from '../../routes/quizRoute';

export default class QuizScreenContainer extends Component {
	render() {
		const { quizId } = this.props.params;
		return (
			<RootContainer
				Component={QuizScreen}
				route={new quizRoute({quizId})}
				renderFetched={ (data) => <QuizScreen quizId={quizId} {...this.props} {...data} /> }
      />
		);
	}
}
