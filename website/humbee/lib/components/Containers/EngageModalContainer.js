'use strict';

import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import EngageModal from '../DebateScreen/Debate/EngageModal';

import engageModalRoute from '../../routes/engageModalRoute';

export default class ReasonCardContainer extends Component {
	render() {
		const { reasonId } = this.props;
		return (
			<RootContainer
				Component={EngageModal}
				route={new engageModalRoute({reasonId})}
				renderFetched={(data) => <EngageModal {...this.props} {...data} />}
			/>
		);
	}
}
