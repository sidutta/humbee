'use strict';

import React, { Component } from 'react';
import { RootContainer } from 'react-relay';

import Debate from '../DebateScreen/Debate';

import debateRoute from '../../routes/debateRoute';

export default class DebateContainer extends Component {
	render() {
		const { debateId } = this.props;
		return (
			<RootContainer
				Component={Debate}
				route={new debateRoute({debateId})}
				renderFetched={(data) => <Debate {...this.props} {...data} />}
      />
		);
	}
}
