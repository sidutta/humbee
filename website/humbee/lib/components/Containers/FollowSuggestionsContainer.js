'use strict';


import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import FollowSuggestions from '../Login/FollowSuggestions';

import { ActivityIndicator } from 'react-native';

import colors from '../../config/colors';



import homeRoute from '../../routes/homeRoute';

export default class FollowSuggestionsContainer extends Component {
	constructor(props){
		super(props);
	}
	render() {
		return (
			<RootContainer
				Component={FollowSuggestions}
				route={new homeRoute()}
				renderFetched={(data) => <FollowSuggestions {...this.props} {...data} />}
				forceFetch={true}
				renderLoading={() => <ActivityIndicator
																animating={true}
																color={colors.humbeeGreen}
																style={{
																	alignItems: 'center',
																	justifyContent: 'center',
																	padding: 40,
																	height: 200}}
																size="large"/>}
			/>
		);
	}
}
