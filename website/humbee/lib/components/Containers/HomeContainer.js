'use strict';


import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import Home from '../Home';


import homeRoute from '../../routes/homeRoute';

export default class root extends Component {
	constructor(props){
		super(props);
	}
	render() {
		return (
			<RootContainer
				Component={Home}
				route={new homeRoute()}
				renderFetched={(data) => <Home {...this.props} {...data} />}
			/>
		);
	}
}
