'use strict';

import React, { Component } from 'react';
import { RootContainer } from 'react-relay';

import FullStory from '../DebateScreen/FullStory';

import debateRoute from '../../routes/debateRoute';

export default class DebateScreenContainer extends Component {
  render() {
    const { debateId, quizId } = this.props;
    return (
      <RootContainer
        Component={FullStory}
        route={new debateRoute({debateId})}
        renderFetched={(data) => <FullStory {...this.props} {...data} />}
      />
    );
  }
}
