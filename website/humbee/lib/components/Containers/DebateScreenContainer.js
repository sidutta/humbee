'use strict';

import React, { Component } from 'react';
import { RootContainer } from 'react-relay';
import  Relay  from 'react-relay';
import IsomorphicRelay from 'isomorphic-relay';

// import {environment} from '../../client';
import DebateScreen from '../DebateScreen';

import debateRoute from '../../routes/debateRoute';
import debateQuery from '../../queries/DebateQuery';

export default class DebateScreenContainer extends Component {
  render() {
    let { debateId } = this.props.params;
    console.log(this.props);
    return (
      <RootContainer
        Component={DebateScreen}
        route={new debateRoute({debateId})}
        renderFetched={(data) => <DebateScreen {...this.props} {...data} />}
      />
    );
  }
}

// export default class DebateScreenContainer extends Component {
//   render(){
//     let { debateId } = this.props.params;
//     return(
//       <IsomorphicRelay.Renderer
//         Container={DebateScreen}
//         queryConfig={new debateRoute({debateId})}
//         environment={this.props.environment}
//         render={({done, error, props, retry, stale}) => {
//           return <DebateScreen {...this.props} {...props} />
//         }}
//       />
//     );
//   }
// }

