'use strict';

import React, { Component } from 'react';
import {
	RootContainer
} from 'react-relay';

import DebateTitleCard from '../Common/DebateTitleCard';

import debateRoute from '../../routes/debateRoute';

export default class UserCardContainer extends Component {
	render() {
		const { debateId } = this.props;
		return (
			<RootContainer
				Component={DebateTitleCard}
				route={new debateRoute({debateId})}
				renderFetched={(data) => <DebateTitleCard {...this.props} {...data} />}
			/>
		);
	}
}
