'use strict';

import React, { Component } from 'react';
import {
  RootContainer
} from 'react-relay';

import QuizResultScreen from '../DebateScreen/Opinions/QuizResultScreen';

import quizResultRoute from '../../routes/quizResultRoute';

export default class QuizResultScreenContainer extends Component {
  render() {
    const { quizId } = this.props;
    return (
      <RootContainer
        Component={QuizResultScreen}
        route={new quizResultRoute({quizId})}
        renderFetched={ (data) => <QuizResultScreen {...this.props} {...data} /> }
      />
    );
  }
}
