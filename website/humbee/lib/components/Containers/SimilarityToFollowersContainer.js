'use strict';

import React, { Component } from 'react';
import {
  RootContainer
} from 'react-relay';

import SimilarityToFollowers from '../DebateScreen/Opinions/SimilarityToFollowers';

import similarityToFollowerRoute from '../../routes/similarityToFollowerRoute';

export default class FollowerSuggestionByQuizCardContainer extends Component {
  render() {
    const { subquizScoresArg } = this.props;
    return (
      <RootContainer
        Component={SimilarityToFollowers}
        route={new similarityToFollowerRoute({subquizScoresArg})}
        renderFetched={(data) => <SimilarityToFollowers {...this.props} {...data} />}
      />
    );
  }
}
