'use strict';

import React, { Component } from 'react';
import { RootContainer } from 'react-relay';

import QuizCard from '../Home/quiz/QuizCard';
import quizRoute from '../../routes/quizRoute';

export default class QuizCardContainer extends Component {
	render() {
		const { quizId } = this.props;
		return (
			<RootContainer
				Component={QuizCard}
				route={new quizRoute({quizId})}
				renderFetched={(data) => <QuizCard {...this.props} {...data} />}
      />
		);
	}
}
