import React from 'react';
if (process.env.BROWSER) {
  require("./App.css");
}
import Relay from 'react-relay';
import { Button, Image } from 'react-bootstrap';

import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import { LoginModal } from './LoginModal';
import { SignUpModal } from './SignUpModal';
import { UploadProfilePicModal } from './UploadProfilePicModal';

class LoginPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      mounted: "none"
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.switchToSignUpModal = this.switchToSignUpModal.bind(this);
    this.switchToLoginModal = this.switchToLoginModal.bind(this);
    this.switchToUploadProfilePicModal = this.switchToUploadProfilePicModal.bind(this);
  }

  getInitialState() {
    return { mounted: "none" };
  }

  componentDidMount() {
    if(this.props.location.pathname=='/login') {
      this.setState({ mounted: "login" });
    }
    else if(this.props.location.pathname=='/signup') {
      this.setState({ mounted: "signup" });
    } else if(this.props.location.pathname=='/uploadProfilePic') {
      this.setState({ mounted: "uploadProfilePic" });
    }
    else if(this.state.mounted == "none")
      this.setState({ mounted: "login" });
  }

  handleSubmit(e) {
    this.setState({ mounted: false });
    e.preventDefault();
  }

  switchToSignUpModal(e) {
    e.preventDefault();
    this.setState({ mounted: "signup" });
  }

  switchToLoginModal(e) {
    e.preventDefault();
    this.setState({ mounted: "login" });
  }

  switchToUploadProfilePicModal(id) {
    this.userid = id;
    // window.location.href = '/';
    this.setState({ mounted: "uploadProfilePic" });
  }

  render() {

    let marginTopForSignUpPage = {};
    var child;

    if(this.state.mounted == "login") {
      child = (<LoginModal onSubmit={this.handleSubmit} switchToSignUpModal={this.switchToSignUpModal} />);
    }
    else if(this.state.mounted == "signup") {
      child = (<SignUpModal switchToLoginModal={this.switchToLoginModal} switchToUploadProfilePicModal={this.switchToUploadProfilePicModal} />);
      marginTopForSignUpPage = {"marginTop": "80px"};
    }
    else if(this.state.mounted == "uploadProfilePic") {
      child = (<UploadProfilePicModal onSubmit={this.handleSubmit} userid={39} />);
      marginTopForSignUpPage = {"marginTop": "80px"};
    }

    return (
      <div className="bg-page">
        <div style={{"backgroundImage": "url('img/obama.jpg')"}} className="main">
          <div className="overlay"></div>
          <div className="container">
            <p className="social"><a href="#" title="" className="facebook"><i className="fa fa-facebook"></i></a><a href="#" title="" className="twitter"><i className="fa fa-twitter"></i></a><a href="#" title="" className="gplus"><i className="fa fa-google-plus"></i></a><a href="#" title="" className="instagram"><i className="fa fa-instagram"></i></a></p>
            <h1 className="cursive">HumBee</h1>
            <h2 className="sub">INDIA'S FIRST NETWORK FOR VOTERS</h2>
            <div className="mailing-list" style={marginTopForSignUpPage}>
              <div className="col-lg-4 col-lg-offset-4">
                <ReactCSSTransitionGroup
                  transitionName="example"
                  transitionEnterTimeout={500}
                  transitionLeaveTimeout={300}>
                  {child}
                </ReactCSSTransitionGroup>
                {
                  this.state.mounted == "uploadProfilePic"
                  ?
                  <div />
                  :
                  <Button className="btn btn-default" style={{"backgroundColor":"#2c235f", "borderColor":"#150f38", "width":"auto", "margin":"auto", "marginTop":"20px"}}>
                    <Image src="img/fb-white.png" style={{"height":"20px","width":"20px", "verticalAlign":"top"}} rounded />
                    LOGIN WITH FACEBOOK
                  </Button>
                }
              </div>
              <div id="thanks" style={{"display":"none"}}><h4 className="mailing-list-heading">Thank you!</h4></div>
              <div id="validation_err" style={{"display":"none"}}><h5 className="mailing-list-heading">Please enter a valid email address</h5></div>
            </div>
          </div>
          <div className="footer">
            <div className="container">
              <div className="row">
                <div className="col-md-6">
                  <p></p>
                </div>
                <div className="col-md-6">
                  <p className="credit"></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Relay.createContainer(LoginPage, {
  fragments: {
    // viewer: () => Relay.QL`
    //   fragment on Viewer {
    //     id
    //   }
    // `,
  },
});
