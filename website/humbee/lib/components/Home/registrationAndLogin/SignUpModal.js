import React from 'react';
if (process.env.BROWSER) {
  require("./App.css");
}
import Relay from 'react-relay';

import SignupMutation from '../../../mutations/SignupMutation';

import { Input } from './Input';
import { postXHR } from '../../../utils/XHR';

export class SignUpModal extends React.Component {

  constructor(props) {
    super(props);
    this.firstname = '';
    this.lastname = '';
    this.username = '';
    this.email = '';
    this.password = '';
    this.state = {
      errors: ''
    }
    this.onFirstNameChange = this.onFirstNameChange.bind(this);
    this.onLastNameChange = this.onLastNameChange.bind(this);
    this.onEmailChange = this.onEmailChange.bind(this);
    this.onUsernameChange = this.onUsernameChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
    this.validatePassword = this.validatePassword.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onFirstNameChange(e) {
    this.firstname = e.target.value;
  }

  onLastNameChange(e) {
    this.lastname = e.target.value;
  }

  onEmailChange(e) {
    this.email = e.target.value;
  }

  onUsernameChange(e) {
    this.username = e.target.value;
  }

  onPasswordChange(e) {
    this.password = e.target.value;
  }

  validateEmail(email)  {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.setState({ errors : '' });
    return re.test(email);
  };

  validatePassword(password) {
    let length = password.length;
    if(length < 8) {
      return {valid:false, err:"Password must have atleast 8 characters"};
    }
    let hasLetter = false;
    let hasNum = false;
    let hasSpclChar = false;
    for(let i=0; i<length; i++) {
      let x = password.charCodeAt(i);
      if((x>64&&x<91)||(x>96&&x<123)||(x>127&&x<155)||(x>159&&x<166)) {
        hasLetter = true;
      }
      else if(x>47&&x<58) {
        hasNum = true;
      }
      else {
        hasSpclChar = true;
      }
    }
    if(hasNum&&hasLetter&&hasSpclChar) {
      this.setState({ errors : "" });
    }
    return {valid:hasNum&&hasLetter&&hasSpclChar, err:"Password must have atleast one alphabet, number and special character"};
  };

  onSignUpSuccess( response )
  {
    try{
      let responseJSON = JSON.parse( response );
      if( responseJSON.success != true ) {
        this.setState({ errors : responseJSON.errors });
        return;
      }
    }
    catch( err ) {
      this.setState({ errors : "New User Creation failed" });
      return;
    }

    location.replace( location.href );
  }

  onSignUpFailure( response )
  {
    let message;
    try{
      let responseJSON = JSON.parse( response );
      message = responseJSON.error;
      this.setState({ errors : "New User Creation failed" });
    }
    catch( err ) {
      this.setState({ errors : "New User Creation failed" });
    }
  }

  onSubmit(e) {
    e.preventDefault();
    const {username,email,firstname,lastname,password} = this;
    if(!this.validateEmail(email)) {
      let errorState = "Please enter a valid email id!";
      this.setState({ errors : errorState });
      return;
    }
    let pwdValid = this.validatePassword(password);
    if(!(pwdValid.valid)) {
      let errorState = pwdValid.err;
      this.setState({ errors : errorState });
      return;
    }

    var loc = window.location;
    var host = loc.protocol + "//" + loc.hostname + ":" + loc.port;

    postXHR(
      host + '/auth/createuser',
      {
        email:    email,
        password: password,
        firstName: firstname,
        lastName: lastname,
        username: username,
        client: 'web'
      },
      (response) => this.onSignUpSuccess(response),
      (response) => this.onSignUpFailure(response)
    );

  }

  render() {
    return (
      <div className="Modal" style={{"margin": "auto", "paddingBottom": "10px"}}>
        <form
          onSubmit={this.props.onSubmit}
          className="ModalForm">
          <Input
            id="firstname"
            type="text"
            placeholder="First Name"
            onChange={this.onFirstNameChange} />
          <Input
            id="lastname"
            type="text"
            placeholder="Last Name"
            onChange={this.onLastNameChange} />
          <Input
            id="username"
            type="text"
            placeholder="username"
            onChange={this.onUsernameChange} />
          <Input
            id="email"
            type="email"
            placeholder="email id"
            onChange={this.onEmailChange} />
          <Input
            id="password"
            type="password"
            placeholder="password"
            onChange={this.onPasswordChange} />
          <div style={{"color":"red"}}>
            {this.state.errors}
          </div>
          <div className="row">
            <button onClick={this.onSubmit} style={{"display":"inline", "width":"auto"}}>
              Sign up
            </button>
            <button onClick={this.props.switchToLoginModal} style={{"display":"inline", "margin-left":"15px", "width":"auto"}}>
              Log in
            </button>
          </div>
          <div style={{"color":"rgb(46, 191, 168)", "marginTop": "10px"}}>
            Forgot Password?
          </div>
        </form>
      </div>
    )
  }
}
