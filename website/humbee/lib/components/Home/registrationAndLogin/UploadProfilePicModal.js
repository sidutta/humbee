import React from 'react';
if (process.env.BROWSER) {
  require("./App.css");
}
import Relay from 'react-relay';

import {  FormGroup,
          ControlLabel,
          FormControl,
          HelpBlock } from 'react-bootstrap';

import SetProfilePictureMutation from '../../../mutations/SetProfilePictureMutation';

import AvatarEditor from 'react-avatar-editor'

function FieldGroup({ id, label, help, ...props }) {
  return (
    <FormGroup controlId={id} style={{"marginBottom": "0px", "paddingBottom": "0px", "marginTop": "0px"}}>
      <ControlLabel style={{"marginTop": "0px"}}>{label}</ControlLabel>
      <FormControl {...props} />
      {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
  );
}

export class UploadProfilePicModal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      errors: '',
      image: 'https://storage.googleapis.com/humbee_images/cartoon-bee-1.jpg',
      picture : {
        data_uri : null,
        filename : '',
        fileType : ''
      }
    }
    this.onSubmit = this.onSubmit.bind(this);
    this.onSkip = this.onSkip.bind(this);
    this.handleFile = this.handleFile.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    const { image } = this.state;
    const { userid } = this.props;
    Relay.Store.commitUpdate(
      new SetProfilePictureMutation({
        id : userid,
        image : image
      }),
      {
        onFailure : (transaction) => {
          console.log(transaction.getError())
          console.log('na')
        },
        onSuccess : (response) => {
          console.log('yay')
        }
      }
    );
  }

  onSkip() {
    const { image } = this.state;
    const { userid } = this.props;
    Relay.Store.commitUpdate(
      new SetProfilePictureMutation({
        id : userid,
        image : 'skip'
      }),
      {
        onFailure : (transaction) => {
          console.log(transaction.getError())
          console.log('na')
        },
        onSuccess : (response) => {
          console.log('yay')
        }
      }
    );
  }

  setEditorRef (editor) {
    if (editor) this.editor = editor
  }

  handleFile(e) {
    const reader = new FileReader();
    const file = e.target.files[0];

    reader.onload = (upload) => {
      this.setState( {
        picture : {
          data_uri : upload.target.result,
          filename : file.name,
          fileType : file.type
        },
        image : upload.target.result
      } );
    };
    reader.readAsDataURL(file);
  }

  render() {
    let imageurl = this.state.image;
    return (
      <div className="Modal" style={{"margin": "auto", "paddingBottom": "10px"}}>
        <form
          onSubmit={this.props.onSubmit}
          className="ModalForm">
          <AvatarEditor
                    ref={this.setEditorRef.bind(this)}
                    image={imageurl}
                    width={250}
                    height={250}
                    border={10}
                    scale={1.2}
                  />
          <div style={{"color":"grey"}}>
            Drag a picture above or
          </div>
          <button style={{"marginTop":"10px", "width":"auto", "margin":"auto"}}>
            <FieldGroup
              id="formControlsFile"
              type="file"
              label="Click Here"
              help="Choose file to upload as image."
              onChange={this.handleFile} />
          </button>
          <div style={{"color":"red"}}>
            {this.state.errors}
          </div>
          <div className="row">
            <button onClick={()=>this.onSubmit()} style={{"display":"inline", "width":"auto"}}>
              Submit
            </button>
          </div>
          <div onClick={()=>this.onSkip()} style={{"color":"rgb(46, 191, 168)", "marginTop": "10px"}}>
            <a>Skip this step</a>
          </div>
        </form>
      </div>
    )
  }
}
