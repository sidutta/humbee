import React from 'react';

if (process.env.BROWSER) {
  require("./App.css");
}

export var Input = React.createClass({
  render: function() {
    return (
      <div className="Input">
        <input
          id={this.props.name}
          autoComplete="true"
          type={this.props.type}
          placeholder={this.props.placeholder}
          onChange={this.props.onChange}
        />
        <label htmlFor={this.props.name}></label>
      </div>
    );
  }
});
