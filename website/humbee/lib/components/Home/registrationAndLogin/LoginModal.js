import React from 'react';
if (process.env.BROWSER) {
  require("./App.css");
}
import Relay from 'react-relay';

import { Input } from './Input';
import { postXHR } from '../../../utils/XHR';

export class LoginModal extends React.Component {

  constructor(props) {
    super(props);
    this.email = '';
    this.password = '';
    this.state = {
      errors: ''
    }
    this.onEmailChange = this.onEmailChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onSignUpSuccess = this.onSignUpSuccess.bind(this);
    this.onSignUpFailure = this.onSignUpFailure.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onEmailChange(e) {
    this.email = e.target.value;
  }

  onPasswordChange(e) {
    this.password = e.target.value;
  }

  onSignUpSuccess( response )
  {
    try{
      let responseJSON = JSON.parse( response );
      if( responseJSON.success != true ) {
        this.setState({ errors : responseJSON.errors });
        return;
      }
    }
    catch( err ) {
      this.setState({ errors : "New User Creation failed" });
      return;
    }

    location.replace( location.href );
  }

  onSignUpFailure( response )
  {
    let message;
    try{
      let responseJSON = JSON.parse( response );
      message = responseJSON.error;
      this.setState({ errors : "New User Creation failed" });
    }
    catch( err ) {
      this.setState({ errors : "New User Creation failed" });
    }
  }

  onSubmit(e) {
    e.preventDefault();
    const {email, password} = this;

    var loc = window.location;
    var host = loc.protocol + "//" + loc.hostname + ":" + loc.port;

    postXHR(
      host + '/auth/login',
      {
        email:    email,
        password: password,
      },
      (response) => this.onSignUpSuccess(response),
      (response) => this.onSignUpFailure(response)
    );
    // this.props.onSubmit();
  }

  render() {
    return (
      <div className="Modal" style={{"margin": "auto", "paddingBottom": "10px"}}>
        <form
          className="ModalForm">
          <Input
            id="email"
            type="email"
            placeholder="email id/ username"
            onChange={this.onEmailChange} />
          <Input
            id="password"
            type="password"
            placeholder="password"
            onChange={this.onPasswordChange} />
          <div style={{"color":"red"}}>
            {this.state.errors}
          </div>
          <div className="row">
            <button onClick={this.onSubmit} style={{"display":"inline", "width":"auto"}}>
              Log in
            </button>
            <button onClick={this.props.switchToSignUpModal} style={{"display":"inline", "margin-left":"15px", "width":"auto"}} formnovalidate={true}>
              Sign up
            </button>
          </div>
          <div style={{"color":"rgb(46, 191, 168)", "marginTop": "10px"}}>
            Forgot Password?
          </div>
        </form>
      </div>
    )
  }
}
