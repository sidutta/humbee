import React from 'react';
import Relay from 'react-relay';

import HumbeeNavbar from '../HumbeeNavbar';

class NavHeader extends React.Component {
  render() {
    return (
      <div>
        <HumbeeNavbar viewer={this.props.viewer} />
        <div style={{"paddingTop":"70px"}}> {/* padding needed o/w fixedTop navbar causes body to hide usnder it even on start */}
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Relay.createContainer(NavHeader, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        user {
          id,
          role,
        },
        ${HumbeeNavbar.getFragment('viewer')}
      }
    `,
  },
});
