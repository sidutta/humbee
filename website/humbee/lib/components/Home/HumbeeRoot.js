import React from 'react';
import Relay from 'react-relay';

import LoginPage from './registrationAndLogin/LoginPage';
import HumbeeHome from './HumbeeHome';
import NavHeader from './NavHeader';

class HumbeeRoot extends React.Component {
  render() {
    let isAnon = (this.props.viewer.user.role=='AnonymousUser');
    return (
      isAnon
      ?
      <LoginPage {...this.props} />
      :
      <NavHeader viewer={this.props.viewer}>
        <HumbeeHome viewer={this.props.viewer} />
      </NavHeader>
    );
  }
}

export default Relay.createContainer(HumbeeRoot, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        user {
          id,
          role
        },
        ${NavHeader.getFragment('viewer')}
        ${HumbeeHome.getFragment('viewer')}
      }
    `,
  },
});
