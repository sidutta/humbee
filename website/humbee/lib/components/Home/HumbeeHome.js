import React from 'react';
import Relay from 'react-relay';

import Waypoint from 'react-waypoint';

import DebateCardContainer from '../Containers/DebateCardContainer';
import QuizCardContainer from '../Containers/QuizCardContainer';
// import ArticleCardContainer from '../Containers/ArticleCardContainer';
// import VideoCardContainer from '../Containers/VideoCardContainer';
import TakeQuizBox from '../common/TakeQuizBox';
import FollowerSuggestionBox from '../common/FollowerSuggestionBox';
import HumbeeHelpBox from '../common/HumbeeHelpBox';

class HumbeeHome extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      pageSize: 10
    };
    this._loadMoreItems = this._loadMoreItems.bind(this);
    this._renderWaypoint = this._renderWaypoint.bind(this);
  }

  _loadMoreItems() {
    let {pageSize}  = this.state;
    pageSize = pageSize + 10;
    this.props.relay.setVariables({pageSize});
    this.setState({pageSize : pageSize, isLoading : true});
  }

  _renderWaypoint() {
    const { newsFeedItems } = this.props.viewer;
    const { hasNextPage } = newsFeedItems.pageInfo;
    if (!this.state.isLoading && hasNextPage) {
      return (
        <Waypoint
          onEnter={this._loadMoreItems}
          threshold={2.0}
        />
      );
    }
  }

  render() {
    const { newsFeedItems } = this.props.viewer;
    const { hasNextPage } = newsFeedItems.pageInfo;
    const cards = newsFeedItems.edges.map( (item, index) => {
      const { node } = item;
      switch(node.type){
      case 'DebateType':
        return <DebateCardContainer debateId={node.debateId} key={index} />;
      // case 'VideoType':
      //   return <VideoCardContainer videoId={node.videoId} key={index} />;
      // case 'ArticleType':
      //   return <ArticleCardContainer articleId={node.articleId} key={index} />;
      case 'QuizType':
        return <QuizCardContainer quizId={node.quizId} key={index} />;
      }
    } );

    return (
      <div className="row">
        <div className="col-xs-hidden col-sm-3 col-md-2 col-lg-2">
        </div>
        <div className="col-xs-12 col-sm-6 col-md-5 col-lg-5">
          {cards}

          {this._renderWaypoint()}

          {
            hasNextPage &&
            <div style={{'textAlign': 'center'}}>
              Loading more items…
            </div>
          }
        </div>
        <div className="col-xs-hidden col-sm-3 col-md-3 col-lg-3">

          <TakeQuizBox />

          <FollowerSuggestionBox />

          <HumbeeHelpBox />

          <div className="col-xs-hidden col-sm-hidden col-md-2 col-lg-2">
          </div>
        </div>

      </div>
    );
  }
}

export default Relay.createContainer(HumbeeHome, {
  initialVariables : {
    type : 'all',
    pageSize : 10
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        user{
          id
        },
        newsFeedItems(first : $pageSize, type : $type, community : 0){
          pageInfo{
            hasNextPage
          },
          edges{
            node{
              type,
              videoId,
              debateId,
              articleId,
              quizId
            }
          }
        }
      }
    `
  },
});
