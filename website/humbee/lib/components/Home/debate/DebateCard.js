import React from 'react';
import Relay from 'react-relay';

import { Image, Button } from 'react-bootstrap';

import Flex from 'react-uikit-flex';
import { LinkContainer } from 'react-router-bootstrap';

import CreatePositionMutation from '../../../mutations/CreatePositionMutation';

class DebateCard extends React.Component {

  constructor(props) {
    super(props);
    this.createPosition = this.createPosition.bind(this);
  }

  createPosition(agree) {
    Relay.Store.commitUpdate(
      new CreatePositionMutation({
        debateId : this.props.viewer.id,
        agree : agree
      }),{
        onFailure : () => {
          alert('Oops something went wrong. Please try again.');
        },
        onSuccess : () => {
          ;
        }
      }
    );
  }

  render() {
    const { viewer } = this.props;
    const { title, motionShort, motionFull, imageUrl, userposition} = viewer;
    return (
      <div style={{"margin":"10px","padding":"5px","flexDirection":"column","backgroundColor":"#ffffff","flexWrap":"wrap","borderRadius":"5px"}}>
        <div style={{"position":"relative"}}>
          <img src={imageUrl} className="align-center img-responsive" />
          <div style={{"position":"absolute","zIndex":"1","top":"0","left":"0", "height": "100%"}} className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <Flex direction='column' style={{ "height": "100%"}}>
              <div style={{ "flex": "1"}}></div>
              <div style={{ "flex": "1"}}></div>
              <div className="debate-caption">
                <p>{title}: {motionShort}</p>
              </div>
              <div style={{ "flex": "1"}}></div>
              <div style={{ "flex": "1"}}></div>
            </Flex>
          </div>
        </div>
        <div className="other-box">
          <div className="Motion">{motionFull}</div>
          <div className="seperator"> </div>
            {
              (userposition!=null) ?
              <Flex row='around' className="agree-disagree">
                <div style={{"flex":7}}>
                  <div className="progress">
                    <div className="progress-bar progress-bar-success" style={{"width":"75%"}}></div>
                    <div className="progress-bar progress-bar-danger" style={{"width":"25%"}}></div>
                  </div>
                  <div className="label-agree-disagree">
                    <div className="agree-label"> 75% agree</div>
                    <div className="disagree-label"> 25% agree</div>
                  </div>
                </div>
                <div style={{"flex":1}} />
                <LinkContainer to={{ pathname: '/debate/' + this.props.viewer.id + '/fullStory' }}>
                  <Button bsStyle="primary" style={{"flex":2, "margin":"0px"}}>See Debate</Button>
                </LinkContainer>
              </Flex>
              :
              <div style={{textAlign: "right"}}>
                <Button bsStyle="success" onClick={() => this.createPosition(true)}>+ Agree</Button>
                <Button bsStyle="danger" onClick={() => this.createPosition(false)}>- Disagree</Button>
              </div>
            }
          <div className="seperator"> </div>
          <div className="action-bar">
            <div className="like">
              <a href="#">
                <span className="material-icons action-icons">thumb_up</span>
              </a>
            </div>
            <div className="bookmark">
              <a href="#">
                <span className="material-icons action-icons">bookmark</span>
              </a>
            </div>
            <div className="share">
              <a href="#">
                <span className="material-icons action-icons">share</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Relay.createContainer(DebateCard, {
  fragments : {
    viewer : () => Relay.QL`
      fragment on Debate {
        id,
        quizId,
        title,
        userposition,
        motionFull,
        motionShort,
        imageUrl,
        agree,
        disagree,
        numberOfLikes
        
      }
    `
  },
});
