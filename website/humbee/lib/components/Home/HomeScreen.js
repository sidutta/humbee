import React from 'react';
import Relay from 'react-relay';

class HomeScreen extends React.Component {
  render() {
    return (
      <div className="bg-page">
        {this.props.children}
      </div>
    );
  }
}

export default Relay.createContainer(HomeScreen, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        user {
          id,
          role
        },
      }
    `,
  },
});
