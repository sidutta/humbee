import React from 'react';
import Relay from 'react-relay';

class VideoCard extends React.Component {
  render() {
    return (
        <div style={{ "margin": "10px", "padding": "5px", "flexDirection": "column", "backgroundColor": "#ffffff", "flexWrap": "wrap", "borderRadius": "5px"}}>
          <div className="video-title">
            <div> John Oliver's take on the Indian Elections</div>
          </div>
          <div style={{textAlign: "center"}}>
            <iframe width="80%" height="auto" className="video-link" src="https://www.youtube.com/embed/D8Ymd-OCucs" allowFullScreen="allowFullScreen">
            </iframe>
          </div>
          <div className="video-description">
            <div> Watch John Oliver's hilarious take on Modi and Rahul Gandhi and all the election drama in the ongoing election campaign in India! </div>
          </div>
          <div className="seperator"> </div>
          <div className="video-other-box">
            <div className="video-bar">
              <div className="video-like">
                <a href="#">
                  <span className="material-icons action-icons">thumb_up</span>
                </a>
              </div>
              <div className="video-comment">
                <a href="#">
                  <span className="material-icons action-icons">comment</span>
                </a>
              </div>
              <div className="video-share">
                <a href="#">
                  <span className="material-icons action-icons">share</span>
                </a>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default Relay.createContainer(VideoCard, {
  fragments: {
    // viewer: () => Relay.QL`
    // fragment on User {
    //   id
    // }
    // `,
  },
});
