import React from 'react';
import Relay from 'react-relay';

class ArticleCard extends React.Component {
  render() {
    return (
      <div className="flex-container" id="video-container">
        <div className="video-box">
          <div className="video-title">
            <div> John Oliver's take on the Indian Elections</div>
          </div>
          <iframe className="video-link" src="https://www.youtube.com/embed/P12dpREC8QY?" allowFullScreen="allowFullScreen">
          </iframe>
          <div className="video-description">
            <div> Watch John Oliver's hilarious take on Modi and Rahul Gandhi and all the election drama in the ongoing election campaign in India! </div>
          </div>
          <div className="seperator"> </div>
          <div className="video-other-box">
            <div className="video-bar">
              <div className="video-like">
                <a href="#">
                  <span className="material-icons action-icons">thumb_up</span>
                </a>
              </div>
              <div className="video-comment">
                <a href="#">
                  <span className="material-icons action-icons">comment</span>
                </a>
              </div>
              <div className="video-share">
                <a href="#">
                  <span className="material-icons action-icons">share</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Relay.createContainer(ArticleCard, {
  fragments: {
    // viewer: () => Relay.QL`
    // fragment on User {
    //   id
    // }
    // `,
  },
});
