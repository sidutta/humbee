import React from 'react';
import Relay from 'react-relay';

import { Row } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

import Flex from 'react-uikit-flex';

class QuizCard extends React.Component {

  render() {

    const { title } = this.props.viewer;

    return (
      <div style={{ "margin": "10px", "padding": "5px", "flexDirection": "column", "backgroundColor": "#ffffff", "flexWrap": "wrap", "borderRadius": "5px"}}>
        <div style={{ "position": "relative"}}>
          <img src="img/trump_hillary.jpg" className="align-center img-responsive" />
          <div style={{ "position": "absolute", "zIndex": "1", "top": "0", "left": "0", "height": "100%", "paddingBottom": "10px"}} className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <Flex direction='column' style={{ "height": "100%"}}>
              <div style={{ "flex": "1"}}></div>
              <div style={{ "flex": "1"}}></div>
              <div className="quiz-caption">
                <p>{title}</p>
              </div>
              <div style={{ "flex": "1"}}></div>
              <div style={{textAlign: "right", "flex": "1"}}>
                <LinkContainer to={{ pathname: '/quiz/' + this.props.viewer.id, query: { quizId: this.props.viewer.quizId } }}>
                  <button type="button" className="btn btn-primary" id="quiz_button">Take Quiz</button>
                </LinkContainer>
              </div>
            </Flex>
          </div>
        </div>
        <div className="quiz-other-box">
          <div className="action-bar" style={{ "padding": "5px"}}>
            <div className="quiz-like">
              <a href="#">
                <span className="material-icons action-icons">thumb_up</span>
              </a>
            </div>
            <div className="quiz-comment">
              <a href="#">
                <span className="material-icons action-icons">comment</span>
              </a>
            </div>
            <div className="quiz-share">
              <a href="#">
                <span className="material-icons action-icons">share</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Relay.createContainer(QuizCard, {
  fragments: {
    viewer : () => Relay.QL`
      fragment on Quiz {
        title,
        imageUrl,
        numberOfLikes,
        liked,
        id
      }
    `
  },
});
