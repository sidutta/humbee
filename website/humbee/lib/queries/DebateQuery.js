import Relay from 'react-relay';

export default {
  viewer: (Component) => Relay.QL`query {
    node(id : $debateId) {
      ${Component.getFragment('viewer')}
    }
  }`
};
