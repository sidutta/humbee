import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateInput(data) {
	let errors = {};

	if(Validator.isEmpty(data.email)) {
		errors.email = 'Email is required';
	}


	if(!Validator.isEmail(data.email)) {
		errors.email = 'invalid email';
	}


	if(Validator.isEmpty(data.username)) {
		errors.username = 'username is required';
	}

	if(Validator.isEmpty(data.firstName)) {
		errors.firstName = 'First name is required';
	}


	if(Validator.isEmpty(data.password)) {
		errors.password = 'password is required';
	}

	return {
		errors,
		isValid : isEmpty(errors)
	};
}
