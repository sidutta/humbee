// Change this to use different service/transport-method of sending mails.
import {transport} from './pepipostNodemailerSmptTransport';

export default async function sendEmail(to, subject, htmlContent) {
  var message = {
      from: 'Humbee <no-reply@humbee.in>',
      to: '<' + to + '>',
      subject: subject,
      html: htmlContent
  };

  // TODO have some retry mechanism in case of failures
  transport.sendMail(message, function(error){
    if(error){
        console.error(error.message);
        return;
    }
  });
}
