import express from 'express';
import cookieParser from 'cookie-parser';
import compression from 'compression';
import graphQLHTTP from 'express-graphql';
import path from 'path';
import renderOnServer from './renderOnServer'
import Schema from './data/schema';

import models from './data/models';
import redis from './data/redis';

import auth from './auth'; // Authentication server

import isUser from './utils/isUser';

delete process.env.BROWSER; // done for importing css

const APP_PORT = 8080;
let GRAPHQL_PORT = 3000;

let graphQLServer = express();
graphQLServer.use('/graphql',
  (req, res, next) => {
    return graphQLHTTP({
      schema: Schema,
      pretty: true,
      graphiql: true,
      context: {db : models, redis: redis, req, res, next, ...isUser('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlcm5hbWUiOiJqaGFuc2kiLCJyb2xlIjoiVXNlciIsImZpcnN0TmFtZSI6IkpoYW5zaSIsImxhc3ROYW1lIjoiRWxhbmdvIiwiZW1haWwiOiJzcGFya2xpbmdqaGFuc2lAaG90bWFpbC5jb20iLCJwcm9maWxlUGljIjoiaHR0cHM6Ly9ncmFwaC5mYWNlYm9vay5jb20vMTA3MjA2ODMxOTU0NDE5NS9waWN0dXJlP3R5cGU9bGFyZ2UiLCJpYXQiOjE0ODUyODc2NDd9.XW3nQSJ-WO0UmvTMWrBUPmPfHBrF2JkP2MiEpCGT6Hc', req, res)}
    })(req, res, next)
  }
);

graphQLServer.listen(GRAPHQL_PORT, () => console.log(
    `GraphQL Server is now running on http://localhost:${GRAPHQL_PORT}`
  )
);

var app = express();

app.use( function ( req, res, next ) {
  // Website you wish to allow to connect
  res.setHeader( 'Access-Control-Allow-Origin', '*' )
  // Request methods you wish to allow
  res.setHeader( 'Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE' )
  // Request headers you wish to allow
  res.setHeader( 'Access-Control-Allow-Headers', 'X-Requested-With,content-type' )
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader( 'Access-Control-Allow-Credentials', true )
  // Pass to next layer of middleware
  next()
} )

// have to check the meaning of the following
app.set( 'trust proxy', 'loopback' );

app.disable( 'x-powered-by' );

app.use( compression() );
app.use( cookieParser() );

// Expose a GraphQL endpoint
app.use('/graphql', (req, res, next) => {
  return graphQLHTTP({
    schema: Schema,
    pretty: true,
    context: {db : models, redis: redis, req, res, next, ...isUser(null, req, res)}
  })(req, res, next)
});

app.use('/auth', auth);

// Serve CSS
app.use('/css/', express.static(path.resolve(__dirname, '..', 'css')));
app.use('/fonts/', express.static(path.resolve(__dirname, '..', 'fonts')));
app.use('/img/', express.static(path.resolve(__dirname, '..', 'img')));
app.use('/js/', express.static(path.resolve(__dirname, '..', 'js')));
app.use('/lib/', express.static(path.resolve(__dirname)));
// Serve JavaScript
// app.get('/app.js', (req, res) => {
//   console.log('2');
//     res.setHeader('Content-Type', 'application/javascript');
//     res.sendFile('app.js', {root: __dirname});
// });

// Serve HTML
app.get('/*', (req, res, next) => {
  renderOnServer(req, res, next);
});

models.sequelize.sync().then( () => {
  app.listen(APP_PORT, () => { console.log(`App is now running on http://localhost:${APP_PORT}`); });
})
