import React from 'react'
import { createRoutes, IndexRoute, Route } from 'react-router'

import HumbeeRoot from './components/Home/HumbeeRoot';
import HomeScreen from './components/Home/HomeScreen';
import HumbeeHome from './components/Home/HumbeeHome';
import ViewerProfile from './components/ViewerProfile';
import ReasonCards from './components/ViewerProfile/ReasonCards';
// import QuizCards from './components/ViewerProfile/QuizCards';
// import FollowerCards from './components/ViewerProfile/FollowerCards';
// import FollowingCards from './components/ViewerProfile/FollowingCards';
import NavHeader from './components/Home/NavHeader';
import LoginPage from './components/Home/registrationAndLogin/LoginPage';
import DebateScreen from './components/DebateScreen';
import DebateScreenContainer from './components/Containers/DebateScreenContainer';
import QuizScreenContainer from './components/Containers/QuizScreenContainer';
import Debate from './components/DebateScreen/Debate';
import FullStory from './components/DebateScreen/FullStory';
import Opinions from './components/DebateScreen/Opinions';
import ViewerQueries from './queries/ViewerQueries';
import DebateQuery from './queries/DebateQuery';
import ContentRecommendation from './components/DebateScreen/Opinions/ContentRecommendation';

export default createRoutes(
  <Route path="/" component={HomeScreen} queries={ViewerQueries} prepareParams={(params) => {return {...params}}} >
    <IndexRoute component={HumbeeRoot} queries={ViewerQueries} prepareParams={(params) => {return {...params}}} />

    <Route path="login">
      <IndexRoute component={LoginPage} />
    </Route>

    <Route path="signup">
      <IndexRoute component={LoginPage} />
    </Route>

    <Route path="uploadProfilePic">
      <IndexRoute component={LoginPage} />
    </Route>

    <Route component={NavHeader} queries={ViewerQueries} prepareParams={(params) => {return {...params}}} >
      <Route path="debate">
        <Route path=":debateId">
          <IndexRoute component={DebateScreen} queries={DebateQuery} prepareParams={(params) => {return {...params, tabIndex: 0}}} />
          <Route path="fullStory" component={DebateScreen} queries={DebateQuery} prepareParams={(params) => {return {...params, tabIndex: 0}}} />
          <Route path="opinion" component={DebateScreen} queries={DebateQuery} prepareParams={(params) => {return {...params, tabIndex: 1}}} />
          <Route path="discuss" component={DebateScreen} queries={DebateQuery} prepareParams={(params) => {return {...params, tabIndex: 2}}} />
        </Route>
      </Route>

      <Route path="quiz">
        <Route path=":quizId" component={QuizScreenContainer} prepareParams={(params) => {return {...params}}} />
      </Route>

      <Route path="contentRecommendation">
        <Route path=":quizId" component={ContentRecommendation} />
      </Route>

      <Route path="profile">
        <Route path=":userId" component={ViewerProfile} queries={ViewerQueries}>
          <IndexRoute component={ReasonCards} queries={ViewerQueries} prepareParams={(params) => {return {...params, tabIndex: 0}}} />
          {/*<Route path="quizzes" component={QuizCards} queries={ViewerQueries} prepareParams={(params) => {return {...params, tabIndex: 1}}} />
          <Route path="followers" component={FollowerCards} queries={ViewerQueries} prepareParams={(params) => {return {...params, tabIndex: 2}}} />
          <Route path="followings" component={FollowingCards} queries={DebateQuery} prepareParams={(params) => {return {...params, tabIndex: 3}}} />
        */}
        </Route>
      </Route>
    </Route>
  </Route>
)
