var path = require('path');
var webpack = require('webpack');


// definePlugin takes raw strings and inserts them, so you can put strings of JS if you want.
var definePlugin = new webpack.DefinePlugin({
  __DEV__: JSON.stringify(JSON.parse(process.env.BUILD_DEV || 'true')),
  __PRERELEASE__: JSON.stringify(JSON.parse(process.env.BUILD_PRERELEASE || 'false'))
});

var setBrowserTruePlugin = new webpack.DefinePlugin({
        "process.env": {
            BROWSER: JSON.stringify(true)
        }
    })

module.exports = {
    entry: ['babel-polyfill', 'whatwg-fetch', path.resolve(__dirname, 'lib', 'client.js')],
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, 'lib'),
    },
    plugins: [definePlugin, setBrowserTruePlugin],
    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
          query: {
              presets: ['es2015']
          }
        },
        {
          test: /\.js$/,
          loaders: ['babel'],
          exclude: /node_modules/,
        },
        {
          test: /\.js$/,
          include: [
            path.join(__dirname, 'client'),
            path.join(__dirname, 'server/shared')
          ],
          loaders: [ 'babel']
        },
        {
          test: /\.css$/,
          loader: 'style-loader!css-loader'
        },
        {
          test: require.resolve('react'),
          loader: 'expose-loader?React',
          query: 'expose-loader?React',
        },
      ]
    },
};
