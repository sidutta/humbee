$(function () {

    utils();
    demo();

});

function btnClick() {

    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    var rand = function() {
        return Math.random().toString(36).substr(2); // remove `0.`
    };

    var token = function() {
        return rand() + rand(); // to make it longer
    };

    $("#subscribeBtn").button().click(function(){
        if(emailReg.test($("#email").val())) {
          $("#sub_form").hide();
          $('#validation_err').hide();
          $('#thanks').show();
        }
        else {
            $('#validation_err').show();
            setTimeout(function() {$('#validation_err').hide();}, 2000);
        }
    });

    $('#email').keypress(function (e) {
       if (e.which == 13) {
          e.preventDefault();
          if(emailReg.test($("#email").val())) {
              $("#sub_form").hide();
              $('#validation_err').hide();
              $('#thanks').show();
          }
          else {
            $('#validation_err').show();
            setTimeout(function() {$('#validation_err').hide();}, 2000);
          }
       }
    });

    $('#subscribeBtn').keypress(function (e) {
       if (e.which == 13) {
          e.preventDefault();
          if(emailReg.test($("#email").val())) {
              $("#sub_form").hide();
              $('#validation_err').hide();
              $('#thanks').show();
          }
          else {
            $('#validation_err').show();
            setTimeout(function() {$('#validation_err').hide();}, 2000);
          }
       }
    });
}


/* for demo purpose only - can be deleted */

function demo() {

    $("#page").change(function () {

        if ($(this).val() !== '') {

            window.location.href = $(this).val();

        }

        return false;
    });
}

function utils() {

    /* tooltips */

    $('[data-toggle="tooltip"]').tooltip();

    /* click on the box activates the radio */

    $('#checkout').on('click', '.box.shipping-method, .box.payment-method', function (e) {
        var radio = $(this).find(':radio');
        radio.prop('checked', true);
    });
    /* click on the box activates the link in it */

    $('.box.clickable').on('click', function (e) {

        window.location = $(this).find('a').attr('href');
    });
    /* external links in new window*/

    $('.external').on('click', function (e) {

        e.preventDefault();
        window.open($(this).attr("href"));
    });
    /* animated scrolling */

    $('.scroll-to, .scroll-to-top').click(function (event) {

        var full_url = this.href;
        var parts = full_url.split("#");
        if (parts.length > 1) {

            scrollTo(full_url);
            event.preventDefault();
        }
    });
    function scrollTo(full_url) {
        var parts = full_url.split("#");
        var trgt = parts[1];
        var target_offset = $("#" + trgt).offset();
        var target_top = target_offset.top - 100;
        if (target_top < 0) {
            target_top = 0;
        }

        $('html, body').animate({
            scrollTop: target_top
        }, 1000);
    }
}

$.fn.alignElementsSameHeight = function () {
    $('.same-height-row').each(function () {

        var maxHeight = 0;
        var children = $(this).find('.same-height');
        children.height('auto');
        if ($(window).width() > 768) {
            children.each(function () {
                if ($(this).innerHeight() > maxHeight) {
                    maxHeight = $(this).innerHeight();
                }
            });
            children.innerHeight(maxHeight);
        }

        maxHeight = 0;
        children = $(this).find('.same-height-always');
        children.height('auto');
        children.each(function () {
            if ($(this).innerHeight() > maxHeight) {
                maxHeight = $(this).innerHeight();
            }
        });
        children.innerHeight(maxHeight);
    });
}

$(window).load(function () {

    windowWidth = $(window).width();

    $(this).alignElementsSameHeight();
});
$(window).resize(function () {

    newWindowWidth = $(window).width();

    if (windowWidth !== newWindowWidth) {
        setTimeout(function () {
            $(this).alignElementsSameHeight();
        }, 100);
        windowWidth = newWindowWidth;
    }

});
