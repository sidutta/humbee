import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateInput(data) {
	let errors = {};

	if(Validator.isEmpty(data.name)) {
		errors.name = 'Name is required';
	}

	if(Validator.isEmpty(data.jurisdiction)) {
		errors.jurisdiction = 'Jurisdiction is required';
	}

	if(Validator.isEmpty(data.filledBy)) {
		errors.filledBy = 'This field is required';
	}

	if(Validator.isEmpty(data.term)) {
		errors.term = 'Term is required';
	}


	return {
		errors,
		isValid : isEmpty(errors)
	};
}
