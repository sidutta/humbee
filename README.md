# Website Installation

* `git clone https://gitlab.com/sidutta/humbee.git`
* `cd humbee/website/humbee`
* `git pull`
* `git branch <your_name>`
* `git checkout <your_name>`
* `npm install`
* `npm run server` to start the server
* Go to `localhost:8080` in your browser

# Android Installation

* `git clone https://gitlab.com/sidutta/humbee.git`
* `cd humbee/website/humbee`
* `git pull`
* `npm install`
* `npm run server` to start the server
* `cd humbee/app/humbee/humbee_mobile`
* Attach your phone to your laptop via usb
* `sudo react-native run-android --variant=debug` will start the packaging server as well as install the apk on your phone
* Slightly jerk the phone, in dev settings, there will be an option to add your host address. Type in your_ip_address:8081

# Folder organization
* `humbee/website/humbee` has all required files for the website
* `humbee/humbee_mobile` has all required files for the mobile app
* `humbee/website/humbee/lib/data` has most of the server side components
* `humbee/website/humbee/lib/data/models`: The sequelize models, each file here defines a table whose column names and types are mentioned in the file
* `humbee/website/humbee/lib/data/mutations`: Server side description of possible mutations
* `humbee/website/humbee/lib/data/type`: Defines GraphQL types, most of them will correspond to files defined in models
* `humbee/website/humbee/lib/data/interface`: Defines NodeInterface as required by Relay
* `humbee/website/humbee/components`: Defines components for website

# Info for work on website
* Unless backend changes are required and you have discussed the same with Siddhartha or Vivek, do not edit files within `humbee/website/humbee/lib` as they account for API changes and risk breaking functioning features and have an impact on code structure beyond a particular feature. 
* `humbee/website/humbee/components` is where most of the changes should go to. Refer to `humbee/humbee_mobile/app/components` since most likely there will be parallels 

# Git guidelines (for interns)
* DO NOT push to master, always push to the branch named after you(the one you created above)
* Before pushing, checkout master and pull(`git checkout master` followed by `git pull`)
* Then go back to your branch(`git checkout <your_name>`)
* Then merge latest changes in master with your branch(`git merge master`)
* Then commit and push

-----------------------
Irrelevant part(continuous integration has not been enabled for this repo)

# Git guidelines (full-time employees only)

* There are two primary branches 
  1. master
  2. staging
* Any changes should be done in a feature branch and merged with staging branch
  1. Checkout staging(`git checkout staging`)
  2. Create a feature branch(`git branch feature/<name>`)
  3. Switch to feature branch(`git checkout feature/<name>`)
  4. Make changes and test
  5. Commit changes and switch to staging to pull latest changes(`git checkout` staging followed by `git pull`)
  6. Switch back to feature branch(`git checkout feature/<name>`) 
  7. Merge with staging within feature branch so that conflicts are resolve within feature branch(`git merge staging`)
  8. Switch to staging(`git checkout staging`)
  9. Merge with feature branch(`git merge feature/<name>`)
  10. `git push`
* Above will trigger staging pipeline, test on staging server.
* If everything goes well and no db changes are required, merge staging with master
  1. `git checkout master`
  2. `git pull`
  3. `git checkout staging`
  4. `git merge master`
  5. `git checkout master`
  6. `git merge staging`
  7. `git push`
* Pipeline for master needs a manual trigger, if confident, start the deployment from gitlab portal.

# Info 
* Env variables for cloud are dictated by values in app.yaml and stage.yaml
* Server addresses  
  * Prod: https://humb-app.appspot.com/
  * Staging: https://staging-dot-humb-app.appspot.com/
